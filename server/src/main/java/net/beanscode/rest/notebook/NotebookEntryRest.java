package net.beanscode.rest.notebook;

import static java.lang.String.format;
import static net.beanscode.rest.BeansServevrConst.MAGIC_KEY;
import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;
import static net.hypki.libs5.utils.string.StringUtilities.nullOrEmpty;
import static net.hypki.libs5.utils.string.StringUtilities.substringLast;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.StreamingOutput;

import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import mjson.Json;
import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.cli.datasets.Table;
import net.beanscode.cli.notebooks.GnuplotEntry;
import net.beanscode.model.BeansConst;
import net.beanscode.model.BeansSettings;
import net.beanscode.model.cass.notification.Notification;
import net.beanscode.model.cass.notification.NotificationType;
import net.beanscode.model.notebook.Notebook;
import net.beanscode.model.notebook.NotebookEntry;
import net.beanscode.model.notebook.NotebookEntryFactory;
import net.beanscode.model.notebook.NotebookEntryProgress;
import net.beanscode.model.notebook.NotebookEntryProgressFactory;
import net.beanscode.model.plugins.PluginManager;
import net.beanscode.rest.BeansRest;
import net.beanscode.rest.query.PlotRest;
import net.beanscode.rest.uploads.UploadRest;
import net.beanscode.web.view.dataset.TableBetaPanel;
import net.beanscode.web.view.ui.modals.OpBeansError;
import net.beanscode.web.view.ui.modals.OpBeansInfo;
import net.beanscode.web.view.upload.UploadView;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.pjf.op.OpJs;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.pjf.op.OpPrepend;
import net.hypki.libs5.search.SearchResults;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.file.FileExt;
import net.hypki.libs5.utils.file.FileUtils;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.reflection.SystemUtils;
import net.hypki.libs5.utils.string.Meta;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.utils.utils.IOUtils;
import net.hypki.libs5.weblibs.settings.SettingFactory;
import net.hypki.libs5.weblibs.settings.SettingValue;

@Path("api/notebook/entry")
public class NotebookEntryRest extends BeansRest {
	
	public static final String UPLOAD = "upload";
	
	@POST
	@Path(UPLOAD)
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response upload(@Context SecurityContext sc,
			@FormDataParam("file") InputStream uploadedInputStream,
			@FormDataParam("file") FormDataContentDisposition fileDetail,
			@QueryParam("entryId") UUID entryId) throws IOException, ValidationException {
		
		try {
			LibsLogger.debug(NotebookEntryRest.class, "file " + fileDetail);
			LibsLogger.debug(NotebookEntryRest.class, "entryId", entryId);
//			Table table = new ApiCallCommand(getSessionBean(sc))
//				.setApi(ApiCallCommand.API_FILE_UPLOAD)
//				.addParam("datasetId", datasetId)
//				.addParam("tableId", tableId)
//				.addParam("tableName", fileDetail.getFileName())
//				.addStream(fileDetail.getFileName(), uploadedInputStream)
//				.run()
//				.getResponse()
//				.getAsObject(Table.class);
			
			final NotebookEntry ne = NotebookEntryFactory.getNotebookEntry(entryId);
			validateNotFound(ne != null, "NotebookEntry" + entryId + " not found");
			
			String filename = fileDetail.getFileName();
			if (nullOrEmpty(filename)) {
				LibsLogger.warn(NotebookEntryRest.class, "Uploaded file does not have specified name");
				filename = "no_name";
			}
			
			ne.upload(uploadedInputStream, filename);
			ne.save();
			
//			OpList opList = new OpList();
//			opList
//				.add(new OpBeansInfo("File uploaded successfully!"))
//				;
			
//			if (table != null) {
////				Table table = new TableGet().run(newTableId);
//				opList.add(new OpPrepend("#dataset-tables .tables", new TableBetaPanel(null, table).setSessionBean(sc).toHtml()));
//				opList.add(new OpJs("new Dropzone('#dropzone-" + table.getId() + "', " +
//						"{url: '" + "/api/file/upload?datasetId=" + table.getDatasetId() + "&tableId=" + table.getId() + "', addRemoveLinks: true, maxFilesize: 1000, maxFiles: 1, clickable: true})"
//						+ ".on('success', function(file, response) { pjfOps(response); });"));
//			}
			
			return Response
					.ok()
					.entity(Json
							.object()
							.set("entryId", entryId.getId())
							.set("filename", filename)
							.toString())
					.build();
		} catch (Exception e) {
			LibsLogger.error(UploadView.class, "Cannot upload a file", e);
			return Response.status(500).entity(new OpList().add(new OpBeansError(e.getMessage())).toString()).build();
		}
	}
	
	@GET
	@Path("attachment")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response attachmentGET(@Context SecurityContext sc,
			@QueryParam("entryId") String entryId,
			@QueryParam("fileName") String fileName) throws IOException, ValidationException {
		validateBadRequest(notEmpty(entryId), "Entry ID is not specified");
		
		NotebookEntry ne = NotebookEntryFactory.getNotebookEntry(entryId);
		validateNotFound(ne != null, format("Entry with ID= %s was not found", entryId));
		
		// TODO not general
		Meta filesMeta = ne.getMeta().get("META_FILES");
		List<String> files = new ArrayList<>();
		if (filesMeta != null)
			files = JsonUtils.fromJson(filesMeta.getAsString(), List.class);
		validateNotFound(files.contains(fileName), format("No file to download= %s", fileName));
		
		FileExt folder = new FileExt(BeansSettings.getPluginDefaultFolder(), "attachment/" + ne.getId().getId());
		folder.mkdirs();
		
		FileExt tmp = new FileExt(folder.getAbsolutePath(), fileName);
		
		final InputStream fStream = new FileInputStream(tmp);
		
//		return new StreamingOutput() {
//			@Override
//			public void write(OutputStream output) throws IOException, WebApplicationException {
//				try {
//					IOUtils.pipe(fStream, output);
//				} catch (Throwable t) {
//					throw new IOException("Cannot read data for the plot", t);
//				}
//			}
//		};
		
		return Response
			.ok(fStream)
			.header("content-disposition", "inline; filename = " + fileName)
			.build();
	}
	
	@GET
	@Path("attachment/pdf")
	@Produces("application/pdf")
//	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response notebookEntryDownload(@Context SecurityContext sc,
			@QueryParam("entryId") String entryId,
			@QueryParam("fileName") String fileName) throws IOException, ValidationException {
//		StreamingOutput stream = new ApiCallCommand(getSessionBean(sc))
//			.setApi("api/notebook/entry/download")
//			.addParam("entryId", entryId)
//			.addParam("metaName", metaName)
//			.runStream();
////		StreamingOutput stream = new NotebookEntryStreamGet(getSessionBean(sc)).run(entryId, metaName);
//		
//		GnuplotEntry ge = new ApiCallCommand(getSessionBean(sc))
//			.setApi("api/notebook/entry/get")
//			.addParam("entryId", entryId)
//			.run()
//			.getResponse()
//			.getAsObject(GnuplotEntry.class);
//		
//		String filename = ge.getMetaAsString(metaName);
//		if (nullOrEmpty(filename))
//			filename = "file";
//		else
//			filename = new FileExt(filename).getFilenameOnly();
		
		validateBadRequest(notEmpty(entryId), "Entry ID is not specified");
		
		NotebookEntry ne = NotebookEntryFactory.getNotebookEntry(entryId);
		validateNotFound(ne != null, format("Entry with ID= %s was not found", entryId));
		
		// TODO not general
//		Meta filesMeta = ne.getMeta().get("META_FILES");
//		List<String> files = new ArrayList<>();
//		if (filesMeta != null)
//			files = JsonUtils.fromJson(filesMeta.getAsString(), List.class);
//		validateNotFound(files.contains(fileName), format("No file to download= %s", fileName));
		
		FileExt folder = new FileExt(BeansSettings.getPluginDefaultFolder(), "attachment/" + ne.getId().getId());
		folder.mkdirs();
		
		FileExt tmp = new FileExt(folder.getAbsolutePath(), fileName);
		
		final InputStream fStream = new FileInputStream(tmp);

		return Response
				.ok(fStream)
				.header("content-disposition", "inline; filename = " + fileName)
				.build();
	}
	
	@GET
	@Path("columns")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response getColumnsOutput(@Context SecurityContext sc,
			@QueryParam("entryId") UUID entryId) throws IOException, ValidationException {
		final NotebookEntry ne = NotebookEntryFactory.getNotebookEntry(entryId);
		final Notebook note = ne.getNotebook();
		
		validateNotFound(note != null, "Notebook is not found");
		validateUnauthorized(isAdmin(sc) || note.isReadAllowed(getUserId(sc)), "Permission denied");
		
		Json columnsArr = Json.array();
		for (Object split : ne.getOutputColumns()) {
			columnsArr.add(split);
		}
		
		return Response
				.ok()
				.entity(Json
						.object()
						.set("columns", columnsArr)
						.toString())
				.build();
	}
	
	@GET
	@Path("splits")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response getSplits(@Context SecurityContext sc,
			@QueryParam("entryId") UUID entryId) throws IOException, ValidationException {
		final NotebookEntry ne = NotebookEntryFactory.getNotebookEntry(entryId);
		final Notebook note = ne.getNotebook();
		
		validateNotFound(note != null, "Notebook is not found");
		validateUnauthorized(isAdmin(sc) || note.isReadAllowed(getUserId(sc)), "Permission denied");
		
		Json splitsArr = Json.array();
		for (Object split : ne.getSplits()) {
			splitsArr.add(split);
		}
		
		return Response
				.ok()
				.entity(Json
						.object()
						.set("splits", splitsArr)
						.toString())
				.build();
	}
	
	@GET
	@Path("image")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public StreamingOutput staticPlot(@Context SecurityContext sc,
			@QueryParam("entryId") UUID entryId,
			@QueryParam("split") String split,
			@QueryParam("type") String type) throws IOException, ValidationException {
		final NotebookEntry ne = NotebookEntryFactory.getNotebookEntry(entryId);
		final Notebook note = ne.getNotebook();
		
		validateNotFound(note != null, "Notebook is not found");
		validateUnauthorized(isAdmin(sc) || note.isReadAllowed(getUserId(sc)), "Permission denied");
		
		InputStream image = ne.getImageAsStream(split);
		
		boolean emptyPlot = false;
		if (image == null)
			emptyPlot = true;
		
		final InputStream fStream = !emptyPlot ? image : 
			SystemUtils.getResourceAsStream(BeansConst.class, "PlotNoData.png");
		
		return new StreamingOutput() {
			@Override
			public void write(OutputStream output) throws IOException, WebApplicationException {
				try {
					IOUtils.pipe(fStream, output);
				} catch (Throwable t) {
					throw new IOException("Cannot read data for the plot", t);
				}
			}
		};
	}
	
	@GET
	@Path("data")
	@Produces(MediaType.APPLICATION_JSON)
	public StreamingOutput dataHistogram(@Context SecurityContext sc,
			@QueryParam("entryId") UUID entryId,
			@QueryParam("split") String split,
			@QueryParam("type") final String type
			) throws IOException, ValidationException {
		
		LibsLogger.debug(PlotRest.class, "Data for entryId ", entryId, " split ", split);
		
//		assertTrue(split != null, "queryId or plotIndex not specified");
		
		// retrieving plot data
		final NotebookEntry ne = NotebookEntryFactory.getNotebookEntry(entryId);
//		final ColorPalette palette = ColorPalette.getColorPalette(getSessionBean(sc).getUserId(), ColorPalette.defaultPalette());
		final Notebook note = ne.getNotebook();
		
		validateNotFound(note != null, "Notebook is not found");
		validateUnauthorized(isAdmin(sc) || note.isReadAllowed(getUserId(sc)), "Permission denied");
		
		return ne.getDataAsStream(split, type);
		
//		if (plot.getHistogramFile().exists() == false) {
//			LibsLogger.error(PlotRest.class, "No HDF5 file with histogram");
//			return null;
//		}
//		
//		return new BarStreamingOutput(plot, plotNr, palette);
	}
	
	@DELETE
	@Path("remove")
	public void remove(@Context SecurityContext sc, 
			@QueryParam("notebookId") UUID notebookId,
			@QueryParam("entryId") List<UUID> entryId) throws IOException, ValidationException {
		final NotebookEntry ne = NotebookEntryFactory.getNotebookEntry(entryId.get(0));
		
		validateNotFound(ne != null, "Notebook Entry is not found");
		
		final Notebook note = ne.getNotebook();
		
		validateNotFound(note != null, "Notebook is not found");
		validateUnauthorized(isAdmin(sc) || note.isWriteAllowed(getUserId(sc)), "Permission denied");
		
		for (UUID toRemove : entryId) {
			if (note.removeEntry(toRemove)) {
				LibsLogger.debug(NotebookEntryRest.class, "Entry ", entryId, " removed from notebook ", note.getId(), " ", note.getName());
			}
		}
		
		note.save();
	}
	
	@POST
	@Path("reload")
	public void reload(@Context SecurityContext sc, 
			@FormParam("notebookEntryId") UUID notebookEntryId) throws ValidationException, IOException {
		final NotebookEntry ne = NotebookEntryFactory.getNotebookEntry(notebookEntryId);
		
		validateNotFound(ne != null, "Notebook Entry is not found");
		
		final Notebook notebook = ne.getNotebook();
		
		validateNotFound(notebook != null, "Notebook is not found");
		validateUnauthorized(isAdmin(sc) || notebook.isWriteAllowed(getUserId(sc)), "Permission denied");
		
		ne.reload();
	}
	
	@POST
	@Path("move")
	public void move(@Context SecurityContext sc, 
			@FormParam("notebookEntryId") UUID notebookEntryId,
			@FormParam("notebookId") UUID notebookId) throws ValidationException, IOException {
		final NotebookEntry ne = NotebookEntryFactory.getNotebookEntry(notebookEntryId);
		
		validateNotFound(ne != null, "Notebook Entry is not found");
		
		final Notebook notebook = Notebook.getNotebook(notebookId);
		
		validateNotFound(notebook != null, "Notebook is not found");
		validateUnauthorized(isAdmin(sc) || notebook.isWriteAllowed(getUserId(sc)), "Permission denied");
		
		ne.move(notebook);
	}
	
	@POST
	@Path("add")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addNew(@Context SecurityContext sc, 
			@FormParam("notebookId") UUID notebookId,
			@FormParam("notebookEntryClass") String notebookEntryClass,
			@FormParam("refEntryId") UUID refEntryId,
			@FormParam("position") int position) throws IOException, ValidationException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		final Notebook destNotebook = Notebook.getNotebook(notebookId);
		
		validateNotFound(destNotebook != null, "Notebook is not found");
		validateUnauthorized(isAdmin(sc) || destNotebook.isWriteAllowed(getUserId(sc)), "Permission denied");
		validateNotFound(notebookEntryClass != null, "NotebookEntry class is not specified");
		
		NotebookEntry ne = null;
		String fullname = PluginManager.getFullname(substringLast(notebookEntryClass, '.'));
		
		ne = (NotebookEntry) Class.forName(fullname).newInstance();
		ne.setUserId(getUserId(sc));
		ne.setNotebookId(notebookId);
		ne.save();
	
		if (position != 0
				&& refEntryId != null
				&& refEntryId.isValid()) {
			// adding at the specific position in the notebook
			int positionIdx = destNotebook.getEntryPosition(refEntryId);
			destNotebook.addEntryId(positionIdx + (position < 0 ? position + 1 : position), ne.getId());
		} else {
			// adding at the end of the notebook 
			destNotebook.addEntryId(ne.getId());
		}
		
		destNotebook.save();
		
		return Response
				.ok()
				.entity(ne.getData())
				.build();
	}
	
	@POST
	@Path("clone/entry")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addCloneEntry(@Context SecurityContext sc, 
			@FormParam("notebookId") UUID notebookId,
//			@FormParam("destinationAfterEntryId") UUID destinationAfterEntryId,
//			@FormParam("sourceNotebookId") UUID sourceNotebookId,
			@FormParam("sourceEntryId") UUID sourceEntryId,
//			@FormParam("sourceNotebookEntryClass") String sourceNotebookEntryClass,
			@FormParam("refEntryId") UUID refEntryId,
			@FormParam("position") int position) throws IOException, ValidationException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		final Notebook destNotebook = Notebook.getNotebook(notebookId);
		
		validateNotFound(destNotebook != null, "Notebook is not found");
		validateUnauthorized(isAdmin(sc) || destNotebook.isWriteAllowed(getUserId(sc)), "Permission denied");
		validateTrue(sourceEntryId != null && sourceEntryId.isValid(), "Source EntryId to clone must be provided");
		
		NotebookEntry sourceEntry = NotebookEntryFactory.getNotebookEntry(sourceEntryId);
		
		validateNotFound(sourceEntry != null, "Source Entry not found");
		
		NotebookEntry ne = sourceEntry.clone();
		ne.setId(UUID.random());
		ne.setUserId(getUserId(sc));
		ne.setNotebookId(notebookId);
		ne.save();
	
		if (position != 0
				&& refEntryId != null
				&& refEntryId.isValid()) {
			// adding at the specific position in the notebook
			int positionIdx = destNotebook.getEntryPosition(refEntryId);
			destNotebook.addEntryId(positionIdx + (position < 0 ? position + 1 : position), ne.getId());
		} else {
			// adding at the end of the notebook 
			destNotebook.addEntryId(ne.getId());
		}
		
		destNotebook.save();
		
		return Response
				.ok()
				.entity(ne.getData())
				.build();
	}
	
	@POST
	@Path("clone/notebook")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addCloneNotebook(@Context SecurityContext sc, 
			@FormParam("destinationNotebookId") UUID destinationNotebookId,
//			@FormParam("destinationAfterEntryId") UUID destinationAfterEntryId,
			@FormParam("sourceNotebookId") UUID sourceNotebookId,
			@FormParam("sourceNotebookEntryClass") String sourceNotebookEntryClass,
			@FormParam("refEntryId") UUID refEntryId,
			@FormParam("position") int position) throws IOException, ValidationException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		final Notebook destNotebook = Notebook.getNotebook(destinationNotebookId);
		
		validateNotFound(destNotebook != null, "Notebook is not found");
		validateUnauthorized(isAdmin(sc) || destNotebook.isWriteAllowed(getUserId(sc)), "Permission denied");
		
		validateTrue(false, "Not implemented");
		
		NotebookEntry ne = null;
	
		if (position != 0
				&& refEntryId != null
				&& refEntryId.isValid()) {
			// adding at the specific position in the notebook
			int positionIdx = destNotebook.getEntryPosition(refEntryId);
			destNotebook.addEntryId(positionIdx + (position < 0 ? position + 1 : position), ne.getId());
		} else {
			// adding at the end of the notebook 
			destNotebook.addEntryId(ne.getId());
		}
		
		destNotebook.save();
		
		return Response
				.ok()
				.entity(ne.getData())
				.build();
	}
	
	@GET
	@Path("query")
	@Produces(MediaType.APPLICATION_JSON)
	public Response query(@Context SecurityContext sc,
			@QueryParam("notebookId") String notebookId,
			@QueryParam("userId") UUID userId,
			@QueryParam("query") String query,
			@QueryParam("from") int from,
			@QueryParam("size") int size) throws IOException, ValidationException {
		if (userId != null && !userId.isValid())
			userId = null;
		if (nullOrEmpty(notebookId))
			notebookId = null;
//		else
//			return Response
//					.ok()
//					.entity(Json
//							.object()
//							.set("maxHits", 0)
//							.set("entries", "")
//							.toString())
//					.build();
		
		SearchResults<NotebookEntry> res = notebookId != null && UUID.isValid(notebookId) ?
				NotebookEntryFactory.searchNotebookEntries(new UUID(notebookId), userId , query, from, size)
				: NotebookEntryFactory.searchNotebookEntries(notebookId, userId , query, from, size);
		
		Json arr = Json.array();
		for (NotebookEntry ne : res) {
			if (ne != null) {
				JsonObject e = JsonUtils.toJson(ne).getAsJsonObject();
				Notebook notebook = ne.getNotebook(); // TODO OPTY
				
				validateUnauthorized(isAdmin(sc) || notebook.isReadAllowed(getUserId(sc)), "Permission denied");
				
				if (notebook != null) {
					e.addProperty("notebookName", notebook.getName());
				} else {
					LibsLogger.error("Cannot find Notebook " + ne.getNotebookId());
				}
				
				arr.add(Json.read(e.toString()));
			}
		}
		
		return Response
				.ok()
				.entity(Json
						.object()
						.set("maxHits", res.maxHits())
						.set("entries", arr)
						.toString())
				.build();
	}
	
	@GET
	@Path("get")
	@Produces(MediaType.APPLICATION_JSON)
	public Response get(@Context SecurityContext sc, 
			@QueryParam("entryId") UUID entryId,
			@QueryParam(MAGIC_KEY) UUID mk) throws IOException, ValidationException {
		final NotebookEntry ne = NotebookEntryFactory.getNotebookEntry(entryId);
		
		validateNotFound(ne != null, "Notebook Entry is not found");
		
		final Notebook notebook = ne.getNotebook();
		
		validateNotFound(notebook != null, "Notebook is not found");
		validateUnauthorized(isAdmin(sc) 
				|| notebook.isReadAllowed(getUserId(sc))
				|| notebook.isReadAllowedByShareLink(notebook.getId()), "Permission denied");
		
		Json n = Json.read(ne.getData());
				
		if (ne.getClass().getSimpleName().equals("TextEntry"))
			n.set("textHtml", ne.getMetaAsString(net.beanscode.cli.notebooks.TextEntry.META_TEXT_HTML, ""));
		
		n.set("notebookName", ne.getNotebook().getName());
		
		return Response
				.ok()
				.entity(n.toString())
				.build();
	}
	
	@GET
	@Path("progress")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getProgress(@Context SecurityContext sc, 
			@QueryParam("entryId") UUID entryId,
			@QueryParam(MAGIC_KEY) UUID mk) throws IOException, ValidationException {
		NotebookEntryProgress pr = NotebookEntryProgressFactory.getNotebookEntryProgress(entryId);
		
		if (pr == null) {
			pr = new NotebookEntryProgress();
			pr.setEntryId(entryId);
			pr.save();
		}
		
		validateNotFound(pr != null, "Notebook Entry progress is not found");
		
		return Response
				.ok()
				.entity(pr.getData())
				.build();
	}
	
	@GET
	@Path("log")
	@Produces(MediaType.APPLICATION_JSON)
	public Response log(@Context SecurityContext sc, 
			@QueryParam("entryId") UUID entryId,
			@QueryParam("filter") String filter) throws IOException, ValidationException {
		final NotebookEntry ne = NotebookEntryFactory.getNotebookEntry(entryId);
		
		validateNotFound(ne != null, "Notebook Entry is not found");
		
		final Notebook notebook = ne.getNotebook();
		validateNotFound(notebook != null, "Notebook is not found");
		validateUnauthorized(isAdmin(sc) || notebook.isReadAllowed(getUserId(sc)), "Permission denied");
		
		FileExt f = null;
		
		// TODO make it general 
		if (ne.getClass().getSimpleName().contains("PigScript")) {
			SettingValue pigPath = SettingFactory.getSettingValue(BeansSettings.SETTING_PIG_FOLDER, "path");
			f = new FileExt(pigPath.getValueAsString() + "/beans-" + ne.getId().getId() + ".log");
//		} else if (ne instanceof ButcheredPigEntry) {
//			SettingValue pigPath = SettingFactory.getSettingValue(BeansSettings.SETTING_PIG_FOLDER, "path");
//			f = new FileExt(pigPath.getValueAsString() + "/beans-" + ne.getId().getId() + ".log");
		} else if (ne.getClass().getSimpleName().equals("GnuplotEntry")) {
			f = new FileExt(BeansSettings.getBeansHomeFolder().getAbsolutePath(), 
					"/plugins/gnuplot/",
					"/gnuplot-" + ne.getId() + ".log");
		} else if (ne.getClass().getSimpleName().equals("PythonEntry")) {
			f = new FileExt(BeansSettings.getBeansHomeFolder().getAbsolutePath(), 
					"/plugins/python/",
					"/python-" + ne.getId() + ".log");
		}
		
		if (f == null || (f != null && !f.exists()))
			return Response
					.ok()
					.entity(Json
							.object()
							.set("log", "")
							.toString()
							)
					.build();
		
		if ((f.length() / 1_000_000) > 1.0) {
			
			// for log files larger than 1 MB return last 1MB
			StringBuilder sb = new StringBuilder();
			
			sb.append("LOG file is too large and thus showing only last 1 MB:\n");
			sb.append(FileUtils.tail(f, 1_000_000));
			
			return Response
					.ok()
					.entity(Json
							.object()
							.set("log", sb.toString())
							.toString()
							)
					.build();
			
		} else if (notEmpty(filter)) {
			
			// if filter is defined then filter the file
			StringBuilder sb = new StringBuilder();
			
			for (String line : f) {
				if (line.matches(filter)) {
					sb.append(line);
					sb.append("\n");
				}
			}
			
			return Response
					.ok()
					.entity(Json
							.object()
							.set("log", sb.toString())
							.toString()
							)
					.build();
			
		} else {
		
			return Response
					.ok()
					.entity(Json
							.object()
							.set("log", f != null ? f.readString() : null)
							.toString()
							)
					.build();
		}
	}
	
	@GET
	@Path("output")
	@Produces(MediaType.APPLICATION_JSON)
	public Response output(@Context SecurityContext sc, 
			@QueryParam("entryId") UUID entryId,
			@QueryParam("filter") String filter) throws IOException, ValidationException {
		final NotebookEntry ne = NotebookEntryFactory.getNotebookEntry(entryId);
		
		validateNotFound(ne != null, "Notebook Entry is not found");
		
		final Notebook notebook = ne.getNotebook();
		validateNotFound(notebook != null, "Notebook is not found");
		validateUnauthorized(isAdmin(sc) || notebook.isReadAllowed(getUserId(sc)), "Permission denied");
		
		FileExt f = null;
		
		// TODO make it general 
		if (ne.getClass().getSimpleName().contains("PigScript")) {
			SettingValue pigPath = SettingFactory.getSettingValue(BeansSettings.SETTING_PIG_FOLDER, "path");
			f = new FileExt(pigPath.getValueAsString() + "/beans-" + ne.getId().getId() + ".log");
		} else if (ne.getClass().getSimpleName().equals("GnuplotEntry")) {
			f = new FileExt(BeansSettings.getBeansHomeFolder().getAbsolutePath(), 
					"/plugins/gnuplot/",
					"/gnuplot-" + ne.getId() + ".log");
		} else if (ne.getClass().getSimpleName().equals("PythonEntry")) {
			f = new FileExt(BeansSettings.getBeansHomeFolder().getAbsolutePath(), 
					"/plugins/python/",
					ne.getId().getId(),
					"/python.out");
		}
		
		if (f != null && !f.exists())
			return Response
					.ok()
					.entity(Json
							.object()
							.set("log", "")
							.toString()
							)
					.build();
		
		if (notEmpty(filter)) {
			StringBuilder sb = new StringBuilder();
			
			for (String line : f) {
				if (line.matches(filter)) {
					sb.append(line);
					sb.append("\n");
				}
			}
			
			return Response
					.ok()
					.entity(Json
							.object()
							.set("log", sb.toString())
							.toString()
							)
					.build();
		}
		
		return Response
				.ok()
				.entity(Json
						.object()
						.set("log", f != null ? f.readString() : null)
						.toString()
						)
				.build();			
	}
	
	@POST
	@Path("stop")
	public void stop(@Context SecurityContext sc, 
			@FormParam("entryId") UUID entryId) throws IOException, ValidationException {
		final NotebookEntry ne = NotebookEntryFactory.getNotebookEntry(entryId);
		final Notebook notebook = ne.getNotebook();
		
		validateNotFound(notebook != null, "Notebook is not found");
		validateUnauthorized(isAdmin(sc) || notebook.isWriteAllowed(getUserId(sc)), "Permission denied");
		
		// TODO there are problem of closing workers for Lucene
		ne.stop();
	}
	
	@POST
	@Path("moveUp")
	public void moveUp(@Context SecurityContext sc, 
			@FormParam("entryId") UUID entryId) throws IOException, ValidationException {
		final NotebookEntry ne = NotebookEntryFactory.getNotebookEntry(entryId);
		final Notebook notebook = ne.getNotebook();
		
		validateNotFound(notebook != null, "Notebook is not found");
		validateUnauthorized(isAdmin(sc) || notebook.isWriteAllowed(getUserId(sc)), "Permission denied");
		
		if (notebook.moveUpEntry(entryId)) {
			notebook.save();
		}
	}
	
	@POST
	@Path("autoUpdate")
	public void autoUpdate(@Context SecurityContext sc, 
			@FormParam("entryId") UUID entryId,
			@FormParam("autoUpdateOption") String autoUpdateOption) throws IOException, ValidationException {
		final NotebookEntry ne = NotebookEntryFactory.getNotebookEntry(entryId);
		final Notebook notebook = ne.getNotebook();
		
		validateNotFound(notebook != null, "Notebook is not found");
		validateUnauthorized(isAdmin(sc) || notebook.isWriteAllowed(getUserId(sc)), "Permission denied");
		
		ne.getAutoUpdateOptions().setOption(autoUpdateOption);
		
		ne.save();
	}
	
	@POST
	@Path("moveDown")
	public void moveDown(@Context SecurityContext sc, 
			@FormParam("entryId") UUID entryId) throws IOException, ValidationException {
		final NotebookEntry ne = NotebookEntryFactory.getNotebookEntry(entryId);
		final Notebook notebook = ne.getNotebook();
		
		validateNotFound(notebook != null, "Notebook is not found");
		validateUnauthorized(isAdmin(sc) || notebook.isWriteAllowed(getUserId(sc)), "Permission denied");
		
		if (notebook.moveDownEntry(entryId)) {
			notebook.save();
		}
	}
	
	@POST
	@Path("")
	public void set(@Context SecurityContext sc,
			@FormParam("entryId") String entryId,
			@FormParam("name") String name,
			MultivaluedMap<String, String> meta) throws IOException, ValidationException {
		validateBadRequest(notEmpty(entryId), "Entry ID is not specified");
		
		NotebookEntry ne = NotebookEntryFactory.getNotebookEntry(entryId);
		
		validateNotFound(ne != null, format("Entry with ID= %s was not found", entryId));
		
		final Notebook note = ne.getNotebook();
		
		validateNotFound(note != null, "Notebook is not found");
		validateUnauthorized(isAdmin(sc) || note.isWriteAllowed(getUserId(sc)), "Permission denied");
		
		// TODO so it is not possible to clear the name?
//		if (notEmpty(name))
			ne.setName(name);
		
		// setting meta
		meta.remove("name");
		meta.remove("entryId");
		for (String key : meta.keySet()) {
			List<String> v = meta.get(key);
			
			Object vv = null;
			if (v.size() == 1
					&& JsonUtils.isJsonArray(v.get(0))) {
				JsonArray ja = JsonUtils.parseJson(v.get(0)).getAsJsonArray();
				vv = new ArrayList<>();
				for (JsonElement j : ja) 
					((ArrayList) vv).add(j.toString());
			}
				
			if (vv == null) {
				if (v instanceof List) {
					List l = (List) v;
					boolean allEmpty = true;
					for (Object o : l)
						if (!nullOrEmpty(o)) {
							allEmpty = false;
							break;
						}
					if (!allEmpty)
						vv = v.size() > 1 ? JsonUtils.objectToString(l) : v.get(0);
					else
						vv = null;
				} else {
					vv = v.size() > 1 ? JsonUtils.objectToString(v) : v.get(0);
				}
			}
			
			ne.getMeta().add(key, vv);
		}
		
		ne.save();
	}
	
	@POST
	@Path("start")
	public void start(@Context SecurityContext sc,
			@FormParam("entryId") String entryId) throws IOException, ValidationException {
		validateBadRequest(notEmpty(entryId), "Entry ID is not specified");
		
		NotebookEntry ne = NotebookEntryFactory.getNotebookEntry(entryId);
		
		validateNotFound(ne != null, format("Entry with ID= %s was not found", entryId));		
		validateNotFound(ne.getNotebook() != null, "Notebook is not found");
		validateUnauthorized(isAdmin(sc) || ne.getNotebook().isWriteAllowed(getUserId(sc)), "Permission denied");
		
		ne
			.getProgress()
			.running(0.0, "Starting...")
			.save();
		
		ne.start();
	}
	
	@GET
	@Path("download")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public StreamingOutput staticPlot(@Context SecurityContext sc,
			@QueryParam("entryId") String entryId,
			@QueryParam("metaName") String metaName) throws IOException, ValidationException {
		validateBadRequest(notEmpty(entryId), "Entry ID is not specified");
		
		NotebookEntry ne = NotebookEntryFactory.getNotebookEntry(entryId);
		
		validateNotFound(ne != null, format("Entry with ID= %s was not found", entryId));
		
		FileExt toDownload = new FileExt(ne.getMetaAsString(metaName, null));
		
		validateNotFound(toDownload != null, format("No file to download= %s", toDownload));
		
		final InputStream fStream = new FileInputStream(toDownload);
		
		return new StreamingOutput() {
			@Override
			public void write(OutputStream output) throws IOException, WebApplicationException {
				try {
					IOUtils.pipe(fStream, output);
				} catch (Throwable t) {
					throw new IOException("Cannot read data for the plot", t);
				}
			}
		};
	}

	@POST
	@Path("meta/add")
	public void metaAdd(@Context SecurityContext sc, 
			@FormParam("entryId") UUID entryId,
			@FormParam("metaName") String metaName,
			@FormParam("metaValue") String metaValue,
			@FormParam("metaDesc") String metaDesc) throws IOException, ValidationException {
		NotebookEntry ne = NotebookEntryFactory.getNotebookEntry(entryId);
		
		validateNotFound(ne != null, "Notebook is not found");
//		validateUnauthorized(isAdmin(sc) || ne.isWriteAllowed(getUserId(sc)), "Permission denied");

		ne.getMeta().add(metaName, metaValue, metaDesc);
		ne.save();
	}
}
