package net.beanscode.rest.demo;

import java.io.IOException;

import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.SecurityContext;

import net.beanscode.model.demo.DemoDataJob;
import net.beanscode.model.demo.StressTestingJob;
import net.beanscode.rest.BeansRest;
import net.hypki.libs5.db.db.weblibs.ValidationException;

@Path("api/demo")
public class DemoRest extends BeansRest {
	
	@POST
	@Path("stress")
	public void stressTesting(@Context SecurityContext sc,
			@FormParam("datasetCount") int datasetCount,
			@FormParam("tableCount") int tablePerDataset,
			@FormParam("tableRows") int rowsPerTable,
			@FormParam("tableColumns") int columnsPerTable) throws IOException, ValidationException {
		
		new StressTestingJob()
			.setUserId(getLoggedUser(sc).getUserId())
			.setDatasetCount(datasetCount)
			.setTableCount(tablePerDataset)
			.setRowsPerTable(rowsPerTable)
			.setColumnsPerTable(columnsPerTable)
			.save();
	}
	
	@POST
	@Path("demoData")
	public void demoData(@Context SecurityContext sc) throws IOException, ValidationException {
		new DemoDataJob(getUserId(sc)).save();
		
	}
	
//	@POST
//	@Path("demoQueries")
//	public void demoQueries(@Context SecurityContext sc) throws IOException, ValidationException {
//		new DemoQueriesJob(getUserId(sc)).save();
//	}

}
