package net.beanscode.rest.uploads;

import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;
import static net.hypki.libs5.utils.string.StringUtilities.nullOrEmpty;
import static net.hypki.libs5.utils.utils.AssertUtils.assertTrue;

import java.io.IOException;
import java.io.InputStream;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

import mjson.Json;
import net.beanscode.model.BeansSettings;
import net.beanscode.model.BeansStorageManager;
import net.beanscode.model.dataset.Dataset;
import net.beanscode.model.dataset.DatasetFactory;
import net.beanscode.model.dataset.ImportFileJob;
import net.beanscode.model.dataset.Table;
import net.beanscode.model.dataset.TableFactory;
import net.beanscode.model.plugins.Connector;
import net.beanscode.rest.BeansRest;
import net.beanscode.rest.dataset.TableRest;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.file.FileExt;
import net.hypki.libs5.utils.file.FileUtils;
import net.hypki.libs5.utils.string.MetaList;

@Path(UploadRest.API_FILE)
public class UploadRest extends BeansRest {
	
	public static final String API_FILE = "api/file";
	
	public static final String UPLOAD = "upload";
	
	public static final String API_FILE_UPLOAD = API_FILE + "/" + UPLOAD;
	
	public static final String META_FILE_NAME = "META_FILE_NAME";
	
	/**
	 * Uploading a file from the console using curl e.g.: 
	 *   curl --cookie "lc=c481761e8e295608962d14e4bd260de8" -F "datasetId=5a7a8de7f698f2b9020932a14ee7682f" -F "file.dat=@/path/to/file.dat" http://localhost:25001/api/file/upload/
	 * 
	 * @param sc
	 * @param uploadedInputStream
	 * @param fileDetail
	 * @param datasetId
	 * @param tableId
	 * @return
	 * @throws IOException
	 * @throws ValidationException
	 */
	@POST
	@Path("upload/file")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces({MediaType.APPLICATION_JSON})
	public Response uploadFile(@Context SecurityContext sc,
			@FormDataParam("file") InputStream uploadedInputStream,
			@FormDataParam("file") FormDataContentDisposition fileDetail,
			@FormDataParam("filename") String filename) throws IOException, ValidationException {
		try {
			assertTrue(uploadedInputStream != null, "Stream of the uploaded file is null");
			assertTrue(fileDetail != null, "File details of the uploaded file is null");
			assertTrue(notEmpty(fileDetail.getFileName()), "filename cannot be empty");
			
			if (nullOrEmpty(filename))
				filename = fileDetail.getFileName();
			final UUID fileId = UUID.random();
			
			MetaList metaList = new MetaList();
			metaList.add(META_FILE_NAME, filename, "Name of the file uploaded through BEANS API");
			
			BeansStorageManager
				.getStorageProvider()
				.save(getLoggedUser(sc).getUserId().getId(), 
						fileId, 
						uploadedInputStream,
						metaList);
			return Response
				.ok()
				.entity(Json
						.object()
						.set("fileId", fileId)
						.toString())
				.build();
		} catch (Exception e) {
			LibsLogger.error(UploadRest.class, "Cannot upload file", e);
			throw new IOException("Cannot upload file", e);
		}
	}
		
	/**
	 * Uploading a file from the console using curl e.g.: 
	 *   curl --cookie "lc=c481761e8e295608962d14e4bd260de8" -F "datasetId=5a7a8de7f698f2b9020932a14ee7682f" -F "file.dat=@/path/to/file.dat" http://localhost:25001/api/file/upload/
	 * 
	 * @param sc
	 * @param uploadedInputStream
	 * @param fileDetail
	 * @param datasetId
	 * @param tableId
	 * @return
	 * @throws IOException
	 * @throws ValidationException
	 */
	@POST
	@Path(UPLOAD)
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces({MediaType.APPLICATION_JSON})
	public Response uploadTable(@Context SecurityContext sc,
			@FormDataParam("file") InputStream uploadedInputStream,
			@FormDataParam("file") FormDataContentDisposition fileDetail,
			@FormDataParam("datasetId") UUID datasetId,
			@FormDataParam("tableId") UUID tableId,
			@FormDataParam("tableName") String tableName) throws IOException, ValidationException {
		try {
			LibsLogger.debug(UploadRest.class, "datasetId ", datasetId);
			LibsLogger.debug(UploadRest.class, "tableId ", tableId);
			
			assertTrue(uploadedInputStream != null, "Stream of the uploaded file is null");
			assertTrue(fileDetail != null, "File details of the uploaded file is null");
			assertTrue(!nullOrEmpty(datasetId), "datasetId cannot be empty");
			assertTrue(notEmpty(fileDetail.getFileName()), "filename cannot be empty");
			
			final String filename = fileDetail.getFileName();
			
			if (nullOrEmpty(tableName))
				tableName = filename;
			
			// first save the file
			FileExt tmp = new FileExt(BeansSettings.getTmpFolder().getAbsolutePath(), UUID.random().getId());
			LibsLogger.debug(UploadRest.class, "Saving uploading file to ", tmp.getAbsolutePath());
			FileUtils.saveToFile(uploadedInputStream, tmp);
			LibsLogger.debug(UploadRest.class, "Upload to ", tmp.getAbsolutePath(), " finished");
			
			// create Table object
			Table table = null;
			
			if (nullOrEmpty(tableId)) {
				Dataset ds = DatasetFactory.getDataset(datasetId);
				table = new Table(ds, tableName);
			} else
				table = TableFactory.getTable(tableId);
			
			validateNotFound(table != null, "Table is not found");		
			validateUnauthorized(isAdmin(sc) || table.isWriteAllowed(getUserId(sc)), "Permission denied");
			validateBadRequest(table.isTableEmpty(), "Table has to be empty before importing a file");
			
			Connector conn =  BeansSettings.getDefaultConnector(table.getId(), UUID.random().getId());
			LibsLogger.debug(UploadRest.class, "Default Connector is ", conn);
			
//			ConnectorInitParams initParams = new ConnectorInitParams()
//					.setTableId(table.getId());
//			initParams.getMeta().add("path", tmp.getAbsolutePath());
			
			table.setName(tableName);
			table.getMeta().add("IMPORT_PATH", filename);
			table.getConnectorList().addConnector(conn);
	//		table.importFile(filename, uploadedInputStream);
			table.save();
			LibsLogger.debug(UploadRest.class, "Table ", table.getId(), " saved");
			
			// schedule for later the real import
			new ImportFileJob(table.getId(), tmp).save();
	 
	//		return Response.status(200).entity(table.getId()).build();
	//		return new TableRest().get(sc, table.getId().getId());
			return TableRest.toResponse(table);
		} catch (Exception e) {
			LibsLogger.error(UploadRest.class, "Cannot upload file", e);
			throw new IOException("Cannot upload file", e);
		}
	}
}
