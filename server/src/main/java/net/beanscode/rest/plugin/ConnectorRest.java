package net.beanscode.rest.plugin;

import java.io.IOException;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import mjson.Json;
import net.beanscode.model.plugins.Connector;
import net.beanscode.model.plugins.Plugin;
import net.beanscode.model.plugins.PluginManager;
import net.beanscode.rest.BeansRest;
import net.hypki.libs5.db.db.weblibs.ValidationException;

@Path("api/connector")
public class ConnectorRest extends BeansRest {

	@GET
	@Path("list")
	@Produces(MediaType.APPLICATION_JSON)
	public Response get(@Context SecurityContext sc) throws IOException, ValidationException {
		Json connectors = Json.array();
		
		for (Plugin plugin : PluginManager.iterateAllPlugins()) {
			connectors.add(Json.object().set("name", plugin.getConnectorClasses()));
		}
		
		return Response
				.ok()
				.entity(connectors.toString())
				.build();
	}
}
