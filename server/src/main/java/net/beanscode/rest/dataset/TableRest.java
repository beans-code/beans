package net.beanscode.rest.dataset;

import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;
import static net.hypki.libs5.utils.utils.AssertUtils.assertTrue;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.StreamingOutput;

import mjson.Json;
import net.beanscode.model.BeansConst;
import net.beanscode.model.cass.DataSearchFactory;
import net.beanscode.model.dataset.Dataset;
import net.beanscode.model.dataset.Table;
import net.beanscode.model.dataset.TableFactory;
import net.beanscode.model.dataset.TableRemoveJob;
import net.beanscode.model.dataset.TableSearchStatus;
import net.beanscode.model.notebook.Notebook;
import net.beanscode.model.plugins.ColumnDef;
import net.beanscode.rest.BeansRest;
import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.db.db.Results;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.pjf.components.jersey.SessionBean;
import net.hypki.libs5.search.SearchResults;
import net.hypki.libs5.search.query.Query;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.date.SimpleDate;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.string.Meta;
import net.hypki.libs5.utils.url.HttpResponse;

@Path("api/table")
public class TableRest extends BeansRest {
	
	@GET
	@Path("data")
	@Produces(MediaType.APPLICATION_JSON)
	public StreamingOutput data(@Context SecurityContext sc,
			@QueryParam("tableId") UUID tableId//,
//			@QueryParam("type") final DataType format
			) throws IOException, ValidationException {
		final Table table = TableFactory.getTable(tableId);
		
		validateNotFound(table != null, "Table is not found");
		validateUnauthorized(isAdmin(sc) || table.isReadAllowed(getUserId(sc)), "Permission denied");
		
		return new StreamingOutput() {
	        public void write(OutputStream output) throws IOException, WebApplicationException {
	            table.streamPlain(output);
	        }
	    };
	}
	
	@POST
	@Path("meta/add")
	public void addMeta(@Context SecurityContext sc, 
			@FormParam("tableId") UUID tableId,
			@FormParam("metaName") String metaName,
			@FormParam("metaValue") String metaValue) throws IOException, ValidationException {
		Table table = TableFactory.getTable(tableId);
		
		validateNotFound(table != null, "Table is not found");
		validateUnauthorized(isAdmin(sc) || table.isWriteAllowed(getUserId(sc)), "Permission denied");

		table.addMeta(metaName, metaValue);
		table.save();
	}
	
	@DELETE
	@Path("meta/remove")
	public void deleteMeta(@Context SecurityContext sc, 
			@QueryParam("tableId") UUID tableId,
			@QueryParam("metaName") List<String> metaNames) throws IOException, ValidationException {
		Table table = TableFactory.getTable(tableId);
		
		validateNotFound(table != null, "Table is not found");
		validateUnauthorized(isAdmin(sc) || table.isWriteAllowed(getUserId(sc)), "Permission denied");

		for (String toRemove : metaNames)
			table.getMeta().remove(toRemove);
		table.save();
	}
	
	@GET
	@Path("data/search")
	@Produces(MediaType.APPLICATION_JSON)
	public Response searchRows(@Context SecurityContext sc, 
			@QueryParam("tableId") UUID tableId,
			@QueryParam("query") String query,
			@QueryParam("from") int from,
			@QueryParam("size") int size) throws IOException, ValidationException {
		Table table = TableFactory.getTable(tableId);
		
		validateBadRequest(table != null, "Table not found");
		validateUnauthorized(isAdmin(sc) || table.isReadAllowed(getUserId(sc)), "Permission denied");
		
		if (table.getSearchStatus() == null || table.getSearchStatus() == TableSearchStatus.NOT_INDEXED) {
			return Response
					.ok()
					.entity(Json
							.object()
							.set("rows", null)
							.set("maxHits", 0)
							.toString())
					.build();
		} 
		
		SearchResults<Row> rows = DataSearchFactory.getRows(tableId, query, from, size);
		
		return Response
				.ok()
				.entity(Json
						.object()
						.set("rows", rows.getObjectsAsJson())
						.set("maxHits", rows.maxHits())
						.toString())
				.build();
	}
	
	@GET
	@Path("data/rows")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getRows(@Context SecurityContext sc, 
			@QueryParam("tableId") UUID tableId,
			@QueryParam("query") String query,
			@QueryParam("size") int size,
			@QueryParam("state") String state) throws IOException, ValidationException {
		Table table = TableFactory.getTable(tableId);
		
		validateBadRequest(table != null, "Table not found");
		validateUnauthorized(isAdmin(sc) || table.isReadAllowed(getUserId(sc)), "Permission denied");
		
		Query where = null;
		if (notEmpty(query)) {
			where = Query.parseQuery(query);
			
			where.toLowerCase();
		}
		
		Results res = table
				.getConnectorList()
				.iterateRows(where, state, size);
				
		return Response
				.ok()
				.entity(Json
						.object()
						.set("rows", res.getRowsAsJson())
						.set("maxHits", Long.MAX_VALUE)
						.set("state", res.getNextPage())
						.toString())
				.build();
	}
			
	@POST
	@Path("create")
	@Produces(MediaType.APPLICATION_JSON)
	public Response create(@Context SecurityContext sc, 
			@FormParam("datasetId") List<UUID> datasetIds,
			@FormParam("name") List<String> names,
			MultivaluedMap<String, String> meta) throws IOException, ValidationException {
		meta.remove("name");
		meta.remove("datasetId");
		
		assertTrue(datasetIds.size() == names.size(), "Number of datasetIds is different than number od names of tables, cannot create tables in bulk");
		
		List<DbObject> dbObjects = new ArrayList<>();
		Json tableIdArray = Json.array();
		
		// creating multiple tables in bulk
		for (int i = 0; i < datasetIds.size(); i++) {
			final UUID datasetId = datasetIds.get(i);
			final String name = names.get(i);
			
			assertTrue(datasetId != null && datasetId.isValid(), "Dataset ID is not specified");
			
			Table table = new Table(getUserId(sc), datasetId, BeansConst.DEFAULT_TABLE_NAME + " (" + SimpleDate.now().toStringHour() + ")");
			Dataset dataset = table.getDataset();
			
			validateNotFound(dataset != null, "Dataset is not found");
			validateUnauthorized(isAdmin(sc) || dataset.isWriteAllowed(getUserId(sc)), "Permission denied");
			
			if (notEmpty(name))
				table.setName(name);
			
			for (Map.Entry<String, List<String>> e : meta.entrySet()) {
//				assertTrue(e.getValue().size() <= 1, "Currently additional meta parameters are allowed only for key=value pairs");
				table.getMeta().add(e.getKey(), e.getValue().get(i));
			}
			
			table.getMeta().add("datasetId", datasetId.getId());
			table.getMeta().add("datasetName", dataset.getName());
			
			dbObjects.add(table);
//			table.save();
			
//			ts.save();
			
			tableIdArray.add(table.getId().getId());
			
			LibsLogger.debug(TableRest.class, "New table '", table.getName() , "' created with id " + table.getId());
		}
		
		DbObject.saveBulk(dbObjects);
		
		return Response
				.ok()
				.entity(Json
						.object()
						.set("tables", tableIdArray)
						.toString())
				.build();
	}
	
	@DELETE
	@Path("clear")
	public Response clearTable(@Context SecurityContext sc, 
			@QueryParam("tableId") UUID tableId) throws IOException, ValidationException {
		assertTrue(tableId != null && tableId.isValid(), "Table ID is not specified");
		
		Table table = TableFactory.getTable(tableId);
		
		validateNotFound(table != null, "Dataset is not found");
		validateUnauthorized(isAdmin(sc) || table.isWriteAllowed(getUserId(sc)), "Permission denied");
		
		table.clear(true);
		LibsLogger.debug(TableRest.class, "Table '", table.getName() , "' cleared");
		
		return Response
				.ok()
				.entity(Json
						.object()
						.set("cleared", true)
						.toString())
				.build();
	}
	
	@POST
	@Path("optimize")
	public void optimizeTable(@Context SecurityContext sc, 
			@FormParam("tableId") UUID tableId) throws IOException, ValidationException {
		assertTrue(tableId != null && tableId.isValid(), "Table ID is not specified");
		
		Table table = TableFactory.getTable(tableId);
		
		validateNotFound(table != null, "Dataset is not found");
		validateUnauthorized(isAdmin(sc) || table.isWriteAllowed(getUserId(sc)), "Permission denied");
		
		table.optimize(true);
		LibsLogger.debug(TableRest.class, "Table '", table.getName() , "' cleared");
	}
	
	@POST
	@Path("validate")
	public void validateTable(@Context SecurityContext sc, 
			@FormParam("tableId") UUID tableId) throws IOException, ValidationException {
		
		assertTrue(tableId != null && tableId.isValid(), "Table ID is not specified");
		
		Table table = TableFactory.getTable(tableId);
		
		validateNotFound(table != null, "Dataset is not found");
		validateUnauthorized(isAdmin(sc) || table.isWriteAllowed(getUserId(sc)), "Permission denied");
		
		table.validate(true, true);
		
		LibsLogger.debug(TableRest.class, "Table '", table.getName() , "' scheduled for validation");
	}
	
	@POST
	@Path("index")
	public void indexTableData(@Context SecurityContext sc, 
			@FormParam("tableId") UUID tableId) throws IOException, ValidationException {
		
		assertTrue(tableId != null && tableId.isValid(), "Table ID is not specified");
		
		Table table = TableFactory.getTable(tableId);
		
		validateNotFound(table != null, "Dataset is not found");
		validateUnauthorized(isAdmin(sc) || table.isWriteAllowed(getUserId(sc)), "Permission denied");
		
		table.indexData(true);
		LibsLogger.debug(TableRest.class, "Table '", table.getName() , "' scheduled for indexing");
	}
	
	@GET
	@Path("search")
	@Produces(MediaType.APPLICATION_JSON)
	public String searchTables(@Context SecurityContext sc,
			@QueryParam("datasetQuery") String datasetQuery,
			@QueryParam("tableQuery") String tableQuery, 
			@QueryParam("from") int from,
			@QueryParam("size") int size) throws IOException, ValidationException {
		final SearchResults<Table> ntsList = //UUID.isValid(datasetQuery) ?
				//TableFactory.searchTables(getUserId(sc), new UUID(datasetQuery), tableQuery, from, size)
				//:
				TableFactory.searchTables(getUserId(sc), datasetQuery, tableQuery, from, size);
		
		List<Table> readAllowedTables = new ArrayList<Table>();
		for (Table table : ntsList) {
			try {
				// TODO I switched that off
//				table.getColumnDefs(); // init columns
				
				if (isAdmin(sc) || table.isReadAllowed(getUserId(sc)))
					readAllowedTables.add(table);
			} catch (Throwable e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return new HttpResponse()
			.set("tables", JsonUtils.toJson(readAllowedTables))// readAllowedTables)
			.set("maxHits", ntsList.maxHits())
			.toString();
	}
	
	
	@GET
	@Path("get")
	@Produces(MediaType.APPLICATION_JSON)
	public Response get(@Context SecurityContext sc,
			@QueryParam("table-id") String tableId) throws IOException, ValidationException {
		Table table = TableFactory.getTable(new UUID(tableId));
		
		validateNotFound(table != null, "Table is not found");
		validateUnauthorized(isAdmin(sc) || table.isReadAllowed(getUserId(sc)), "Permission denied");
		
		table.getColumnDefs();
				
		return toResponse(table);
	}
	
	@GET
	@Path("getByIndex")
	@Produces(MediaType.APPLICATION_JSON)
	public Response get(@Context SecurityContext sc,
			@QueryParam("notebookId") UUID notebookId,
			@QueryParam("queryId") UUID queryId,
			@QueryParam("tableIndex") int tableIndex) throws IOException, ValidationException {
		
		Table table = null;
		int tableCounter = 0;
		
		final Notebook notebook = Notebook.getNotebook(notebookId);
		
		validateNotFound(notebook != null, "Notebook is not found");
		validateUnauthorized(isAdmin(sc) || notebook.isReadAllowed(getUserId(sc)), "Permission denied");
		
		for (Table notebookTable : TableFactory.iterateTables(notebookId, queryId)) {
			if (tableCounter == tableIndex) {
				table = notebookTable;
				break;
			}
			
			tableCounter++;
		}
		
		if (table != null)
			validateUnauthorized(isAdmin(sc) || table.isReadAllowed(getUserId(sc)), "Permission denied");
		
		if (table == null)
			return Response
					.noContent()
					.build();
		
		return toResponse(table);
	}
	
	@GET
	@Path("list")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getList(@Context SecurityContext sc,
			@QueryParam("notebookId") UUID notebookId,
			@QueryParam("entryId") UUID queryId) throws IOException, ValidationException {
		final Notebook notebook = Notebook.getNotebook(notebookId);
		
		validateNotFound(notebook != null, "Notebook is not found");
		validateUnauthorized(isAdmin(sc) || notebook.isReadAllowed(getUserId(sc)), "Permission denied");
		
		List<Table> tables = new ArrayList<Table>();
		for (Table table : TableFactory.iterateTables(notebookId, queryId)) {
			if (isAdmin(sc) || table.isReadAllowed(getUserId(sc)))
				tables.add(table);
		}
		
		return Response
				.ok()
				.entity(Json
						.object()
						.set("maxHits", tables.size())
						.set("tables", Json.read(JsonUtils.objectToString(tables)))
						.toString())
				.build();
	}
	
	@DELETE
	@Path("delete")
	public void delete(@Context SecurityContext sc, 
			@QueryParam("table-id") List<UUID> tableIds,
			@QueryParam("in-background") boolean inBackground) throws IOException, ValidationException {
		final SessionBean sb = (SessionBean) sc.getUserPrincipal();
		
		if (inBackground) {
			
			new TableRemoveJob(getUserId(sc)).setTablesToRemove(tableIds).save();
			
		} else {
		
			List<DbObject> dbObjects = new ArrayList<>();
			
			for (UUID tableId : tableIds) {
				assertTrue(tableId != null && tableId.isValid(), "Table ID is not specified");
				
				Table table = TableFactory.getTable(tableId);
				
				validateNotFound(table != null, "Table is not found");
				validateUnauthorized(isAdmin(sc) || table.isWriteAllowed(getUserId(sc)), "Permission denied");
				
				dbObjects.add(table);
				table.remove();
				
				LibsLogger.debug(TableRest.class, "Table ", table.getName() , " with id " + table.getId(), " removed");
			}
			
			DbObject.removeBulk(dbObjects);
		}
	}
	
	@GET
	@Path("rowscount")
	@Produces(MediaType.TEXT_PLAIN)
	public long rowsCount(@Context SecurityContext sc, 
			@QueryParam("tableId") UUID tableId) throws IOException, ValidationException {
		
		assertTrue(tableId != null && tableId.isValid(), "Table ID is not specified");
		
		Table table = TableFactory.getTable(tableId);
		
		validateNotFound(table != null, "Table is not found");
		validateUnauthorized(isAdmin(sc) || table.isReadAllowed(getUserId(sc)), "Permission denied");
		
		return table.getNrOfRows();
	}
	
	@GET
	@Path("isempty")
	@Produces(MediaType.TEXT_PLAIN)
	public boolean isEmpty(@Context SecurityContext sc, 
			@QueryParam("tableId") UUID tableId) throws IOException, ValidationException {
		
		assertTrue(tableId != null && tableId.isValid(), "Table ID is not specified");
		
		Table table = TableFactory.getTable(tableId);
		
		validateNotFound(table != null, "Table is not found");
		validateUnauthorized(isAdmin(sc) || table.isReadAllowed(getUserId(sc)), "Permission denied");
		
		return table.isTableEmpty();
	}
	
	@GET
	@Path("isReadAllowed")
	@Produces(MediaType.TEXT_PLAIN)
	public Response isReadAllowed(@Context SecurityContext sc,
			@QueryParam("userId") UUID userId, 
			@QueryParam("tableId") UUID tableId) throws IOException {
		Table tb = TableFactory.getTable(tableId);
		
		validateNotFound(tb != null, "Table " + tableId + " not found");
		
		return Response
				.ok()
				.entity(Json
						.object()
						.set("readAllowed", tb.isReadAllowed(userId))						
						.toString())
				.build();
	}
	
	@GET
	@Path("isWriteAllowed")
	@Produces(MediaType.TEXT_PLAIN)
	public Response isWriteAllowed(@Context SecurityContext sc,
			@QueryParam("userId") UUID userId, 
			@QueryParam("tableId") UUID tableId) throws IOException {
		Table tb = TableFactory.getTable(tableId);
		
		validateNotFound(tb != null, "Table " + tableId + " not found");
		
		return Response
				.ok()
				.entity(Json
						.object()
						.set("writeAllowed", tb.isWriteAllowed(userId))
						.toString())
				.build();
	}
	
	@GET
	@Path("isDeleteAllowed")
	@Produces(MediaType.TEXT_PLAIN)
	public Response isDeleteAllowed(@Context SecurityContext sc,
			@QueryParam("userId") UUID userId, 
			@QueryParam("tableId") UUID tableId) throws IOException {
		Table tb = TableFactory.getTable(tableId);
		
		if (tb == null) {
			LibsLogger.error(TableRest.class, "Table " + tableId + " not found");
			return Response
					.ok()
					.entity(Json
							.object()
							.set("deleteAllowed", tb.isWriteAllowed(userId))
							.toString())
					.build();
		}
		
		
		return Response
				.ok()
				.entity(Json
						.object()
						.set("deleteAllowed", tb.isWriteAllowed(userId))
						.toString())
				.build();
	}
	
	public static Response toResponse(Table tb) {
		Json tbJson = Json
			.object()
			.set("id", Json.object().set("id", tb.getId().getId()))
			.set("userId", Json.object().set("id", tb.getUserId().getId()))
			.set("datasetId", Json.object().set("id", tb.getDatasetId().getId()))
			.set("importPath", tb.getMeta().getAsString("IMPORT_PATH"))
			.set("tableStatus", tb.getTableStatus().toString())
			.set("create", tb.getCreate().getTimeInMillis())
			.set("searchStatus", tb.getSearchStatus().toString())
			.set("name", tb.getName())
			.set("connectorList", Json.read(JsonUtils.objectToString(tb.getConnectorList())))
			;
		
		Json columns = Json.array();
		if (tb.isColumsDefsDefined())
			for (ColumnDef col : tb.getColumnDefs()) {
				columns.add(Json.read(JsonUtils.objectToString(col)));
			}
		tbJson.set("columns", columns);
		
		Json tbMeta = Json.object();
		for (Meta meta : tb.getMeta()) {
			tbMeta.set(meta.getName(), Json
					.object()
					.set("name", meta.getName())
					.set("value", meta.getValue() != null ? meta.getValue().toString() : null)
					.set("description", meta.getDescription()));
		}
		tbJson.set("meta", Json.object().set("meta", tbMeta));
		
		return Response
				.ok()
				.entity(tbJson.toString())
				.build();
	}
	
	@POST
	@Path("name")
	@Produces(MediaType.APPLICATION_JSON)
	public void setName(@Context SecurityContext sc, 
			@FormParam("tableId") UUID tableId,
			@FormParam("name") String name) throws IOException, ValidationException {
		Table table = TableFactory.getTable(tableId);
		
		assertTrue(table != null, "Table " + tableId + " not found");
		
		table.changeName(name);
	}
}
