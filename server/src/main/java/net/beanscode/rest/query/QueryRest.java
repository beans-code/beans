package net.beanscode.rest.query;

import static java.lang.String.format;
import static net.hypki.libs5.utils.utils.AssertUtils.assertTrue;

import java.io.IOException;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.commons.lang.NotImplementedException;

import net.beanscode.model.cass.notification.Notification;
import net.beanscode.model.cass.notification.NotificationFactory;
import net.beanscode.model.cass.notification.NotificationType;
import net.beanscode.rest.BeansRest;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.pjf.op.Op;
import net.hypki.libs5.pjf.op.OpSet;

@Path("api/query")
public class QueryRest extends BeansRest {
	
//	@GET
//	@Path("info")
//	@Produces(MediaType.APPLICATION_JSON)
//	public String info(@Context SecurityContext sc,
//			@QueryParam("queryId") String queryId) throws IOException, ValidationException {
//		PigScript q = PigScript.getQuery(queryId);
//		
//		validateNotFound(q != null, format("Cannot find query %s", queryId));
//		
//		final Notebook note = q.getNotebook();
//		
//		validateNotFound(note != null, "Notebook is not found");
//		validateUnauthorized(isAdmin(sc) || note.isWriteAllowed(getUserId(sc)), "Permission denied");
//		
////		setResponse(new RestResponse(RestResponse.OK, "Query info")
////			.put("create", q.getCreate().toStringHuman())
////			.put("lastEdit", q.getLastEdit().toStringHuman())
////			.put("user", q.getUserId().getId())
////			);
//		
//		return toString();
//	}

//	@POST
//	@Path("run")
//	public void run(@Context SecurityContext sc,
//			@FormParam("queryId") String queryId,
//			@FormParam("query") String query) throws IOException, ValidationException {
//		PigScript q = PigScript.getQuery(queryId);
//		
//		validateNotFound(q != null, format("Cannot find query %s", queryId));
//		
//		final Notebook note = q.getNotebook();
//		
//		validateNotFound(note != null, "Notebook is not found");
//		validateUnauthorized(isAdmin(sc) || note.isWriteAllowed(getUserId(sc)), "Permission denied");
//		
////		q.setQuery(query);
////		q.save();
//		
//		q.runQuery();
//		
//		try {
//			// update last edit
//			note.save();
//		} catch (Throwable t) {
//			LibsLogger.error(QueryRest.class, "Cannot update last edit for Notebook " + note, t);
//		}
//		
//		addToResponse(new OpSet("#" + q.getId() + " .status .label", "SUBMITTED"));
//		
////		return toString();
//	}
	
//	@POST
//	@Path("edit")
//	public void edit(@Context SecurityContext sc,
//			@FormParam("queryId") String queryId,
//			@FormParam("name") String newName,
//			@FormParam("pigMode") String pigMode,
//			@FormParam("query") String query,
//			@FormParam("notes") String notes) throws IOException, ValidationException {
//		
//		PigScript q = PigScript.getQuery(new UUID(queryId));
//		
//		validateNotFound(q != null, format("Cannot find query %s", queryId));
//		
//		final Notebook note = q.getNotebook();
//		
//		validateNotFound(note != null, "Notebook is not found");
//		validateUnauthorized(isAdmin(sc) || note.isWriteAllowed(getUserId(sc)), "Permission denied");
//		
//		if (notEmpty(newName))
//			q.setName(newName);
//		if (notEmpty(query))
//			q.setQuery(query);
//		q.setPigMode(pigMode);
//		q.save();
//		
//		LibsLogger.debug(getClass(), format("Name for the query %s updated to %s", queryId, newName));
//	}
	
	@GET
	@Path("badge")
	@Produces(MediaType.APPLICATION_JSON)
	public String badge(@QueryParam("queryId") String queryId) throws IOException, ValidationException {
//		net.beanscode.model.query.QueryStatus qs = net.beanscode.model.query.QueryStatus.getStatus(new UUID(queryId));
		Notification n = NotificationFactory.getNotification(queryId);
		
		assertTrue(n != null, format("Cannot find query status %s", queryId));
		
		addToResponse(getBadge(n));
		
		return toString();
	}
	
//	// TODO removing it entirely, I do not understand this API ending
//	@GET
//	@Path("status")
//	@Produces(MediaType.APPLICATION_JSON)
//	public Response status(@Context SecurityContext sc,
//			@QueryParam("queryId") String queryId) throws IOException, ValidationException {
//		
//		assertTrue(notEmpty(queryId), "queryId not specified");
//		
////		QueryStatus qs = QueryStatus.getStatus(new UUID(queryId));
//		Notification qs = NotificationFactory.getNotification(queryId);
//		
//		assertTrue(qs != null, "Cannot read query status ");
//		
//		if (qs.getNotificationType() == NotificationType.OK) {
//			
//			JsonObject plot = new JsonObject();
//			
//			Plot p = PlotFactory.getPlot(qs.getId());
//			
//			validateUnauthorized(isAdmin(sc) || p.getNotebook().isReadAllowed(getUserId(sc)), "Permission denied");
//			
//			if (p != null) {
//				plot.addProperty("plotType", p.getPlotType(0).toString());
//				
//				JsonArray columnsArr = new JsonArray();
//				for (String column : p.getAllColumns()) {
//					columnsArr.add(new JsonPrimitive(column));
//				}
//				plot.add("columns", columnsArr);
//				
//				// plot options
//				JsonObject options = new JsonObject();
//				
//				JsonObject colors = new JsonObject();
//				for (String colorKey : p.getColorsKeys()) {
//					Color color = p.getColor(colorKey);
//					if (color.getName() != null)
//						colors.addProperty(colorKey, color.getName());
//				}
//				options.add("colors", colors);
//				
//				JsonObject sizes = new JsonObject();
//				for (String sKey : p.getSizesKeys()) {
//					if (sKey != null)
//						sizes.addProperty(sKey, p.getSize(sKey));
//				}
//				options.add("sizes", sizes);
//				
//				plot.add("options", options);
//				
//				
//	//			setResponse(new RestResponse(RestResponse.OK, "Query done")
//	//				.put("done", 1)
//	//				.put("plot", plot));
//			}
//		} else {
////			setResponse(new RestResponse(RestResponse.OK, "Query done").put("done", 0));
//			
//		}
//
//		return Response
//				.ok()
//				.entity(qs.getData())
//				.build();
//	}
	
	public static Op getBadge(Notification qs) {
		if (qs == null)
			return new OpSet("#query-" + qs.getId() + " #query-status", "<span ><i class='glyphicon glyphicon-info-sign'/> Submitted</span>");
		else if (qs.getNotificationType() == NotificationType.INFO)
			return new OpSet("#query-" + qs.getId() + " #query-status", "<span ><i class='glyphicon glyphicon-info-sign'/> Running... (" + qs.getMeta().get("progress").getAsString() + "%)</span>");
		else if (qs.getNotificationType() == NotificationType.OK)
			return new OpSet("#query-" + qs.getId() + " #query-status", "<span ><i class='glyphicon glyphicon-info-sign'/> Done</span>");
		if (qs.getNotificationType() == NotificationType.ERROR)
			return new OpSet("#query-" + qs.getId() + " #query-status", "<span title='" + qs.getDescription() + "' rel='tooltip'><i class='glyphicon glyphicon-info-sign'/> Failed</span>");
		else
			throw new NotImplementedException();
	}
}
