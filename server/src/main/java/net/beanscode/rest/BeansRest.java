package net.beanscode.rest;

import java.io.IOException;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import net.beanscode.model.rights.Rights;
import net.beanscode.rest.jersey.ValidationRestException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.pjf.components.jersey.SessionBean;
import net.hypki.libs5.pjf.op.Op;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.weblibs.user.Allowed;
import net.hypki.libs5.weblibs.user.Email;
import net.hypki.libs5.weblibs.user.User;
import net.hypki.libs5.weblibs.user.UserFactory;

public class BeansRest {
	
	private OpList opList = null;
	
	public BeansRest() {
		
	}
	
	@Override
	public String toString() {
		return getOpList().toString();
	}
	
	public OpList getOpList() {
		if (opList == null)
			opList = new OpList();
		return opList;
	}
	
	protected void addToResponse(Op op) {
		getOpList().add(op);
	}
	
	protected void addToResponse(OpList opList) {
		getOpList().add(opList);
	}
	
	public SessionBean getSessionBean(final SecurityContext sc) {
		return (SessionBean) sc.getUserPrincipal();
	}
	
	protected UUID getUserId(final String userEmail) {
		try {
			User u = User.getUser(new Email(userEmail));
			return u != null ? u.getUserId() : null;
		} catch (IOException e) {
			LibsLogger.error(BeansRest.class, "Cannot get User from DB", e);
			return null;
		}
	}
	
	protected UUID getUserId(final SecurityContext sc) {
		return getSessionBean(sc) != null ? getSessionBean(sc).getUserId() : null;
	}
	
	protected User getLoggedUser(final SecurityContext sc) throws IOException {
		return UserFactory.getUser(getUserId(sc));
	}
	
	protected boolean isAdmin(final SecurityContext sc) {
		UUID userId = getUserId(sc);
		return userId != null ? Allowed.isAllowed(userId, Rights.ADMIN) : false;
	}
	
	protected void validateBadRequest(boolean validateTrue, String message) {
		if (!validateTrue)
			throw new ValidationRestException(Response.Status.BAD_REQUEST, message);
	}
	
	protected void validateNotFound(boolean validateTrue, String message) {
		if (!validateTrue)
			throw new ValidationRestException(Response.Status.NOT_FOUND, message);
	}
	
	protected void validateUnauthorized(boolean validateTrue, String message) {
		if (!validateTrue)
			throw new ValidationRestException(Response.Status.UNAUTHORIZED, message);
	}
	
	protected void validateTrue(boolean validateTrue, String message) {
		if (!validateTrue)
			throw new ValidationRestException(Response.Status.UNAUTHORIZED, message);
	}
}
