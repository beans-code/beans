package net.beanscode.rest.jersey;

//import com.sun.jersey.api.container.MappableContainerException;

//import com.sun.jersey.core.util.Base64;
import static net.hypki.libs5.utils.string.StringUtilities.nullOrEmpty;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import javax.annotation.Priority;
import javax.ws.rs.Priorities;
//import javax.annotation.Priority;
//import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

import net.beanscode.web.BeansWebConst;
import net.hypki.libs5.db.db.Results;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.weblibs.WeblibsConst;
import net.hypki.libs5.weblibs.user.CookieUUID;
import net.hypki.libs5.weblibs.user.CookieUUIDFactory;
import net.hypki.libs5.weblibs.user.User;

@Provider
// @PreMatching
@Priority(Priorities.AUTHENTICATION)
public class SecurityContextFilter implements ContainerRequestFilter {
	
	private static boolean userCountChecked = false;

	public SecurityContextFilter() {
		LibsLogger.debug(SecurityContextFilter.class, getClass().getSimpleName(), " initialized!");
	}

	@Override
	public void filter(final ContainerRequestContext requestContext) throws IOException {
		final String path = requestContext.getUriInfo().getPath();
		LibsLogger.debug(SecurityContextFilter.class, "BEANS Request path ", path);
		
		if (path.startsWith("/api/setting/registration")
				|| path.startsWith("/api/admin/status")
				|| path.startsWith("/api/user/login")
//				|| path.startsWith("/api/setting/getSetting")
				|| path.startsWith("/static/")
				|| path.startsWith("/favicon.ico")
				|| path.startsWith("/dist/")
				|| path.startsWith("/res/")
				|| path.startsWith("/wl/")
				|| path.startsWith("/pjf/")
				|| path.startsWith("/assets/")
				|| path.startsWith("/login")
				|| path.startsWith("/confirm")
				|| path.startsWith("/favicon")
				|| path.startsWith("/plugins/")
				)
//		if (path.startsWith("/static/") || path.startsWith("/res/") || path.startsWith("/wl/") || path.startsWith("/pjf/") || path.startsWith("/assets/")
//				|| path.startsWith("/api/user/login")
//				|| path.startsWith("/api/user/recover")
//				|| path.startsWith("/api/user/confirmResetPass")
//				|| path.startsWith("/api/user/login")
//				|| path.startsWith("/view/login")
//				|| path.startsWith("/login")
//				|| path.startsWith("/api/user/createAdmin") || path.startsWith("/login") || path.startsWith("/createAdmin"))
			return;
	
//		final String host = requestContext.getUriInfo().getBaseUri().getHost();
		
		final Cookie lc = requestContext.getCookies().get(BeansWebConst.COOKIE_LOGIN);
		LibsLogger.debug(SecurityContextFilter.class, "Login cookie ", lc);

		if (lc == null || nullOrEmpty(lc.getValue()) || lc.getValue().equalsIgnoreCase("null")) {
			LibsLogger.debug(SecurityContextFilter.class, "No cookie no login");
			requestContext.setSecurityContext(new MySecurityContext(null, null));
//			throw new SecurityException("You need to log in");
			return;
		}

		// validate cookie
		CookieUUID cookieUUID = CookieUUIDFactory.getCookieID(lc.getValue());

		if (cookieUUID == null) {
			LibsLogger.debug(SecurityContextFilter.class, "Cookie exists, but ", CookieUUID.class.getName(), " does not");
			requestContext.setSecurityContext(new MySecurityContext(null, null));
//			throw new SecurityException("You need to log in");
			return;
		}

		// check if cookie has not expired
//		if (cookieUUID.isExpired()) {
//			LibsLogger.debug(SecurityContextFilter.class, "Cookie expired for user ", cookieUUID.getUserEmail());
//			requestContext.setSecurityContext(new MySecurityContext(null));
////			throw new SecurityException("You need to log in");
//			return;
//		}

//		// if client domain differs then no login
//		if (cookieUUID.getClientDomain().equalsIgnoreCase(requestContext.getUriInfo().getRequestUri().getHost()) == false) {
//			requestContext.setSecurityContext(new MySecurityContext(null, null));
////			throw new SecurityException("You need to log in");
//			return;
//		}

		requestContext.setSecurityContext(new MySecurityContext(new UUID(lc.getValue()), cookieUUID.getUserUUID()));
		return;
	}

}
