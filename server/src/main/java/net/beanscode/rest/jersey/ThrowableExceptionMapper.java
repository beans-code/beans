package net.beanscode.rest.jersey;

import java.io.IOException;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.url.HttpResponse;

@Provider
public class ThrowableExceptionMapper implements ExceptionMapper<Throwable> {

	public Response toResponse(Throwable t) {
		LibsLogger.error(ThrowableExceptionMapper.class, "Throwable exception", t);
		
		return Response
				.status(Response.Status.NOT_ACCEPTABLE)
				.entity(new HttpResponse()
							.setCode(Response.Status.NOT_ACCEPTABLE.getStatusCode())
							.setMessage(t.getMessage()))
//				.entity(new OpList()
//								.add(new OpError(exception.toString())).toString())
				.build();
	}

}
