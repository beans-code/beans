package net.beanscode.rest;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;

import javax.ws.rs.Path;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.api.API;
import net.hypki.libs5.utils.file.FileUtils;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.json.JsonWrapper;
import net.hypki.libs5.utils.reflection.ReflectionUtility;

public class BeansApiGenerator {

	public static void main(String[] args) throws IOException, ClassNotFoundException {
		API api = API.buildAPI(BeansServerMain.class);
		
		FileUtils.saveToFile(api.toString(), new File("../model/src/main/resources/beans-api.json"), true);
		FileUtils.saveToFile(api.toString(), new File("../web/src/main/resources/beans-api.json"), true);
		
		LibsLogger.info(BeansApiGenerator.class, "Finished");
	}
}
