package net.beanscode.pojo.tables;

import java.io.Serializable;

import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.string.MetaList;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotNull;

import com.google.gson.annotations.Expose;

public class ConnectorInitParams implements Serializable {

	@Expose
	@NotNull
	@AssertValid
	private UUID tbid = null;
	
	@Expose
	@NotNull
	@AssertValid
	private MetaList meta = null;
	
	public ConnectorInitParams() {
		
	}

	public UUID getTableId() {
		return tbid;
	}
	
	@Override
	public String toString() {
		return "tableId=" + getTableId().toString() + ",meta=" + (meta != null ? meta.toString() : "");
	}

	public ConnectorInitParams setTableId(UUID tableId) {
		this.tbid = tableId;
		return this;
	}

	public MetaList getMeta() {
		if (meta == null)
			this.meta = new MetaList();
		return meta;
	}

	public ConnectorInitParams setMeta(MetaList meta) {
		this.meta = meta;
		return this;
	}
}
