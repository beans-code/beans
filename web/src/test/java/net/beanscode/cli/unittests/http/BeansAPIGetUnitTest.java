package net.beanscode.cli.unittests.http;

import java.io.IOException;

import org.junit.Test;

import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.cli.datasets.TableSearchResults;
import net.beanscode.cli.unittests.BeansTestCase;
import net.hypki.libs5.cli.noninteractivecli.CLIException;

public class BeansAPIGetUnitTest extends BeansTestCase {

	@Test
	public void testGET() throws CLIException, IOException {
		TableSearchResults tableSearchResults = new ApiCallCommand(getSessionBean())
				.setApi("api/table/search")
				.addParam("datasetQuery", "mocca")
				.addParam("tableQuery", "system")
				.addParam("from", 0)
				.addParam("size", 100)
				.run()
				.getResponse()
				.getAsObject(TableSearchResults.class);
		
		System.out.println(tableSearchResults.toString());
	}
}
