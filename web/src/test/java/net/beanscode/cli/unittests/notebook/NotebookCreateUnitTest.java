package net.beanscode.cli.unittests.notebook;

import java.io.IOException;

import net.beanscode.cli.BeansCLISettings;
import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.cli.notebooks.Notebook;
import net.beanscode.cli.unittests.BeansTestCase;
import net.hypki.libs5.cli.noninteractivecli.CLIException;
import net.hypki.libs5.pjf.components.jersey.SessionBean;
import net.hypki.libs5.utils.date.SimpleDate;

import org.junit.Test;

public class NotebookCreateUnitTest extends BeansTestCase {

	@Test
	public void testCreate() throws CLIException, IOException {
		final Notebook notebook = new ApiCallCommand(getSessionBean())
									.setApi("api/notebook/create")
									.addParam("name", "Notebook " + new SimpleDate().toString())
									.run()
									.getResponse()
									.getAsObject(Notebook.class);
		
		
	}
}
