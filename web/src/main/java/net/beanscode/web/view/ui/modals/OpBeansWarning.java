package net.beanscode.web.view.ui.modals;

import net.hypki.libs5.pjf.op.OpJs;

public class OpBeansWarning extends OpJs {

	public OpBeansWarning(String warning) {
		super("toastr.warning('" + warning + "');");
	}
}
