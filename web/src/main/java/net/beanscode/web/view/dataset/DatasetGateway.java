package net.beanscode.web.view.dataset;

import java.io.IOException;

import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.cli.datasets.DatasetSearchResults;
import net.beanscode.pojo.dataset.Dataset;
import net.hypki.libs5.cli.noninteractivecli.CLIException;
import net.hypki.libs5.pjf.components.jersey.SessionBean;

public class DatasetGateway {

	public static DatasetSearchResults getDatasetSearchResults(SessionBean sb, String filter, int page, int perPage) throws CLIException, IOException {
		return new ApiCallCommand(sb)
				.setApi("api/dataset/query")
				.addParam("query", filter)
				.addParam("from", page)
				.addParam("size", perPage)
				.run()
				.getResponse()
				.getAsObject(DatasetSearchResults.class);
	}
	
	public static Dataset getDataset(final SessionBean sb, final String datasetId) throws CLIException, IOException {
		return new ApiCallCommand(sb)
				.setApi("api/dataset")
				.addParam("dataset-id", datasetId)
				.run()
				.getResponse()
				.getAsObject(Dataset.class);
	}
}
