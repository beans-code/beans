package net.beanscode.web.view.plots;

import static org.rendersnake.HtmlAttributesFactory.id;
import static org.rendersnake.HtmlAttributesFactory.type;

import java.io.IOException;

import net.beanscode.pojo.NotebookEntry;
import net.beanscode.pojo.autoupdate.AutoUpdateOption;
import net.beanscode.web.view.ui.BeansComponent;
import net.beanscode.web.view.ui.RadioComponent;

import org.rendersnake.HtmlCanvas;

import com.google.gson.annotations.Expose;

public class AutoUpdateSettingsPanel extends BeansComponent {
	
	@Expose
	private String currentValue = null;
	
	@Expose
	private String entryId = null;
	
	public AutoUpdateSettingsPanel() {
		
	}

	public AutoUpdateSettingsPanel(BeansComponent parent) {
		super(parent);
	}
	
	public AutoUpdateSettingsPanel(BeansComponent parent, AutoUpdateOption option) {
		super(parent);
		this.currentValue = option != null ? option.name() : null;
	}
	
	public AutoUpdateSettingsPanel(BeansComponent parent, NotebookEntry ne) {
		super(parent);
		this.currentValue = ne.getAutoUpdateOptions().getOption().name();
		this.entryId = ne.getId().getId();
	}
	
	@Override
	public void renderOn(HtmlCanvas html) throws IOException {
		html
			.div(id(getId()))
				.input(type("hidden")
						.name("entryId")
						.value(entryId))
				.render(new RadioComponent(this, "auto-update-setting")
						.addValue(AutoUpdateOption.NO_CONFIRMATION_NEEDED.name(), "update automatically")
						.addValue(AutoUpdateOption.CONFIRMATION_NEEDED.name(), "ask for confirmation")
						.addValue(AutoUpdateOption.IGNORE.name(), "ignore")
						.setSelectedValue(this.currentValue)
						)
			._div()
			;
	}
}
