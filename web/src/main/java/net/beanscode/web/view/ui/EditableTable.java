package net.beanscode.web.view.ui;

import static net.hypki.libs5.pjf.OpsFactory.opsCall;
import static net.hypki.libs5.utils.utils.AssertUtils.assertTrue;
import static org.rendersnake.HtmlAttributesFactory.class_;
import static org.rendersnake.HtmlAttributesFactory.id;
import static org.rendersnake.HtmlAttributesFactory.name;
import static org.rendersnake.HtmlAttributesFactory.type;

import java.io.IOException;
import java.util.Map;

import javax.ws.rs.core.SecurityContext;

import net.beanscode.web.view.ui.modals.OpBeansError;
import net.beanscode.web.view.ui.modals.OpBeansInfo;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.pjf.components.Component;
import net.hypki.libs5.pjf.components.buttons.Button;
import net.hypki.libs5.pjf.components.buttons.ButtonGroup;
import net.hypki.libs5.pjf.components.buttons.ButtonSize;
import net.hypki.libs5.pjf.components.ops.OpComponentClick;
import net.hypki.libs5.pjf.op.OpAppend;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.url.Params;

import org.rendersnake.HtmlCanvas;

import com.google.gson.annotations.Expose;

public abstract class EditableTable extends BeansComponent {
	
	private ButtonSize buttonSize = ButtonSize.EXTRA_SMALL;
	
	private String[] columnNames = null;
	
	private Object[][] values = null;
	
	private Params hiddenParams = null;
	
	@Expose
	private boolean autosave = false;
	
	public EditableTable() {
		
	}
	
	public EditableTable(Component parent) {
		super(parent);
	}

	public EditableTable(Component parent, String [] columnNames, Object[][] values) {
		super(parent);
		setColumnNames(columnNames);
		setValues(values);
	}
	
	protected abstract void saveTable(Params params, String[][] newValues) throws IOException;
	
	protected abstract String getAdditionalCssClasses();
	
	protected abstract OpList getAdditionalSaveOp();
	
	protected String getSaveClass() {
		return getClass().getName();
	}
	
	@Override
	public void renderOn(HtmlCanvas html) throws IOException {
		assertTrue(getColumnNames() != null, "Column names for ", getClass(), " cannot be empty");
//		assertTrue(getValues() != null, "Values for ", getClass(), " cannot be empty");
		
		html
			.div(id(getId())
					.class_("editable-table-div"))
			.if_(!isAutosave())
				.render(new ButtonGroup()
							.addButton(new Button("Save table")
											.add(new OpComponentClick(this, "saveTableClick", getHiddenParams()))))
			._if()
//			.div(class_("btn-group editable-table-btn-group")
//		    		.role("group"))
//		        .div(class_("btn-group").role("group"))
//			        .button(type("button")
//			        		.class_("btn btn-default " + getBootstrapSize(getButtonSize()))
//			        		.onClick(opsCall(opAjaxJs(SAVE_URL, "$('#" + getId() + " :input').serialize()", RestMethod.POST))))
//			        	.content(" Save table")
//		        ._div()
//		    ._div()
			.table(class_("table responsive-table table-hover editable-table " + getAdditionalCssClasses()));
		
		// hidden fields
		html
			.input(name("saveclass")
				.value(getSaveClass())
				.type("hidden"));
		if (getHiddenParams() != null) {
			for (Map.Entry<String, Object> e : getHiddenParams().getParams().entrySet()) {
				html
					.input(name("hidden-" + e.getKey())
							.value(e.getValue() != null ? e.getValue().toString() : "")
							.type("hidden"));
			}
		}
		
		html
				.thead();
		
		// column headers
		if (getColumnNames() != null)
			for (String column : getColumnNames()) {
				html
					.th()
						.content(column);
			}
		html
					.th()
						.content("")
				._thead();
		
		// rows
		if (getValues() != null && getValues().length > 0) {
			for (int row = 0; row < getValues().length; row++) {
				createRow(html, getValues()[row]);
			}
		} else {
			createRow(html, null);
		}
		
		html
			._table()
			._div();
	}
	
	private OpList createRowClick(SecurityContext sc, Params params) throws IOException, ValidationException {
		final int columnCount = params.getInteger("columnCount", 0);
		
		assertTrue(columnCount > 0, "Column count must be > 0");
		
		Object [] values = new String[columnCount];
		for (int col = 0; col < columnCount; col++) {
			values[col] = "";
		}
		
		return new OpList()
			.add(new OpAppend("#" + getId() + " table", createRow(new HtmlCanvas(), values).toHtml()));
	}
	
	private OpList saveTableClick(SecurityContext sc, Params params) throws IOException, ValidationException {
//			MultivaluedMap<String, String> params) throws IOException, ValidationException {
		
		try {
			int rowCount = 0;
			int colCount = 0;
//			int hiddenCount = 0;
			
			for (String key : params.getParams().keySet()) {
				if (key.startsWith("col")) {
					colCount = Math.max(colCount, net.hypki.libs5.utils.utils.NumberUtils.toInt(key.substring(3)));
					rowCount = params.isList(key) ? params.getList(key).size() : 1;
				}// else if (key.startsWith("hidden-")) {
				//	hiddenCount++;
				//}
			}
			
			String[][] newValues = new String[rowCount][colCount + 1];
			for (int row = 0; row < rowCount; row++) {
				for (int col = 0; col <= colCount; col++) {
					newValues[row][col] = params.isList("col" + col) ? (String) params.getList("col" + col).get(row) : params.getString("col" + col);
				}
			}
//			
//			String[][] hiddenValues = new String[hiddenCount][2];
//			int hiddenCounter = 0;
//			for (String key : params.keySet()) {
//				if (key.startsWith("hidden-")) {
//					hiddenValues[hiddenCounter][0] = key.substring("hidden-".length());
//					hiddenValues[hiddenCounter][1] = params.get(key).get(0);
//					hiddenCounter++;
//				}
//			}
//			
//			final String saveClass = params.getFirst("saveclass");
//			((EditableTable) Class.forName(saveClass).newInstance()).saveTable(newValues, hiddenValues);
			
			saveTable(params, newValues);
			
			return new OpList()
				.add(new OpBeansInfo("Table saved"));
		} catch (Exception e) {
			LibsLogger.error(EditableTable.class, "Cannot save table", e);
			return new OpList()
				.add(new OpBeansError("Table not saved: " + e.getMessage()));
		}
	}

	private HtmlCanvas createRow(HtmlCanvas html, Object[] values) throws IOException {
		if (values == null) {
			values = new String[getColumnNames().length];
			for (int col = 0; col < getColumnNames().length; col++) {
				values[col] = "";
			}
		}
		
		final String trId = UUID.random().getId();
		html
			.tr(id(trId));
		
		for (int col = 0; col < values.length; col++) {
			html
				.td()
					.render(new EditableInput("col" + col, values[col] != null ? String.valueOf(values[col]) : "") {
						@Override
						protected OpList getAdditionalSaveOp() {
							return EditableTable.this.getAdditionalSaveOp();
						}
					})
				._td()
				;
		}
		
		return html
				.td()
					.div(class_("btn-group")
				    		.role("group"))
				        .div(class_("btn-group").role("group"))
					        .button(type("button")
					        		.class_("btn btn-default " + getBootstrapSize(getButtonSize()))
//					        		.onClick(opsCall(new OpPOST(CREATE_URL, "componentId", getId(), "columnCount", values.length))))
					        		.onClick(opsCall(new OpComponentClick(this, "createRowClick", new Params().add("columnCount", values.length)))))
					        	.i(class_("glyphicon glyphicon-plus"))
					            	.content(" ")
				           ._button()
				           .button(type("button")
					        		.class_("btn btn-default " + getBootstrapSize(getButtonSize()))
					        		.onClick("$('#" + getId() + " #" + trId + "').remove()"))
					        	.i(class_("glyphicon glyphicon-remove"))
					            	.content(" ")
				           ._button()
				        ._div()
				    ._div()
				._td()
			._tr();
	}

	public String[] getColumnNames() {
		return columnNames;
	}


	public void setColumnNames(String[] columnNames) {
		this.columnNames = columnNames;
	}

	public Object[][] getValues() {
		return values;
	}

	public void setValues(Object[][] values) {
		this.values = values;
	}
	
	private String getBootstrapSize(ButtonSize size) {
		switch (size) {
		case EXTRA_SMALL:
			return "btn-xs";
		case SMALL:
			return "btn-sm";
		case NORMAL:
			return "";
		default:
			return "btn-sm";
		}
	}

	public ButtonSize getButtonSize() {
		return buttonSize;
	}

	public void setButtonSize(ButtonSize buttonSize) {
		this.buttonSize = buttonSize;
	}

	public Params getHiddenParams() {
		if (hiddenParams == null)
			hiddenParams = new Params();
		return hiddenParams;
	}

	private void setHiddenParams(Params hiddenParams) {
		this.hiddenParams = hiddenParams;
	}

	public boolean isAutosave() {
		return autosave;
	}

	public void setAutosave(boolean autosave) {
		this.autosave = autosave;
	}
}
