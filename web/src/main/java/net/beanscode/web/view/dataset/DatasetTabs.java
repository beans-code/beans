package net.beanscode.web.view.dataset;

import static org.rendersnake.HtmlAttributesFactory.class_;
import static org.rendersnake.HtmlAttributesFactory.id;

import java.io.IOException;

import net.beanscode.cli.BeansCliCommand;
import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.cli.datasets.Table;
import net.beanscode.cli.datasets.TableGateway;
import net.beanscode.cli.datasets.TableSearchResults;
import net.beanscode.pojo.dataset.Dataset;
import net.beanscode.web.view.ui.BeansComponent;
import net.beanscode.web.view.ui.BetaButtonGroup;
import net.beanscode.web.view.ui.SpinnerLabel;
import net.beanscode.web.view.ui.TabsBetaComponent;
import net.beanscode.web.view.ui.modals.InputType;
import net.beanscode.web.view.ui.modals.ModalWindow;
import net.beanscode.web.view.ui.modals.OpBeansSuccess;
import net.beanscode.web.view.ui.modals.OpModal;
import net.beanscode.web.view.ui.modals.OpModalQuestion;
import net.beanscode.web.view.upload.UploadView;
import net.beanscode.web.view.users.PermissionPanel;
import net.hypki.libs5.cli.noninteractivecli.CLIException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.pjf.components.PagerRenderable;
import net.hypki.libs5.pjf.components.buttons.Button;
import net.hypki.libs5.pjf.components.buttons.ButtonSize;
import net.hypki.libs5.pjf.components.ops.OpComponentClick;
import net.hypki.libs5.pjf.op.OpJs;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.pjf.op.OpReplace;
import net.hypki.libs5.pjf.op.OpSet;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.url.Params;

import org.apache.commons.lang.NotImplementedException;
import org.rendersnake.HtmlCanvas;
import org.rendersnake.Renderable;

import com.google.gson.annotations.Expose;

public class DatasetTabs extends TabsBetaComponent {
	
	@Expose
	private int perPage = 5;

	@Expose
	private UUID datasetId = null;
	
	private Dataset dataset = null;
	
	public DatasetTabs() {
		
	}

	public DatasetTabs(BeansComponent parent, Dataset ds) {
		super(parent);
		setDataset(ds);
		setDatasetId(ds.getId());
	}
	
	@Override
	protected OpList getAdditionalOpListOnTabClick(int tabIndex) throws IOException {
		OpList opList = super.getAdditionalOpListOnTabClick(tabIndex);
		
		if (tabIndex == 4) {
			if (opList == null)
				opList = new OpList();
			opList
				.add(new OpSet(".tabs-tables-" + getId(), new SpinnerLabel("Loading tables..").toHtml()))
				.add(new OpComponentClick(this, "onLoadTables"));
		}
		
		return opList;
	}
	
	private OpList onLoadTables() throws IOException {
		return new OpList()
			.add(new OpSet(".tabs-tables-" + getId(), 
					new DatasetTablesBetaTableComponent(DatasetTabs.this, getDatasetId()).toHtml()));
	}
	
	@Override
	protected String getAdditionalTabHtmlClasses() {
		return "dataset-tabs";
	}
	
	@Override
	public int getTabsCount() {
		return 5; // desc, import, params, permissions, tables
	}
	
	@Override
	public String getTabName(int tabIndex) {
		if (tabIndex == 0)
			return "Summary";
		else if (tabIndex == 1)
			return "Import files";
		else if (tabIndex == 2)
			return "Parameters";
		else if (tabIndex == 3)
			return "Permissions";
//		else if (tabIndex == 4)
//			return "Other";
		else if (tabIndex == 4)
			return "Tables";
		else
			throw new NotImplementedException();
	}
	
	@Override
	public Renderable getTabHtml(final int tabIndex) throws IOException {
		final boolean isWriteAllowed = isWriteAllowed();
		
		return new Renderable() {
			@Override
			public void renderOn(HtmlCanvas html) throws IOException {
				if (tabIndex == 0)
					html
						.div(id("dataset-description-" + getDataset().getId()))
							.dl(class_("row"))
								.dt(class_("col-sm-4"))
									.content("ID")
								.dd(class_("col-sm-8 dataset-id"))
									.content(getDataset().getId().toString(" - "), false)
									
								.dt(class_("col-sm-4"))
									.content("Name")
								.dd(class_("col-sm-8 dataset-name"))
									.content(getDataset().getName(), false)
									
								.dt(class_("col-sm-4"))
									.content("Description")
								.dd(class_("col-sm-8 dataset-description"))
									.content(getDataset().getDescriptionHtml(), false)
							._dl()
//							.if_(notEmpty(getDataset().getDescription()))
//								.p()
//							._if()
//							.if_(!notEmpty(getDataset().getDescription()))
//								.p()
//									.i()
//										.content("No description yet")
//								._p()
//							._if()
						._div()
						.div()
							.render(new BetaButtonGroup()
									.setAsList(true)
									.add(new Button("Edit title/description")
											.add(new OpComponentClick(DatasetTabs.this, "onEditTitle")))
//									.add(new Button("Edit description"))
									.add(new Button("Remove dataset")
											.setReplacedHtmlClass("btn btn-danger")
											.add(new OpComponentClick(DatasetTabs.this, "onRemoveQuestion")))
									.add(new Button("Clear dataset")
										.setReplacedHtmlClass("btn btn-danger")
										.add(new OpComponentClick(DatasetTabs.this, "onClearQuestion")))
									)
						._div();
				else if (tabIndex == 1)
					if (isWriteAllowed) {
						html
							.div(id("import-multiple-tables")
									.action(UploadView.UPLOAD_URL)
									.class_("dropzone no-image"))
								.span()
									.content("<b>Drop one or multiple files</b> to upload (<b>or click</b>)", false)
							._div()
							
							.write(new OpJs("new Dropzone('#import-multiple-tables', " +
								"{url: '" + UploadView.UPLOAD_URL + "?datasetId=" + getDataset().getId() + "', addRemoveLinks: false, maxFilesize: 100000, clickable: true})"
								+ ".on('success', function(file, response) { pjfOps(response); })"
								+ ".on('error', function(file, response) { pjfOps(response); });").toStringAdhock(), false)
							;
					} else {
						html
							.span()
								.content("Dataset is read-only. Uploading new Tables is forbidden.");
					}
				else if (tabIndex == 2) {
					html
						.div(class_("tabs-permissions-" + getId()))
						._div();
					html
						.render(new MetaTableComponent(DatasetTabs.this, getDataset()));
					
				} else if (tabIndex == 3) {
					if (!getDataset().getUserId().equals(getUserId()))
						html
							.span()
								.content("Permissions visible only to the owner");
					else
						html
							.render(new PermissionPanel(DatasetTabs.this, getDataset().getId(), "Dataset"));
//				} else if (tabIndex == 4) {
//					html
//						.dl(class_("dl-horizontal"))
//							.dt()
//								.content("Dataset ID")
//							.dd()
//								.span()
//									.content(getDataset().getId().getId())
//							._dd()
//						._dl();
				} else if (tabIndex == 4) {
					html
						.div(class_("tabs-tables-" + getId()))
						._div();
				} else
					throw new NotImplementedException();
			}
		};
	}
	
	private OpList onClearQuestion() throws IOException {
		return new OpList()
			.add(new OpModalQuestion("Clearing dataset", 
					"Do you want to clear dataset <strong>" + getDataset().getName() + "</strong>?", 
					new OpComponentClick(this, "onClear", new Params().add("datasetToClear", getDatasetId().getId()))));
	}
	
	private OpList onClear() throws IOException {
		String datasetId = getParams().getString("datasetToClear");
		
		if (datasetId != null)
			new ApiCallCommand(this.getSessionBean())
				.setApi(BeansCliCommand.API_DATASET_CLEAR)
				.addParam("id", datasetId)
				.run();
		
		return new OpList()
			.add(new OpBeansSuccess("Dataset cleared"));
	}
	
	private OpList onRemoveQuestion() throws IOException {
		return new OpList()
			.add(new OpModalQuestion("Removing dataset", 
					"Do you want to remove dataset <strong>" + getDataset().getName() + "</strong>?", 
					new OpComponentClick(this, "onRemove", new Params().add("datasetToRemove", getDatasetId().getId()))));
	}
	
	private OpList onRemove() throws IOException, InterruptedException {
		String datasetId = getParams().getString("datasetToRemove");
		
		if (datasetId != null)
//			new DatasetRemove(getSessionBean()).run(new String[]{"--id", datasetId});
			new ApiCallCommand(this)
					.setApi("api/dataset/delete")
					.addParam("datasetId", datasetId)
					.run();
		
		// TODO super ugly
		Thread.sleep(1100);
		
		return new OpList()
			.add(new OpBeansSuccess("Dataset removed"))
			.add(new DatasetsBetaContent().onShow())
//			.add(new OpComponentTimer(this, "onShow", 1000))
			;
	}
	
	private OpList onEditTitle() throws IOException {
//		ModalWindow modal = new ModalWindow(this);
//		
//		modal
//			.setTitle("Editing title")
//			.add(new Button("Save changes")
//				.add(new OpComponentClick(DatasetTabs.class, modal.getId(), "onTitleSave")))
//			.add(new Button("Cancel")
//				.add(ModalWindow.onClose()))
//			.getContent()
//				.input(type("hidden")
//						.name("datasetId")
//						.value(getDataset().getId().getId()))
//				.input(class_("form-control")
//						.type("text")
//						.name("title")
//						.value(getDataset().getName()))
//				.textarea(class_("form-control")
//						.name("description")
//						.style("min-height: 200px;"))
//					.content(getDataset().getDescription());
		
		ModalWindow modal = new ModalWindow();
		modal
			.setTitle("Editing title")
			.add(new Button("Save changes")
				.add(new OpComponentClick(DatasetTabs.class, modal.getId(), "onTitleSave"))
				.add(modal.onClose()))
			.add(new Button("Cancel")
				.add(modal.onClose()))
			.add("", InputType.HIDDEN, "datasetId", getDataset().getId().getId())
			.add("Name", InputType.STRING, "title", getDataset().getName())
			.add("Description", InputType.TEXT, "description", getDataset().getDescription());
		
		return new OpList()
			.add(new OpModal(modal));
	}
	
	private OpList onTitleSave() throws IOException {
		String newTitle = getParams().getString("title");
		String newDesc = getParams().getString("description");
		
//		getDataset().setName(newTitle);
//		getDataset().setDescription(newDesc);
		
		new ApiCallCommand(this)
			.setApi("api/dataset/set")
			.addParam("id", getDataset().getId().getId())
			.addParam("name", newTitle)
			.addParam("description", newDesc)
			.run();
		
		return new OpList()
			.add(new OpSet(".beans-content-header span", newTitle))
			.add(new OpSet("#dataset-description-" + getDataset().getId() + " .dataset-name", newTitle))
			.add(new OpSet("#dataset-description-" + getDataset().getId() + " .dataset-description", newDesc));
	}
	
	private OpList onRefresh() throws IOException {
		HtmlCanvas html = new HtmlCanvas();
		renderTable(html);
		return new OpList()
				.add(new OpReplace(".tables", html.toHtml()));
	}
	
	protected Table getTable(String tableId) throws CLIException, IOException {
		Table table = TableGateway.getTable(getSessionBean(), tableId);
		return table;
	}
	
	private void renderTable(HtmlCanvas html) throws IOException {
		final int page = getParams().getInteger("page", 0);
		final String filterTables = getParams().getString("filter-tables");
		
		TableSearchResults tables = new ApiCallCommand(this)
										.setApi("api/table/search")
										.addParam("datasetQuery", getDataset().getId().getId())
										.addParam("tableQuery", filterTables)
										.addParam("from", page * getPerPage())
										.addParam("size", getPerPage())
										.run()
										.getResponse()
										.getAsObject(TableSearchResults.class);
		
		html
			.div(class_("tables"));
		
		if (tables.getTables().size() > 0) {
			for (final Table table : tables.getTables()) {
				try {
					// getting table with columns
					Table tableWithColumns = getTable(table.getId().getId());
					
					TableBetaPanel tablePanel = new TableBetaPanel(this, tableWithColumns);
						html
							.render(tablePanel);
					getOpList().addLast(new OpJs(TableBetaPanel.jsCodeNewDropzone(tablePanel.getId(), tableWithColumns)));
				} catch (Exception e) {
					LibsLogger.error(DatasetTabs.class, "Cannot render table " + table, e);
				}
			}
			
			html
				.render(new PagerRenderable(this, "onRefresh", page, getPerPage(), tables.getMaxHits(), ButtonSize.NORMAL))
				._div();
		} else {
			html
				.content("");
		}
	}
	
	private boolean isWriteAllowed() throws CLIException, IOException {
//		return new DatasetIsWriteAllowed(getSessionBean()).run(getDataset().getUser(getSessionBean()).getEmail(), getDataset().getId().getId());
		return new ApiCallCommand(this)
			.setApi("api/dataset/isWriteAllowed")
			.addParam("datasetId", getDataset().getId().getId())
			.addParam("userId", getUserId().getId())
			.run()
			.getResponse()
			.getAsBoolean("writeAllowed");
	}
	
	private boolean isDeleteAllowed() throws CLIException, IOException {
//		return new DatasetIsDeleteAllowed(getSessionBean()).run(getDataset().getUser(getSessionBean()).getEmail(), getDataset().getId().getId());
		return new ApiCallCommand(this)
			.setApi("api/dataset/isDeleteAllowed")
			.addParam("datasetId", getDataset().getId().getId())
			.addParam("userId", getUserId().getId())
			.run()
			.getResponse()
			.getAsBoolean("deleteAllowed");
	}

	public Dataset getDataset() {
		if (dataset == null) {
			try {
				dataset = new ApiCallCommand(this)
								.setApi("api/dataset")
								.addParam("dataset-id", getDatasetId().getId())
								.run()
								.getResponse()
								.getAsObject(Dataset.class);
			} catch (CLIException | IOException e) {
				LibsLogger.error(DatasetTabs.class, "Cannot get Dataset from DB", e);
			}
		}
		return dataset;
	}

	public void setDataset(Dataset dataset) {
		this.dataset = dataset;
	}
	
	private int getPerPage() {
		return perPage;
	}

	private void setPerPage(int perPage) {
		this.perPage = perPage;
	}

	public UUID getDatasetId() {
		if (datasetId == null) {
			String datasetIdTmp = getParams().getString("datasetId", null);
			datasetId = datasetIdTmp != null ? new UUID(datasetIdTmp) : null;
		}
		return datasetId;
	}

	public void setDatasetId(UUID datasetId) {
		this.datasetId = datasetId;
	}
}
