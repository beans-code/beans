package net.beanscode.web.view.settings;

import java.io.IOException;
import java.util.List;

import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.cli.settings.NewSettingsSearchResults;
import net.beanscode.cli.settings.SettingGateway;
import net.beanscode.web.view.ui.BeansComponent;
import net.beanscode.web.view.ui.BetaButtonGroup;
import net.beanscode.web.view.ui.BetaTableComponent;
import net.beanscode.web.view.ui.modals.ModalWindow;
import net.beanscode.web.view.ui.modals.OpBeansInfo;
import net.beanscode.web.view.ui.modals.OpModal;
import net.hypki.libs5.cli.noninteractivecli.CLIException;
import net.hypki.libs5.pjf.components.buttons.Button;
import net.hypki.libs5.pjf.components.ops.OpComponentClick;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.url.Params;

import org.rendersnake.HtmlCanvas;

import com.google.gson.annotations.Expose;

public class SettingsTableComponent extends BetaTableComponent {
	
	private NewSettingsSearchResults settings = null;
	
	@Expose
	private boolean withSystemSettings = true;

	public SettingsTableComponent() {
		
	}
	
	public SettingsTableComponent(BeansComponent parent) {
		super(parent);
	}
	
	@Override
	protected String getSettingsPrefix() {
		return "settings-table-";
	}
	
//	@Override
//	protected String getPanelHtmlClasses() {
//		return super.getPanelHtmlClasses() + " settings-table";
//	}
	
	@Override
	protected long getMaxHits() throws IOException {
		return getSettings().getMaxHits();
	}
	
	@Override
	protected int getNumberOfColumns() throws IOException {
		return 4;
	}
	
	@Override
	protected boolean isExapandable() throws IOException {
		return false;
	}
	
	@Override
	protected void renderExpandable(int row, HtmlCanvas html) throws IOException {
		
	}
	
	@Override
	protected String getColumnName(int column) throws IOException {
		if (column == 0)
			return "Name";
		else if (column == 2)
			return "Values";
		else if (column == 3)
			return "Description";
		else
			return "Options";
	}
	
	@Override
	protected void renderCell(int row, int column, HtmlCanvas html) throws IOException {
		Setting s = getSettings().getSettings().get(row);
		
		if (column == 0)
			html
				.div()
					.content(s.getName());
		else if (column == 1)
			html
				.div()
					.content(s.getDescription());
		else if (column == 2) {
			html
				.dl();
			for (SettingValue setting : s.getValues()) {
				if (setting.getType() == SettingType.PASS) {
					html
						.dt()
							.content(setting.getName())
						.dd()
							.content("***** ***");
				} else {
					html
						.dt()
							.content(setting.getName())
						.dd()
							.content(setting.getValueAsString());
				}
			}
			html
				._dl();
		} else
			html
				.render(new BetaButtonGroup()
						.add(new Button("")
								.setAwesomeicon("fa fa-edit")
								.add(new OpComponentClick(this, 
										"onSettingEdit",
										new Params()
											.add("settingId", s.getId())))));
		
//		html
//			.tr(HtmlAttributesFactory.class_("setting-row"))
//				.td()
//					.render(new SettingPanel(s))
//				._td()
//			._tr();
	}
	
	private OpList onSettingSace() {
		
		return new OpList()
			.add(new OpBeansInfo("New value: " ));
	}
	
	private OpList onSettingEdit() throws IOException {
		final String settingId = getParams().getString("settingId");
		
		Setting s = SettingGateway.getSetting(getSessionBean(), settingId);
		
		ModalWindow modal = new ModalWindow(this);
		
		modal.setTitle("Editing setting");
		modal.getContent().render(new SettingPanel(s));
		modal.add(new Button("Close")
				.add(modal.onClose())
//				.add(new OpComponentClick(this.getClass(), 
//						modal.getId(), 
//						"onSettingSave"))
				)
				;
		
		return new OpList()
			.add(new OpModal(modal));
	}
	
	@Override
	protected boolean isHeaderColumnVisible() throws IOException {
		return true;
	}
	
	@Override
	protected void renderTitle(HtmlCanvas html) throws IOException {
		html
			.write("Settings");
	}

	@Override
	protected boolean isFilterVisible() {
		return true;
	}

	@Override
	protected List<Button> getMenu() {
		return null;
	}

	private NewSettingsSearchResults getSettings() {
		if (settings == null) {
			try {
				ApiCallCommand cmd = (ApiCallCommand) new ApiCallCommand(getSessionBean())
					.setApi("api/setting/getSettingList")
					.addParam("filter", getFilter())
					.addParam("from", getPage() * getPerPage())
					.addParam("size", getPerPage())
					.addParam("userEmail", isWithSystemSettings() ? null : getUserEmail())
					.run();
//				settings = new NewSettingsSearch(getSessionBean()).run(getFilter(), 
//						getCurrentPage() * getPerPage(), 
//						getPerPage());
				
				settings = new NewSettingsSearchResults();
				settings.setMaxHits(cmd.getResponse().getAsLong("maxHits"));
				settings.setSettings((List<Setting>) cmd.getResponse().getAsList("settings", Setting.class));
			} catch (CLIException | IOException e) {
				LibsLogger.error(SettingsTableComponent.class, "Cannot get list of settings", e);
				settings = new NewSettingsSearchResults();
			}
		}
		return settings;
	}

	private void setSettings(NewSettingsSearchResults settings) {
		this.settings = settings;
	}

	public boolean isWithSystemSettings() {
		return withSystemSettings;
	}

	public SettingsTableComponent setWithSystemSettings(boolean withSystemSettings) {
		this.withSystemSettings = withSystemSettings;
		return this;
	}

	@Override
	protected String getTableCss() throws IOException {
		return "beans-table-settings";
	}
}
