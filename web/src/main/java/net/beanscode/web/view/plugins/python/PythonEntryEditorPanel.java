package net.beanscode.web.view.plugins.python;

import static java.lang.Math.max;
import static java.lang.Math.min;
import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;
import static net.hypki.libs5.utils.string.StringUtilities.nullOrEmpty;
import static org.rendersnake.HtmlAttributesFactory.class_;
import static org.rendersnake.HtmlAttributesFactory.data;
import static org.rendersnake.HtmlAttributesFactory.href;
import static org.rendersnake.HtmlAttributesFactory.id;
import static org.rendersnake.HtmlAttributesFactory.name;
import static org.rendersnake.HtmlAttributesFactory.src;
import static org.rendersnake.HtmlAttributesFactory.type;

import java.io.IOException;

import javax.ws.rs.core.SecurityContext;

import org.rendersnake.HtmlCanvas;
import org.rendersnake.Renderable;

import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.cli.notification.Notification;
import net.beanscode.cli.notification.NotificationType;
import net.beanscode.web.view.jobs.QueryCheckpoints;
import net.beanscode.web.view.notebook.NotebookEntryEditorPanel;
import net.beanscode.web.view.plots.PlotPanel;
import net.beanscode.web.view.ui.SpinnerLabel;
import net.beanscode.web.view.ui.badge.BadgeError;
import net.beanscode.web.view.ui.badge.BadgeInfo;
import net.beanscode.web.view.ui.badge.BadgeSuccess;
import net.hypki.libs5.pjf.components.buttons.Button;
import net.hypki.libs5.pjf.components.ops.OpComponentTimer;
import net.hypki.libs5.pjf.op.OpJs;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.pjf.op.OpReplace;
import net.hypki.libs5.pjf.op.OpSet;
import net.hypki.libs5.pjf.op.OpSetAttr;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.api.APICallType;
import net.hypki.libs5.utils.file.FileExt;
import net.hypki.libs5.utils.string.Meta;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.utils.url.Params;

public class PythonEntryEditorPanel extends NotebookEntryEditorPanel {

	public PythonEntryEditorPanel() {
		
	}

	@Override
	public Renderable getView() {
		return new Renderable() {
			@Override
			public void renderOn(HtmlCanvas html) throws IOException {
//				final Notification qs = new ApiCallCommand(PythonEntryEditorPanel.this)
//					.setApi(ApiCallCommand.API_NOTIFICATION_GET)
//					.addParam("id", getNotebookEntry().getId().getId())
//					.run()
//					.getResponse()
//					.getAsObject(Notification.class);
//				
//				if (qs != null && !qs.isDone()) {
//					html
//						.div(class_("beans-plugin-python-view"))
//							.content("Python running...");
//					return;
//				}
				
				String code = getNotebookEntry().getMetaAsString("code");
				int nrLines = StringUtilities.countCharacters(code, '\n');
				nrLines = max(nrLines, 3);
				nrLines = min(nrLines, 100);
				
				// rendering Entry's badge
				html	
					.div(class_("beans-plugin-python-view p-2"));
				
//				getStatusBadge(qs, html);
				
				// rendering Entry's title
				html
					.span(class_("label"))
						.content(getNotebookEntry().getName());
				
				// rendering Python script
				html
						.textarea(id(getId() + "-python-view"))
							.content(notEmpty(code) ? code : "")
					
						.write(new OpJs("window.myCodeMirror" + getId() 
										+ " = CodeMirror.fromTextArea(document.getElementById(\"" + getId() + "-python-view\"), "
								+ "{ "
								+ "	mode:  \"text/x-python\", "
								+ "	indentUnit: 4,"
								+ " readOnly : true,"
								+ " theme: \"default beans-plugin-python\","
								+ "	lineNumbers: true "
								+ "});").toStringAdhock(), 
								false)
					;
				
				// setting size
				html
					.write(new OpSetAttr("#"+ getId() + " .cm-s-beans-plugin-python", 
							"style", 
							"height: " + (nrLines*20) + "px !important;")
							.toStringAdhock(), false);

				// rendering Python output
				String pyOut = new ApiCallCommand(PythonEntryEditorPanel.this)
					.setApi("api/notebook/entry/output")
					.addParam("entryId", getNotebookEntry().getId().getId())
					.run()
					.getResponse()
					.getAsString("log");
				
				if (notEmpty(pyOut)) {
					html
						.p(class_("beans-plugin-python-output p-2 pl-4"))
							.content(notEmpty(pyOut) ? pyOut.replaceAll("\n", "<br/>") : "", false);
				} else {
					html
						.render(new SpinnerLabel("Running Python..."));
				}
							
				// rendering files
				renderFiles(html);
				
				html
					._div();
			}
		};
	}
	
	@Override
	protected Button getMenu() throws IOException {
		return null;
	}
	
	private HtmlCanvas renderFiles(HtmlCanvas html) throws IOException {
		for (Meta meta : getNotebookEntry().getMeta()) {
			if (meta.getName().startsWith("file-name-")) {
				if (meta.getAsString().endsWith(".pdf")) {
					html
						.object(data("/view/notebook/entry/pdf/" 
									+ "?entryId=" + getNotebookEntry().getId() 
									+ "&metaName=" + meta.getName())
								.type("application/pdf")
								.class_("beans-gnuplot-output beans-gnuplot-pdf")
								.width("500px")
								.height("375px"))
						._object();
				} else if (meta.getAsString().endsWith(".png")) {
					html
						.img(src("/view/notebook/entry/download/" 
								+ "?entryId=" + getNotebookEntry().getId() 
								+ "&metaName=" + meta.getName())
								.class_("beans-gnuplot-output beans-gnuplot-image"));
				} else {
					html
						.a(href("/view/notebook/entry/download/" 
								+ "?entryId=" + getNotebookEntry().getId() 
								+ "&metaName=" + meta.getName())
								.target("_blank")
								.class_("btn btn-oval btn-secondary wrepi-link beans-gnuplot-output beans-gnuplot-file"))
							.content(new FileExt(meta.getAsString()).getFilenameOnly());
				}
			}
		}
		return html;
	}

	@Override
	protected Renderable getEditor() {
		return new Renderable() {
			@Override
			public void renderOn(HtmlCanvas html) throws IOException {
				html
					.input(name("python-name")
							.class_("python-name form-control")
							.type("text")
							.value(getNotebookEntry().getName())
							.add("placeholder", "Python script title"))
					
					.input(type("hidden")
							.name(getId() + "-python-script")
							.id(getId() + "-python-script")
							.value(" "))
							
					.textarea(id(getId() + "-python"))
						.content(getNotebookEntry().getMetaAsString("code"))
						
					.write(new OpJs("window.myCodeMirror" + getId() + " = CodeMirror.fromTextArea(document.getElementById(\"" + getId() + "-python\"), "
							+ "{ "
							+ "	mode:  \"text/x-python\", "
							+ "	indentUnit: 4,"
							+ "	lineNumbers: true "
							+ "});").toStringAdhock(), false)
					;
			}
		};
	}

	@Override
	protected OpList onBeforePlay() {
		OpList ops = new OpList();
		
		ops.add(new OpJs("document.getElementById(\"" + getId() + "-python-script\").value = "
				+ "window.myCodeMirror" + getId() + ".getValue(); "));
		
		return ops;
	}

	@Override
	protected OpList opOnPlay(SecurityContext sc, Params params) throws IOException {
		try {
			String name = params.getString("python-name");
			final String script = params.getString(getId() + "-python-script");
			final String entryId = params.getString("entryId");
			
			if (nullOrEmpty(name))
				name = "no name";
			
			ApiCallCommand call = (ApiCallCommand) new ApiCallCommand(this)
				.setApi("api/notebook/entry/")
				.addParam("entryId", entryId)
				.addParam("name", name)
				.addParam("code", script)
				.run();
			
			call = (ApiCallCommand) new ApiCallCommand(this)
				.setApi("api/notebook/entry/start")
				.addParam("entryId", entryId)
				.run();
			
			return new OpList()
				.add(new OpSet("#" + getId() + " .beans-plugin-python-view", "Running python..."))
				.add(new OpComponentTimer(this, "onRefresh", 1000, new Params().add("entryId", getNotebookEntry().getId().getId())))
				;
		} catch (Exception e) {
			throw new IOException("Cannot start Python script: " + e.getMessage());
		}
	}

	@Override
	protected OpList opOnEdit(SecurityContext sc, Params params)
			throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected OpList opOnStop(SecurityContext sc, Params params)
			throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected OpList opOnRemove(SecurityContext sc, Params params) throws IOException {
		try {
			new ApiCallCommand(this)
				.setApi("api/notebook/entry/remove")
				.setApiCallType(APICallType.DELETE)
				.addParam("entryId", params.getString("entryId"))
				.run();
		} catch (IOException e) {
			LibsLogger.error(PlotPanel.class, "Cannot remove Notebook Entry", e);
		}
		
		return new OpList();
	}
	
	protected HtmlCanvas getStatusBadge(Notification qs, HtmlCanvas html) throws IOException {
		if (qs == null) {
			
			html
				.render(new BadgeInfo("Not executed"));
			
		} else if (qs.getNotificationType() == NotificationType.INFO) {
			
			html
				.render(new BadgeInfo(qs.getTitle()));
			
		} else if (qs.getNotificationType() == NotificationType.ERROR) {
			
			html
				.render(new BadgeError("Failed: " + qs.getDescriptionNotNull()));
			
		} else if (qs.getNotificationType() == NotificationType.OK) {
			
			html
				.render(new BadgeSuccess("Done"));
			
		} else {
			LibsLogger.error(QueryCheckpoints.class, "Unimplemented case for " + qs.getNotificationType());
		}
		return html;
	}

	@Override
	protected OpList onRefresh(SecurityContext sc, Params params) throws IOException {
		final Notification qs = new ApiCallCommand(this)
			.setApi(ApiCallCommand.API_NOTIFICATION_GET)
			.addParam("id", getNotebookEntry().getId().getId())
			.run()
			.getResponse()
			.getAsObject(Notification.class);
				
		return new OpList()
//			.add(new OpReplace("#" + getId() + " .notification .badge", getStatusBadge(qs, new HtmlCanvas()).toHtml()))
//			.add(new OpSet("#" + getId() + " .notification .label", getNotebookEntry().getName()))
//			.add(new OpBeansInfo("onRefresh"))
			.add(qs != null && qs.isDone() ? 
					new OpReplace("#" + getId() + " .beans-plugin-python-view", 
							new HtmlCanvas()
								.render(getView())
							.toHtml()) : null)
			.add(qs != null && !qs.isDone() ? new OpComponentTimer(this, 
					"onRefresh", 
					1000, 
					new Params()
						.add("entryId", getNotebookEntry().getId().getId())) : null)
			;
	}
}
