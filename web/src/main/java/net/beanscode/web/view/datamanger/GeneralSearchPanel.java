package net.beanscode.web.view.datamanger;

import static net.beanscode.cli.BeansCliCommand.API_NOTEBOOK_QUERY;
import static net.hypki.libs5.utils.string.StringUtilities.nullOrEmpty;
import static org.rendersnake.HtmlAttributesFactory.class_;
import static org.rendersnake.HtmlAttributesFactory.id;
import static org.rendersnake.HtmlAttributesFactory.name;

import java.io.IOException;

import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.cli.datasets.DatasetSearchResults;
import net.beanscode.cli.datasets.Table;
import net.beanscode.cli.datasets.TableSearchResults;
import net.beanscode.cli.notebooks.Notebook;
import net.beanscode.cli.notebooks.NotebookSearchResults;
import net.beanscode.pojo.dataset.Dataset;
import net.beanscode.web.view.ui.BeansComponent;
import net.hypki.libs5.pjf.components.ComboComponent;
import net.hypki.libs5.pjf.components.PagerRenderable;
import net.hypki.libs5.pjf.components.buttons.Button;
import net.hypki.libs5.pjf.components.buttons.ButtonGroup;
import net.hypki.libs5.pjf.components.buttons.ButtonSize;
import net.hypki.libs5.pjf.components.ops.OpComponentClick;
import net.hypki.libs5.pjf.components.ops.OpComponentTimer;
import net.hypki.libs5.pjf.op.Op;
import net.hypki.libs5.pjf.op.OpHide;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.pjf.op.OpSet;
import net.hypki.libs5.pjf.op.OpShow;

import org.rendersnake.HtmlCanvas;

import com.google.gson.annotations.Expose;

public class GeneralSearchPanel extends BeansComponent {
	
	private static final int PER_PAGE = 20;
	
	private static final String NO_OBJECTS_MSG = "<i>No datasets, tables or notebooks selected yet..</i>";
	
	@Expose
	private boolean optionsVisible = true;
	
	@Expose
	private boolean showListOnEmptyFilters = true;
	
	@Expose
	private OpList onChangeType = null;
	
	public GeneralSearchPanel() {
		
	}

	public GeneralSearchPanel(BeansComponent parent) {
		super(parent);
	}
	
	@Override
	public void renderOn(HtmlCanvas html) throws IOException {
		html
			.div(id(getId())
					.class_("generalsearch"))
				.render(new ComboComponent("generalsearch-change-type")
						.setValues("Datasets", "Tables", "Notebooks")
						.setSelectedValue("Tables")
						.addOp(getOnChangeType())
						.addOp("Datasets", new OpHide("#" + getId() + " .generalsearch-change-filter"))
						.addOp("Datasets", new OpHide("#" + getId() + " .generalsearch-change-operation"))
						.addOp("Datasets", new OpShow("#" + getId() + " .generalsearch-change-datasets"))
						.addOp("Datasets", new OpShow("#" + getId() + " .generalsearch-change-operation-datasets"))
						.addOp("Tables", new OpHide("#" + getId() + " .generalsearch-change-filter"))
						.addOp("Tables", new OpHide("#" + getId() + " .generalsearch-change-operation"))
						.addOp("Tables", new OpShow("#" + getId() + " .generalsearch-change-datasets"))
						.addOp("Tables", new OpShow("#" + getId() + " .generalsearch-change-tables"))
						.addOp("Tables", new OpShow("#" + getId() + " .generalsearch-change-operation-tables"))
						.addOp("Notebooks", new OpHide("#" + getId() + " .generalsearch-change-filter"))
						.addOp("Notebooks", new OpHide("#" + getId() + " .generalsearch-change-operation"))
						.addOp("Notebooks", new OpShow("#" + getId() + " .generalsearch-change-notebooks"))
						.addOp("Notebooks", new OpShow("#" + getId() + " .generalsearch-change-operation-notebooks"))
						)
				.input(name("generalsearch-datasets")
						.class_("generalsearch-change-filter generalsearch-change-datasets")
						.type("text")
						.add("placeholder", "datasets filter...")
						.onKeyup(new OpComponentTimer(this, "refresh", 300).toStringOnclick()))
				.input(name("generalsearch-tables")
						.class_("generalsearch-change-filter generalsearch-change-tables")
						.type("text")
	//					.style("display: none;")
						.add("placeholder", "tables filter...")
						.onKeyup(new OpComponentTimer(this, "refresh", 300).toStringOnclick()))
				.input(name("generalsearch-notebooks")
						.class_("generalsearch-change-filter generalsearch-change-notebooks")
						.type("text")
						.style("display: none;")
						.add("placeholder", "notebooks filter...")
						.onKeyup(new OpComponentTimer(this, "refresh", 300).toStringOnclick()))
				.render(new ButtonGroup()
						.setButtonSize(ButtonSize.NORMAL)
						.addButton(new Button()
								.setGlyphicon("refresh")
								.add(new OpComponentClick(GeneralSearchPanel.this, "refresh"))))
				.div(class_("generalsearch-found-objects"))
					.i()
						.content(NO_OBJECTS_MSG, false)
				._div()
			._div()
			;
	}
	
	@Override
	public OpList getOpList() {
		return super.getOpList()
				.add(new OpComponentTimer(this, "refresh", 300));
	}
	
	private OpList refresh() throws IOException {
		final String searchType = getParams().getString("generalsearch-change-type");
		final String searchDs = getParams().getString("generalsearch-datasets");
		final String searchTb = getParams().getString("generalsearch-tables");
		final String searchNote = getParams().getString("generalsearch-notebooks");
		final int page = getParams().getInteger("page", 0);
		
		int maxHits = 0;
		
		HtmlCanvas html = new HtmlCanvas();
		html.table(class_("results"));
		if (searchType.equalsIgnoreCase("Datasets")) {
			if (isShowListOnEmptyFilters() == false && nullOrEmpty(searchDs))
				return new OpList()
						.add(new OpSet("#" + getId() + " .generalsearch-found-objects", NO_OBJECTS_MSG));
			maxHits += appendDatasets(html, searchDs, page).getMaxHits();
		} else if (searchType.equalsIgnoreCase("Tables")) {
			if (isShowListOnEmptyFilters() == false && nullOrEmpty(searchDs) && nullOrEmpty(searchTb))
				return new OpList()
						.add(new OpSet("#" + getId() + " .generalsearch-found-objects", NO_OBJECTS_MSG));
			maxHits = appendTables(html, searchDs, searchTb, page).getMaxHits();
		} else if (searchType.equalsIgnoreCase("Notebooks")) {
			if (isShowListOnEmptyFilters() == false && nullOrEmpty(searchNote))
				return new OpList()
						.add(new OpSet("#" + getId() + " .generalsearch-found-objects", NO_OBJECTS_MSG));
			maxHits = appendNotebooks(html, searchNote, page).getMaxHits();
		}
		html
			._table()
			.render(new PagerRenderable(this, "refresh", page, PER_PAGE, maxHits, ButtonSize.NORMAL));
		
		return new OpList()
			.add(new OpSet("#" + getId() + " .generalsearch-found-objects", html.toHtml()))
//			.add(new OpComponentTimer(this, "refresh", 300))
			;
	}
	
	private DatasetSearchResults appendDatasets(HtmlCanvas html, String searchDs, int page) throws IOException {
		DatasetSearchResults datasets = new ApiCallCommand(getSessionBean())
									.setApi("api/dataset/query")
									.addParam("query", searchDs)
									.addParam("from", page * PER_PAGE)
									.addParam("size", PER_PAGE)
									.run()
									.getResponse()
									.getAsObject(DatasetSearchResults.class);
		
		html
			.tr()
				.th()
					.content("Dataset")
				.if_(isOptionsVisible())
					.th()
						.content("Options")
				._if()
			._tr();
		for (Dataset ds : datasets.getDatasets()) {
			html
				.tr(class_("notfirst"))
					.td()
						.content(ds.getName())
					.if_(isOptionsVisible())
						.td()
							.render(new ButtonGroup()
									.addButton(new Button())
									)
						._td()
					._if()
				._tr();
		}
		return datasets;
	}
	
	private TableSearchResults appendTables(HtmlCanvas html, String searchDs, String searchTb, int page) throws IOException {
		TableSearchResults tables = new ApiCallCommand(this)
									.setApi("api/table/search")
									.addParam("datasetQuery", searchDs)
									.addParam("tableQuery", searchTb)
									.addParam("from", page * PER_PAGE)
									.addParam("size", PER_PAGE)
									.run()
									.getResponse()
									.getAsObject(TableSearchResults.class);
		
		html
			.tr()
				.th()
					.content("Dataset")
				.th()
					.content("Table")
				.if_(isOptionsVisible())
					.th()
						.content("Options")
				._if()
			._tr();
		for (Table table : tables.getTables()) {
			html
				.tr(class_("notfirst"))
					.td()
						.content(table.getDataset(getSessionBean()).getName() + " (" + table.getDatasetId().getId() + ")")
					.td()
						.content(table.getName())
					.if_(isOptionsVisible())
						.td()
							.render(new ButtonGroup()
									.addButton(new Button())
									)
						._td()
					._if()
				._tr();
		}
		return tables;
	}
	
	private NotebookSearchResults appendNotebooks(HtmlCanvas html, String searchNote, int page) throws IOException {
		NotebookSearchResults notebooks = new ApiCallCommand(this)
											.setApi(API_NOTEBOOK_QUERY)
											.addParam("query", searchNote)
											.addParam("from", page * PER_PAGE)
											.addParam("size", PER_PAGE)
											.run()
											.getResponse()
											.getAsObject(NotebookSearchResults.class);
		
		html
			.tr()
				.th()
					.content("Notebook")
				.if_(isOptionsVisible())
					.th()
						.content("Options")
				._if()
			._tr();
		for (Notebook notebook : notebooks.getNotebooks()) {
			html
				.tr(class_("notfirst"))
					.td()
						.content(notebook.getName())
					.if_(isOptionsVisible())
						.td()
							.render(new ButtonGroup()
									.addButton(new Button())
									)
						._td()
					._if()
				._tr();
		}
		return notebooks;
	}

	public boolean isOptionsVisible() {
		return optionsVisible;
	}

	public GeneralSearchPanel setOptionsVisible(boolean optionsVisible) {
		this.optionsVisible = optionsVisible;
		return this;
	}

	public boolean isShowListOnEmptyFilters() {
		return showListOnEmptyFilters;
	}

	public GeneralSearchPanel setShowListOnEmptyFilters(boolean showListOnEmptyFilters) {
		this.showListOnEmptyFilters = showListOnEmptyFilters;
		return this;
	}

	private OpList getOnChangeType() {
		if (onChangeType == null)
			onChangeType = new OpList();
		return onChangeType;
	}

	private void setOnChangeType(OpList onChangeType) {
		this.onChangeType = onChangeType;
	}

	public GeneralSearchPanel addOpOnChangeType(Op op) {
		getOnChangeType().add(op);
		return this;
	}
}
