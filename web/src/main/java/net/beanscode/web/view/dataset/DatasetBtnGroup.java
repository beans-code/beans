package net.beanscode.web.view.dataset;

import static net.hypki.libs5.pjf.OpsFactory.opsCall;
import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;
import static net.hypki.libs5.utils.utils.AssertUtils.assertTrue;
import static org.rendersnake.HtmlAttributesFactory.class_;
import static org.rendersnake.HtmlAttributesFactory.id;
import static org.rendersnake.HtmlAttributesFactory.type;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.SecurityContext;

import net.beanscode.cli.BeansCliCommand;
import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.cli.datasets.Table;
import net.beanscode.pojo.dataset.Dataset;
import net.beanscode.web.view.ui.OpComponentDialogOpen;
import net.beanscode.web.view.ui.modals.OpModalQuestion;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.pjf.components.Component;
import net.hypki.libs5.pjf.components.buttons.ButtonSize;
import net.hypki.libs5.pjf.components.ops.OpComponentClick;
import net.hypki.libs5.pjf.op.OpAfter;
import net.hypki.libs5.pjf.op.OpAppend;
import net.hypki.libs5.pjf.op.OpDialogClose;
import net.hypki.libs5.pjf.op.OpJs;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.pjf.op.OpRemove;
import net.hypki.libs5.pjf.op.OpSet;
import net.hypki.libs5.pjf.op.OpWarn;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.utils.url.Params;
import net.hypki.libs5.utils.utils.AssertUtils;

import org.rendersnake.HtmlCanvas;

public class DatasetBtnGroup extends Component {

	private ButtonSize buttonSize = ButtonSize.EXTRA_SMALL;
	
	private Dataset dataset = null;
	
	private boolean isInTable = false;
	
	public DatasetBtnGroup() {
		
	}
	
	public DatasetBtnGroup(Dataset dataset, ButtonSize buttonSize, boolean isInTable) {
		setDataset(dataset);
		setButtonSize(buttonSize);
		setInTable(isInTable);
	}
	
	
	public OpList createTable(SecurityContext sc, Params params) throws IOException, ValidationException {
		String datasetId = params.getString("datasetId");
		
		assertTrue(datasetId != null, "Dataset ID is not specified");
		
		List<String> datasetIds = new ArrayList<>();
		datasetIds.add(datasetId);
		
		List<String> names = new ArrayList<>();
		names.add("New table");//BeansConst.DEFAULT_TABLE_NAME);
		
		List<Table> table = new ApiCallCommand(getSessionBean(sc))
								.setApi("api/table/create")
								.addParam("datasetId", datasetIds)
								.addParam("name", names)
								.run()
								.getResponse()
								.getAsList("tables", Table.class);
		
//		Table table = fromJson(new TableRest().create(sc, datasetIds, names, null), Table.class);
				
		assertTrue(table != null, "Table was not found");
		
		TableBetaPanel tablePanel = new TableBetaPanel(this, table.get(0));
		
		return new OpList()
			.add(new OpAppend("#dataset-tables .tables", tablePanel.toHtml()))
			.add(new OpJs(TableBetaPanel.jsCodeNewDropzone(tablePanel.getId(), table.get(0))));
//		addToResponse(new OpJs("new Dropzone('#dropzone-" + tablePanel.getId() + "', " +
//				"{url: '" + UploadRest.UPLOAD_URL + "?datasetId=" + table.getDatasetId() + "&tableId=" + table.getId() + "', addRemoveLinks: true, maxFilesize: 10000, maxFiles: 1, clickable: true})"
//				+ ".on('success', function(file, response) { pjfOps(response); });"));
			
	}
	
	public OpList remove(SecurityContext sc, Params params) throws IOException, ValidationException {
		Object dId = params.get("datasetId");
		boolean inTable = getParams().getBoolean("isInTable", false);
		
		if (StringUtilities.nullOrEmpty(dId)) {
			return new OpList()
					.add(new OpWarn("Warning", "No datasets were selected"));
		}
		
		List<String> datasetIds = new ArrayList<>();
		
		if (dId instanceof String)
			datasetIds.add(dId.toString());
		else if (dId instanceof List) {
			for (Object o : ((List) dId)) {
				datasetIds.add(o.toString());
			}
		}
		
//		new DatasetRemove(getSessionBean()).run(datasetIds);
//		new DatasetRest().delete(sc, datasetIds);
		ApiCallCommand api = new ApiCallCommand(this.getSessionBean())
			.setApi("api/dataset/delete");
			
		for (String dsid : datasetIds)
			api.addParam("datasetId", dsid);
			
		api
			.run();
		
		OpList opList = new OpList();
		
		// removing dataset from the list of datasets
		for (String dsId : datasetIds)
			opList.add(new OpRemove("#tr-" + dsId));
		
		// if the dataset is not in the table then show Dataset list again
		if (!inTable)
			opList.add(new OpComponentClick(DatasetsBetaContent.class, UUID.random(), "onShow"));
		
		return opList;
	}
	
	public OpList clear(SecurityContext sc) throws IOException, ValidationException {
		final String dId = getParams().getString("datasetId");
		final boolean inTable = getParams().getBoolean("isInTable", false);
		
		AssertUtils.assertTrue(dId != null, "No datasets were selected");
		
		List<String> datasetIds = new ArrayList<>();
		
//		if (dId instanceof String)
//			datasetIds.add(dId.toString());
//		else if (dId instanceof List) {
//			for (Object o : ((List) dId)) {
//				datasetIds.add(o.toString());
//			}
//		}
		
//		new DatasetClear(getSessionBean()).run(datasetIds);
//		new DatasetRest().clear(sc, datasetIds);
		new ApiCallCommand(this.getSessionBean())
			.setApi(BeansCliCommand.API_DATASET_CLEAR)
			.addParam("id", dId)
			.run();
		
		OpList opList = new OpList();
				
		// clear all tables from the dataset
		if (!inTable)
			opList.add(new OpSet("#dataset-tables .tables", ""));
		
		return opList;
	}
	
	public OpList datasetDetails(SecurityContext sc, Params params) throws IOException {
		final String datasetId = params.getString("datasetId");
		final Dataset dataset = new ApiCallCommand(getSessionBean())
									.setApi("api/dataset")
									.addParam("dataset-id", datasetId)
									.run()
									.getResponse()
									.getAsObject(Dataset.class);
		
		return new OpList()
			.add(new OpRemove("#tr-ds-detail-" + dataset.getId()))
			.add(new OpAfter("#content #main #tr-" + dataset.getId(), new DatasetDetailsRow(dataset).toHtml()));
	}
	
	public OpList editDataset(SecurityContext sc, Params params) throws IOException, ValidationException {
		String datasetId = params.getString("datasetId");
		String newName = params.getString("name");
		String newDesc = params.getString("description");
		
		final Dataset dataset = new ApiCallCommand(getSessionBean())
									.setApi("api/dataset")
									.addParam("dataset-id", datasetId)
									.run()
									.getResponse()
									.getAsObject(Dataset.class);
		
		OpList opList = new OpList();
		boolean changed = false;
		
		if (notEmpty(newName)) {
			dataset.setName(newName);
			changed = true;
			opList.add(new OpSet("#page-title", dataset.getName()));
		}
		
		if (notEmpty(newDesc)) {
			dataset.setDescription(newDesc);
			changed = true;
			opList.add(new OpSet("#dataset-description-" + dataset.getId(), dataset.getDescription()));
		}
		
		if (changed) {
			new ApiCallCommand(getSessionBean(sc))
				.setApi("api/dataset/set")
				.addParam("id", datasetId)
				.addParam("name", newName)
				.addParam("description", newDesc)
				.run();
		}
		
		return opList.add(new OpDialogClose());
	}
	
	@Override
	public void renderOn(HtmlCanvas html) throws IOException {
		html
		    .div(id(getId())
		    		.class_("btn-group")
		    		.role("group"))
		    	.if_(isInTable())
			        .button(type("button")
			        		.class_("btn btn-default " + getBootstrapSize(getButtonSize()))
			        		.onClick(opsCall(new OpComponentClick(this, "datasetDetails", new Params().add("userId", dataset.getUserId()).add("datasetId", dataset.getId())))))
			            .content("Details")
	            ._if()
		        .div(class_("btn-group").role("group"))
		            .button(type("button")
		            		.class_("btn btn-default dropdown-toggle " + getBootstrapSize(getButtonSize()))
		            		.data("toggle", "dropdown")
		            		)
		                .write("Edit ")
		                .span(class_("caret"))
		                	.content("")
		            ._button()
		            .ul(class_("dropdown-menu")
		            		.role("menu"))
		                .li()
		                    .a(class_("btn btn-default ")
		                    		.onClick(opsCall(new OpComponentDialogOpen(this, "editDataset", "Editing dataset name", new DatasetEditTitlePanel(getDataset()).toHtml(), 400, 200))))
		                        .content("Edit title")
		                ._li()
		                .li()
		                    .a(class_("btn btn-default ")
		                    		.onClick(opsCall(new OpComponentDialogOpen(this, "editDataset", "Editing dataset description", new DatasetEditDescPanel(getDataset()).toHtml(), 400, 400))))
		                        .content("Edit description")
		                ._li()
		                .li()
		                    .a(class_("btn btn-default ")
		                    		.onClick(
		                    				new OpModalQuestion("Removing datasets", 
		                    						"Do you really want to delete selected datasets? (That operation cannot be reverted)", 
		                    						new OpComponentClick(this, 
		                    								"remove",
		                    								new Params()
					    		                    			.add("datasetId", getDataset().getId().getId())
					    		                    			.add("isInTable", isInTable())))
		                    				.toStringOnclick()
		                    				))
                    			.content("Remove dataset")
                			.a(class_("btn btn-default ")
		                    		.onClick(opsCall(
		                    				new OpModalQuestion("Clearing datasets", 
		                    						"Do you really want to clear all tables from the selected datasets? (That "
		                    						+ "operation cannot be reverted)", 
		                    						new OpComponentClick(this, 
		                    								"clear",
		                    								new Params()
			            		                    			.add("datasetId", getDataset().getId().getId())
			            		                    			.add("isInTable", isInTable())
		                    								))
		                    				)
		                    		))
                    			.content("Clear dataset")
//		                    new OpAjax(DatasetView.DELETE_URL,
//		                    		RestMethod.DELETE,
//									opConfirmation("Removing datasets", String.format("Do you really want to delete dataset?<p><b>%s</b></p>  <p><b>NOTE: That operation cannot be reverted</b></p>", getDataset().getName())),
//									"datasetId", getDataset().getId().getId(),
//									"isInTable", isInTable()
//									))))
		                ._li()
		            ._ul()
		        ._div()
		        .if_(!isInTable())
			        .button(type("button")
			        		.class_("btn btn-default " + getBootstrapSize(getButtonSize()))
			        		.onClick(new OpComponentClick(this, "createTable", new Params().add("datasetId", dataset.getId())).toStringOnclick()))
			            .content("Add table")
	            ._if()
		    ._div()
		;
	}

	private String getBootstrapSize(ButtonSize size) {
		switch (size) {
		case EXTRA_SMALL:
			return "btn-xs";
		case SMALL:
			return "btn-sm";
		case NORMAL:
			return "";
		default:
			return "btn-sm";
		}
	}
	
	public Dataset getDataset() {
		return dataset;
	}

	public void setDataset(Dataset dataset) {
		this.dataset = dataset;
	}

	public ButtonSize getButtonSize() {
		return buttonSize;
	}

	public void setButtonSize(ButtonSize buttonSize) {
		this.buttonSize = buttonSize;
	}

	public boolean isInTable() {
		return isInTable;
	}

	public void setInTable(boolean isInTable) {
		this.isInTable = isInTable;
	}
}
