package net.beanscode.web.view.ui.modals;

public enum InputType {
	STRING,
	TEXT,
	BOOLEAN,
	HIDDEN,
	PASSWORD
}
