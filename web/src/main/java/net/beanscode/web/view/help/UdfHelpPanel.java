package net.beanscode.web.view.help;

import static org.rendersnake.HtmlAttributesFactory.id;

import java.io.IOException;

import org.rendersnake.HtmlAttributesFactory;
import org.rendersnake.HtmlCanvas;

import net.beanscode.web.view.ui.BeansComponent;
import net.beanscode.web.view.ui.CardPanel;

public class UdfHelpPanel extends CardPanel {

	public UdfHelpPanel() {
		
	}
	
	@Override
	protected void renderTitle(HtmlCanvas html) throws IOException {
		html
			.h4()
				.content("User Defined Fucntions");
	}

	@Override
	protected void renderContent(HtmlCanvas html) throws IOException {
		html
			.div(id(getId()))
				.h3()
					.content("Usef Defined Functions from plugins")
			
				.render(new UdfHelpTable())
			._div();
	}

	@Override
	protected void renderFooter(HtmlCanvas html) throws IOException {
		
	}
}
