package net.beanscode.web.view.settings;

import java.io.IOException;

import org.rendersnake.HtmlCanvas;

import net.beanscode.web.view.ui.BeansComponent;
import net.beanscode.web.view.ui.CardPanel;


public class AutoUpdatesCard extends CardPanel {

	public AutoUpdatesCard() {
		
	}
	
	public AutoUpdatesCard(BeansComponent parent) {
		super(parent);
	}

	@Override
	protected void renderTitle(HtmlCanvas html) throws IOException {
		html
			.span()
				.content("Auto-updates of Notebooks");
	}

	@Override
	protected void renderContent(HtmlCanvas html) throws IOException {
		html
			.render(new AutoUpdatesPanel(this));
	}

	@Override
	protected void renderFooter(HtmlCanvas html) throws IOException {
		
	}
}
