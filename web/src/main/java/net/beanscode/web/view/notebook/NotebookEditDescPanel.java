package net.beanscode.web.view.notebook;

import static org.rendersnake.HtmlAttributesFactory.class_;
import static org.rendersnake.HtmlAttributesFactory.name;
import static org.rendersnake.HtmlAttributesFactory.type;

import java.io.IOException;

import net.beanscode.cli.notebooks.Notebook;
import net.hypki.libs5.pjf.components.Component;

import org.rendersnake.HtmlCanvas;

public class NotebookEditDescPanel extends Component {
	
	private Notebook notebook = null;
	
	public NotebookEditDescPanel() {
		
	}
	
	public NotebookEditDescPanel(Notebook notebook) {
		setNotebook(notebook);
	}

	@Override
	public void renderOn(HtmlCanvas html) throws IOException {
		html
			.form(class_("notebook-edit-description"))
				.span()
					.content("Notebook description:")
				.br()
				.input(type("hidden")
						.name("userId")
						.value(getNotebook().getUserId().getId()))
				.input(type("hidden")
						.name("notebookId")
						.value(getNotebook().getId().getId()))
				.textarea(name("description"))
					.content(getNotebook().getDescription())
//				._textarea()
			._form()
			;
	}

	public Notebook getNotebook() {
		return notebook;
	}

	public void setNotebook(Notebook notebook) {
		this.notebook = notebook;
	}
}
