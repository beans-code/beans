package net.beanscode.web.view.notification;

import static org.rendersnake.HtmlAttributesFactory.id;

import java.io.IOException;

import javax.ws.rs.core.SecurityContext;

import net.beanscode.web.view.ui.BeansComponent;
import net.hypki.libs5.utils.url.Params;

import org.rendersnake.HtmlCanvas;

import com.google.gson.annotations.Expose;

public class NotificationsPanel extends BeansComponent {
	
	
	@Expose
	private boolean extended = false;
	
	public NotificationsPanel() {
	
	}

	public NotificationsPanel(BeansComponent parent) {
		super(parent);
	}
	
	public NotificationsPanel(SecurityContext securityContext, Params params) {
		super(securityContext, params);
	}
	
	@Override
	public void renderOn(HtmlCanvas html) throws IOException {
		html
			.div(id(getId())
					.class_("notifications notifications-list"))
				
				.render(new NotificationsBeansTableComponent(this)
						.setPostponeReload(true))
			._div()
			;
	}
		
	public boolean isExtended() {
		return extended;
	}

	public NotificationsPanel setExtended(boolean extended) {
		this.extended = extended;
		return this;
	}
	
	
}
