package net.beanscode.web.view.dataset;

import java.io.IOException;

import javax.annotation.security.PermitAll;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.UriInfo;

import net.beanscode.web.BeansPage;
import net.beanscode.web.BetaPage;
import net.beanscode.web.view.notebook.NotebookContent;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.pjf.components.ops.OpComponentClick;
import net.hypki.libs5.utils.url.Params;

@Path("/" + DatasetPage.PATH)
public class DatasetPage extends BeansPage {
	
	public final static String PATH = "dataset";

	public DatasetPage() {
		
	}
	
	@GET
	@Path("{datasetId: [a-zA-Z0-9]+}")
	@PermitAll
	@Produces(MediaType.TEXT_HTML)
    public String init(@Context SecurityContext sc, 
    		@Context UriInfo uriInfo,
    		@PathParam("datasetId") String datasetId) throws IOException {
		String tmp = new BetaPage().createPage(sc);
		tmp = tmp.replace("<!--content-->", new OpComponentClick(DatasetBetaContent.class, 
												UUID.random().getId(), 
												"onShow", 
												new Params().add("datasetId", datasetId))
											.toStringAdhock());
		return tmp;
    }
}
