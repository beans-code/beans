package net.beanscode.web.view.summary;

import java.io.IOException;

import net.beanscode.web.beta.BetaContent;
import net.beanscode.web.view.ui.BeansComponent;
import net.beanscode.web.view.ui.BetaButtonGroup;

import org.rendersnake.HtmlCanvas;

public class DiagnosticsContent extends BetaContent {
	
	public DiagnosticsContent() {
		
	}

	public DiagnosticsContent(BeansComponent parent) {
		super(parent);
	}
	
	@Override
	protected void renderHeader(HtmlCanvas html) throws IOException {
		html
			.h1()
				.content("Diagnostics");
	}
	
	protected void renderContent(HtmlCanvas html) throws IOException {
		html
			.render(new VitalChecksPanel(this));
	}

	@Override
	protected BetaButtonGroup getMenu() throws IOException {
		return null;
	}

	@Override
	protected String getMenuName() throws IOException {
		return null;
	}
	
}
