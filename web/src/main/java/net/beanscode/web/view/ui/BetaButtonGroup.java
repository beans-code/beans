package net.beanscode.web.view.ui;

import static org.rendersnake.HtmlAttributesFactory.class_;
import static org.rendersnake.HtmlAttributesFactory.name;
import static org.rendersnake.HtmlAttributesFactory.type;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import net.hypki.libs5.pjf.components.buttons.Button;
import net.hypki.libs5.pjf.components.buttons.ButtonSize;
import net.hypki.libs5.pjf.components.buttons.InputButton;

import org.rendersnake.HtmlAttributesFactory;
import org.rendersnake.HtmlCanvas;
import org.rendersnake.Renderable;

import com.google.gson.annotations.Expose;

public class BetaButtonGroup extends BeansComponent {
	
	@Expose
	private boolean asList = false; 

	private List<Button> buttons = null;
	
	private boolean visible = true;
	
	public BetaButtonGroup() {
		
	}
	
	@Override
	public void renderOn(HtmlCanvas html) throws IOException {
		html
			.render(new Renderable() {
				@Override
				public void renderOn(HtmlCanvas html) throws IOException {
//					if (!isAsList()) {
						html
							.div(class_("btn-group beans-btn-group")
						    		.add("style", !isVisible() ? "display: none;" : ""));
//					}
					
					for (Button button : getButtons()) {
						
//						if (isAsList()) {
//							// one btn-group per one group
//							html
//								.div(class_("btn-group beans-btn-group"));
//						}
					
						if (button.isHierarchical()) {
							html
								.div(class_("btn-group"))
									.button(class_("btn btn-default dropdown-toggle dropdown-icon " + button.getActualHtmlClasses())
											.data("toggle", "dropdown")
											.add("aria-expanded", "false")
											.title(button.getTooltipNotNull()))
										.if_(button.isIconDefined())
											.i(class_(button.getAwesomeicon()))
											._i()
											.write(" ")
										._if()
										.write(button.isNameDefined() ? button.getName() + " " : " ")
//										.i(class_("fa fa-chevron-down"))
//										._i()
									._button()
									.div(class_("dropdown-menu"))
										.render(new Renderable() {
											@Override
											public void renderOn(HtmlCanvas html) throws IOException {
												for (Button button2 : button.getButtons()) {
													if (button2.isSeparator()) {
														html
															.hr(class_("button-hr"))
															;
													} else if (button2.isVisible()) {
														html
															.a(class_("dropdown-item")
//																	.href("#")
																	.onClick(button2.getOpList().toStringOnclick()))
																.if_(button2.isIconDefined())
																	.i(class_(button2.getAwesomeicon()))
																	._i()
																	.write(" ")
																._if()
																.write(button2.isNameDefined() ? button2.getName() + " " : " ")
															._a();
													} else {
														html
															.a(class_("dropdown-item")
//																	.href("#")
																	.add("style", "display: none;")
																	.onClick(button2.getOpList().toStringOnclick()))
																.if_(button2.isIconDefined())
																	.i(class_(button2.getAwesomeicon()))
																	._i()
																	.write(" ")
																._if()
																.write(button2.isNameDefined() ? button2.getName() + " " : " ")
															._a();
													}
												}
											}
										})
									._div()
								._div();
						} else {
							if (button instanceof InputButton) {
								InputButton inp = (InputButton) button;
								html
									.input(type("text")
											.title(button.getTooltipNotNull())
											.add("style", button.isVisible() ? "" : "display: none;")
											.class_(button.getActualHtmlClasses())
											.name(inp.getName()));
							} else if (button.isSeparator()) {
								html
//									.button(type("separator"))
									.hr(class_("button-hr"))
									;
							} else {
								html
									.button(type("button")
											.title(button.getTooltipNotNull())
											.add("style", button.isVisible() ? "" : "display: none;")
											.class_(button.getActualHtmlClasses())
											.onClick(button.getOpList().toStringOnclick()))
										.if_(button.isIconDefined())
											.i(class_(button.getAwesomeicon()))
											._i()
										._if()
										.if_(button.isNameDefined())
											.write(button.getName())
										._if()
									._button()
									;
							}
						}
					
//						if (isAsList()) {
//							html
//								._div();
//						}
					}
					
//					if (!isAsList()) {
						html
							._div();
//					}
				}
			});
	}
	
	public BetaButtonGroup add(Button button) {
		button
			.setButtonSize(ButtonSize.NORMAL);
		
		getButtons().add(button);
		return this;
	}

	public List<Button> getButtons() {
		if (buttons == null)
			buttons = new ArrayList<Button>();
		return buttons;
	}

	private void setButtons(List<Button> buttons) {
		this.buttons = buttons;
	}

	public boolean isAsList() {
		return asList;
	}

	public BetaButtonGroup setAsList(boolean asList) {
		this.asList = asList;
		return this;
	}

	public boolean isVisible() {
		return visible;
	}

	public BetaButtonGroup setVisible(boolean visible) {
		this.visible = visible;
		return this;
	}
}
