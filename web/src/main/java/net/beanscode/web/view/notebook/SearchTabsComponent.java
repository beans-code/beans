package net.beanscode.web.view.notebook;

import java.io.IOException;

import org.rendersnake.HtmlCanvas;
import org.rendersnake.Renderable;

import net.beanscode.web.view.dataset.TablesBetaTableComponent;
import net.beanscode.web.view.help.UdfHelpTable;
import net.beanscode.web.view.ui.TabsBetaComponent;
import net.beanscode.web.view.ui.modals.OpBeansInfo;
import net.hypki.libs5.pjf.components.Component;
import net.hypki.libs5.pjf.op.OpList;

public class SearchTabsComponent extends TabsBetaComponent {
	
	public SearchTabsComponent() {
		
	}

	public SearchTabsComponent(Component parent) {
		super(parent);
	}

	@Override
	public int getTabsCount() {
		return 4;
	}

	@Override
	public String getTabName(int tabIndex) {
		if (tabIndex == 0)
			return "Notebooks";
		else if (tabIndex == 1)
			return "Datasets/tables";
		else if (tabIndex == 2)
			return "Entries";
		else if (tabIndex == 3)
			return "UDFs";
		return "";
	}

	@Override
	public Renderable getTabHtml(int tabIndex) throws IOException {
		if (tabIndex == 0)
			return new NotebooksTableComponent(this);
		else if (tabIndex == 1)
			return new TablesBetaTableComponent(this);
		else if (tabIndex == 2)
			return new CloneEntryPanel(this, null)
				.setCloneBtnVisible(false);
		else if (tabIndex == 3)
			return new UdfHelpTable(this);
		else
			return new Renderable() {
				@Override
				public void renderOn(HtmlCanvas arg0) throws IOException {
					
				}
			};
	}
}
