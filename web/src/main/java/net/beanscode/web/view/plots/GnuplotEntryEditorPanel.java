package net.beanscode.web.view.plots;

import static net.hypki.libs5.utils.string.StringUtilities.nullOrEmpty;
import static org.rendersnake.HtmlAttributesFactory.class_;
import static org.rendersnake.HtmlAttributesFactory.data;
import static org.rendersnake.HtmlAttributesFactory.href;
import static org.rendersnake.HtmlAttributesFactory.id;
import static org.rendersnake.HtmlAttributesFactory.name;
import static org.rendersnake.HtmlAttributesFactory.src;
import static org.rendersnake.HtmlAttributesFactory.type;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.ws.rs.core.SecurityContext;

import org.rendersnake.HtmlCanvas;
import org.rendersnake.Renderable;

import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.cli.datasets.Table;
import net.beanscode.cli.notebooks.GnuplotEntry;
import net.beanscode.cli.notebooks.Notebook;
import net.beanscode.cli.notebooks.NotebookGateway;
import net.beanscode.pojo.NotebookEntryStatus;
import net.beanscode.web.BeansAPI;
import net.beanscode.web.view.notebook.LogPanel;
import net.beanscode.web.view.notebook.NotebookEntryEditorPanel;
import net.hypki.libs5.cli.noninteractivecli.CLIException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.pjf.components.buttons.Button;
import net.hypki.libs5.pjf.components.ops.OpComponentClick;
import net.hypki.libs5.pjf.components.ops.OpComponentTimer;
import net.hypki.libs5.pjf.op.OpJs;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.pjf.op.OpRemove;
import net.hypki.libs5.pjf.op.OpSet;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.api.APICallType;
import net.hypki.libs5.utils.file.FileExt;
import net.hypki.libs5.utils.string.Meta;
import net.hypki.libs5.utils.string.RegexUtils;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.utils.string.Tokenizer;
import net.hypki.libs5.utils.url.Params;

public class GnuplotEntryEditorPanel extends NotebookEntryEditorPanel {

	public GnuplotEntryEditorPanel() {
		
	}

	@Override
	public Renderable getView() {
		return new Renderable() {
			
			@Override
			public void renderOn(HtmlCanvas html) throws IOException {
				html
					.span()
						.content(getNotebookEntry().getName());
				
				if (getNotebookEntryProgress().isFinished())
					renderFiles(html);
				else {
					html
						.div(class_("gnuplot-files"))
							.i()
								.content("no files yet")
						._div();
					
//					html
//						.write(new OpComponentTimer(GnuplotEntryEditorPanel.this, 
//								"onRefresh",
//								1000,
//								new Params()
//									.add("entryId", getNotebookEntryProgress().getEntryId().getId()))
//								.toStringAdhock(), 
//							false);
				}
			}
		};
	}
	
	@Override
	protected Button getMenu() throws IOException {
		return null;
	}
	
	private HtmlCanvas renderFiles(HtmlCanvas html) throws IOException {
		List<Meta> allFiles = new ArrayList<>();
		for (Meta meta : getGnuplotEntry().getMeta())
			if (meta.getName().startsWith("file-name-"))
				allFiles.add(meta);
		
		// sorting all files according to the place they appear in compiled 
		// gnuplot script
		Collections.sort(allFiles, new Comparator<Meta>() {
			@Override
			public int compare(Meta o1, Meta o2) {
				String f1 = new FileExt(o1.getAsString()).getFilenameOnly();
				String f2 = new FileExt(o2.getAsString()).getFilenameOnly();
				int i1 = getGnuplotEntry().getGnuplotCompiledScript().indexOf(f1);
				int i2 = getGnuplotEntry().getGnuplotCompiledScript().indexOf(f2);
				if (i1 >= 0 && i2 >= 0)
					return i1 > i2 ? 1 : (i1 < i2 ? -1 : 0);
				return 0;
			}
		});
		
		for (Meta meta : allFiles) {
			if (meta.getAsString().endsWith(".pdf")) {
				html
					.object(data("/view/notebook/entry/pdf/" 
								+ "?entryId=" + getNotebookEntry().getId() 
								+ "&metaName=" + meta.getName()
								+ "&t=" + System.currentTimeMillis())
							.type("application/pdf")
							.class_("beans-gnuplot-output beans-gnuplot-pdf")
							.width("500px")
							.height("375px"))
					._object();
			} else if (meta.getAsString().endsWith(".png")) {
				html
					.img(src("/view/notebook/entry/download/" 
							+ "?entryId=" + getNotebookEntry().getId() 
							+ "&metaName=" + meta.getName()
							+ "&t=" + System.currentTimeMillis())
							.class_("beans-gnuplot-output beans-gnuplot-image"));
			} else {
				html
					.a(href("/view/notebook/entry/download/" 
							+ "?entryId=" + getNotebookEntry().getId() 
							+ "&metaName=" + meta.getName()
							+ "&t=" + System.currentTimeMillis())
							.target("_blank")
							.class_("btn btn-oval btn-secondary wrepi-link beans-gnuplot-output beans-gnuplot-file"))
						.content(new FileExt(meta.getAsString()).getFilenameOnly());
			}
		}
		return html;
	}
	
	protected GnuplotEntry getGnuplotEntry() {
		return (GnuplotEntry) getNotebookEntry();
	}

	@Override
	protected Renderable getEditor() {
		return new Renderable() {
			@Override
			public void renderOn(HtmlCanvas html) throws IOException {
				html
					.input(name("gnuplot-name")
							.class_("gnuplot-name form-control")
							.type("text")
							.value(getGnuplotEntry().getName())
							.add("placeholder", "Gnuplot script title"))
					
					.input(type("hidden")
							.name(getId() + "-gnuplot-script")
							.id(getId() + "-gnuplot-script")
							.value(" "))
							
					.textarea(id(getId() + "-gnuplot"))
						.content(getGnuplotEntry().getMetaAsString("script"))
						
					.write(new OpJs("window.myCodeMirror" + getId() + " = CodeMirror.fromTextArea(document.getElementById(\"" + getId() + "-gnuplot\"), "
							+ "{ "
							+ "	mode:  \"text/x-rsrc\", "
							+ "	indentUnit: 4,"
							+ "	lineNumbers: true "
							+ "});").toStringAdhock(), false)
					
					.div(class_("will-read-from"))
						.content("")
//					.write(new OpComponentTimer(GnuplotEntryEditorPanel.this, 
//												"onUpdateInput", 
//												1000)
//							.toStringAdhock(), 
//							false)
					;
			}
		};
	}
	
	private OpList onUpdateInput() {
		return new OpList()//onBeforePlay()
				.add(new OpJs("document.getElementById(\"" + getId() + "-gnuplot-script\").value = "
				+ "window.myCodeMirror" + getId() + ".getValue(); "))
				.add(new OpComponentTimer(this, 
						"onUpdateSource", 
						100, 
						new Params()
							.add("entryId", getNotebookEntryProgress().getEntryId().getId())));
	}
	
	// TODO coopied from MetaUtils
	public static String applyMeta(final String s, final net.beanscode.cli.notebooks.Notebook n) {
		if (n == null)
			return s;
		
		List<String> groups = RegexUtils.allGroups("\\$([\\w\\d]+)", s);
		String newS = s;
		for (String group : groups) {
			Meta m = n.getMeta().get(group);
			if (m != null) {
				String newVal = m.getAsString();
				if (newVal != null)
					newS = newS.replaceAll("\\$" + group, newVal);
			}
		}
		
		newS = newS.replace("THIS_NOTEBOOK", n.getId().getId());
		
		return newS;
	}
	
	private OpList onUpdateSource() throws CLIException, IOException {
		final String script = getParams().getString(getId() + "-gnuplot-script");
		
		if (nullOrEmpty(script))
			return new OpList();
		
		StringBuilder sb = new StringBuilder();

		String line = null;
		boolean append = false;
		for (String l : StringUtilities.split(script, '\n')) {
			// check for multilines in gnuplot script
			if (l.trim().endsWith("\\")) {
				line += l.substring(0, l.length() - 2) + " ";
				append = true;
				continue;
			} else {
				if (append) {
					append = false;
					line += l;
				} else {
					line = l;
				}
			}
			
			if ((line.contains("plot ") || line.contains("splot ") || line.contains("fit "))
					&& (RegexUtils.firstGroupCaseInsensitive("(datasets[\\s]*=[\\s]*\")", 
							line.trim().toLowerCase()) != null
						|| RegexUtils.firstGroupCaseInsensitive("(tables[\\s]*=[\\s]*\")", 
							line.trim().toLowerCase()) != null)) {
				Tokenizer t = new Tokenizer(line);
				
				// parsing input data
				Notebook n = NotebookGateway.getNotebook(getSessionBean(), getNotebookEntry().getNotebookId());
				String dsQ = t.pullOut("datasets[\\s]*=[\\s]*\"([^\\\"]+)\"");
				dsQ = RegexUtils.firstGroup("\"(.*)\"", dsQ);
				dsQ = applyMeta(dsQ, n);
				
				String tbQ = t.pullOut("tables[\\s]*=[\\s]*\"([^\\\"]+)\"");
				tbQ = RegexUtils.firstGroup("\"(.*)\"", tbQ);
				tbQ = applyMeta(tbQ, n);
			
				ApiCallCommand api = new ApiCallCommand(this)
						.setApi(BeansAPI.API_TABLE_SEARCH)
						.addParam("datasetQuery", dsQ)
						.addParam("tableQuery", tbQ)
						.addParam("size", "" + 5)
						.run();
				
				long maxHits = api.getResponse().getAsLong("maxHits");
				sb.append("<b>" + dsQ + ", " + tbQ + "</b> will read from " + "<a class=\"beans-link\">" + maxHits + "</a>" + "tables: ");
				for (Table tab : api.getResponse().getAsList("tables", Table.class))
					sb.append("<a class=\"beans-link\">" + 
								tab.getName() + " (" + tab.getDataset(this.getSessionBean()).getName() + ")</a>");
				if (maxHits > 5)
					sb.append("<a class=\"beans-link\">...</a><br/>");
			}
		}
		
		return new OpList()
				.add(new OpSet("#" + getId() + " .will-read-from", 
						sb.toString()))
				.add(new OpComponentTimer(this, 
						"onUpdateInput", 
						1000, 
						new Params()
							.add("entryId", getNotebookEntryProgress().getEntryId().getId())));
	}

	@Override
	protected OpList onBeforePlay() {
		OpList ops = new OpList();
		
		ops.add(new OpJs("document.getElementById(\"" + getId() + "-gnuplot-script\").value = "
				+ "window.myCodeMirror" + getId() + ".getValue(); "));
		
		return ops;
	}
	
	@Override
	protected OpList opOnPlay(SecurityContext sc, Params params) throws IOException {
		try {
			String name = params.getString("gnuplot-name");
			final String script = params.getString(getId() + "-gnuplot-script");
			final String entryId = params.getString("entryId");
			
			if (nullOrEmpty(name))
				name = "no name";
			
			new ApiCallCommand(this)
				.setApi("api/notebook/entry/")
				.addParam("entryId", entryId)
				.addParam("name", name)
				.addParam("script", script)
				.run();
			
			new ApiCallCommand(this)
				.setApi("api/notebook/entry/start")
				.addParam("entryId", entryId)
				.run();
			
			return new OpList()
				.add(new OpRemove("#" + getId() + " .beans-gnuplot-output"))
				.add(new OpComponentClick(GnuplotEntryEditorPanel.this, 
								"onRefresh",
								new Params()
									.add("entryId", getNotebookEntryProgress().getEntryId().getId())))
//				.add(new OpHide("#" + getId() + " .pig-panel-tables"))
//				.add(new OpShow("#" + getId() + " .refresh"))
//				.add()
				;
		} catch (Exception e) {
			throw new IOException("Cannot start gnuplot script: " + e.getMessage());
		}
	}

	@Override
	protected OpList opOnEdit(SecurityContext sc, Params params) throws IOException {
		return new OpList()
				.add(new OpComponentTimer(GnuplotEntryEditorPanel.this, 
												"onUpdateInput", 
												1000,
												new Params()
													.add("entryId", getNotebookEntryProgress().getEntryId().getId())))
				;
	}

	@Override
	protected OpList opOnStop(SecurityContext sc, Params params) throws IOException {
		return null;
	}

	@Override
	protected OpList opOnRemove(SecurityContext sc, Params params) throws IOException {
		try {
			new ApiCallCommand(this)
				.setApi("api/notebook/entry/remove")
				.setApiCallType(APICallType.DELETE)
				.addParam("entryId", params.getString("entryId"))
				.run();
		} catch (IOException e) {
			LibsLogger.error(PlotPanel.class, "Cannot remove Notebook Entry", e);
		}
		
		return new OpList();
	}

	@Override
	protected OpList onRefresh(SecurityContext sc, Params params) throws IOException {
		LibsLogger.info(GnuplotEntryEditorPanel.class, "onRefresh entering");
		
		boolean finished = getNotebookEntryProgress() != null && getNotebookEntryProgress().isFinished();
		boolean running = getNotebookEntryProgress() != null && getNotebookEntryProgress().getNotebookEntryStatus() == NotebookEntryStatus.RUNNING;
		
		LibsLogger.info(GnuplotEntryEditorPanel.class, "onRefresh finished ", finished);
				
		return new OpList()
//			.add(new OpReplace("#" + getId() + " .label", getStatusBadge(qs, new HtmlCanvas()).toHtml()))
			.add(finished, new OpSet("#" + getId() + " .gnuplot-files", 
					renderFiles(new HtmlCanvas()).toHtml()))
			.add(running, new OpComponentTimer(GnuplotEntryEditorPanel.this, 
					"onRefresh", 
					1000, 
					new Params()
						.add("entryId", getNotebookEntryProgress().getEntryId().getId())))
			;
	}
}
