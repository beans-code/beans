package net.beanscode.web.view.settings;

import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.date.SimpleDate;
import net.hypki.libs5.utils.string.StringUtilities;
import net.sf.oval.constraint.NotNull;

import com.google.gson.annotations.Expose;

public class SettingValue {

	@Expose
	@NotNull
	private String name = null;
	
	@Expose
//	@NotNull
	private String vall = null;
	
	@Expose
	private String def = null;
	
	@Expose
	private String description = null;
	
	@Expose
	@NotNull
	private SettingType type = null;
	
	public SettingValue() {
		
	}
	
	public SettingValue(String name, String value) {
		setName(name);
		setValue(value);
		setType(SettingType.STRING);
	}
	
	public SettingValue(String name, boolean value) {
		setName(name);
		setValue(value);
		setType(SettingType.BOOLEAN);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Object getValue() {
		try {
			if (getType() == SettingType.BOOLEAN)
				return notEmpty(vall) ? Boolean.valueOf(vall) : null;
			else if (getType() == SettingType.FLOAT)
				return notEmpty(vall) ? Double.valueOf(vall) : null;
			else if (getType() == SettingType.INTEGER)
				return notEmpty(vall) ? 
						Integer.valueOf(vall.contains("[") ? vall.replace("[", "").replace("]", "") : vall) : null;
			else
				return vall;
		} catch (Throwable e) {
			LibsLogger.error(SettingValue.class, "Cannot get value for " + getName(), e);
			return null;
		}
	}
	
	public String getValueAsString() {
		Object v = getValue();
		if (v instanceof String)
			return (String) v;
		else
			return String.valueOf(v);
	}
	
	public SimpleDate getValueAsDate() {
		Object v = getValue();
		if (v instanceof SimpleDate)
			return (SimpleDate) v;
		else
			return SimpleDate.parse(String.valueOf(v));
	}
	
	public boolean getValueAsBoolean() {
		Object v = getValue();
		if (v instanceof Boolean)
			return (Boolean) v;
		else
			return Boolean.getBoolean(String.valueOf(v));
	}

	public void setValue(Object value) {
		this.vall = String.valueOf(value);
	}

	public String getDescription() {
		return description;
	}
	
	public boolean isDescriptionEmpty() {
		return StringUtilities.nullOrEmpty(description);
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public SettingType getType() {
		return type;
	}

	public void setType(SettingType type) {
		this.type = type;
	}

	public String getDef() {
		return def;
	}

	public void setDef(String def) {
		this.def = def;
	}
}
