package net.beanscode.web.view.ui;

import net.beanscode.web.BeansWebConst;
import net.hypki.libs5.pjf.RestMethod;
import net.hypki.libs5.pjf.components.Component;
import net.hypki.libs5.pjf.op.OpDialogOpen;
import net.hypki.libs5.utils.url.Params;

public class OpComponentDialogOpen extends OpDialogOpen {

	public OpComponentDialogOpen(Component component, String componentMethod, String title, String html) {
		super(Component.CLICK_URL,
			new Params()
				.add("componentClass", component.getClass().getName())
				.add("componentid", component.getId())
				.add("method", componentMethod),
			RestMethod.POST.toString(), 
			title, 
			html, 
			BeansWebConst.DEFAULT_DIALOG_WIDTH, 
			BeansWebConst.DEFAULT_DIALOG_HEIGHT);
	}
	
	public OpComponentDialogOpen(Component component, String componentMethod, String title, String html, int width, int height) {
		super(Component.CLICK_URL,
			new Params()
				.add("componentClass", component.getClass().getName())
				.add("componentid", component.getId())
				.add("method", componentMethod),
			RestMethod.POST.toString(), 
			title, html, width, height);
	}
	
	public OpComponentDialogOpen(Class componentClass, String componentId, String componentMethod, String title, String html, int width, int height) {
		super(Component.CLICK_URL,
			new Params()
				.add("componentClass", componentClass.getName())
				.add("componentid", componentId)
				.add("method", componentMethod),
			RestMethod.POST.toString(), 
			title, html, width, height);
	}
}
