package net.beanscode.web.view.notebook;

import static org.rendersnake.HtmlAttributesFactory.class_;
import static org.rendersnake.HtmlAttributesFactory.id;

import java.io.IOException;

import org.rendersnake.HtmlCanvas;

import com.google.gson.JsonObject;
import com.google.gson.annotations.Expose;

import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.cli.datasets.Table;
import net.beanscode.cli.notebooks.NotebookEntryGateway;
import net.beanscode.cli.notebooks.NotebookEntryHistoryGateway;
import net.beanscode.pojo.NotebookEntry;
import net.beanscode.pojo.NotebookEntryHistory;
import net.beanscode.web.view.ui.BeansComponent;
import net.beanscode.web.view.ui.Link;
import net.beanscode.web.view.ui.modals.ModalWindow;
import net.hypki.libs5.cli.noninteractivecli.CLIException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.pjf.components.buttons.Button;
import net.hypki.libs5.pjf.components.ops.OpComponentClick;
import net.hypki.libs5.pjf.op.OpError;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.pjf.op.OpSet;
import net.hypki.libs5.pjf.op.OpWarn;
import net.hypki.libs5.search.SearchResults;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.url.Params;

public class NotebookEntryHistoryPanel extends BeansComponent {

	@Expose
	private UUID entryId = null;
	
	private SearchResults<NotebookEntryHistory> versionsCache = null;

	public NotebookEntryHistoryPanel() {
		
	}
	
	public NotebookEntryHistoryPanel(BeansComponent parent, UUID entryId) {
		super(parent);
		setEntryId(entryId);
	}
	
	public NotebookEntryHistoryPanel(BeansComponent parent, String entryId) {
		super(parent);
		setEntryId(new UUID(entryId));
	}
	
	@Override
	public void renderOn(HtmlCanvas html) throws IOException {
		html
			.div(id(getId())
					.class_("history-panel"))
				.div(class_("versions"))
					.h3()
						.content("Versions");
		
		for (NotebookEntryHistory neh : getVersions())
			html
				.render(new Link(neh.getLastEdit().toStringISO(), 
						new OpComponentClick(this, "onVersion", new Params().add("entryId", neh.getId().getId()))));
		
		html
				._div()
				.div(class_("preview"))
				._div()
			._div();
	}
	
	private OpList onVersion() throws CLIException, IOException {
		try {
			String entryId = getParams().getString("entryId");
			
			NotebookEntryHistory neh = NotebookEntryHistoryGateway.getNotebookEntryHistory(getSessionBean(), entryId);
			Class<? extends NotebookEntry> t = (Class<? extends NotebookEntry>) Class.forName(neh.getType());
						
			NotebookEntryEditorPanel nePanel = NotebookEntryEditorFactory.getEditorPanel(this, t);
			nePanel.setNotebookEntry(NotebookEntryGateway.getNotebookEntry((JsonObject) JsonUtils.toJson(neh)));
//		nePanel.setParams(getParams());
			nePanel.setSessionBean(getSessionBean());
					
			return new OpList()
					.add(new OpSet("#" + getId() + " .preview", nePanel.toHtml()));
		} catch (CLIException | ClassNotFoundException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
			return new OpList()
					.add(new OpError("Error", "Cannot show this version"));
		}
	}
	
	private SearchResults<NotebookEntryHistory> getVersions() {
		if (versionsCache == null) {
			try {
				ApiCallCommand api = new ApiCallCommand(this)
						.setApi("api/notebook/entry/history/search")
						.addParam("entryId", getEntryId().getId())
						.run();
				
				versionsCache = new SearchResults<NotebookEntryHistory>(
						api.getResponse().getAsList("objects", NotebookEntryHistory.class), 
						api.getResponse().getAsLong("maxHits"));
			} catch (CLIException | IOException e) {
				LibsLogger.error(NotebookEntryHistoryPanel.class, "Cannot get versions", e);
			}
		}
		return versionsCache;
	}

	public UUID getEntryId() {
		return entryId;
	}

	public void setEntryId(UUID entryId) {
		this.entryId = entryId;
	}
}
