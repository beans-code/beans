package net.beanscode.web.view.dataset;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import net.beanscode.cli.datasets.Table;
import net.beanscode.cli.datasets.TableGateway;
import net.beanscode.pojo.tables.ColumnDef;
import net.beanscode.web.view.ui.BetaTableComponent;
import net.hypki.libs5.cli.noninteractivecli.CLIException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.pjf.components.buttons.Button;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.string.StringUtilities;

import org.rendersnake.HtmlCanvas;

import com.google.gson.annotations.Expose;

public class TableColumnsTableComponent extends BetaTableComponent {
	
	@Expose
	private UUID tableId = null;
	
	private Table table = null;
	
	private List<ColumnDef> selectedColumns = null;

	public TableColumnsTableComponent() {
		
	}
	
	public TableColumnsTableComponent(Table table) {
		setTableId(table.getId());
		setTable(table);
	}
	
	@Override
	protected String getSettingsPrefix() {
		return "tablecolumns-table-";
	}

	public Table getTable() {
		if (table == null) {
			try {
				table = TableGateway.getTable(getSessionBean(), getTableId().getId());
			} catch (CLIException | IOException e) {
				LibsLogger.error(TableColumnsTableComponent.class, "Cannot get Table", e);
			}
		}
		return table;
	}

	public void setTable(Table table) {
		this.table = table;
	}

	@Override
	protected void renderTitle(HtmlCanvas html) throws IOException {
		html
			.write("Table " + getTable().getName() + " columns");
	}

	@Override
	protected boolean isFilterVisible() throws IOException {
		return true;
	}

	@Override
	protected boolean isHeaderColumnVisible() throws IOException {
		return true;
	}

	@Override
	protected int getNumberOfColumns() throws IOException {
		return 3;
	}

	@Override
	protected String getColumnName(int col) throws IOException {
		if (col == 0)
			return "Name";
		else if (col == 1)
			return "Type";
		else if (col == 2)
			return "Description";
		return "";
	}

	@Override
	protected boolean isExapandable() throws IOException {
		return false;
	}

	@Override
	protected void renderExpandable(int row, HtmlCanvas html)
			throws IOException {
		
	}

	@Override
	protected long getMaxHits() throws IOException {
		return getSelectedColumns().size();
//		return getTable() != null && getTable().getColumns() != null ? getTable().getColumns().size() : 0;
	}

	@Override
	protected void renderCell(int row, int col, HtmlCanvas html) throws IOException {
		row = row + (getPage() * getPerPage());
		
		if (col == 0)
			html.write(getSelectedColumns().get(row).getName());
		else if (col == 1)
			html.write(getSelectedColumns().get(row).getType().toString());
		else if (col == 2)
			html.write(getSelectedColumns().get(row).getDescription());
	}

	@Override
	protected List<Button> getMenu() throws IOException {
		return null;
	}

	public UUID getTableId() {
		return tableId;
	}

	public void setTableId(UUID tableId) {
		this.tableId = tableId;
	}

	public List<ColumnDef> getSelectedColumns() {
		if (this.selectedColumns == null) {
			this.selectedColumns = new ArrayList<ColumnDef>();
			String [] filters = getFilter() != null ? getFilter().toLowerCase().split("[\\s]+") : null;
			
			if (getTable().getColumns() != null)
				for (ColumnDef col : getTable().getColumns()) {
					String colName = col.getName();
					String colDescription = col.getDescription();
					String colType = col.getType().toString();
					
					if (filters == null
							|| filterFulfilled(filters, colName, colDescription, colType)) {
						this.selectedColumns.add(col);
					}	
				}
		}
		return selectedColumns;
	}

	public void setSelectedColumns(List<ColumnDef> selectedColumns) {
		this.selectedColumns = selectedColumns;
	}
	
	private static boolean filterFulfilled(String [] filters, String colName, String colDescription, String colType) {
		for (String filter : filters) {
			if (StringUtilities.nullOrEmpty(filter) == false
					&& colName.toLowerCase().contains(filter) == false
					&& (colDescription == null || (colDescription != null && colDescription.toLowerCase().contains(filter) == false))
					&& colType.toLowerCase().contains(filter) == false)
				return false;
		}
		return true;
	}

	@Override
	protected String getTableCss() throws IOException {
		// TODO Auto-generated method stub
		return null;
	}
}
