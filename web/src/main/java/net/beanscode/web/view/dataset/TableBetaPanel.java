package net.beanscode.web.view.dataset;

import static net.hypki.libs5.utils.utils.AssertUtils.assertTrue;

import java.io.IOException;

import javax.ws.rs.core.SecurityContext;

import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.cli.datasets.Table;
import net.beanscode.cli.datasets.TableGateway;
import net.beanscode.cli.datasets.TableSearchStatus;
import net.beanscode.web.view.ui.BeansComponent;
import net.beanscode.web.view.ui.modals.OpBeansError;
import net.beanscode.web.view.ui.modals.OpBeansInfo;
import net.beanscode.web.view.upload.UploadView;
import net.hypki.libs5.cli.noninteractivecli.CLIException;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.pjf.components.Component;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.pjf.op.OpSet;
import net.hypki.libs5.utils.LibsLogger;

import org.rendersnake.HtmlCanvas;

public class TableBetaPanel extends BeansComponent {
	
	private Table table = null;
	
	public TableBetaPanel() {
		
	}
	
	public TableBetaPanel(Component parent, Table table) {
		super(parent);
		this.table = table;
		if (parent != null)
			setSecurityContext(parent.getSecurityContext());
		setId(table.getId().getId());
		this.table = table;
	}
	
	protected Table getTable() throws CLIException, IOException {
		if (this.table == null) {
			final String tableId = getParams().getString("tableId");
			
			assertTrue(tableId != null, "Table ID is not specified");
			
			this.table = TableGateway.getTable(getSessionBean(), tableId); 
		}
		return this.table;
	}
	
	public OpList columnsFilter() throws IOException, ValidationException {
		final String filter = getParams().getString("column-filter");
		
		Table table = getTable();// getTab Table.getTable(new UUID(tableId));
		
		assertTrue(table != null, "Table was not found");
		
		return new OpList()
//			.add(new OpSet("#" + table.getId() + " .values", new HtmlCanvas()
//				.render(TableBetaPanel.createColumnNames(table, filter)).toHtml()))
				;
	}

	
	private OpList indexTable(SecurityContext sc) throws IOException, ValidationException {
		String tableId = getParams().getString("tableId");
		
		assertTrue(tableId != null, "Table ID is not specified");
		
//		boolean ok = new TableIndex(getSessionBean()).run(tableId);
		new ApiCallCommand(this)
							.setApi("api/table/index")
							.addParam("tableId", tableId)
							.run();
		boolean ok = true;
		
		if (ok) {
			return new OpList()
				.add(new OpSet("#" + getId() + " .index-status .text", TableSearchStatus.INDEXING.toString(true)))
				.add(new OpBeansInfo("Table scheduled for indexing in the search engine"));
		} else {
			return new OpList()
				.add(new OpBeansError("Scheduling table " + tableId + " for indexing failed. Try again later."));
		}
	}
	
	
	@Override
	public void renderOn(HtmlCanvas html) throws IOException {
//		final boolean isWriteAllowed = isWriteAllowed();
//		final boolean isDeleteAllowed= isDeleteAllowed();
		
		html
			.render(new TableTabs(this, getTable()));
		
//		html
//		.div(class_("beans-panel").id(getId()))
//		    .div(class_("box dark"))
//		        .header()
//		            
//		            
//		            .div(class_("toolbar"))
//		                .ul(class_("nav"))
//							.li(class_("dropdown"))
//		                    ._li()
//		                    .li()
//		                    // INFO remove collapsed to have expanded it by default
//		                        .a(class_("minimize-box collapsed").add("data-toggle", "collapse").href("#panel-" + getId()))
//		                            .i(class_("fa fa-chevron-up"))
//					                ._i()
//		                        ._a()
//		                    ._li()
//		                ._ul()
//		            ._div()
//		            
//		            .div(class_("icons"))
//		                .i(class_("fa fa-table"))
//		                ._i()
//		            ._div()
//		            
//		            .h5()
//		                .content(table.getName())
//		        ._header()
//	        ._div()
//	        .div(id("panel-" + getId()).class_("collapse body"))// INFO add class 'in' to have expanded it by default
//	        	.render(getPanelHtml(isWriteAllowed, isDeleteAllowed))
//	        ._div()
//        ._div()
//        ;
	}
	
	private OpList changeDriver() {
		String option = getParams().getString("choose-selected");
		
		// TODO not implemented yet
		
		return new OpList()
				.add(new OpBeansInfo("Driver changed"));
	}
	
	
	private OpList onValidate() throws IOException {
		try {
			new ApiCallCommand(this)
				.setApi("api/table/validate")
				.addParam("tableId", getParams().getString("tableId"))
				.run();
			
		} catch (CLIException e) {
			LibsLogger.error(TableBetaPanel.class, "Cannot start validation", e);
			
			return new OpList()
				.add(new OpBeansError("Cannot start validation, try again later"));
		}
		
		return new OpList()
			.add(new OpSet("#" + getId() + " .validation-check .text", "Validation started. This can take a while, so the summary will be send to your email"));
	}
	
	private OpList onIndexStatusRefresh() throws CLIException, IOException {
		return new OpList()
			.add(new OpSet("#" + getId() + " .index-status .text", getTable().getSearchStatus().toString(true)));
	}

	public static String jsCodeNewDropzone(final String tablePanelId, final net.beanscode.cli.datasets.Table table) {
		return "new Dropzone('#dropzone-" + tablePanelId + "', " +
				"{url: '" + UploadView.UPLOAD_URL + "?datasetId=" + table.getDatasetId() + "&tableId=" + table.getId() + "', addRemoveLinks: true, maxFilesize: 1000, maxFiles: 1, clickable: true})"
				+ ".on('success', function(file, response) { pjfOps(response); })"
				+ ".on('error', function(file, response) { pjfOps(response); });";
	}

	
}
