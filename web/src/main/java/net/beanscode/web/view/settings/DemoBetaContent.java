package net.beanscode.web.view.settings;

import static net.hypki.libs5.pjf.OpsFactory.opsCall;
import static org.rendersnake.HtmlAttributesFactory.class_;
import static org.rendersnake.HtmlAttributesFactory.onClick;

import java.io.IOException;

import javax.ws.rs.core.SecurityContext;

import net.beanscode.web.beta.BetaContent;
import net.beanscode.web.view.ui.BetaButtonGroup;
import net.beanscode.web.view.ui.modals.OpBeansError;
import net.beanscode.web.view.ui.modals.OpBeansInfo;
import net.beanscode.web.view.ui.modals.OpModalQuestion;
import net.hypki.libs5.pjf.components.Component;
import net.hypki.libs5.pjf.components.ops.OpComponentClick;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.url.Params;

import org.rendersnake.HtmlCanvas;

public class DemoBetaContent extends BetaContent {
	
	public DemoBetaContent() {
		
	}
	
	public DemoBetaContent(Component parent) {
		super(parent);
	}
	
	public OpList demoDatasets(SecurityContext sc, Params params) {
//		try {
//			new DemoData(getSessionBean()).run();
//			new DemoDataJob(getSessionBean(sc).getUserId()).save();
			
			return new OpList()
				.add(new OpBeansInfo("Demo datasets will be created shortly"));
//		} catch (IOException e) {
//			LibsLogger.error(DemoBetaContent.class, "Cannot create job to start demo datasets creation", e);
//			return new OpList()
//				.add(new OpBeansError("Demo datasets cannot be created, try later"));
//		}
	}
	
	public OpList demoQueries(SecurityContext sc, Params params) {
//		try {
//			new DemoQueries(getSessionBean()).run();
//			new DemoQueriesJob(getSessionBean(sc).getUserId()).save();
			return new OpList()
				.add(new OpBeansInfo("Demo queries will be created shortly"));
//		} catch (IOException e) {
//			LibsLogger.error(DemoBetaContent.class, "Cannot create job to start demo datasets creation", e);
//			return new OpList()
//				.add(new OpBeansError("Demo queries cannot be created, try later"));
//		}
	}
		
	@Override
	protected void renderHeader(HtmlCanvas html) throws IOException {
		html
			.h1()
				.content("Demo data");
	}
	
	@Override
	protected void renderContent(HtmlCanvas html) throws IOException {
		html
			.div(class_("options-page"))
				.h3()
					.content("Demo data")
				.div()
					.a(onClick(opsCall(
							new OpModalQuestion("Demo datasets", 
									"Do you want to create demo datasets?", 
									new OpComponentClick(this, "demoDatasets", null)))))
						.content("Generate demo datasets")
					.br()
					.a(onClick(opsCall(
							new OpModalQuestion("Demo queries", 
									"Do you want to create demo notebooks?", 
									new OpComponentClick(this, "demoQueries", null)))))
						.content("Generate demo notebooks")
				._div()
			._div()
			;
	}

	@Override
	protected BetaButtonGroup getMenu() throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected String getMenuName() throws IOException {
		// TODO Auto-generated method stub
		return null;
	}
}
