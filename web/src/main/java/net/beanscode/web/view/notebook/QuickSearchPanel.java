package net.beanscode.web.view.notebook;

import static org.rendersnake.HtmlAttributesFactory.class_;

import java.io.IOException;

import javax.ws.rs.core.SecurityContext;

import net.beanscode.web.view.search.BeansExplorer;
import net.beanscode.web.view.ui.BeansComponent;
import net.hypki.libs5.utils.url.Params;

import org.rendersnake.HtmlCanvas;


public class QuickSearchPanel extends BeansComponent {

	public QuickSearchPanel() {
		
	}
	
	public QuickSearchPanel(SecurityContext sc, Params params) {
		super(sc, params);
	}
	
	@Override
	public void renderOn(HtmlCanvas html) throws IOException {
		html
			.div(class_("dropdown-menu beans-quick-search-panel"))
				.render(new BeansExplorer(this))
		
//		html
//			.render(new SearchTabsComponent(this));
		
			._div(); // dropdown-menu beans-quick-search-panel
	}
}
