package net.beanscode.web.view.notebook;

import static net.beanscode.cli.BeansCliCommand.API_NOTEBOOK_QUERY;
import static org.rendersnake.HtmlAttributesFactory.class_;
import static org.rendersnake.HtmlAttributesFactory.name;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.SecurityContext;

import net.beanscode.cli.BeansCLISettings;
import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.cli.notebooks.Notebook;
import net.beanscode.cli.notebooks.NotebookGateway;
import net.beanscode.cli.notebooks.NotebookSearchResults;
import net.beanscode.web.view.ui.BetaButtonGroup;
import net.beanscode.web.view.ui.BetaTableComponent;
import net.beanscode.web.view.ui.SpinnerLabel;
import net.beanscode.web.view.ui.modals.ModalWindow;
import net.beanscode.web.view.ui.modals.OpModal;
import net.beanscode.web.view.ui.modals.OpModalQuestion;
import net.hypki.libs5.cli.noninteractivecli.CLIException;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.pjf.components.Component;
import net.hypki.libs5.pjf.components.buttons.Button;
import net.hypki.libs5.pjf.components.buttons.ButtonGroup;
import net.hypki.libs5.pjf.components.ops.OpComponentClick;
import net.hypki.libs5.pjf.components.ops.OpComponentTimer;
import net.hypki.libs5.pjf.op.Op;
import net.hypki.libs5.pjf.op.OpAppendVal;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.pjf.op.OpRemove;
import net.hypki.libs5.pjf.op.OpReplace;
import net.hypki.libs5.pjf.op.OpSet;
import net.hypki.libs5.pjf.op.OpWarn;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.url.HttpResponse;
import net.hypki.libs5.utils.url.Params;

import org.rendersnake.HtmlAttributes;
import org.rendersnake.HtmlAttributesFactory;
import org.rendersnake.HtmlCanvas;

import com.google.gson.annotations.Expose;

public class NotebooksTableComponent extends BetaTableComponent {
	
	@Expose
	private boolean compact = false;
	
	private NotebookSearchResults notebookSearchResults = null;
	
	@Expose
	private OpList opOnClick = null;
	
	public NotebooksTableComponent() {
		
	}

	public NotebooksTableComponent(Component parent) {
		super(parent);
	}
	
	@Override
	protected String getSettingsPrefix() {
		return "notebooks-table-";
	}
	
	protected NotebookSearchResults getNotebookSearchResults() {
		if (notebookSearchResults == null) {
			try {
				notebookSearchResults = NotebookGateway.searchNotebooks(this.getSessionBean(), getFilter(), getPage(), getPerPage());
//				notebookSearchResults = new ApiCallCommand(this)
//											.setApi(API_NOTEBOOK_QUERY)
//											.addParam("query", getFilter() == null ? "" : getFilter())
//											.addParam("from", getPage() * getPerPage())
//											.addParam("size", getPerPage())
//											.run()
//											.getResponse()
//											.getAsObject(NotebookSearchResults.class);
				
//				BeansCLISettings.set("notebookSearch", getFilter());
			} catch (CLIException | IOException e) {
				LibsLogger.error(NotebooksTableComponent.class, "Cannot search for Notebooks", e);
				notebookSearchResults = new NotebookSearchResults();
			}
		}
		return notebookSearchResults;
	}

	@Override
	protected void renderTitle(HtmlCanvas html) throws IOException {
		html
			.write("Notebooks");
	}

	@Override
	protected boolean isFilterVisible() throws IOException {
		return true;
	}

	@Override
	protected boolean isHeaderColumnVisible() throws IOException {
		return true;
	}

	@Override
	protected int getNumberOfColumns() throws IOException {
		if (isCompact())
			return 3;
		else
			return 6;
	}

	@Override
	protected String getColumnName(int col) throws IOException {
		if (col == 0)
			return "Name";
		else if (!isCompact() && col == 1)
			return "Owner";
		else if (!isCompact() && col == 2)
			return "ID";
		else if (!isCompact() && col == 3)
			return "Create";
		else if ((!isCompact() && col == 4) || (isCompact() && col == 1))
			return "Last edit";
		else if (col == 5 || (isCompact() && col == 2))
			return "Options";
		else
			return "";
	}

	@Override
	protected long getMaxHits() throws IOException {
		return getNotebookSearchResults().getMaxHits();
	}
	
	private HtmlCanvas renderTags(HtmlCanvas html, Notebook n, String parentId) throws IOException {
		OpList onClickTag = getOpOnClick();
		if (onClickTag == null)
			onClickTag = new OpList()
						.add(new OpComponentClick(NotebookContent.class,
									UUID.random(),
									"onShow",
									new Params().add("notebookId", n.getId().getId())));
		else {
			for (Op op : onClickTag) {
				op.addParamParam("notebookId", n.getId().getId());
				op.addParamParam("notebookName", n.getName());
			}
		}
		
		html
			.div(class_("notebook-row-tags-" + n.getId().getId()))
				.a(class_("beans-link")
						.onClick(onClickTag
									.toStringOnclick()
									))
					.content(n.getName())
				.br();
		
		for (String tag : n.getTags().getTags()) {
			html
				.a(class_("beans-tag")
						.onClick(new OpList()
									.add(new OpAppendVal("#" + parentId + " .beans-table-filter", "+" + tag))
									.add(new OpComponentClick(this.getClass(), parentId, "onRefresh"))
									.toStringOnclick()))
					.content(tag)
	//			.span()
	//				.content(" ")
				.a(class_("beans-tag")
						.onClick(new OpList()
									.add(new OpAppendVal("#" + parentId + " .beans-table-filter", "!" + tag))
									.add(new OpComponentClick(this.getClass(), parentId, "onRefresh"))
									.toStringOnclick()))
					.content("-")
				.span()
					.content(" ")
					;
		}
		
		html
			._div();
		
		return html;
	}

	@Override
	protected void renderCell(int row, int col, HtmlCanvas html)
			throws IOException {
		Notebook n = getNotebookSearchResults().getNotebooks().get(row % getPerPage());
		
		if ((!isCompact() && col == 0) || (isCompact() && col == 0)) {
			
			renderTags(html, n, getId());
		
		} else if (!isCompact() && col == 1) {
			html
				.span()
					.content(n.getUserEmail());
		} else if (!isCompact() && col == 2)
			html
				.span()
					.content(n.getId().toString(" - "));
		else if (!isCompact() && col == 3)
			html
				.span()
					.content(n.getCreationDate().toStringHuman());
		else if ((!isCompact() && col == 4) || (isCompact() && col == 1))
			html
				.span()
					.content(n.getLastEdit().toStringHuman());
		else if (col == 5 || (isCompact() && col == 2)) {
			html
//				.button(HtmlAttributesFactory
//						.data("toggle", "modal")
//						.data("target", "#modal-lg"))
//					.content("BUM")
				.render(new BetaButtonGroup()
						.add(new Button()
								.setAwesomeicon("fa fa-cog")
								.addButton(new Button()
										.setSeparator(true))
								.addButton(new Button("Edit tags")
										.setAwesomeicon("fa fa-tag")
										.add(new OpComponentClick(this, "onTagsEdit", new Params().add("notebookId", n.getId()))))
								.addButton(new Button("Remove")
										.setAwesomeicon("fa fa-trash")
										.add(new OpModalQuestion("Removing notebook",
												"Do you really want to delete this notebook? (That operation cannot be reverted)",
												new OpComponentClick(this, 
														"onDelete",
														new Params().add("notebookId", n.getId().getId())))))
								.addButton(new Button("Clone")
										.setAwesomeicon("fa fa-clone")
										.add(new OpModalQuestion("Clone notebook", 
												"Do you want to clone this notebook?",
												new OpComponentClick(this, 
														"onClone",
														new Params().add("notebookId", n.getId().getId())))))))
				;
		} else {
			// do nothing
		}
	}
	
	private OpList onTagsEdit() throws CLIException, IOException {
		String notebookIdToEdit = getParams().getString("notebookId");
		ModalWindow modal = new ModalWindow(this);
		Notebook n = getNotebookSearchResults().getNotebook(new UUID(notebookIdToEdit));
		
		modal
			.setTitle("Editing tags")
			.add(new Button("Cancel")
					.setAwesomeicon("fa fa-cancel")
					.add(modal.onClose()))
			.add(new Button("Save")
					.setAwesomeicon("fa fa-save")
					.add(new OpComponentClick(this.getClass(), modal.getId(), "onTagsSave"))
					.add(modal.onClose()))
			.getContent()
				.input(name("parentId")
					.type("hidden")
					.value(getId()))
				.input(name("notebookId")
						.type("hidden")
						.value(notebookIdToEdit))
				.textarea(class_("notebook-tags")
						.name("notebook-tags"))
					.content(n.getTags().getTagsJoined(" "))
			;
		
		return new OpList()
				.add(new OpModal(modal));
	}
	
	private OpList onTagsSave() throws CLIException, IOException {
		String notebookIdToEdit = getParams().getString("notebookId");
		String newTags = getParams().getString("notebook-tags");
		String parentId = getParams().getString("parentId");

		Notebook n = getNotebookSearchResults().getNotebook(new UUID(notebookIdToEdit));
		
		n.getTags().clear();
		n.getTags().addTags(newTags);
		
		new ApiCallCommand(this)
			.setApi("api/notebook/tags")
			.addParam("notebookId", n.getId().getId())
			.addParam("tags", newTags)
			.run();
		
		return new OpList()
				.add(new OpReplace(".notebook-row-tags-" + n.getId().getId(), renderTags(new HtmlCanvas(), n, parentId).toHtml()))
				.add(new OpSet("#" + parentId + " .beans-table-rows", new SpinnerLabel("Refreshing notebooks").toHtml()))
				.add(new OpComponentTimer(NotebooksTableComponent.class, new UUID(parentId), "onRefresh", 1200));
	}
	
	private OpList onClone() throws CLIException, IOException {
		HttpResponse resp = new ApiCallCommand(getSessionBean())
			.setApi("api/notebook/clone")
			.addParam("--notebookId", getParams().getString("notebookId"))
			.run()
			.getResponse();
		
		return new OpList()
			.add(new OpComponentClick(NotebookContent.class, 
					UUID.random(), 
					"onShow", 
					new Params()
						.add("userId", getUserId())
						.add("notebookId", resp.getAsString("notebookId"))));
	}
	
	@Override
	protected String getRowCss(int row) {
		Notebook n = getNotebookSearchResults().getNotebooks().get(row % getPerPage());
		
		return super.getRowCss(row) + " notebook-" + n.getId().getId();
	}
	
	public OpList onDelete(SecurityContext sc, Params params) throws IOException, ValidationException {
		List notebookId = params.getList("notebookId");
		
		if (notebookId == null || notebookId.size() == 0) {
			return new OpList().add(new OpWarn("Warning", "No notebook were selected"));
		}
		
		List<String> notebookUUIDs = new ArrayList<>();
		for (Object id : notebookId) {
			notebookUUIDs.add(id.toString());
		}
		
//		new NotebookRest().remove(sc, notebookUUIDs);
		new ApiCallCommand(getSessionBean())
			.setApi("api/notebook/remove")
			.addParam("notebookId", notebookUUIDs)
			.run();
		
		OpList opList = new OpList();
		for (Object noteId : notebookId)
			opList.add(new OpRemove(".notebook-" + noteId));
			
		return opList;
	}
	
	@Override
	protected List<Button> getMenu() throws IOException {
		return null;
	}

	public boolean isCompact() {
		return compact;
	}

	public NotebooksTableComponent setCompact(boolean compact) {
		this.compact = compact;
		return this;
	}

	@Override
	protected boolean isExapandable() throws IOException {
		return false;
	}

	@Override
	protected void renderExpandable(int row, HtmlCanvas html) throws IOException {
		
	}

	@Override
	protected String getTableCss() throws IOException {
		return isCompact() ? "beans-table-compact" : "beans-table-normal";
	}

	public OpList getOpOnClick() {
		return opOnClick;
	}

	public NotebooksTableComponent setOpOnClick(OpList opOnClick) {
		this.opOnClick = opOnClick;
		return this;
	}
}
