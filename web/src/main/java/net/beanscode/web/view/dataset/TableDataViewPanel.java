package net.beanscode.web.view.dataset;

import static net.hypki.libs5.pjf.OpsFactory.opsCall;
import static net.hypki.libs5.utils.string.StringUtilities.split;
import static org.rendersnake.HtmlAttributesFactory.class_;
import static org.rendersnake.HtmlAttributesFactory.href;
import static org.rendersnake.HtmlAttributesFactory.name;
import static org.rendersnake.HtmlAttributesFactory.type;

import java.io.IOException;
import java.util.List;

import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.cli.data.DataSearchResults;
import net.beanscode.cli.datasets.Table;
import net.beanscode.cli.datasets.TableGateway;
import net.beanscode.cli.datasets.TableSearchStatus;
import net.beanscode.pojo.tables.ColumnDef;
import net.beanscode.web.view.ui.BeansComponent;
import net.beanscode.web.view.ui.BeansTableComponent;
import net.beanscode.web.view.ui.Link;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.schema.ColumnType;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.pjf.components.buttons.Button;
import net.hypki.libs5.pjf.components.buttons.ButtonGroup;
import net.hypki.libs5.pjf.components.buttons.ButtonSize;
import net.hypki.libs5.pjf.components.ops.OpComponentClick;
import net.hypki.libs5.pjf.components.ops.OpComponentTimer;
import net.hypki.libs5.pjf.components.ops.OpDialogOpen;
import net.hypki.libs5.pjf.op.OpAddClass;
import net.hypki.libs5.pjf.op.OpDialogClose;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.pjf.op.OpRemoveClass;
import net.hypki.libs5.pjf.op.OpSetVal;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.collections.CollectionUtils;
import net.hypki.libs5.utils.url.Params;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotNull;

import org.rendersnake.HtmlCanvas;

import com.google.gson.annotations.Expose;

public class TableDataViewPanel extends BeansTableComponent {
	
	@Expose
	@NotNull
	@AssertValid
	private UUID tableId = null;
	
	@Expose
	@NotNull
	private String state = null;
	
	private List<String> visibleColumns = null;
	
	private Table tableCache = null;
	
	private DataSearchResults dataSearch = null;
	
	public TableDataViewPanel() {
		
	}

	public TableDataViewPanel(BeansComponent parent) {
		super(parent);
	}
	
	public TableDataViewPanel(BeansComponent parent, UUID tableId) {
		super(parent);
		setTableId(tableId);
	}
	
	@Override
	protected int getPerPage() {
		return 5;
	}
	
	@Override
	protected String getFilterPlaceholder() {
		return "Filter...";
	}
	
	@Override
	protected String getRefreshButtonText() {
		return "";
	}
	
	@Override
	protected int getRefreshDelayMs() {
		return 500;
	}
	
//	@Override
//	public void renderOn(HtmlCanvas html) throws IOException {
//		html
//			.div(id(getId())
//					.class_("table-data-view"))
//				
//				.div(class_("table-data"))
//					.content("")
//			._div()
//			;
//	}
	
	@Override
	protected void renderHeader(HtmlCanvas html) throws IOException {
		if (getTable() == null)
			return;
		
		html
			.dl(class_("table-name row"))
				.dt(class_("col-sm-1"))
					.content("Table: ")
				.dd(class_("col-sm-11"))
					.content(getTable().getName() + " (" + getTable().getId().toString(" - ") + ")")
//				.br()
				.dt(class_("col-sm-1"))
					.content("Columns: ")
				.dd(class_("col-sm-11 columns"))
					.render(new Link("-", new OpList()
											.add(new OpSetVal("#" + getId() + " .table-data-filter-visible-columns", "tbid"))
											.add(new OpRemoveClass("#" + getId() + " .beans-column-name", "beans-link-selected"))
											.add(new OpComponentClick(this, "onRefresh"))))
					.render(new Link("+", new OpList()
											.add(new OpSetVal("#" + getId() + " .table-data-filter-visible-columns", ""))
											.add(new OpComponentClick(this, "onRefresh"))))
				;
		
		if (getTable() != null && getTable().getColumns() != null)
			for (ColumnDef colDef : getTable().getColumns()) {
				html
					.render(new Link(colDef.getName(), new OpList()
														.add(new OpComponentClick(this, 
																"onAddColumn", 
																new Params().add("newcolumn", colDef.getName())))
									)
								.addHtmlClass(isVisible(colDef.getName()) ? "beans-link-selected" : "")
								.addHtmlClass("beans-column-name " + colDef.getName()))
					.write(" ");
			}
		
		// link to download the data
		html
			._dd()
			.dt(class_("col-sm-1"))
				.content("Data: ")
			.dd(class_("col-sm-11"))
				.a(href("/view/table/data?tableId=" + getTable().getId().getId())
										.target("_blank")
										.class_("beans-link"))
									.content("Download table (as plain text)")
			._dd();
		
		html
			._dl() // table-name
			.if_(isPagerWithNumbers())
				.input(class_("table-data-filter")
						.name("table-data-filter")
						.onKeyup(opsCall(new OpComponentTimer(this, "onRefresh", 300))))
			._if()
			.input(type("hidden")
					.name("table-data-filter-visible-columns")
					.class_("table-data-filter-visible-columns"));
	}
	
	private OpList onAddColumn() {
		String newcolumn = getParams().getString("newcolumn");
		
		if (isVisible(newcolumn))
			return new OpList();
		
		return new OpList()
			.add(new OpSetVal("#" + getId() + " .table-data-filter-visible-columns", 
					getParams().getString("table-data-filter-visible-columns", "") + " " + newcolumn))
			.add(new OpAddClass("#" + getId() + " ." + newcolumn, "beans-link-selected"))
			.add(new OpComponentClick(this, "onRefresh"));
	}
	
	private OpList onShowHideColumnsShow() throws IOException {		
		return new OpList()
			.add(new OpDialogOpen(TableDataViewPanel.this, "onShowHideColumnsClose", "Show/hide columns for display", 
					htmlDialogShowHideColumns()));
	}
	
	private String htmlDialogShowHideColumns() throws IOException {
		HtmlCanvas html = new HtmlCanvas();
		
		StringBuilder sb = new StringBuilder();
		for (ColumnDef colDef : getTable().getColumns()) {
			sb.append(colDef.getName() + " ");
		}
		
		html
			.span()
			.content("All columns: " + sb.toString());
		
		html
			.br()
			.span()
				.content("Write names of columns to show (separated by space or comma):")
			.br()
			.textarea(name("table-data-filter-visible-columns").
					class_("table-data-filter-visible-columns"))
				.content(CollectionUtils.toString(getVisibleColumns(), ", "))
//			._textarea()
			;
		
		return html.toHtml();
	}
	
	private Table getTable() {
		try {
			if (tableCache != null)
				return tableCache;
			
			if (tableId == null)
				return null;
			
			tableCache = TableGateway.getTable(getSessionBean(), getTableId().getId());
			return tableCache;
		} catch (Exception e) {
			LibsLogger.error(TableDataViewPanel.class, "Cannot get Table from DB", e);
			return tableCache;
		}
	}
	
	public OpList onShowHideColumnsClose() {
		return new OpList()
			.add(new OpDialogClose())
			.add(new OpSetVal("#" + getId() + " .table-data-filter-visible-columns", getParams().getString("table-data-filter-visible-columns")))
			.add(new OpComponentClick(this, "onRefresh"));
	}
	
//	public OpList onRefresh() throws ValidationException, IOException {
//		assertTrue(getTable() != null, "Cannot get data from the Table. Try again later..");
//		
//		BeansTableComponent table = new () {
//			
//			public BeansTableComponent() {
//				
//			}
//			
//			
//		};
//				
//		return new OpList()
//				.add(new OpSet("#" + getId() + " .table-data", table.toHtml()));
//	}
	
	@Override
	protected boolean isPagerWithNumbers() {
		if (getTable() == null)
			return super.isPagerWithNumbers();
		return !(getTable().getSearchStatus() == TableSearchStatus.NOT_INDEXED);
	}
	
	@Override
	protected long totalRows() {
		if (isPagerWithNumbers())
			return getDataSearchResults().getMaxHits();
		else
			return getDataSearchResults().getMaxHits();
	}
	
	@Override
	protected void prepareRow(HtmlCanvas html, int row) throws IOException {
		row = row % getPerPage();
		if (row >= getDataSearchResults().getRows().size())
			return;
		
		Row r = getDataSearchResults().getRows().get(row);
		
		html
			.tr()
				.td();
		
//		for (Map.Entry<String, Object> e : r.getColumns().entrySet()) {
		if (isVisibleColumnsSet() == false) {
			for (ColumnDef column : getTable().getColumns()) {
				Object o = null;
				if (column.getType() == ColumnType.DOUBLE)
					o = r.getAsDouble(column.getName());
				else if (column.getType() == ColumnType.INTEGER
						|| column.getType() == ColumnType.LONG)
					o = r.getAsLong(column.getName());
				else if (column.getType() == ColumnType.STRING)
					o = r.getAsString(column.getName());
				o = o != null ? o : "";
				html
					.b()
						.content(" " + column.getName() + " ")
					.span()
						.content(o != null ? o.toString() : "");
			}
		} else {
			for (String columnName : getVisibleColumns()) {
				ColumnDef column = getTable().getColumnDef(columnName);
				Object o = null;
				if (column.getType() == ColumnType.DOUBLE)
					o = r.getAsDouble(column.getName());
				else if (column.getType() == ColumnType.INTEGER
						|| column.getType() == ColumnType.LONG)
					o = r.getAsLong(column.getName());
				else if (column.getType() == ColumnType.STRING)
					o = r.getAsString(column.getName());
				o = o != null ? o : "";
				html
					.b()
						.content(" " + column.getName() + " ")
					.span()
						.content(o != null ? o.toString() : "");
			}
		}
		
		html
				._td()
			._tr();
	}
	
	@Override
	protected boolean isFilterVisible() {
		return true;
	}
	
	@Override
	protected String getMessageIfNoRows() {
		return "No rows";
	}
	
	@Override
	protected List<Button> getMenu() {
		return new ButtonGroup()
			.setButtonSize(ButtonSize.SMALL)
//		.addButton(new Button()
//				.setGlyphicon("refresh")
//				.add(new OpComponentClick(TableDataViewPanel.this, "onRefresh")))
//			.addButton(new Button()
//					.setGlyphicon("cog")
//					.addButton(new Button("Show/hide columns")
//						.setGlyphicon("onRefresh")
//						.add(new OpComponentClick(TableDataViewPanel.this, "onShowHideColumnsShow"))
//						))
			.getButtons();
	}
	
	@Override
	protected int getColumnCount() {
		if (isVisibleColumnsSet() == false)
			return getTable().getColumns() != null ? getTable().getColumns().size() : 0;
		else
			return getVisibleColumns().size();
	}
	
	@Override
	protected String getHeaderName(int column) {
		if (isVisibleColumnsSet() == false)
			return getTable().getColumns().get(column).getName();
		else
			return getVisibleColumns().get(column);
	}
	
	@Override
	protected boolean isHeaderVisible() {
		return false;
	}
	
	@Override
	protected String getPanelHtmlClasses() {
		return super.getPanelHtmlClasses() + " table-data-view";
	}

	public UUID getTableId() {
		return tableId;
	}

	public void setTableId(UUID tableId) {
		this.tableId = tableId;
	}
	
	private boolean isVisible(String column) {
		return getVisibleColumns().contains(column);
	}
	
	private boolean isVisibleColumnsSet() {
		return getVisibleColumns().size() > 0;
	}

	private List<String> getVisibleColumns() {
		if (visibleColumns == null) {
			String visCols = getParams().getString("table-data-filter-visible-columns");
			visibleColumns = split(visCols, " ", ",");
		}
		return visibleColumns;
	}
	
	private DataSearchResults getDataSearchResults() {
		if (dataSearch == null) {
			try {
				if (isPagerWithNumbers()) {
					int page = getParams().getInteger("page", 0);
//					dataSearch = new DataSearch(getSessionBean()).run(getTable().getId(), 
//							getParams().getString("table-data-filter"), 
//							page * getPerPage(), 
//							getPerPage());
					ApiCallCommand call = new ApiCallCommand(this.getSessionBean())
						.setApi(ApiCallCommand.API_TABLE_DATA_SEARCH)
						.addParam("tableId", getTableId().getId())
						.addParam("query", getParams().getString("table-data-filter"))
						.addParam("from", page * getPerPage())
						.addParam("size", getPerPage())
						.run();
					dataSearch = new DataSearchResults()
						.setMaxHits(call.getResponse().getAsLong("maxHits"))
						.setState(call.getResponse().getAsString("state"))
						.setRows(call.getResponse().getAsList("rows", Row.class));
				} else {
					if (getParams().getInteger("page", -1) == 0)
						setState(null);
					ApiCallCommand call = new ApiCallCommand(this.getSessionBean())
						.setApi(ApiCallCommand.API_TABLE_DATA_ROWS)
						.addParam("tableId", getTableId().getId())
						.addParam("query", getFilter())
						.addParam("state", getState())
						.addParam("size", getPerPage())
						.run();
					dataSearch = new DataSearchResults()
						.setMaxHits(call.getResponse().getAsLong("maxHits"))
						.setState(call.getResponse().getAsString("state"))
						.setRows(call.getResponse().getAsList("rows", Row.class));
					setState(dataSearch.getState());
				}
			} catch (Exception e) {
				LibsLogger.error(TableDataViewPanel.class, "Cannot get data rows for table " + getTableId(), e);
				dataSearch = new DataSearchResults();
			}
		}
		return dataSearch;
	}

	private String getState() {
		return state;
	}

	private void setState(String state) {
		this.state = state;
	}
}
