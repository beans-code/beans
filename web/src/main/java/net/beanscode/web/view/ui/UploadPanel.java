package net.beanscode.web.view.ui;

import static org.rendersnake.HtmlAttributesFactory.id;

import java.io.IOException;

import org.rendersnake.HtmlCanvas;

import com.google.gson.annotations.Expose;

import net.hypki.libs5.pjf.op.Op;
import net.hypki.libs5.pjf.op.OpJs;
import net.hypki.libs5.pjf.op.OpList;

public class UploadPanel extends BeansComponent {
	
	@Expose
	private String url = null;
	
	@Expose
	private OpList onSuccess = null;
	
	@Expose
	private OpList onError = null;

	public UploadPanel() {
		
	}
	
	public UploadPanel(BeansComponent parent, String id) {
		super(parent);
		setId(id);
	}
	
	@Override
	public void renderOn(HtmlCanvas html) throws IOException {
		html
			.div(id(getId())
					.action(getUrl().substring(0, getUrl().indexOf('?')))
					.class_("dropzone no-image"))
				.span()
					.content("<b>Drop one or multiple files</b> to upload (<b>or click</b>)", false)
			._div()
			
			.write(new OpJs("new Dropzone('#" + getId() + "', " +
				"{url: '" + getUrl() + "', "
						+ "addRemoveLinks: false, "
						+ "maxFilesize: 100000, "
						+ "clickable: true}"
				+ ")"
				+ ".on('success', function(file, response) { " + getOnSuccess().toStringOnclick() + "; })"
				+ ".on('error', function(file, response) { " + getOnError().toStringOnclick() + "; });").toStringAdhock(), false)
			;
	}

	public String getUrl() {
		return url;
	}
	
	public UploadPanel onSuccess(Op op) {
		getOnSuccess().add(op);
		return this;
	}
	
	public UploadPanel onError(Op op) {
		getOnError().add(op);
		return this;
	}

	public UploadPanel setUrl(String url) {
		this.url = url;
		return this;
	}

	public OpList getOnSuccess() {
		if (onSuccess == null)
			onSuccess = new OpList();
		return onSuccess;
	}

	private void setOnSuccess(OpList onSuccess) {
		this.onSuccess = onSuccess;
	}

	public OpList getOnError() {
		if (onError == null)
			onError = new OpList();
		return onError;
	}

	private void setOnError(OpList onError) {
		if (onError == null)
			onError = new OpList();
		this.onError = onError;
	}
}
