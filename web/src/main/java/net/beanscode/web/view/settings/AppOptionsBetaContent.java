package net.beanscode.web.view.settings;

import java.io.IOException;

import net.beanscode.web.beta.BetaContent;
import net.beanscode.web.view.ui.BeansComponent;
import net.beanscode.web.view.ui.BetaButtonGroup;
import net.hypki.libs5.utils.utils.AssertUtils;

import org.rendersnake.HtmlCanvas;

public class AppOptionsBetaContent extends BetaContent {
	
	public AppOptionsBetaContent() {
		
	}
	
	public AppOptionsBetaContent(BeansComponent parent) {
		super(parent);
	}
		
	@Override
	protected void renderHeader(HtmlCanvas html) throws IOException {
		html
			.h1()
				.content("Application options");
	}
	
	@Override
	protected void renderContent(HtmlCanvas html) throws IOException {
		AssertUtils.assertTrue(isAdmin(), "You have no rights to see this content");
		
		html
			.render(new SettingsTableComponent(this));
		
//		content
//			.render(new PropertyPanel(this))
		
			;
	}

	@Override
	protected BetaButtonGroup getMenu() throws IOException {
		return null;
	}

	@Override
	protected String getMenuName() throws IOException {
		return null;
	}
	
	
}
