package net.beanscode.web.view.notebook;

import static org.rendersnake.HtmlAttributesFactory.class_;

import java.io.IOException;

import net.beanscode.cli.notebooks.Notebook;
import net.hypki.libs5.pjf.components.Component;

import org.rendersnake.HtmlCanvas;

public class NotebookProperties extends Component {
	
	private Notebook notebook = null;

	public NotebookProperties() {
		
	}
	
	public NotebookProperties(Notebook notebook) {
		setNotebook(notebook);
	}
	
	@Override
	public void renderOn(HtmlCanvas html) throws IOException {
		html
			.dl(class_("dl-horizontal"))
				.dt()
					.content("Notebook ID")
				.dd()
					.span()
						.content(getNotebook().getId().getId())
				._dd()
				.dt()
					.content("Notebook owner")
				.dd()
					.span()
						.content(getNotebook().getUserEmail())
				._dd()
				.dt()
					.content("Description")
				.dd()
					.span()
						.content(getNotebook().getDescription())
				._dd()
			._dl();
	}

	public Notebook getNotebook() {
		return notebook;
	}

	public void setNotebook(Notebook notebook) {
		this.notebook = notebook;
	}
}
