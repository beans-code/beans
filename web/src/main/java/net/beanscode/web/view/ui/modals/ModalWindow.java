package net.beanscode.web.view.ui.modals;

import static org.rendersnake.HtmlAttributesFactory.add;
import static org.rendersnake.HtmlAttributesFactory.class_;
import static org.rendersnake.HtmlAttributesFactory.type;

import java.io.IOException;

import net.beanscode.web.view.ui.BeansComponent;
import net.hypki.libs5.pjf.components.Component;
import net.hypki.libs5.pjf.components.buttons.Button;
import net.hypki.libs5.pjf.components.buttons.ButtonGroup;
import net.hypki.libs5.pjf.components.ops.OpComponentClick;
import net.hypki.libs5.pjf.op.Op;
import net.hypki.libs5.pjf.op.OpJs;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.pjf.op.OpRemove;

import org.rendersnake.HtmlAttributesFactory;
import org.rendersnake.HtmlCanvas;

public class ModalWindow extends BeansComponent {
	
	private String title = null;
	
	private ButtonGroup buttons = null;
	
	private HtmlCanvas content = new HtmlCanvas();

	public ModalWindow() {
		
	}
	
	public ModalWindow(Component parent) {
		super(parent);
	}
	
	public ModalWindow(Component parent, String title, Component content, OpList onSave) throws IOException {
		super(parent);
		
		setTitle(title);
		setContent(content);
		
		if (onSave != null)
			add(new Button("Save")
					.add(onSave)
					.add(onClose()));
		add(new Button("Cancel")
				.add(onClose()));
	}
	
	public ModalWindow(Component parent, String title, Component content, Op onSave) throws IOException {
		super(parent);
		
		setTitle(title);
		setContent(content);
		
		if (onSave != null)
			add(new Button("Save")
					.add(onSave)
					.add(onClose()));
		add(new Button("Cancel")
				.add(onClose()));
	}

	public ModalWindow add(String inputTitle, InputType inputType, 
			String inputName, Object value) throws IOException {
		if (inputType == InputType.STRING) {
			getContent()
				.dl(class_("row"))
					.dt(class_("col-sm-4"))
						.content(inputTitle)
					.dd(class_("col-sm-8"))
						.input(class_("form-control")
								.type("text")
								.name(inputName)
								.value(String.valueOf(value)))
					._dd()
				._dl();
		} else if (inputType == InputType.PASSWORD) {
			getContent()
				.dl(class_("row"))
					.dt(class_("col-sm-4"))
						.content(inputTitle)
					.dd(class_("col-sm-8"))
						.input(class_("form-control")
								.type("password")
								.name(inputName)
								.value(String.valueOf(value)))
					._dd()
				._dl();
		} else if (inputType == InputType.TEXT) {
			getContent()
				.dl(class_("row"))
					.dt(class_("col-sm-4"))
						.content(inputTitle)
					.dd(class_("col-sm-8"))
						.textarea(class_("form-control")
								.name(inputName)
								.style("min-height: 200px;"))
							.content(String.valueOf(value))
					._dd()
				._dl();
		} else if (inputType == InputType.HIDDEN) {
			getContent()
				.input(type("hidden")
						.name(inputName)
						.value(String.valueOf(value)));
		} else if (inputType == InputType.HIDDEN) {
			getContent()
				.dl(class_("row"))
					.dt(class_("col-sm-4"))
						.content(inputTitle)
					.dd(class_("col-sm-8"))
						.input(type("checkbox")
								.name(inputName)
								.value((value instanceof Boolean && ((Boolean) value)) ? "on" : ""))
					._dd()
				._dl();
		}
		return this;
	}
	
	@Override
	public void renderOn(HtmlCanvas html) throws IOException {
		html
			.div(class_("modal fade")
					.id("beans-modal-" + getId()))
				.div(class_("modal-dialog modal-lg")
						.id(getId()))
					.div(class_("modal-content"))
						.div(class_("modal-header"))
							.h4(class_("modal-title"))
								.content(getTitle())
							.button(type("button")
									.class_("close")
									.add("data-dismiss", "modal")
									.add("aria-label", "Close"))
								.span(HtmlAttributesFactory.add("aria-hidden", "true", false))
									.content("&times;", false)
							._button()
						._div()
						.div(class_("modal-body"))
							.write(getContent().toHtml(), false)
//							.span()
//								.content("body")
						._div()
						.div(class_("modal-footer"))
						;
		
		for (Button button : getButtons().getButtons()) {
			html
				.render(button);
		}
		
		html
//							.render(getButtons())
						._div()
					._div()
				._div()
			._div()
			;
	}
	
	public OpList onClose() {
		return new OpList()
			.add(new OpJs("$('#beans-modal-" + getId() + "').modal('toggle');"))
//			.add(new OpRemove("#beans-modal-" + getId())) // TODO what about this?
			;
	}

	public String getTitle() {
		return title;
	}

	public ModalWindow setTitle(String title) {
		this.title = title;
		return this;
	}
	
	public ModalWindow add(Button button) {
		getButtons().addButton(button);
		return this;
	}

	public ButtonGroup getButtons() {
		if (buttons == null)
			buttons = new ButtonGroup();
		return buttons;
	}

	private void setButtons(ButtonGroup buttons) {
		this.buttons = buttons;
	}

	public HtmlCanvas getContent() {
		return content;
	}

	public void setContent(HtmlCanvas content) {
		this.content = content;
	}

	public ModalWindow setContent(Component component) throws IOException {
		getContent()
			.render(component);
		return this;
	}
}
