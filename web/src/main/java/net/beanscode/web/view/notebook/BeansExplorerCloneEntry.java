package net.beanscode.web.view.notebook;

import java.io.IOException;

import org.rendersnake.HtmlCanvas;

import net.beanscode.web.view.search.BeansExplorer;
import net.beanscode.web.view.ui.BeansComponent;
import net.beanscode.web.view.ui.Link;
import net.beanscode.web.view.ui.modals.OpBeansInfo;
import net.hypki.libs5.pjf.components.ops.OpComponentClick;

public class BeansExplorerCloneEntry extends BeansExplorer {
	
	AddNewEntryPanel parentPanel = null;

	public BeansExplorerCloneEntry() {
		
	}
	
	public BeansExplorerCloneEntry(BeansComponent parent, AddNewEntryPanel parentPanel) {
		super(parent);
		this.parentPanel = parentPanel;
	}
	
	@Override
	public boolean isDatasetsVisible() {
		return false;
	}
	
	protected void renderAfterEntry(int idx, HtmlCanvas html) throws IOException {
//		html
//			.render(
//					);
	};
}
