package net.beanscode.web.view.notebook;

import static net.beanscode.cli.BeansCliCommand.API_NOTEBOOK_META_SET;

import java.io.IOException;

import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.cli.notebooks.Notebook;
import net.beanscode.web.view.ui.EditableTable;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.pjf.components.Component;
import net.hypki.libs5.pjf.components.buttons.ButtonSize;
import net.hypki.libs5.pjf.components.ops.OpComponentClick;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.utils.string.Meta;
import net.hypki.libs5.utils.string.RegexUtils;
import net.hypki.libs5.utils.url.Params;
import net.hypki.libs5.utils.utils.NumberUtils;

public class NotebookParams extends EditableTable {

	private Notebook notebook = null;
	
	public NotebookParams() {
		
	}
	
	public NotebookParams(Component parent) {
		super(parent);
		setAutosave(true);
	}
	
	public NotebookParams(Component parent, Notebook notebook) {
		super(parent);
		setNotebook(notebook);
		setAutosave(true);
		
		setColumnNames(new String[] {"Key", "Value", "Description"});
		
		if (notebook.getMeta().size() > 0) {
			Object[][] values = new Object[notebook.getMeta().size()][3];
			int row = 0;
			for (Meta meta : notebook.getMeta()) {
				values[row][0] = meta.getName();
				values[row][1] = meta.getValue();
				values[row++][2] = meta.getDescription();
			}
			setValues(values);
		}
		
		getHiddenParams()
			.add("notebookId", notebook.getId().getId())
			.add("userId", notebook.getUserId().getId());
	}
	
	@Override
	protected String getAdditionalCssClasses() {
		return "notebook-params";
	}
	
	@Override
	protected OpList getAdditionalSaveOp() {
		return new OpList()
			.add(new OpComponentClick(this, "saveTableClick"));
	}
	
	@Override
	protected void saveTable(Params params, String[][] newValues) throws IOException {
		UUID notebookId = new UUID(params.getString("hidden-notebookId"));
		
		Notebook notebook = new ApiCallCommand(this)
							.setApi("api/notebook/get")
							.addParam("notebookId", notebookId.getId())
							.run()
							.getResponse()
							.getAsObject(Notebook.class);
		
		notebook.getMeta().clear();
		for (int row = 0; row < newValues.length; row++) {
			String value = String.valueOf(newValues[row][1]);
			String desc = String.valueOf(newValues[row][2]);
			
			if (RegexUtils.isInt(value))
				notebook.getMeta().add(String.valueOf(newValues[row][0]), NumberUtils.toInt(value), desc);
			else if (RegexUtils.isDouble(value))
				notebook.getMeta().add(String.valueOf(newValues[row][0]), NumberUtils.toDouble(value), desc);
			else
				notebook.getMeta().add(String.valueOf(newValues[row][0]), value, desc);
		}
		
		ApiCallCommand api = new ApiCallCommand(this)
			.setApi(API_NOTEBOOK_META_SET)
			.addParam("notebookId", notebook.getId());
		
		for(Meta meta : notebook.getMeta())
			api
				.addParam("metaName", meta.getName())
				.addParam("metaDesc", meta.getDescription())
				.addParam("metaValue", meta.getValue());
		api.run();
	}
	
	@Override
	protected String getSaveClass() {
		return getClass().getName();
	}
	
	@Override
	public ButtonSize getButtonSize() {
		return ButtonSize.EXTRA_SMALL;
	}

	public Notebook getNotebook() {
		return notebook;
	}

	public void setNotebook(Notebook notebook) {
		this.notebook = notebook;
	}
}
