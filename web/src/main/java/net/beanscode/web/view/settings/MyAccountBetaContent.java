package net.beanscode.web.view.settings;

import static org.rendersnake.HtmlAttributesFactory.class_;

import java.io.IOException;

import net.beanscode.web.beta.BetaContent;
import net.beanscode.web.view.ui.BeansComponent;
import net.beanscode.web.view.ui.BetaButtonGroup;
import net.beanscode.web.view.users.PassChangeCard;
import net.beanscode.web.view.users.SummaryUserCard;

import org.rendersnake.HtmlCanvas;

public class MyAccountBetaContent extends BetaContent {
	
	public MyAccountBetaContent() {
		
	}
	
	public MyAccountBetaContent(BeansComponent parent) {
		super(parent);
	}
		
	@Override
	protected void renderHeader(HtmlCanvas html) throws IOException {
		html
			.h1()
				.content("Profile: " + getUserEmail());
	}
	
	@Override
	protected void renderContent(HtmlCanvas html) throws IOException {
		html
			.div(class_("row"))
				.render(new SummaryUserCard(this))
				
				.render(new PassChangeCard(this))
			._div();
	}
	
	

	@Override
	protected BetaButtonGroup getMenu() throws IOException {
		return null;
	}

	@Override
	protected String getMenuName() throws IOException {
		return null;
	}
}
