package net.beanscode.web.view.ui;

import static org.rendersnake.HtmlAttributesFactory.class_;
import static org.rendersnake.HtmlAttributesFactory.href;

import java.io.IOException;

import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.pjf.components.Component;
import net.hypki.libs5.pjf.components.buttons.Button;
import net.hypki.libs5.pjf.op.OpAddClass;
import net.hypki.libs5.pjf.op.OpRemoveClass;

import org.rendersnake.HtmlCanvas;
import org.rendersnake.Renderable;

@Deprecated
public class SidebarMenu extends BeansComponent {
	
	private BetaButtonGroup betaButtonGroup = null;

	public SidebarMenu() {
		
	}
	
	public SidebarMenu(Component parent, BetaButtonGroup buttons) {
		super(parent);
		setBetaButtonGroup(buttons);
	}
	
	@Override
	public void renderOn(HtmlCanvas html) throws IOException {
		if (getBetaButtonGroup() == null) {
			html
				.li(class_("nav-item beans-submenu-content beans-content-menu"))
				._li();
			return;
		}
		
		for (Button mainButton : getBetaButtonGroup().getButtons()) {
			
			html
				.li(class_("nav-item beans-submenu-content beans-content-menu")
						.onClick(mainButton
								.getOpList()
//								.add(new OpRemoveClass(".sidebar .nav-link", "active"))
//								.add(new OpAddClass(".beans-sidebar-" + mainButtonId + " .nav-link", "active"))
								.toStringOnclick()))
					.a(href("#")
							.class_("nav-link"))
						.i(class_("nav-icon " + mainButton.getAwesomeicon()))
						._i()
						.p()
							.write(mainButton.getName() + " ")
							.if_(mainButton.isHierarchical())
								.i(class_("fas fa-angle-left right"))
								._i()
							._if()
						._p()
					._a()
					.render(new Renderable() {
						@Override
						public void renderOn(HtmlCanvas html) throws IOException {
							if (mainButton.getButtons().size() > 0) {
								html
									.ul(class_("nav nav-treeview"));
								for (Button subButton : mainButton.getButtons()) {
									UUID subButtonId = UUID.random();
									html
										.li(class_("nav-item beans-content-menu beans-sidebar-" + subButtonId)
												.onClick(subButton
													.getOpList()
													.add(new OpRemoveClass(".sidebar .nav-link", "active"))
													.add(new OpAddClass(".beans-sidebar-" + subButtonId + " .nav-link", "active"))
													.toStringOnclick()))
											.a(href("#")
												.class_("nav-link"))
												.i(class_("nav-icon " + subButton.getAwesomeicon()))
												._i()
												.p()
													.content(subButton.getName())
											._a()
										._li();
								}
								html
									._ul();
							}
						}
					})
				._li()
				;
		}
	}

	public BetaButtonGroup getBetaButtonGroup() {
		return betaButtonGroup;
	}

	public void setBetaButtonGroup(BetaButtonGroup betaButtonGroup) {
		this.betaButtonGroup = betaButtonGroup;
	}
}
