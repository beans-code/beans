package net.beanscode.web.view.ui.badge;

import static org.rendersnake.HtmlAttributesFactory.class_;

import java.io.IOException;

import org.rendersnake.HtmlCanvas;

import com.google.gson.annotations.Expose;

public class BadgeWarning extends Badge {

	public BadgeWarning() {
		
	}
	
	public BadgeWarning(String text) {
		super(text, Badge.WARNING);
	}
}
