package net.beanscode.web.view.dataset;

import java.io.IOException;

import mjson.Json;
import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.cli.datasets.Table;
import net.beanscode.pojo.tables.ConnectorLink;
import net.beanscode.web.view.ui.BeansComponent;
import net.hypki.libs5.db.db.weblibs.utils.UUID;

import org.rendersnake.HtmlCanvas;

import com.google.gson.JsonElement;
import com.google.gson.annotations.Expose;

public class TableConnectorsPanel extends BeansComponent {
	
	@Expose
	private UUID tableId = null; 
	
	private Table table = null;

	public TableConnectorsPanel() {
		
	}
	
	public TableConnectorsPanel(Table table) {
		setTable(table);
		setTableId(table.getId());
	}
	
	@Override
	public void renderOn(HtmlCanvas html) throws IOException {
		ApiCallCommand cmd = new ApiCallCommand(this)
			.setApi("api/table/connector/list")
			.addParam("tableId", getTableId().getId())
			.run();
		
		for (JsonElement link : cmd.getResponse().getAsJson().getAsJsonArray()) {
			html
				.span()
					.content(link.toString())
					.br();
		}
	}

	public UUID getTableId() {
		return tableId;
	}

	public void setTableId(UUID tableId) {
		this.tableId = tableId;
	}

	private Table getTable() {
		return table;
	}

	private void setTable(Table table) {
		this.table = table;
	}
}
