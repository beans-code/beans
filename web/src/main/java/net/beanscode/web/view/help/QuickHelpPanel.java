package net.beanscode.web.view.help;

import java.io.IOException;

import org.rendersnake.HtmlCanvas;

import net.beanscode.cli.web.utils.MarkdownUtils;
import net.beanscode.web.view.ui.BeansComponent;
import net.beanscode.web.view.ui.CardPanel;
import net.hypki.libs5.utils.reflection.SystemUtils;

public class QuickHelpPanel extends CardPanel {

	public QuickHelpPanel() {
		
	}

	@Override
	protected void renderTitle(HtmlCanvas html) throws IOException {
		html
			.h4()
				.content("Quick help");
	}

	@Override
	protected void renderContent(HtmlCanvas html) throws IOException {
		String txt = SystemUtils.readFileContent(QuickHelpPanel.class, QuickHelpPanel.class.getSimpleName() + ".markdown");
		
		html
			.write(MarkdownUtils.toHtml(txt), false);
	}

	@Override
	protected void renderFooter(HtmlCanvas html) throws IOException {
		
	}
}
