package net.beanscode.web.view.dataset;

import static net.hypki.libs5.utils.utils.AssertUtils.assertTrue;
import static org.rendersnake.HtmlAttributesFactory.class_;
import static org.rendersnake.HtmlAttributesFactory.id;

import java.io.IOException;

import javax.ws.rs.core.SecurityContext;

import org.rendersnake.HtmlCanvas;
import org.rendersnake.Renderable;

import com.google.gson.annotations.Expose;

import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.cli.datasets.Table;
import net.beanscode.cli.users.User;
import net.beanscode.pojo.tables.ColumnDef;
import net.beanscode.web.view.ui.BetaButtonGroup;
import net.beanscode.web.view.ui.ChooseComponent;
import net.beanscode.web.view.ui.Link;
import net.beanscode.web.view.ui.SpinnerLabel;
import net.beanscode.web.view.ui.TabsBetaComponent;
import net.beanscode.web.view.ui.modals.InputType;
import net.beanscode.web.view.ui.modals.ModalWindow;
import net.beanscode.web.view.ui.modals.OpBeansError;
import net.beanscode.web.view.ui.modals.OpBeansInfo;
import net.beanscode.web.view.ui.modals.OpModal;
import net.beanscode.web.view.ui.modals.OpModalQuestion;
import net.beanscode.web.view.upload.UploadView;
import net.beanscode.web.view.users.PermissionPanel;
import net.hypki.libs5.cli.noninteractivecli.CLIException;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.pjf.components.Component;
import net.hypki.libs5.pjf.components.buttons.Button;
import net.hypki.libs5.pjf.components.buttons.ButtonGroup;
import net.hypki.libs5.pjf.components.buttons.ButtonSize;
import net.hypki.libs5.pjf.components.ops.OpComponentClick;
import net.hypki.libs5.pjf.op.OpDialogClose;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.pjf.op.OpRemove;
import net.hypki.libs5.pjf.op.OpReplace;
import net.hypki.libs5.pjf.op.OpSet;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.utils.url.Params;
import net.hypki.libs5.utils.utils.AssertUtils;


public class TableTabs extends TabsBetaComponent {
	
	@Expose
	private UUID tableId = null;
	
	private Table table = null;

	public TableTabs() {
		
	}
	
	public TableTabs(Component parent, Table table) {
		super(parent);
		setTable(table);
	}

	public Table getTable() throws CLIException, IOException {
		if (table == null) {
			String tabId = getParams().getString("tableId");
			if (tabId != null) {
				table = new ApiCallCommand(this)
					.setApi("api/table/get")
					.addParam("table-id", tabId)
					.run()
					.getResponse()
					.getAsObject(Table.class);
			}
		}
		return table;
	}

	public TableTabs setTable(Table table) {
		this.table = table;
		setTableId(table.getId());
		return this;
	}

	@Override
	public int getTabsCount() {
		return 7;
	}
	
	@Override
	protected OpList getAdditionalOpListOnTabClick(int tabIndex) {
		if (tabIndex == 1)
			return new OpList()
					.add(new OpComponentClick(this, "onLoadColumns", new Params().add("tableId", getTableId())));
		else if (tabIndex == 2)
			return new OpList()
					.add(new OpComponentClick(this, "onLoadData", new Params().add("tableId", getTableId())));
		else if (tabIndex == 4)
			return new OpList()
					.add(new OpComponentClick(this, "onLoadMeta", new Params().add("tableId", getTableId())));
		else if (tabIndex == 5)
			return new OpList()
					.add(new OpComponentClick(this, "onLoadPermissions", new Params().add("tableId", getTableId())));
		else if (tabIndex == 6)
			return new OpList()
					.add(new OpComponentClick(this, "onLoadConnectors", new Params().add("tableId", getTableId())));

		return null;
	}

	@Override
	public String getTabName(int tabIndex) {
		switch (tabIndex) {
			case 0: return "General info";
			case 1: return "Columns";
			case 2: return "Data";
			case 3: return "Upload";
			case 4: return "Meta";
			case 5: return "Permissions";
			case 6: return "Connectors";
		}
		return "";
	}
	
	private OpList onLoadColumns() throws IOException {
		return new OpList()
			.add(new OpReplace("#" + "table-columns-" + getTable().getId(), 
					new TableColumnsTableComponent(getTable()).toHtml()));
	}
	
	private OpList onLoadData() throws IOException {
		return new OpList()
			.add(new OpReplace("#" + "table-data-" + getTable().getId(), 
					new TableDataViewPanel(TableTabs.this, table.getId()).toHtml()));
	}
	
	private OpList onLoadMeta() throws IOException {
		return new OpList()
			.add(new OpReplace("#" + "table-meta-" + getTable().getId(), new MetaTableComponent(TableTabs.this, table).toHtml()));
	}
	
	private OpList onRenameTableSave() throws IOException {
		new ApiCallCommand(this)
			.setApi("api/table/name")
			.addParam("name", getParams().getString("newTableName"))
			.addParam("tableId", getTable().getId().getId())
			.run();
		
		return new OpList()
				.add(new OpBeansInfo(getParams().getString("newTableName")));
	}
	
	private OpList onRenameTableOK() throws IOException {
		ModalWindow modal = new ModalWindow(this);
		modal
				.add("New name", InputType.STRING, "newTableName", getTable().getName())
				.add(new Button("Save")
						.add(new OpComponentClick(getClass(), 
								modal.getId(),
								"onRenameTableSave",
								getParams()))
						.add(modal.onClose()));
		
		return new OpList()
				.add(new OpModal(modal));
	}
	
	private OpList onLoadPermissions() throws IOException {
		if (getTable().getUserId().equals(getUserId()))
			return new OpList()
				.add(new OpReplace("#" + "table-permissions-" + getTable().getId(), 
						new PermissionPanel(TableTabs.this, getTable().getId(), "Table").toHtml()));
		else
			return new OpList()
				.add(new OpReplace("#" + "table-permissions-" + getTable().getId(), 
						new HtmlCanvas()
							.span()
								.content("Only owner can change the Table's permissions")
							.toHtml()));
	}
	
	private OpList onLoadConnectors() throws IOException {
		return new OpList()
			.add(new OpReplace("#" + "table-connectors-" + getTable().getId(), 
					new TableConnectorsPanel(getTable()).toHtml()));
	}
	
	private boolean isWriteAllowed() {
		try {
			return new ApiCallCommand(this)
						.setApi("api/table/isWriteAllowed")
						.addParam("userId", getUserId().toString())
						.addParam("tableId", table.getId().getId())
						.run()
						.getResponse()
						.getAsBoolean("writeAllowed");
		} catch (CLIException | IOException e) {
			LibsLogger.error(TableBetaPanel.class, "Cannot check the permission", e);
			return false;
		}
	}
	
	private boolean isDeleteAllowed() {
		try {
			return new ApiCallCommand(this)
					.setApi("api/table/isDeleteAllowed")
					.addParam("userId", getUserId().toString())
					.addParam("tableId", table.getId().getId())
					.run()
					.getResponse()
					.getAsBoolean("deleteAllowed");
		} catch (CLIException | IOException e) {
			LibsLogger.error(TableBetaPanel.class, "Cannot check the permission", e);
			return false;
		}
	}

	@Override
	public Renderable getTabHtml(int tabIndex) throws IOException {
		final boolean isWriteAllowed = isWriteAllowed();
		final boolean isDeleteAllowed= isDeleteAllowed();
		
		switch (tabIndex) {
			case 0:
				return new Renderable() {
					@Override
					public void renderOn(HtmlCanvas html) throws IOException {
						User user = new ApiCallCommand(getSessionBean())
										.setApi("api/user/get")
										.addParam("userId", table.getUserId().getId())
										.run()
										.getResponse()
										.getAsObject(User.class);
						html
							.dl(class_("row"))
								.dt(class_("col-sm-3"))
									.content("Owner")
								.dd(class_("col-sm-9"))
									.content(user != null ? user.getEmail() : "")
								.dt(class_("col-sm-3"))
									.content("Creation date")
								.dd(class_("col-sm-9"))
									.content(table.getCreate().toStringHuman())
								.dt(class_("col-sm-3"))
									.content("ID")
								.dd(class_("col-sm-9"))
									.content(table.getId().getId())
								.dt(class_("col-sm-3"))
									.content("Imported file")
								.dd(class_("imported-file col-sm-9"))
									.content(table.getImportPath())
								.dt(class_("col-sm-3"))
									.content("Data indexed")
								.dd(class_("index-status col-sm-9"))
									.render(new ButtonGroup()
											.setButtonSize(ButtonSize.EXTRA_SMALL)
											.addButton(new Button()
													.setAwesomeicon("fa fa-sync")
													.add(new OpComponentClick(TableTabs.this, "onIndexStatusRefresh", new Params().add("tableId", table.getId().getId())))))
									.span(class_("text"))
										.content(" " + table.getSearchStatus().toString(true))
								._dd()
								.dt(class_("col-sm-3"))
									.content("Validation check")
								.dd(class_("validation-check col-sm-9"))
									.render(new ButtonGroup()
											.setButtonSize(ButtonSize.EXTRA_SMALL)
											.addButton(new Button()
													.setAwesomeicon("fa fa-sync")
													.add(new OpComponentClick(TableTabs.this, "onValidate", new Params().add("tableId", table.getId().getId())))))
									.span(class_("text"))
										.content(" Not validated yet")
								._dd()
//								.dt(class_("col-sm-3"))
//									.content("Connectors")
//								.dd(class_(" col-sm-9"))
//									.content(table.getConnectorList().toString().replaceAll("\\n", "<br/>"), false)
	//							._dd()
									
								.dt(class_("col-sm-3"))
									.content("Data")
								.dd(class_(" col-sm-9"))
									.render(new Link("Download data", "/view/table/data?tableId=" + table.getId().getId()))
//									.a(href()
//											.target("_blank"))
//										.content("Download data")
								._dd()
							._dl();
						
						html
							.render(new BetaButtonGroup()
//								.setButtonSize(ButtonSize.SMALL)
								.add(new Button("Data")
										.addButton(new Button("Remove table")
	//											.setVisible(isDeleteAllowed)
												.add(new OpModalQuestion("Removing the table", 
														"Are you sure you want to <b>DELETE</b> the table: " + table.getName(), 
														new OpComponentClick(TableTabs.this, 
																"remove",
						                                		new Params()
						                            				.add("datasetId", table.getDatasetId().getId())
						                            				.add("tableId", table.getId().getId())))))
												
										.addButton(new Button("Clear table")
												.setVisible(isWriteAllowed)
												.add(new OpModalQuestion("Clearing table", 
														"Do you want to clear this table? <b/>WARNING: This operation cannot be reverted.", 
														new OpComponentClick(TableTabs.this, 
																"clearTable",
						                        				new Params()
							                        				.add("datasetId", table.getDatasetId().getId())
							                        				.add("tableId", table.getId().getId())))
												))
												
										.addButton(new Button("Rename table")
												.setVisible(isWriteAllowed)
												.add(new OpModalQuestion("Rename table", 
														"Do you want to rename this table? <b/>WARNING: This operation cannot be reverted.", 
														new OpComponentClick(TableTabs.this, 
																"onRenameTableOK",
						                        				new Params()
							                        				.add("datasetId", table.getDatasetId().getId())
							                        				.add("tableId", table.getId().getId())))
												))
										
										.addButton(new Button("Optimize table")
												.setVisible(isWriteAllowed)
												.add(new OpModalQuestion("Optimize table", 
														"Do you want to optimize this table? <b/>REMARK: This operation can take some "
														+ "time depending on the Table's size", 
														new OpComponentClick(TableTabs.this, 
																"onOptimizeTable",
						                        				new Params()
							                        				.add("datasetId", table.getDatasetId().getId())
							                        				.add("tableId", table.getId().getId())))
												))
//										.addButton(new Button("Download data")
//												.add(new OpComponentClick(TableTabs.this, "onDownload",
//														new Params()
//					                        				.add("tableId", table.getId().getId()))))
										)
								.add(new Button("Search")
										.addButton(new Button("Index data")
												.add(new OpModalQuestion("Indexing table", 
														"Do you want to index this table? <b/>INFO: This operation may "
																+ "take a while depending on the size of the table.",
														new OpComponentClick(TableTabs.this, 
																"indexTable",
						                        				new Params()
							                        				.add("datasetId", table.getDatasetId().getId())
							                        				.add("tableId", table.getId().getId())))
												)
										))
							);
					}
				};
			case 1:
				return new Renderable() {
					@Override
					public void renderOn(HtmlCanvas html) throws IOException {
						html
							.render(new SpinnerLabel("Loading columns...")
									.setId("table-columns-" + getTable().getId()));
					}
				};
			case 2:
				return new Renderable() {
					@Override
					public void renderOn(HtmlCanvas html) throws IOException {
						html
							.render(new SpinnerLabel("Loading table...")
									.setId("table-data-" + getTable().getId()))
							;
					}
				};
			case 3:
				return new Renderable() {
					@Override
					public void renderOn(HtmlCanvas html) throws IOException {
						if (isWriteAllowed) {
							
	//						ConnectorLink c = table.getConnectorList().getConnectorsLinks().size() > 0 ? table.getConnectorList().getConnectorsLinks().get(0) : null;
							html
								.span()
									.content("Driver")
								.render(new ChooseComponent(TableTabs.this, "Cassandra", "MapDB", "Plain")
	//									.setSelectedOption(c != null && c.getConnectorClass() != null ? c.getConnectorInstance().getName() : "")
										.addOpsOnChange(new OpComponentClick(TableTabs.this, "changeDriver")))
								.div(id("dropzone-" + getId()).action(UploadView.UPLOAD_URL).class_("dropzone no-image"))
	//												<input type="file" name="file" />
	//												.input(type("file").name("file"))
	//												.div(HtmlAttributesFactory)
	//												.input(type("hidden").name("datasetId").value(table.getDatasetId().getId()))
									.span()
										.content("<b>Drop a file</b> to upload (<b>or click</b>)", false)
									.br()
								._div()
								;
						} else {
							html
								.span()
									.content("Table is read-only. Uploading new data is forbidden.");
						}
					}
				};
			case 4:
				return new Renderable() {
					@Override
					public void renderOn(HtmlCanvas html) throws IOException {
						html
							.render(new SpinnerLabel("Loading meta parameters...")
									.setId("table-meta-" + getTable().getId()))
							;
					}
				};
			case 5:
				return new Renderable() {
					@Override
					public void renderOn(HtmlCanvas html) throws IOException {
						html
							.render(new SpinnerLabel("Loading permissions...")
									.setId("table-permissions-" + getTable().getId()))
							;
					}
				};
			case 6:
				return new Renderable() {
					@Override
					public void renderOn(HtmlCanvas html) throws IOException {
						html
							.render(new SpinnerLabel("Loading connectors list...")
									.setId("table-connectors-" + getTable().getId()))
							;
					}
				};
		}
		return null;
	}


	public OpList remove() throws IOException, ValidationException {
		String tableId = getParams().getString("tableId");
		
		assertTrue(tableId != null, "Table ID is not specified");
		
//		new TableRemove(getSessionBean()).run(tableId);
		new ApiCallCommand(this)
			.setApi("api/table/delete")
			.addParam("table-id", tableId)
			.addParam("in-background", "true")
			.run();
		
//		if (removed) {
			return new OpList()
				.add(new OpRemove(".table-" + tableId))
				.add(new OpBeansInfo("Table removed"));
//		} else {
//			return new OpList()
//				.add(new OpBeansError("Error", "Removing table " + tableId + " failed. Try again later."));
//		}
	}
	
	private OpList onOptimizeTable() throws IOException, ValidationException {
		String tableId = getParams().getString("tableId");
		
		assertTrue(tableId != null, "Table ID is not specified");
		
		new ApiCallCommand(this)
			.setApi("api/table/optimize")
			.addParam("tableId", tableId)
			.run();
		
		return new OpList()
			.add(new OpBeansInfo("Table scheduled for optimization in the background"));
	}
	
	private OpList clearTable(SecurityContext sc) throws IOException, ValidationException {
		String tableId = getParams().getString("tableId");
		
		assertTrue(tableId != null, "Table ID is not specified");
		
		boolean removed = new ApiCallCommand(this)
							.setApi("api/table/clear")
							.addParam("tableId", tableId)
							.run()
							.getResponse()
							.getAsBoolean("cleared");
		
		if (removed) {
			return new OpList()
				.add(new OpSet("#" + getId() + " .imported-file", ""))
				.add(new OpSet("#" + getId() + " .values", ""))
				.add(new OpBeansInfo("Table cleared"));
		} else {
			return new OpList()
				.add(new OpBeansError("Clearing table " + tableId + " failed. Try again later."));
		}
	}
	
	public OpList columnsFilter() throws IOException {
		final String filter = getParams().getString("column-filter");
		
		Table table = getTable();// getTab Table.getTable(new UUID(tableId));
		
		AssertUtils.assertTrue(table != null, "Table was not found");
		
		return new OpList()
			.add(new OpSet("#" + table.getId() + " .values", new HtmlCanvas()
				.render(createColumnNames(table, filter)).toHtml()))
				;
	}
	
	public static Renderable createColumnNames(final net.beanscode.cli.datasets.Table table, final String filter) {
		return new Renderable() {
			@Override
			public void renderOn(HtmlCanvas html) throws IOException {
				String [] filters = filter.toLowerCase().split("[\\s]+");
				if (table.getColumns() != null)
					for (ColumnDef col : table.getColumns()) {
						String colName = col.getName();
						String colDescription = col.getDescription();
						String colType = col.getType().toString();
						
						if (filterFulfilled(filters, colName, colDescription, colType)) {
							html
								.dl(class_("dl-horizontal"))
									.dt()
										.content(colName)
									.dd()
										.content(String.format("%s (%s)", colDescription != null ? colDescription : "", colType))
								._dl();
						}
						
	//					if (nullOrEmpty(filterLower)
	//							|| colName.toLowerCase().contains(filterLower)
	//							|| colDescription.toLowerCase().contains(filterLower)
	//							|| colType.toLowerCase().contains(filterLower))
							
					}
			}
		};
	}
	
	private static boolean filterFulfilled(String [] filters, String colName, String colDescription, String colType) {
		for (String filter : filters) {
			if (StringUtilities.nullOrEmpty(filter) == false
					&& colName.toLowerCase().contains(filter) == false
					&& (colDescription == null || (colDescription != null && colDescription.toLowerCase().contains(filter) == false))
					&& colType.toLowerCase().contains(filter) == false)
				return false;
		}
		return true;
	}

	public UUID getTableId() {
		if (tableId == null && this.table != null)
			return table.getId();
		return tableId;
	}

	public void setTableId(UUID tableId) {
		this.tableId = tableId;
	}
}
