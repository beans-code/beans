package net.beanscode.web.view.notebook;

import static net.hypki.libs5.utils.string.StringUtilities.nullOrEmpty;

import java.io.IOException;

import org.rendersnake.HtmlCanvas;

import com.google.gson.annotations.Expose;

import net.beanscode.cli.BeansCLISettings;
import net.beanscode.cli.notebooks.Notebook;
import net.beanscode.cli.notebooks.NotebookEntryGateway;
import net.beanscode.cli.notebooks.NotebookGateway;
import net.beanscode.cli.notebooks.NotebookSearchResults;
import net.beanscode.pojo.NotebookEntry;
import net.beanscode.web.view.ui.BeansComponent;
import net.beanscode.web.view.ui.Link;
import net.beanscode.web.view.ui.ListComponent;
import net.hypki.libs5.cli.noninteractivecli.CLIException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.pjf.op.Op;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.search.SearchResults;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.string.StringUtilities;

public class EntriesListComponent extends ListComponent {
	
	@Expose
	private OpList onClick = null;
	
	@Expose
	private UUID notebookId = null;
	
	private Notebook notebook = null;
	private SearchResults<NotebookEntry> entriesCache = null;

	public EntriesListComponent() {
		
	}
	
	public EntriesListComponent(BeansComponent parent) {
		super(parent);
	}
	
	protected Notebook getNotebook() {
		if (notebook == null) {
			try {
				notebook = NotebookGateway.getNotebook(this.getSessionBean(), getNotebookId());
			} catch (CLIException | IOException e) {
				LibsLogger.error(NotebooksTableComponent.class, "Cannot search for Notebooks", e);
			}
		}
		return notebook;
	}
	
	@Override
	protected String getSettingsPrefix() {
		return "notebook-entries-";
	}
	
	@Override
	protected String getFilterTooltip() {
		return "Filter entries...";
	}
	
	@Override
	protected long getMaxHits() throws IOException {
		return getEntries().size();
	}
	
	private SearchResults<NotebookEntry> getEntries() {
		if (entriesCache == null) {
			try {
				entriesCache = NotebookEntryGateway.searchEntries(this.getSessionBean(), getNotebookId().getId(), 
						getFilter(), getPage(), getPerPage());
			} catch (CLIException | IOException e) {
				LibsLogger.error(NotebooksTableComponent.class, "Cannot search for Notebooks", e);
				entriesCache = new SearchResults();
			}
		}
		return entriesCache;
	}
	
	protected void renderAfterItem(NotebookEntry ne, HtmlCanvas html) throws IOException {
		
	}
	
	@Override
	protected void renderItem(int idx, HtmlCanvas html) throws IOException {
		NotebookEntry ne = getEntries().getObjects().get(idx % getPerPage());
//		NotebookEntry ne = NotebookEntryGateway.getNotebookEntry(getSessionBean(), n);
		
		if (ne != null) {
			html
				.render(new Link(!nullOrEmpty(ne.getName()) ? ne.getName() : "<no name>", 
						getOnClick().addParamParam("entryId", ne.getId().getId())));
		
			renderAfterItem(ne, html);
		}
	}

	public OpList getOnClick() {
		if (onClick == null)
			onClick = new OpList();
		return onClick;
	}

	public EntriesListComponent setOnClick(OpList onClick) {
		this.onClick = onClick;
		return this;
	}
	
	public EntriesListComponent setOnClick(Op onClick) {
		getOnClick().add(onClick);
		return this;
	}

	public UUID getNotebookId() {
		return notebookId;
	}

	public EntriesListComponent setNotebookId(UUID notebookId) {
		this.notebookId = notebookId;
		return this;
	}
}
