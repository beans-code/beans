package net.beanscode.web.view.plots;

import static org.rendersnake.HtmlAttributesFactory.id;
import static org.rendersnake.HtmlAttributesFactory.name;
import static org.rendersnake.HtmlAttributesFactory.type;

import java.io.IOException;

import javax.ws.rs.core.SecurityContext;

import org.rendersnake.HtmlCanvas;
import org.rendersnake.Renderable;

import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.cli.notebooks.GnuplotEntry;
import net.beanscode.cli.notebooks.LinkEntry;
import net.beanscode.web.view.notebook.NotebookEntryEditorPanel;
import net.hypki.libs5.pjf.components.buttons.Button;
import net.hypki.libs5.pjf.components.ops.OpComponentTimer;
import net.hypki.libs5.pjf.op.OpJs;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.api.APICallType;
import net.hypki.libs5.utils.url.Params;

public class LinksEntryEditorPanel extends NotebookEntryEditorPanel {

	@Override
	public Renderable getView() {
		// TODO Auto-generated method stub
		return null;
	}
	
	protected LinkEntry getLinkEntry() {
		return (LinkEntry) getNotebookEntry();
	}

	@Override
	protected Renderable getEditor() {
		return new Renderable() {
			@Override
			public void renderOn(HtmlCanvas html) throws IOException {
				html
					.input(name("link-notebook")
							.class_("link-notebook form-control")
							.type("text")
							.value(getLinkEntry().getName())
							.add("placeholder", "Notebook query")
							.onKeyup(new OpComponentTimer(LinksEntryEditorPanel.this, "onEntriesReload", 500).toStringOnclick()))
					
					.input(name("link-entry")
							.class_("link-entry form-control")
							.type("text")
							.value(getLinkEntry().getName())
							.add("placeholder", "Entry query")
							.onKeyup(new OpComponentTimer(LinksEntryEditorPanel.this, "onEntriesReload", 500).toStringOnclick()))
					;
			}
		};
	}
	
	private OpList onEntriesReload() {
		String notebokQuery = getParams().getString("link-notebook");
		String entryQuery = getParams().getString("link-entry");
		
//		new ApiCallCommand(this)
//			.setApi("api/notebook/entry/query")
//			.addParam(notebokQuery, entryQuery)
		
		return new OpList();
	}

	@Override
	protected Button getMenu() throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected OpList opOnPlay(SecurityContext sc, Params params) throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected OpList opOnEdit(SecurityContext sc, Params params) throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected OpList opOnStop(SecurityContext sc, Params params) throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected OpList opOnRemove(SecurityContext sc, Params params) throws IOException {
		try {
			new ApiCallCommand(this)
				.setApi("api/notebook/entry/remove")
				.setApiCallType(APICallType.DELETE)
				.addParam("entryId", params.getString("entryId"))
				.run();
		} catch (IOException e) {
			LibsLogger.error(PlotPanel.class, "Cannot remove Notebook Entry", e);
		}
		
		return new OpList();
	}

	@Override
	protected OpList onRefresh(SecurityContext sc, Params params) throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

}
