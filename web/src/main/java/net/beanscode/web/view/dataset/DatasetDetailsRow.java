package net.beanscode.web.view.dataset;

import static org.rendersnake.HtmlAttributesFactory.class_;
import static org.rendersnake.HtmlAttributesFactory.colspan;
import static org.rendersnake.HtmlAttributesFactory.id;

import java.io.IOException;

import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.cli.users.User;
import net.beanscode.pojo.dataset.Dataset;
import net.hypki.libs5.pjf.components.Component;

import org.rendersnake.HtmlCanvas;

public class DatasetDetailsRow extends Component {
	
	private Dataset dataset = null;
	
	public DatasetDetailsRow() {
		
	}
	
	public DatasetDetailsRow(Dataset dataset) {
		setDataset(dataset);
	}

	@Override
	public void renderOn(HtmlCanvas html) throws IOException {
		User user = new ApiCallCommand(this.getSessionBean())
						.setApi("api/user/get")
						.addParam("userId", dataset.getUserId())
						.run()
						.getResponse()
						.getAsObject(User.class);
		
		html
			.tr(id("tr-ds-detail-" + dataset.getId()))
				.td(colspan("4"))
					.dl(class_("dl-horizontal"))
						.dt()
							.content("ID")
						.dd()
							.content(dataset.getId().getId())
						.dt()
							.content("Owner")
						.dd()
							.content(user != null ? user.getEmail() : "")
					._dl()
				._td()
			._tr()
			;
	}

	public Dataset getDataset() {
		return dataset;
	}

	public void setDataset(Dataset dataset) {
		this.dataset = dataset;
	}

}
