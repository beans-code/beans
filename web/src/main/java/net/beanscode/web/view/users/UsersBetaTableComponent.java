package net.beanscode.web.view.users;

import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;
import static net.hypki.libs5.utils.utils.AssertUtils.assertTrue;
import static org.rendersnake.HtmlAttributesFactory.class_;

import java.io.IOException;
import java.util.List;

import javax.ws.rs.core.SecurityContext;

import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.cli.users.User;
import net.beanscode.cli.users.UserList;
import net.beanscode.cli.users.UsersSearchResult;
import net.beanscode.web.BeansPage;
import net.beanscode.web.view.ui.BeansComponent;
import net.beanscode.web.view.ui.BetaButtonGroup;
import net.beanscode.web.view.ui.BetaTableComponent;
import net.beanscode.web.view.ui.modals.InputType;
import net.beanscode.web.view.ui.modals.ModalWindow;
import net.beanscode.web.view.ui.modals.OpBeansError;
import net.beanscode.web.view.ui.modals.OpBeansInfo;
import net.beanscode.web.view.ui.modals.OpModal;
import net.beanscode.web.view.ui.modals.OpModalQuestion;
import net.hypki.libs5.cli.noninteractivecli.CLIException;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.pjf.components.buttons.Button;
import net.hypki.libs5.pjf.components.buttons.ButtonGroup;
import net.hypki.libs5.pjf.components.ops.OpComponentClick;
import net.hypki.libs5.pjf.components.ops.OpComponentTimer;
import net.hypki.libs5.pjf.op.OpDialogClose;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.url.Params;

import org.rendersnake.HtmlCanvas;

public class UsersBetaTableComponent extends BetaTableComponent {
	
	
	private UsersSearchResult users = null;
	
	public UsersBetaTableComponent() {
		
	}
	
	public UsersBetaTableComponent(BeansComponent parent) {
		super(parent);
	}
	
	@Override
	protected String getSettingsPrefix() {
		return "users-table-";
	}

	private OpList onCreateUser() throws IOException, ValidationException {
		ModalWindow modal = new ModalWindow();
		
		modal.setTitle("Creating new user");
		
		modal.add("E-mail", InputType.STRING, "email", "");
		modal.add("Password", InputType.PASSWORD, "pass1", "");
		modal.add("Password (repeated)", InputType.PASSWORD, "pass2", "");
		
		modal.add(new Button("Save")
				.add(new OpComponentClick(this.getClass(), modal.getId(), "onCreateUserSave"))
				.add(modal.onClose()));
		modal.add(new Button("Cancel")
				.add(modal.onClose()));
		
//		modal
//			.getContent()
//				.render(new NewUserPanel());
		
		return new OpList()
			.add(new OpModal(modal));
	}
	
	private OpList onCreateUserSave(SecurityContext sc, Params params) throws IOException, ValidationException {
		final String email = params.getString("email");
		final String pass1 = params.getString("pass1");
		final String pass2 = params.getString("pass2");
		
		assertTrue(notEmpty(pass1), "Password can not be empty");
		assertTrue(notEmpty(pass2), "Password 2 can not be empty");
		assertTrue(pass1.equals(pass2), "Passwords do not match");
		
		User newUser = new ApiCallCommand(this)
						.setApi("api/user/create")
						.addParam("email", email)
						.addParam("pass", pass1)
						.run()
						.getResponse()
						.getAsObject(User.class);
		
		if (newUser != null) {
			return new OpList()
//				.add(new OpPrepend(".users-panel-table tbody", 
//						UsersTable.createUserRow(this, newUser, new HtmlCanvas()).toHtml()))
				.add(new OpComponentTimer(this, "onRefresh", 500))
				.add(new OpDialogClose())
				.add(new OpBeansInfo("User created successfully"));
		} else {
			return new OpList()
				.add(new OpBeansError("Cannot create user, internal error"));
		}
	}

	public UsersSearchResult getUsers() {
		if (users == null) {
			try {
				users = new UserList(getSessionBean()).run(getFilter(), 
						getPage() * getPerPage(), 
						getPerPage());
			} catch (IOException e) {
				LibsLogger.error(UsersBetaTableComponent.class, "Cannot search for users", e);
				users = new UsersSearchResult();
			}
		}
		return users;
	}
	
	@Override
	protected boolean isHeaderColumnVisible() throws IOException {
		return true;
	}

	public OpList onChangeStatus(SecurityContext sc, Params params) {
		final String userToBlock = params.getString("userToBlock");
		final String newStatus = params.getString("newStatus");
		final OpList ops = new OpList();
		
		try {
			new ApiCallCommand(this)
				.setApi("api/user/status")
				.addParam("userId", userToBlock)
				.addParam("newStatus", newStatus)
				.run();
			
			ops
				.add(new OpComponentTimer(this, "onRefresh", 500))
				.add(new OpBeansInfo("Changes saved"));
		} catch (Exception e) {
			LibsLogger.error(getClass(), String.format("Cannot change status for user %s for the status %s", userToBlock, newStatus));
			ops.add(new OpBeansError("cannot change user's status: " + e.getMessage()));
		}
		
		return ops;
	}
	
	@Override
	protected String getColumnName(int column) throws IOException {
		if (column == 0)
			return "Email";
		else if (column == 1)
			return "ID";
		else if (column == 2)
			return "Status";
		else if (column == 3)
			return "Creation date";
		else if (column == 4)
			return "Administrator";
		else if (column == 5)
			return "Options";
		else
			return "";
	}
	
	@Override
	protected long getMaxHits() throws IOException {
		return getUsers().getMaxHits();
	}
	
	@Override
	protected int getNumberOfColumns() throws IOException {
		return 6;
	}
	
	@Override
	protected boolean isExapandable() throws IOException {
		return false;
	}
	
	@Override
	protected void renderExpandable(int row, HtmlCanvas html) throws IOException {
		
	}
	
	@Override
	protected void renderTitle(HtmlCanvas html) throws IOException {
		html
			.write("Users");
	}
	
	@Override
	protected void renderCell(int row, int column, HtmlCanvas html) throws IOException {
		User user = getUsers().getUsers().get(row % getPerPage());
		
		if (column == 0)
			html
				.write(user.getEmail());
		else if (column == 1)
			html
				.write(user.getId().getId());
		else if (column == 2)
			html
				.span(class_("status"))
					.content(user.getStatus().toString());
		else if (column == 3)
			html
				.write(user.getCreationDate().toStringHuman());
		else if (column == 4)
			html
				.write(new ApiCallCommand(this)
					.setApi("api/user/allowed")
					.addParam("user-email", user.getEmail())
					.addParam("right", BeansPage.RIGHT_ADMIN)
					.run()
					.getResponse()
					.getAsBoolean("allowed") ? "admin" : "", false);
		else if (column == 5) {
			html
				.render(new BetaButtonGroup()
							.add(new Button(user.isBlocked() ? "Unblock user" : "Block user")
									.add(new OpModalQuestion(user.isBlocked() ? "Unblock user" : "Block user", 
											"Are you sure you want to " + (user.isBlocked() ? "un" : "") + "block user: " + user.getEmail(), 
											new OpComponentClick(this, 
													"onChangeStatus",
													new Params()
														.add("userToBlock", user.getId().getId()) 
														.add("newStatus", user.isBlocked() ? "NORMAL" : "BLOCKED")))))
							.add(new Button("Edit")
									.add(new OpComponentClick(this, 
											"onEditOpen", 
											new Params()
												.add("userEmail", user.getEmail()))))
							.add(new Button("Reset")
									.add(new OpModalQuestion("Reseting password", 
											"Are you sure you want to reset password for " + user.getEmail() + "?", 
												new OpComponentClick(this, 
														"onResetPassword", 
														new Params()
															.add("userEmail", user.getId().getId())
												))))
							.add(new Button("Make admin")
									.add(new OpModalQuestion("Granting admin rights", 
											"Are you sure to add admin rights to user " + user.getEmail(), 
											new OpComponentClick(this, 
													"onMakeAdmin",
													new Params()
														.add("userEmail", user.getEmail())
												))))
						);
		}
	}
	
	private OpList onEditOpen() throws IOException, net.hypki.libs5.utils.utils.ValidationException {
		final String userEmail = getParams().getString("userEmail");
		
		ModalWindow modal = new ModalWindow(this);
		modal
			.setTitle("Editing user")
			.setContent(new NewUserPanel(userEmail))
			.add(new Button("Save")
					.add(new OpComponentClick(this.getClass(),
							modal.getId(),
							"onEditUser", 
							new Params()))
					.add(modal.onClose()))
			.add(new Button("Cancel")
					.add(modal.onClose()));
		
		return new OpList()
			.add(new OpModal(modal));
	}
	
	private OpList onEditUser(SecurityContext sc, Params params) throws IOException, net.hypki.libs5.utils.utils.ValidationException {
		final String userEmail = params.getString("email");
//		final String oldPass = params.getString("oldPass", UUID.random().getId());
		final String pass1 = params.getString("pass1");
		final String pass2 = params.getString("pass2");
		
		ApiCallCommand call = new ApiCallCommand(this)
			.setApi("api/user/update")
			.addParam("user-email", userEmail)
//			.addParam("old-pass", oldPass)
			.addParam("new-pass1", pass1)
			.addParam("new-pass2", pass2)
			.run();
		
//		HttpResponse resp = new UserPass(getSessionBean())
//			.run(userEmail, oldPass, pass1, pass2)
//			.getResponse();
		
		if (call.getResponse().isOK()) {
			return new OpList()
				.add(new OpDialogClose())
				.add(new OpBeansInfo("Data saved successfully"));
		} else {
			return new OpList()
				.add(new OpBeansError("Cannot edit user"));
		}
	}
	
	private OpList onMakeAdmin() throws CLIException, IOException {
		String userEmail = getParams().getString("userEmail");
		
		new ApiCallCommand(this)
			.setApi("api/user/createAdmin")
			.addParam("email", userEmail)
			.run();
		
		return new OpList()
			.add(new OpBeansInfo("Admin rights granted to user " + userEmail));
	}
	
	private OpList onResetPassword() throws CLIException, IOException {
		String user = getParams().getString("userEmail");
		
		new ApiCallCommand(this)
			.setApi("api/user/reset")
			.addParam("userId", user)
			.run();
		
		return new OpList()
			.add(new OpBeansInfo("New password has been send to the user"));
	}

	@Override
	protected boolean isFilterVisible() {
		return true;
	}

	@Override
	protected List<Button> getMenu() throws IOException {
		return new ButtonGroup()
			.addButton(new Button("New user")
						.add(new OpComponentClick(this, "onCreateUser")))
			.getButtons();
	}

	@Override
	protected String getTableCss() throws IOException {
		// TODO Auto-generated method stub
		return null;
	}
}
