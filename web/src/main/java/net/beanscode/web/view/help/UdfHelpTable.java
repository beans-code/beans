package net.beanscode.web.view.help;

import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;
import static org.rendersnake.HtmlAttributesFactory.class_;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import net.beanscode.cli.plugin.FuncPOJO;
import net.beanscode.cli.plugin.PluginFuncGetList;
import net.beanscode.web.view.ui.BeansComponent;
import net.beanscode.web.view.ui.BetaTableComponent;
import net.hypki.libs5.cli.noninteractivecli.CLIException;
import net.hypki.libs5.pjf.components.buttons.Button;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.string.StringUtilities;

import org.rendersnake.HtmlCanvas;

public class UdfHelpTable extends BetaTableComponent {
	
	private List<FuncPOJO> funcs = null;

	public UdfHelpTable() {
		
	}
	
	public UdfHelpTable(BeansComponent parent) {
		super(parent);
	}
	
	@Override
	protected String getSettingsPrefix() {
		return "udfs-table-";
	}
	
	private List<FuncPOJO> getFunc() {
		if (funcs == null) {
			try {
				funcs = new PluginFuncGetList(getSessionBean())
					.run(getFilter() != null ? getFilter().toLowerCase() : null);
			} catch (CLIException | IOException e) {
				LibsLogger.error(UdfHelpTable.class, "Cannot get UDF list", e);
				funcs = new ArrayList<FuncPOJO>();
			}
		}
		return funcs;
	}
	
	@Override
	protected long getMaxHits() throws IOException {
		return getFunc().size();
	}
	
	@Override
	protected int getNumberOfColumns() throws IOException {
		return 3;
	}
	
	@Override
	protected void renderTitle(HtmlCanvas html) throws IOException {
		html
			.write("User defined functions");
	}
	
	@Override
	protected String getColumnName(int col) throws IOException {
		if (col == 0)
			return "Plugin";
		else if (col == 1)
			return "UDF name";
		else
			return "Description";
	}
	
	protected String getTableCss() throws IOException {
		return null;
	};
	
	@Override
	protected boolean isExapandable() throws IOException {
		return false;
	}
	
	protected void renderExpandable(int row, HtmlCanvas html) throws IOException {
		
	};
	
	@Override
	protected boolean isHeaderColumnVisible() throws IOException {
		return true;
	}
	
	protected void renderCell(int row, int col, HtmlCanvas html) throws IOException {
		FuncPOJO func = getFunc().get(getPage() * getPerPage() + row);
		
		
		if (col == 0)
			html
				.span()
					.content(func.getPlugin());
		else if (col == 1)
			html
				.span()
					.content(func.getName());
		else {
			String help = func.getHelp();
			
			if (notEmpty(getFilter()))
				for (String what : StringUtilities.split(getFilter())) {
					if (notEmpty(what))
						help = StringUtilities.surround(help, 
								what, 
								"<span class=\"beans-highlight\">", 
								"</span>");
				}
			
			html
				.span()
					.content(help, false);
		}
	}

	@Override
	protected boolean isFilterVisible() {
		return true;
	}

	@Override
	protected List<Button> getMenu() {
		return null;
	}
}
