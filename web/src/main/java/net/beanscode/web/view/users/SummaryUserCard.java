package net.beanscode.web.view.users;

import static org.rendersnake.HtmlAttributesFactory.class_;

import java.io.IOException;

import net.beanscode.web.BeansWebConst;
import net.beanscode.web.view.ui.BeansComponent;
import net.beanscode.web.view.ui.BetaButtonGroup;
import net.beanscode.web.view.ui.CardPanel;
import net.hypki.libs5.pjf.components.buttons.Button;
import net.hypki.libs5.pjf.op.OpJs;

import org.rendersnake.HtmlCanvas;

public class SummaryUserCard extends CardPanel {

	public SummaryUserCard() {
		
	}
	
	public SummaryUserCard(BeansComponent parent) {
		super(parent);
	}
	
	@Override
	protected void renderTitle(HtmlCanvas html) throws IOException {
		html
			.span()
				.content(getUserEmail());
	}
	
	@Override
	protected void renderContent(HtmlCanvas html) throws IOException {
		html
			.dl(class_("row"))
				.dt(class_("col-sm-4"))
					.content("User ID")
				.dd(class_("col-sm-8"))
					.content(getUserId().toString(" - "))
					
				.dt(class_("col-sm-4"))
					.content("User email")
				.dd(class_("col-sm-8"))
					.content(getUserEmail())
			._dl();
	}
	
	@Override
	protected void renderFooter(HtmlCanvas html) throws IOException {
		html
			.render(new BetaButtonGroup()
					.add(new Button("Log out")
							.add(new OpJs("eraseCookie('" + BeansWebConst.COOKIE_LOGIN + "'); "
									+ "eraseCookie('" + BeansWebConst.COOKIE_USERID + "'); "
									+ "window.location = '/login';"))))
			;
	}
}
