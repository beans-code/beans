package net.beanscode.web.view.plugins;

import static org.rendersnake.HtmlAttributesFactory.id;

import java.io.IOException;

import org.apache.http.annotation.Obsolete;
import org.rendersnake.HtmlCanvas;

import net.beanscode.cli.BeansCliCommand;
import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.web.view.ui.BeansComponent;
import net.beanscode.web.view.ui.BetaButtonGroup;
import net.beanscode.web.view.ui.CardPanel;
import net.beanscode.web.view.ui.modals.OpBeansInfo;
import net.hypki.libs5.cli.noninteractivecli.CLIException;
import net.hypki.libs5.pjf.components.buttons.Button;
import net.hypki.libs5.pjf.components.ops.OpComponentClick;
import net.hypki.libs5.pjf.components.ops.OpComponentTimer;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.pjf.op.OpSet;
import net.hypki.libs5.utils.date.SimpleDate;

@Deprecated
public class PlugSelfTestPanel extends CardPanel {

	public PlugSelfTestPanel() {
		
	}
	
	public PlugSelfTestPanel(BeansComponent parent) {
		super(parent);
	}

	@Override
	protected void renderTitle(HtmlCanvas html) throws IOException {
		html
			.span()
				.content("Plugins self tests");
	}

	@Override
	protected void renderContent(HtmlCanvas html) throws IOException {
		html
			.textarea(id("progress")
					.class_("selftest-progress"))
			._textarea()
			.br()
			.render(new BetaButtonGroup()
					.add(new Button("Start self tests", 
							new OpComponentClick(this, "onSelfTestStart")))
					.add(new Button("Refresh", 
							new OpComponentClick(this, "onRefresh")))
					)
//			.write(new OpComponentTimer(this, "onRefresh", 1500).toStringAdhock(), false)
			;
	}

	@Override
	protected void renderFooter(HtmlCanvas html) throws IOException {
		html
			.span()
				.content("Be patient, this may take a while");
	}
	
	public OpList onSelfTestStart() throws CLIException, IOException {
		new ApiCallCommand(this)
				.setApi(BeansCliCommand.API_PLUGIN_SELFTEST_START)
				.run();
		return new OpList()
				.add(new OpBeansInfo("Self tests started"));
	}
	
	public OpList onRefresh() throws CLIException, IOException {
		String log = new ApiCallCommand(this)
			.setApi(BeansCliCommand.API_PLUGIN_SELFTEST_GET)
			.run()
			.getResponse()
			.getAsString("log");
		
		return new OpList()
				.add(new OpSet("#" + getId() + " #progress", log))
//				.add(new OpComponentTimer(this, "onRefresh", 1500))
				;
	}
}
