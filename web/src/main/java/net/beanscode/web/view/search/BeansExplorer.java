package net.beanscode.web.view.search;

import static org.rendersnake.HtmlAttributesFactory.class_;
import static org.rendersnake.HtmlAttributesFactory.id;

import java.io.IOException;
import java.text.BreakIterator;

import org.rendersnake.HtmlCanvas;
import org.rendersnake.Renderable;

import com.google.gson.annotations.Expose;

import net.beanscode.cli.datasets.Table;
import net.beanscode.cli.datasets.TableGateway;
import net.beanscode.cli.notebooks.Notebook;
import net.beanscode.cli.notebooks.NotebookEntryGateway;
import net.beanscode.cli.notebooks.NotebookGateway;
import net.beanscode.pojo.NotebookEntry;
import net.beanscode.pojo.dataset.Dataset;
import net.beanscode.web.view.dataset.ColumnsListComponent;
import net.beanscode.web.view.dataset.DatasetBetaContent;
import net.beanscode.web.view.dataset.DatasetGateway;
import net.beanscode.web.view.dataset.DatasetsListComponent;
import net.beanscode.web.view.dataset.TablesListComponent;
import net.beanscode.web.view.notebook.EntriesListComponent;
import net.beanscode.web.view.notebook.NotebookContent;
import net.beanscode.web.view.notebook.NotebookEntryEditorFactory;
import net.beanscode.web.view.notebook.NotebookEntryEditorPanel;
import net.beanscode.web.view.notebook.NotebooksBetaContent;
import net.beanscode.web.view.notebook.NotebooksListComponent;
import net.beanscode.web.view.ui.BeansComponent;
import net.beanscode.web.view.ui.Link;
import net.beanscode.web.view.ui.modals.ModalWindow;
import net.beanscode.web.view.ui.modals.OpModal;
import net.hypki.libs5.pjf.HtmlSpecial;
import net.hypki.libs5.pjf.components.buttons.Button;
import net.hypki.libs5.pjf.components.ops.OpComponentClick;
import net.hypki.libs5.pjf.op.Op;
import net.hypki.libs5.pjf.op.OpAppend;
import net.hypki.libs5.pjf.op.OpHide;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.pjf.op.OpSet;
import net.hypki.libs5.pjf.op.OpShow;
import net.hypki.libs5.utils.string.HtmlUtils;
import net.hypki.libs5.utils.url.Params;

public class BeansExplorer extends BeansComponent {
	
	private final String prompt = "BEANS explorer";
	
	@Expose
	private boolean datasetsVisible = true;
	
	@Expose
	private Link afterEntryLink = null;

	public BeansExplorer() {
		
	}
	
	public BeansExplorer(BeansComponent parent) {
		super(parent);
	}
	
	@Override
	public void renderOn(HtmlCanvas html) throws IOException {
		html
			.div(id(getId()).class_("beans-explorer"))
				.render(new Link(null, "home", new OpComponentClick(this, "onHome")))
				.div(class_("top"))
					.content(prompt)
//				._div()
				.div(class_("root"))
					.render(new Link("Notebooks", new OpComponentClick(this, "onNotebooks")).setHtmlClass("root"))
					.render_if(new Link("Datasets", new OpComponentClick(this, "onDatasets")).setHtmlClass("root"), 
							isDatasetsVisible())
//					.render(new Link("Tables", new OpComponentClick(this, "onTables")).setHtmlClass("root"))
				._div()
				.div(class_("content"))
					.content("")
			._div()
			;
	}
	
	@SuppressWarnings("unused")
	private OpList onHome() {
		return new OpList()
				.add(new OpShow("#" + getId() + " .root"))
				.add(new OpSet("#" + getId() + " .top", prompt))
				.add(new OpHide("#" + getId() + " .list"));
	}

	@SuppressWarnings("unused")
	private OpList onNotebookEntry() throws IOException {
		String entryId = getParams().getString("entryId");
		NotebookEntry ne = NotebookEntryGateway.getNotebookEntry(getSessionBean(), entryId);
		
		ModalWindow modal = new ModalWindow(this);
		
		NotebookEntryEditorPanel editor = NotebookEntryEditorFactory.getEditorPanel(this, ne.getClass());
		editor.setNotebookEntry(ne);
		editor.getParams().add("notebookId", ne.getNotebookId().getId());
	
		modal
			.add(new Button("Close")
				.add(modal.onClose()))
			.getContent()
				.render(editor)
			;
		
		return new OpList()
				.add(new OpModal(modal));
//				.add(new OpBeansInfo("Clicked on " + ne.getName()));
//				.add(new OpHide("#" + getId() + " .list"))
//				.add(new OpSet("#" + getId() + " .top", new Link(ne.getName(), new Op()).toHtml()))
//				.add(new OpAppend("#" + getId() + " .content", 
//						new EntriesListComponent(this)
//							.setNotebookId(ne.getId())
//							.setOnClick(new OpComponentClick(this, "onEntry"))
//							.setHtmlClass("list")
//							.toHtml()));
	}
	
	protected void renderAfterEntry(int idx, HtmlCanvas html) throws IOException {
		
	}
	
	@SuppressWarnings("unused")
	private OpList onNotebook() throws IOException {
		String notebookId = getParams().getString("notebookId");
		Notebook n = NotebookGateway.getNotebook(getSessionBean(), notebookId);

		HtmlCanvas breadcrumb = new HtmlCanvas();
		breadcrumb
			.render(new Link(n.getName(), onNotebooks())
					.setStopPropagation(true))
			.render(new Link(null, 
					"link",
					new OpComponentClick(NotebookContent.class, 
							"onShow",
							new Params().add("notebookId", n.getId().getId())))
					.addHtmlClass("link"));
			
		return new OpList()
				.add(new OpHide("#" + getId() + " .list"))
				.add(new OpSet("#" + getId() + " .top", breadcrumb.toHtml()))
				.add(new OpAppend("#" + getId() + " .content", 
						new EntriesListComponent(this) {
								protected void renderAfterItem(NotebookEntry ne, HtmlCanvas html) throws IOException {
//									BeansExplorer.this.renderAfterEntry(idx, html);
									if (BeansExplorer.this.afterEntryLink != null) {
										BeansExplorer.this.afterEntryLink.
											getOps()
												.addParamParam("sourceNotebookId", ne.getNotebookId().getId())
												.addParamParam("sourceEntryId", ne.getId().getId());
										html
											.render(BeansExplorer.this.afterEntryLink);
									}
								};
							}
							.setNotebookId(n.getId())
							.setOnClick(new OpComponentClick(this, "onNotebookEntry"))
							.setHtmlClass("list")
							.toHtml()));
	}
	
	@SuppressWarnings("unused")
	private OpList onDataset() throws IOException {
		String dsId = getParams().getString("datasetId");
		Dataset d = DatasetGateway.getDataset(getSessionBean(), dsId);
		
		HtmlCanvas breadcrumb = new HtmlCanvas();
		breadcrumb
			.render(new Link(d.getName(), new Op()))
			.render(new Link(null, 
					"link",
					new OpComponentClick(DatasetBetaContent.class, 
							"onShow",
							new Params().add("datasetId", d.getId().getId())))
					.addHtmlClass("link"))
			;
		
		return new OpList()
				.add(new OpHide("#" + getId() + " .list"))
				.add(new OpSet("#" + getId() + " .top", breadcrumb.toHtml()))
				.add(new OpAppend("#" + getId() + " .content", 
						new TablesListComponent(this)
							.setDatasetId(d.getId())
							.setOnClick(new OpComponentClick(this, "onTable", getParams()))
							.setHtmlClass("list")
							.toHtml()));
	}
	
	@SuppressWarnings("unused")
	private OpList onTable() throws IOException {
		String dsId = getParams().getString("datasetId");
		Dataset d = DatasetGateway.getDataset(getSessionBean(), dsId);
		String tbId = getParams().getString("tableId");
		Table t = TableGateway.getTable(getSessionBean(), tbId);
		
		HtmlCanvas breadcrumb = new HtmlCanvas();
		breadcrumb
			.render(new Link(d.getName(), onDatasets()))
			.render(new Link(null, 
					"link",
					new OpComponentClick(DatasetBetaContent.class, 
							"onShow",
							new Params().add("datasetId", d.getId().getId())))
					.addHtmlClass("link"))
			.span()
				.content("/")
			.render(new Link(t.getName(), 
					new OpSet("#" + getId() + " .content", 
						new TablesListComponent(this.getSecurityContext(), new Params())
							.setDatasetId(d.getId())
							.setOnClick(new OpComponentClick(this, "onTable", getParams()))
							.setHtmlClass("list")
						.toHtml())));
		
		return new OpList()
				.add(new OpHide("#" + getId() + " .list"))
				.add(new OpSet("#" + getId() + " .top", breadcrumb.toHtml()))
				.add(new OpSet("#" + getId() + " .content", 
						new ColumnsListComponent(this.getSecurityContext(), new Params())
							.setTableId(t.getId())
							.setOnClick(new OpComponentClick(this, "onTable"))
							.setHtmlClass("list")
							.setPerPage(20)
							.toHtml()));
	}
	
	@SuppressWarnings("unused")
	private OpList onNotebooks() throws IOException {
		return new OpList()
				.add(new OpHide("#" + getId() + " .root"))
				.add(new OpShow("#" + getId() + " .list"))
				.add(new OpSet("#" + getId() + " .top", prompt))
				.add(new OpSet("#" + getId() + " .content", 
						new NotebooksListComponent(this.getSecurityContext(), new Params())
							.setOnClick(new OpList()
											.add(new OpComponentClick(BeansExplorer.this, "onNotebook")))
							.setHtmlClass("list")
							.toHtml()));
	}
	
	@SuppressWarnings("unused")
	private OpList onDatasets() throws IOException {
		return new OpList()
				.add(new OpHide("#" + getId() + " .root"))
				.add(new OpShow("#" + getId() + " .list"))
				.add(new OpSet("#" + getId() + " .top", prompt))
				.add(new OpSet("#" + getId() + " .content", 
						new DatasetsListComponent(this.getSecurityContext(), new Params())
							.setOnClick(new OpList()
											.add(new OpComponentClick(this, "onDataset")))
							.setHtmlClass("list")
							.toHtml()));
	}

	public boolean isDatasetsVisible() {
		return datasetsVisible;
	}

	public BeansExplorer setDatasetsVisible(boolean datasetsVisible) {
		this.datasetsVisible = datasetsVisible;
		return this;
	}

	public BeansExplorer addAfterEntry(Link link) {
		this.afterEntryLink = link;
		return this;
	}
}
