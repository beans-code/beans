package net.beanscode.web.view.settings;

import java.io.IOException;

import org.rendersnake.HtmlCanvas;

import net.beanscode.web.view.ui.BeansComponent;
import net.beanscode.web.view.ui.CardPanel;

public class DefaultEntryAutoUpdateCard extends CardPanel {

	public DefaultEntryAutoUpdateCard() {
		
	}
	
	public DefaultEntryAutoUpdateCard(BeansComponent parent) {
		super(parent);
	}

	@Override
	protected void renderTitle(HtmlCanvas html) throws IOException {
		html
			.span()
				.content("Auto-update default option for Notebook entries");
	}

	@Override
	protected void renderContent(HtmlCanvas html) throws IOException {
		html
			.render(new DefaultEntryAutoUpdatePanel(this));
	}

	@Override
	protected void renderFooter(HtmlCanvas html) throws IOException {
		
	}
	
	
}
