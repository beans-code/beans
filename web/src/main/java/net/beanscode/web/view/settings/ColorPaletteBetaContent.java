package net.beanscode.web.view.settings;

import java.io.IOException;

import net.beanscode.web.beta.BetaContent;
import net.beanscode.web.view.ui.BeansComponent;
import net.beanscode.web.view.ui.BetaButtonGroup;

import org.rendersnake.HtmlCanvas;

public class ColorPaletteBetaContent extends BetaContent {
	
	public ColorPaletteBetaContent() {
		
	}
	
	public ColorPaletteBetaContent(BeansComponent parent) {
		super(parent);
	}
	
	@Override
	protected void renderHeader(HtmlCanvas html) throws IOException {
		html
			.h1()
				.content("Color palette for plots");
	}
	
	@Override
	protected void renderContent(HtmlCanvas html) throws IOException {
		html
			.render(new ColorPalettePanel(this, null));//new ColorPaletteGet(getSessionBean()).run(getUserEmail())));
	}

	@Override
	protected BetaButtonGroup getMenu() throws IOException {
		return null;
	}

	@Override
	protected String getMenuName() throws IOException {
		return null;
	}
}