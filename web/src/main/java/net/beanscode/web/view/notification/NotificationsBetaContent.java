package net.beanscode.web.view.notification;

import java.io.IOException;

import net.beanscode.web.beta.BetaContent;
import net.beanscode.web.view.ui.BeansComponent;
import net.beanscode.web.view.ui.BetaButtonGroup;

import org.rendersnake.HtmlCanvas;

public class NotificationsBetaContent extends BetaContent {
	
	public NotificationsBetaContent() {
		
	}

	public NotificationsBetaContent(BeansComponent parent) {
		super(parent);
	}
	
	@Override
	protected void renderHeader(HtmlCanvas html) throws IOException {
		html
			.h1()
				.content("Notifications");
	}

	@Override
	protected void renderContent(HtmlCanvas html) throws IOException {
		html
			.render(new NotificationsPanel(this)
					.setExtended(true));
	}

	@Override
	protected BetaButtonGroup getMenu() throws IOException {
		return null;
	}

	@Override
	protected String getMenuName() throws IOException {
		return null;
	}
}
