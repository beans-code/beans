package net.beanscode.web.view.ui.badge;

import static org.rendersnake.HtmlAttributesFactory.class_;

import java.io.IOException;

import org.rendersnake.HtmlCanvas;

import com.google.gson.annotations.Expose;

import net.beanscode.web.view.ui.BeansComponent;

public abstract class Badge extends BeansComponent {
	
	final public static String INFO = "badge badge-secondary";
	final public static String SUCCESS = "badge badge-success";
	final public static String WARNING = "badge badge-warning";
	final public static String ERROR = "badge badge-danger";
	
	@Expose
	private String text = null;
	
	private String htmlClass = "";
	
	private String badge = INFO;

	public Badge() {
		
	}
	
	public Badge(String text, String badge) {
		setText(text);
		setBadge(badge);
	}
	
	@Override
	public void renderOn(HtmlCanvas html) throws IOException {
		html
			.span(class_(getBadge() + " label " + getHtmlClassNotNull()))
				.content(getText() != null ? getText() : "");
	}

	public String getText() {
		return text;
	}

	public Badge setText(String text) {
		this.text = text;
		return this;
	}

	public String getHtmlClass() {
		return htmlClass;
	}
	
	public String getHtmlClassNotNull() {
		return htmlClass != null ? htmlClass : "";
	}

	public Badge setHtmlClass(String htmlClass) {
		this.htmlClass = htmlClass;
		return this;
	}

	private String getBadge() {
		return badge;
	}

	private void setBadge(String badge) {
		this.badge = badge;
	}
}
