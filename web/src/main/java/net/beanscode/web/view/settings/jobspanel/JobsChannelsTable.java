package net.beanscode.web.view.settings.jobspanel;

import java.io.IOException;
import java.util.List;

import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.web.view.ui.BeansComponent;
import net.beanscode.web.view.ui.BetaTableComponent;
import net.beanscode.web.view.ui.modals.OpBeansInfo;
import net.hypki.libs5.cli.noninteractivecli.CLIException;
import net.hypki.libs5.pjf.components.buttons.Button;
import net.hypki.libs5.pjf.components.buttons.ButtonGroup;
import net.hypki.libs5.pjf.components.ops.OpComponentClick;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.url.Params;

import org.rendersnake.HtmlCanvas;

import com.google.gson.annotations.Expose;

public class JobsChannelsTable extends BetaTableComponent {
	
	@Expose
	private List<String> channelNames = null;

	public JobsChannelsTable() {
		
	}
	
	public JobsChannelsTable(BeansComponent parent) {
		super(parent);
	}
	
	@Override
	protected String getSettingsPrefix() {
		return "jobschannels-table-";
	}
	
	private List<String> getChannelNames() {
		if (this.channelNames == null) {
			try {
				channelNames = new ApiCallCommand(this)
							.setApi("api/jobs/channels")
							.run()
							.getResponse()
							.getAsList("", String.class);
			} catch (IOException e) {
				LibsLogger.error(JobsChannelsTable.class, "Cannot get channels list", e);
			}
		}
		return this.channelNames;
	}
	
	@Override
	protected long getMaxHits() throws IOException {
		return getChannelNames().size();
	}
	
	@Override
	protected int getNumberOfColumns() throws IOException {
		return 2;
	}
	
	@Override
	protected void renderCell(int row, int column, HtmlCanvas html) throws IOException {
		final String channel = getChannelNames().get(row);
		
		if (column == 0) {
			html
				.span()
					.content(channel);
		} else if (column == 1) {
			html
				.render(new ButtonGroup()
						.addButton(new Button("Add Jobs Worker")
								.add(new OpComponentClick(this, "onAdd", new Params().add("channelName", channel)))));
		}
	}
	
	private OpList onAdd() throws CLIException, IOException {
		final String channelName = getParams().getString("channelName");
		
		new ApiCallCommand(this)
				.setApi("api/jobs/workers/start")
				.addParam("channel", channelName)
				.run();
		
		return new OpList()
			.add(new OpBeansInfo("New Jobs Worker started"));
	}

	@Override
	protected boolean isFilterVisible() {
		return true;
	}

	
	@Override
	protected String getColumnName(int column) {
		if (column == 0)
			return "Channel name";
		else if (column == 1)
			return "Options";
		else {
			LibsLogger.error(JobsChannelsTable.class, "Unknown column index " + column);
			return "";
		}
	}

	@Override
	protected List<Button> getMenu() {
		return null;
	}

	@Override
	protected void renderTitle(HtmlCanvas html) throws IOException {
		html
			.write("List of jobs channels");
	}

	@Override
	protected boolean isHeaderColumnVisible() throws IOException {
		return true;
	}

	@Override
	protected boolean isExapandable() throws IOException {
		return false;
	}

	@Override
	protected void renderExpandable(int row, HtmlCanvas html)
			throws IOException {
		
	}

	@Override
	protected String getTableCss() throws IOException {
		return null;
	}
}
