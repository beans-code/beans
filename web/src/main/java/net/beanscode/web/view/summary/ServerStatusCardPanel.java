package net.beanscode.web.view.summary;

import static org.rendersnake.HtmlAttributesFactory.class_;

import java.io.IOException;
import java.util.Map.Entry;

import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.web.view.ui.CardPanel;
import net.hypki.libs5.pjf.components.Component;

import org.rendersnake.HtmlCanvas;

import com.google.gson.JsonElement;


public class ServerStatusCardPanel extends CardPanel {

	public ServerStatusCardPanel() {
		
	}
	
	public ServerStatusCardPanel(Component parent) {
		super(parent);
	}

	@Override
	protected void renderTitle(HtmlCanvas html) throws IOException {
		html
			.write("Server status");
	}

	@Override
	protected void renderContent(HtmlCanvas html) throws IOException {
		ApiCallCommand apiCall = (ApiCallCommand) new ApiCallCommand(getSessionBean())
			.setApi("api/admin/status")
			.run();
		
		if (apiCall.getResponse().getAsJson() != null)
			for (Entry<String, JsonElement> entry : apiCall.getResponse().getAsJson().getAsJsonObject().entrySet()) {
				html
					.dl(class_("row"))
						.dt(class_("col-sm-6"))
							.content(entry.getKey())
						.dd(class_("col-sm-6"))
							.content(entry.getValue().toString())
					._dl();
			}
	}

	@Override
	protected void renderFooter(HtmlCanvas html) throws IOException {
		
	}
}
