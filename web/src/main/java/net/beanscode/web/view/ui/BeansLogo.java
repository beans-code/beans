package net.beanscode.web.view.ui;

import static net.beanscode.cli.BeansCliCommand.API_SETTING_GETSETTING;
import static org.rendersnake.HtmlAttributesFactory.class_;
import static org.rendersnake.HtmlAttributesFactory.id;
import static org.rendersnake.HtmlAttributesFactory.src;

import java.io.IOException;

import org.rendersnake.HtmlAttributesFactory;
import org.rendersnake.HtmlCanvas;

import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.web.view.settings.Setting;

public class BeansLogo extends BeansComponent {

	public BeansLogo() {
		
	}
	
	public BeansLogo(BeansComponent parent) {
		super(parent);
	}
	
	@Override
	public void renderOn(HtmlCanvas html) throws IOException {
		// subtitle
		Setting s = new ApiCallCommand(this)
						.setApi(API_SETTING_GETSETTING)
						.addParam("id", "BEANS_SUBTITLE")
						.run()
						.getResponse()
						.getAsObject(Setting.class);
		
		html
			.div(id("beans-logo"))
				.a(class_("brand-linkX")
						.href("/"))
					.img(src("/dist/img/beans-name.png")
							.alt("BEANS logo")
							.class_("brand-image img-circle elevation-3")
							.style("opacity: .8")
							)
				._a()
//					.span(class_("brand-text font-weight-light"))
//						.content("BEANS")
					.span(class_("brand-text"))
						.content(s.getValues().get(0).getValueAsString())
			._div()
			;
		
		
//		<a href="/" class="brand-link">
//	      <img src="/dist/img/beans-logo-letter.png" alt="BEANS small logo" 
//	      	class="brand-image img-circle elevation-3" style="opacity: .8">
//	      <span class="brand-text font-weight-light">BEANS</span>
//	      <span class="beans-brand-subtitle"><!--subtitle--></span>
//	    </a>
	}
}
