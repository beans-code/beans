package net.beanscode.web.view.plots;

import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;
import static org.rendersnake.HtmlAttributesFactory.class_;
import static org.rendersnake.HtmlAttributesFactory.for_;
import static org.rendersnake.HtmlAttributesFactory.id;
import static org.rendersnake.HtmlAttributesFactory.name;

import java.io.IOException;

import javax.ws.rs.core.SecurityContext;

import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.cli.notebooks.PlotParallelCoordinatesEntry;
import net.beanscode.pojo.NotebookEntry;
import net.beanscode.web.view.notebook.NotebookEntryEditorPanel;
import net.beanscode.web.view.ui.BeansComponent;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.pjf.components.buttons.Button;
import net.hypki.libs5.pjf.components.buttons.ButtonGroup;
import net.hypki.libs5.pjf.components.ops.OpComponentClick;
import net.hypki.libs5.pjf.op.OpAfter;
import net.hypki.libs5.pjf.op.OpAppend;
import net.hypki.libs5.pjf.op.OpHide;
import net.hypki.libs5.pjf.op.OpJs;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.pjf.op.OpRemove;
import net.hypki.libs5.pjf.op.OpSet;
import net.hypki.libs5.pjf.op.OpShow;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.api.APICallType;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.url.Params;

import org.rendersnake.HtmlCanvas;
import org.rendersnake.Renderable;

public class PlotParallelCoordinatesPanel extends NotebookEntryEditorPanel {
	
	public PlotParallelCoordinatesPanel() {
		
	}
	
	public PlotParallelCoordinatesPanel(BeansComponent parent) {
		super(parent);
	}

	@Override
	public Renderable getView() {
		return new Renderable() {
			@Override
			public void renderOn(HtmlCanvas html) throws IOException {
				html
					.div()
						.div(class_("plot-view")
								.id("plot-view-" + getId())
								.style("width: 95%;")
//								.data("onvisible", new OpComponentClick(PlotParallelCoordinatesPanel.this, "onRefresh", new Params().add("plotId", getPlot().getId())).toStringOnclick())
								)
							.write("Plot '" + getPlot().getTitle() + "' is not yet ready")
//							.write(new OpComponentTimer(PlotParallelCoordinatesPanel.this, 
//										"onRefresh", 
//										RandomUtils.nextInt(1000, 3000),
//										new Params()
//											.add("plotId", getPlot().getId()))
//								.toStringAdhock(), false)
						._div()
						.div(id("plot-legend-" + getId())
								.class_("dygraph-legend-beans"))
						._div()
						.div(id("plot-logbuttons-" + getId())
								.class_("dygraph-legend-beans"))
						._div()
					._div()
					
					.div(id("grid" + getId())
							.class_("parallel-grid-data"))
					._div()
					;
			}
		};
	}

	@Override
	protected Renderable getEditor() {
		return new Renderable() {
			@Override
			public void renderOn(HtmlCanvas html) throws IOException {
				html
					.div(class_("plot-query plot-parallel"))
						.div(class_("form-group"))
							.label(class_("name")
									.for_("plot-title"))
								.content("Plot title")
							.input(name("plot-title")
									.id("plot-title")
									.class_("title form-control")
									.value(notEmpty(getPlot().getTitle()) ? getPlot().getTitle() : "Plot title")
									.class_("title"))
						._div()
//						.br()
						
						.div(class_("form-group"))
							.label(class_("name")
									.for_("plot-ds-query"))
								.content("Read data from Datasets ")
							.input(name("plot-ds-query")
									.id("plot-ds-query")
									.class_("query form-control")
									.add("placeholder", "datasets filter...")
									.value(getPlot().getDsQuery()))
						._div()
								
						.label(for_("plot-tb-query"))
							.content("from Tables")
						.input(name("plot-tb-query")
								.id("plot-tb-query")
								.class_("query form-control")
								.add("placeholder", "tables filter")
								.value(getPlot().getTbQuery()))
//						.br()
						.div(class_("plot-columns"))
							.label(class_("name"))
								.content("Plot:")
							.br()
								;
				
				for (int i = 0; i < getPlot().getColumnsAsList().size(); i++) {
//					UUID thisRow = UUID.random();
					String col = getPlot().getColumnsAsList().get(i);
					String legend = i < getPlot().getLegends().size() ? getPlot().getLegends().get(i) : "";
					Object minValue = getPlot().getMinValue(i);
					Object maxValue = getPlot().getMaxValue(i);
					
					renderRow(html, col, legend, minValue, maxValue);
				}
				
				html
					._div()
					
					.div(class_("row"))
						.render(new ButtonGroup()
							.addButton(new Button("Add")
									.setReplacedHtmlClass("btn btn-default")
									.setAwesomeicon("fa fa-plus")
									.add(new OpComponentClick(PlotParallelCoordinatesPanel.this, "onNewColumn"))))
					._div();
				
				html
					._div()
					;
			}
		};
	}
	
	private void renderRow(HtmlCanvas html, String col, String legend, Object minValue, 
			Object maxValue) throws IOException {
		UUID thisRow = UUID.random();
		
		html
			.div(class_("row " + thisRow.getId()))
				.div(class_("col-sm-4"))
					.label()
						.content("Column")
					.input(name("plot-column")
							.class_("columns form-control")
							.add("placeholder", "column or math expression with columns to plot")
							.value(col))
				._div()
				
				.div(class_("col-sm-4"))
					.label()
						.content("Title")
					.input(name("plot-legend")
							.class_("columns form-control")
							.add("placeholder", "title for this column")
							.value(legend))
				._div()
				
				.div(class_("col-sm-1"))
					.label()
						.content("Min. value")
					.input(name("plot-min-value")
							.class_("columns form-control")
							.add("placeholder", "minimul value")
							.value("" + (minValue == null ? "" : minValue)))
				._div()
				
				.div(class_("col-sm-1"))
					.label()
						.content("Max. value")
					.input(name("plot-max-value")
							.class_("columns form-control")
							.add("placeholder", "maximum value")
							.value("" + (maxValue == null ? "" : maxValue)))
				._div()
				
				.div(class_("col-sm-2"))
					.label()
						.content("Options")
					.br()
					.render(new ButtonGroup()
							.addButton(new Button()
									.setReplacedHtmlClass("btn  btn-default")
									.setAwesomeicon("fa fa-trash")
									.add(new OpRemove("." + thisRow))))
				._div()
			._div()
			;
//		UUID thisRow = UUID.random();
//		html
//			.div(class_(thisRow.getId()))
//				.input(name("plot-column")
//						.class_("columns")
//						.add("placeholder", "column or math expression with columns to plot")
//						.value(col))
//				.span()
//					.content("with title")
//				.input(name("plot-legend")
//						.class_("columns")
//						.add("placeholder", "title for this column")
//						.value(legend))
//				.render(new ButtonGroup()
//						.addButton(new Button()
//								.setGlyphicon("remove")
//								.add(new OpRemove("." + thisRow))))
//			._div()
//			;
	}
	
	private OpList onNewColumn() throws IOException {
		HtmlCanvas html = new HtmlCanvas();
		
		renderRow(html, "", "", "", "");
		
		return new OpList()
			.add(new OpAppend(".plot-columns", html.toHtml()));
	}

	@Override
	protected Button getMenu() throws IOException {
		return new Button()
				.setHtmlClass("refresh")
				.setAwesomeicon("fa fa-sync")
				.add(new OpComponentClick(this, "onRefresh", new Params().add("plotId", getPlot().getId())))
			;
	}
	
	@Override
	public OpList getInitOpList() {
		return super.getInitOpList()
//				.add(new OpComponentTimer(this, "onRefresh", RandomUtils.nextInt(1, 2000), new Params().add("plotId", getPlot().getId())))
				;
	}
	
	@Override
	protected OpList onRefresh(SecurityContext sc, Params params) throws IOException {
		final UUID plotId = new UUID(params.getString("plotId"));
		OpList ops = new OpList();
		
//		if (getPlot().getPlotStatus().isFinishedWithErrors()) {
//			// check if plot failed
//			return ops
//				.add(new OpSet("#" + getId() + " .plot-view", new DangerLabel(getPlot().getPlotStatus().getErrorMsg()).toString()));
//		} else if (getPlot().getPlotStatus().isStarted() && !getPlot().getPlotStatus().isFinished()) {
//			// plot was started but not finished
//			return ops
//				.add(new OpSet("#" + getId() + " .plot-view", new HtmlCanvas()
//																	.img(add("src", "/static/res/ajax-loader-1.gif", false))
//																	.write(" ")
//																	.span()
//																		.content("Plot '" + getPlot().getTitle() + "' is being prepared...")
//																	.toHtml()))
//				.add(new OpComponentTimer(this, "onRefresh", BeansWebConst.PLOT_AUTOREFRESH_INTERVAL_MS, new Params().add("plotId", getPlot().getId()).add("plotNr", plotNr)))
//				;
//		} else if (!getPlot().getPlotStatus().isStarted() && !getPlot().getPlotStatus().isFinished()) {
//
//				// plot was started but not finished
//				return ops
//					.add(new OpSet("#" + getId() + " .plot-view", new HtmlCanvas()
//																		.span()
//																			.content("Plot is not started")
//																		.toHtml()))
//					;
//
//		} 
				
			
		HtmlCanvas html = new HtmlCanvas();
		html
			.div(class_("chart parcoords")
					.id("ch" + getPlot().getId().getId())
					.add("style", "height:440px;"))
			._div();
		
		String idShort = "ch" + getPlot().getId().getId();
		return ops
			.add(new OpJs("d3.selectAll(\"#" + idShort + " svg\").remove();"))
			.add(new OpRemove("#" + idShort + " svg"))
			.add(new OpRemove("#" + idShort + " "))
			.add(new OpAfter("#" + getId() + " .plot-view", html.toHtml()))
			.add(new OpRemove("#" + idShort + " .chart-title"))
			.add(new OpSet("#" + getId() + " .plot-view", ""))
			.add(new OpJs(WebPlotsFactory.createParallelCoordinates(getPlot())));
			
	}
		
	@Override
	public NotebookEntry getNotebookEntry() {
		if (super.getNotebookEntry() == null) {
			final String plotId = getParams().getString("plotId");
			try {
				setNotebookEntry(new ApiCallCommand(getSessionBean())
									.setApi("api/notebook/entry/get")
									.addParam("entryId", plotId)
									.run()
									.getResponse()
									.getAsObject(PlotParallelCoordinatesEntry.class));
			} catch (IOException e) {
				LibsLogger.error(PlotParallelCoordinatesPanel.class, "Cannot get Notebook entry from DB", e);
			}
		}
		return super.getNotebookEntry();
	}

	@Override
	protected OpList opOnPlay(SecurityContext sc, Params params) throws IOException {
		try {
			new ApiCallCommand(getSessionBean())
				.setApi("api/notebook/entry")
				.setApiCallType(APICallType.POST)
				.addParam("entryId", getPlot().getId())
				.addParam("name", getParams().getString("plot-title"))
				.addParam("META_DS_QUERY", getParams().getString("plot-ds-query"))
				.addParam("META_TB_QUERY", getParams().getString("plot-tb-query"))
				.addParam("META_COLUMNS", getParams().getList("plot-column"))
				.addParam("META_LEGENDS", getParams().getList("plot-legend"))
				.addParam("META_MIN_VALUE", getParams().getList("plot-min-value"))
				.addParam("META_MAX_VALUE", getParams().getList("plot-max-value"))
				.run();
			
			new ApiCallCommand(getSessionBean())
				.setApi("api/notebook/entry/start")
				.addParam("entryId", getPlot().getId())
				.run();
		} catch (IOException e) {
			LibsLogger.error(PlotParallelCoordinatesPanel.class, "Cannot save notebook entry", e);
		}
		
		return new OpList()
			.add(new OpComponentClick(this, "onRefresh", new Params().add("plotId", getPlot().getId())))
			.add(new OpShow("#" + getId() + " .refresh"))
			.add(new OpShow("#" + getId() + " .plotNrInput"));
	}
	
	@Override
	protected OpList opOnEdit(SecurityContext sc, Params params) throws IOException {
		return new OpList()
			.add(new OpHide("#" + getId() + " .refresh"));
	}

	@Override
	protected OpList opOnStop(SecurityContext sc, Params params) throws IOException {
		boolean refresh = params.get("plot-type") != null;
		return new OpList()
			.add(refresh, new OpComponentClick(this, "onRefresh", new Params().add("plotId", getPlot().getId())))
			.add(new OpShow("#" + getId() + " .refresh"));
	}

	@Override
	protected OpList opOnRemove(SecurityContext sc, Params params) throws IOException {
		try {
			new ApiCallCommand(this)
				.setApi("api/notebook/entry/remove")
				.setApiCallType(APICallType.DELETE)
				.addParam("entryId", params.getString("entryId"))
				.run();
		} catch (IOException e) {
			LibsLogger.error(PlotParallelCoordinatesPanel.class, "Cannot remove Notebook Entry", e);
		}
		
		return new OpList();
	}
	
	public PlotParallelCoordinatesEntry getPlot() {
		return (PlotParallelCoordinatesEntry) getNotebookEntry();
	}
}
