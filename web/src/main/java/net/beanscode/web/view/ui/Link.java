package net.beanscode.web.view.ui;

import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;
import static org.rendersnake.HtmlAttributesFactory.class_;
import static org.rendersnake.HtmlAttributesFactory.href;
import static org.rendersnake.HtmlAttributesFactory.onClick;

import java.io.IOException;

import org.rendersnake.HtmlCanvas;

import com.google.gson.annotations.Expose;

import net.hypki.libs5.pjf.op.Op;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.utils.string.StringUtilities;

public class Link extends BeansComponent {
	
	@Expose
	private String text = null;

	@Expose
	private String icon = null;

	@Expose
	private String htmlClass = null;

	@Expose
	private String href = null;

	@Expose
	private OpList ops = null;

	@Expose
	private boolean stopPropagation = true;

	public Link() {
		
	}
	
	public Link(String text, String href) {
		setText(text);
		setHref(href);
	}
	
	public Link(String text, OpList ops) {
		setText(text);
		setOps(ops);
	}
	
	public Link(String text, Op op) {
		setText(text);
		getOps().add(op);
	}
	
	public Link(String text, String icon, Op op) {
		setText(text);
		setIcon(icon);
		getOps().add(op);
	}
	
	@Override
	public void renderOn(HtmlCanvas html) throws IOException {
		if (notEmpty(getHref()))
			html
				.a(href(getHref())
						.target("_blank")
						.class_("beans-link " + (getHtmlClass() != null ? getHtmlClass() : "")))
					.if_(isIconDefined())
						.i(class_("fa fa-" + getIcon()))
						._i()
						.span()
							.content(" ")
						.span()
							.content(getText())
					._if()
					.if_(!isIconDefined() && isTextDefined())
						.span()
							.content(getText())
					._if()
				._a();
		else
			html
				.a(onClick(getOps().toStringOnclick() + (isStopPropagation() ? "; event.stopPropagation();" : ""))
						.class_("beans-link " + (getHtmlClass() != null ? getHtmlClass() : "")))
					.if_(isIconDefined())
						.i(class_("fa fa-" + getIcon()))
						._i()
						.span()
							.content(" ")
						.span()
							.content(getText())
					._if()
					.if_(!isIconDefined() && isTextDefined())
						.span()
							.content(getText())
					._if()
				._a();
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
	
	public boolean isTextDefined() {
		return notEmpty(getText());
	}

	public OpList getOps() {
		if (ops == null)
			ops = new OpList();
		return ops;
	}

	public void setOps(OpList ops) {
		this.ops = ops;
	}

	public String getHtmlClass() {
		return htmlClass;
	}

	public Link setHtmlClass(String htmlClass) {
		this.htmlClass = htmlClass;
		return this;
	}
	
	public Link addHtmlClass(String htmlClass) {
		this.htmlClass = (this.htmlClass != null ? this.htmlClass : "") + " " + (htmlClass != null ? htmlClass : "");
		return this;
	}

	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}
	
	public boolean isIconDefined() {
		return StringUtilities.notEmpty(getIcon());
	}

	public boolean isStopPropagation() {
		return stopPropagation;
	}

	public Link setStopPropagation(boolean stopPropagation) {
		this.stopPropagation = stopPropagation;
		return this;
	}
}
