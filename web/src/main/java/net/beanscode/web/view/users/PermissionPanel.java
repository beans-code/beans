package net.beanscode.web.view.users;

import static net.beanscode.cli.BeansCliCommand.API_PERMISSION_GET;
import static org.rendersnake.HtmlAttributesFactory.id;

import java.io.IOException;

import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.cli.permissions.Permission;
import net.beanscode.web.view.ui.BeansComponent;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.pjf.components.Component;
import net.hypki.libs5.utils.LibsLogger;

import org.rendersnake.HtmlCanvas;

import com.google.gson.annotations.Expose;

public class PermissionPanel extends BeansComponent {
	
	@Expose
	private UUID permissionId = null;
	
	@Expose
	private String permissionClass = null;
	
	private Permission permission = null;
	
	public PermissionPanel() {
		
	}

	public PermissionPanel(Component parent) {
		super(parent);
	}
	
	public PermissionPanel(Component parent, UUID modelId, String permissionClass) {
		super(parent);
		setPermissionId(modelId);
		setPermissionClass(permissionClass);
	}
	
	@Override
	public void renderOn(HtmlCanvas html) throws IOException {
		html
			.div(id(getId()))
				.render(new PermissionBetaTableComponent(this, getPermissionId(), getPermissionClass())
//						.setInitialRefresh(false)
						.setPerPage(5)
//						.setPerPageVisible(false)
						)
			._div();
	}


	private UUID getPermissionId() {
		return permissionId;
	}

	private void setPermissionId(UUID permissionId) {
		this.permissionId = permissionId;
	}

	public Permission getPermission() {
		if (permission == null && permissionId != null) {
			try {
				permission = new ApiCallCommand(this)
								.setApi(API_PERMISSION_GET)
								.addParam("id", getPermissionId().getId())
								.addParam("class", getPermissionClass())
								.run()
								.getResponse()
								.getAsObject(Permission.class);
			} catch (Exception e) {
				LibsLogger.debug(PermissionPanel.class, "Cannot get permission from DB, creating one");
			}
		}
		return permission;
	}

	public String getPermissionClass() {
		return permissionClass;
	}

	public void setPermissionClass(String permissionClass) {
		this.permissionClass = permissionClass;
	}
}


