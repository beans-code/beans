package net.beanscode.web.view.settings;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.pojo.connector.Connector;
import net.beanscode.web.view.ui.BeansComponent;
import net.beanscode.web.view.ui.ChooseComponent;
import net.beanscode.web.view.ui.modals.OpBeansError;
import net.beanscode.web.view.ui.modals.OpBeansInfo;
import net.hypki.libs5.cli.noninteractivecli.CLIException;
import net.hypki.libs5.pjf.components.ops.OpComponentClick;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.utils.string.StringUtilities;

import org.rendersnake.HtmlCanvas;

public class DefaultDatabaseEnginePanel extends BeansComponent {
	
	public DefaultDatabaseEnginePanel() {
		
	}
	
	public DefaultDatabaseEnginePanel(BeansComponent parent) {
		super(parent);
	}

	@Override
	public void renderOn(HtmlCanvas html) throws IOException {
		Connector defaultDriver = new ApiCallCommand(this)
			.setApi(ApiCallCommand.API_SETTING_CONNECTOR_DEFAULT)
			.addParam("userEmail", getUserEmail())
			.run()
			.getResponse()
			.getAsObject(Connector.class);
		
		List<Connector> allConnectors = new ApiCallCommand(this)//new ConnectorAll(getSessionBean()).run();
			.setApi(ApiCallCommand.API_SETTING_CONNECTOR_LIST)
			.run()
			.getResponse()
			.getAsList("/", Connector.class);
		
		List<String> allConnectorsNames = new ArrayList<String>();
		for (Connector connector : allConnectors) {
			allConnectorsNames.add(connector.getName());
		}
		
		html
			.span()
				.content("Default database driver for uploaded files")
			.render(new ChooseComponent(this//new CassandraConnector().getName(), 
//										new mapdbconn"MapDB", 
										//new PlainConnector().getName()
										)
					.setOptions(allConnectorsNames)
					.setSelectedOption(defaultDriver.getName())//ConnectorFactory.getConnector(defaultDriver).getName())
					.addOpsOnChange(new OpComponentClick(DefaultDatabaseEnginePanel.this, "changeDriver")));
	}
	
	private OpList changeDriver(OpList ops) throws CLIException, IOException {
		String newDriver = getParams().getString("choose-selected");
//		Connector conn = ConnectorFactory.getConnector(newDriver);
		
		if (StringUtilities.notEmpty(newDriver)) {
			new ApiCallCommand(this)
				.setApi(ApiCallCommand.API_SETTING_CONNECTOR_DEFAULT)
				.addParam("userEmail", getUserEmail())
				.addParam("connector", newDriver)
				.run();
//			UserSettings.saveNewDefaultConnector(getSessionBean().getUserId(), conn.getClass().getName());
			
			return new OpList()
					.add(new OpBeansInfo("Default database driver changed successfully"));
		} else 
			return new OpList()
					.add(new OpBeansError("Cannot change the default driver"));
	}
}
