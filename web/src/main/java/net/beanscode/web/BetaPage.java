package net.beanscode.web;

import static net.beanscode.cli.BeansCliCommand.API_SETTING_GETSETTING;
import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;
import static org.rendersnake.HtmlAttributesFactory.href;

import java.io.IOException;

import javax.annotation.security.PermitAll;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.UriInfo;

import org.rendersnake.HtmlCanvas;

import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.web.beta.DashboardContent;
import net.beanscode.web.jersey.StaticContent;
import net.beanscode.web.view.backup.BackupBetaContent;
import net.beanscode.web.view.datamanger.BulkContent;
import net.beanscode.web.view.dataset.DatasetBetaContent;
import net.beanscode.web.view.dataset.DatasetsBetaContent;
import net.beanscode.web.view.dataset.TablesBetaContent;
import net.beanscode.web.view.help.HelpBetaContent;
import net.beanscode.web.view.notebook.NotebookContent;
import net.beanscode.web.view.notebook.NotebooksBetaContent;
import net.beanscode.web.view.notebook.QuickSearchPanel;
import net.beanscode.web.view.notification.MenuNotificationsPanel;
import net.beanscode.web.view.notification.NotificationsBetaContent;
import net.beanscode.web.view.plugins.PluginsBetaContent;
import net.beanscode.web.view.settings.AccountSettingsBetaContent;
import net.beanscode.web.view.settings.AppOptionsBetaContent;
import net.beanscode.web.view.settings.DatabaseBetaContent;
import net.beanscode.web.view.settings.DemoBetaContent;
import net.beanscode.web.view.settings.InternalsBetaContent;
import net.beanscode.web.view.settings.MyAccountBetaContent;
import net.beanscode.web.view.settings.SearchBetaContent;
import net.beanscode.web.view.settings.Setting;
import net.beanscode.web.view.settings.jobspanel.JobsBetaContent;
import net.beanscode.web.view.summary.DiagnosticsContent;
import net.beanscode.web.view.ui.BeansComponent;
import net.beanscode.web.view.ui.BeansLogo;
import net.beanscode.web.view.users.AllUsersBetaContent;
import net.beanscode.web.view.users.GroupsBetaContent;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.pjf.components.jersey.SessionBean;
import net.hypki.libs5.pjf.components.ops.OpComponentClick;
import net.hypki.libs5.pjf.op.OpAddClass;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.pjf.op.OpRemoveClass;
import net.hypki.libs5.pjf.op.OpReplace;
import net.hypki.libs5.pjf.op.OpShow;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.date.Watch;
import net.hypki.libs5.utils.reflection.SystemUtils;
import net.hypki.libs5.utils.url.Params;
import net.hypki.libs5.weblibs.user.User;
import net.hypki.libs5.weblibs.user.UserFactory;

@Path("/")
public class BetaPage extends BeansComponent {

	@GET
	@PermitAll
	@Produces(MediaType.TEXT_HTML)
    public String init(@Context SecurityContext sc, 
    		@Context UriInfo uriInfo,
    		@QueryParam("content") String classToLoad) throws IOException {
		try {
			Watch w = new Watch();
			setSecurityContext(sc);
			
			String homeHtml = createPage(sc);
			
			// content
			OpList bc = null;
//			if (notEmpty(classToLoad)) {
//				try {
//					if (classToLoad.equals(BulkContent.class.getSimpleName())
//							|| classToLoad.equals(BulkContent.class.getName()))
//						bc = onBulkClick();
//					else if (classToLoad.equals(NotebookContent.class.getSimpleName())
//							|| classToLoad.equals(NotebookContent.class.getSimpleName()))
//						bc = new OpList()
//							.add(new NotebookContent(this).onShow());
//				} catch (Exception e) {
//					LibsLogger.error(BetaPage.class, "Cannot create instance of " + classToLoad, e);
//					bc = null;
//				}
//			}
			// default content is dashboard
			if (bc == null)
				bc = onDashboardClick();
			// TODO in the page source this line is HUGE
			homeHtml = homeHtml.replace("<!--content-->", bc.toStringAdhock());
			
			LibsLogger.info(BetaPage.class, "Home html prepared in " + w);
			
			return homeHtml;
		} catch (Exception e) {
			LibsLogger.error(BetaPage.class, "Cannot ceate home page", e);
			
			return new StaticContent().getLoginPage();
		}
	}
	
	public String createPage(SecurityContext sc) throws IOException {
		setSecurityContext(sc);
		
		final SessionBean sb = (SessionBean) sc.getUserPrincipal();
		LibsLogger.debug(BetaPage.class, "SessionBean ", sb);
		
		String homeHtml = SystemUtils.readFileContent("adminlte/index.html");
		
		if (sb != null) {
			final User user = UserFactory.getUser(sb.getUserId());
			
			if (user == null)
				throw new SecurityException("Permission denied");
			
			// menu
			homeHtml = homeHtml.replace("<!--username-->", user.getLogin());
			
			// subtitle
			Setting s = new ApiCallCommand(sb)
							.setApi(API_SETTING_GETSETTING)
							.addParam("id", "BEANS_SUBTITLE")
							.run()
							.getResponse()
							.getAsObject(Setting.class);
			homeHtml = homeHtml.replace("<!--subtitle-->", s.getValues().get(0).getValueAsString());
			
			homeHtml = homeHtml.replace("<!--beans-logo-->", new BeansLogo(this).toHtml());
			
			// quick search panel
			homeHtml = homeHtml.replace("<!--beans-quick-search-panel-->", new QuickSearchPanel(sc, new Params()).toHtml());
			homeHtml = homeHtml.replace("beans-menu-notifications-onclick", new OpComponentClick(this, "onMenuNotificationClick").toStringOnclick());
			
			// version
			homeHtml = homeHtml.replace("<!--version-->", new HtmlCanvas()
					.a(href("https://beanscode.net/download/")
							.add("target", "_blank"))
						.content(BeansWebConst.VERSION)
					.toHtml());
		}
		
		return homeHtml;
	}
	
	private OpList onMenuNotificationClick() throws IOException {
		return new OpList()
			.add(new OpReplace(".beans-menu-notifications", new MenuNotificationsPanel(this).toHtml()))
			.add(new OpAddClass(".beans-menu-notifications .dropdown-menu-lg", "show"))
			;
	}

	public OpList onDashboardClick() throws IOException {
		return new OpList()
			.add(new OpRemoveClass(".sidebar .nav-link", "active"))
			.add(new OpAddClass(".beans-sidebar-dashboard .nav-link", "active"))
			.add(new DashboardContent(this).onShow());
	}
		
	
	
//	public OpList onPaletteClick() throws IOException {
//		return new OpList()
//			.add(new OpRemoveClass(".sidebar .nav-link", "active"))
//			.add(new OpAddClass(".beans-sidebar-palette .nav-link", "active"))
//			.add(new ColorPaletteBetaContent(this).onShow());
//	}
	
//	public OpList onDemoClick() throws IOException {
//		return new OpList()
//			.add(new OpRemoveClass(".sidebar .nav-link", "active"))
//			.add(new OpAddClass(".beans-sidebar-demo .nav-link", "active"))
//			.add(new DemoBetaContent(this).onShow());
//	}
	
}
