package net.beanscode.web.beta;

import static org.rendersnake.HtmlAttributesFactory.class_;

import java.io.IOException;

import net.beanscode.web.BetaPage;
import net.beanscode.web.view.dataset.DatasetBetaTableComponent;
import net.beanscode.web.view.notebook.NotebooksTableComponent;
import net.beanscode.web.view.notification.NotificationsPanel;
import net.beanscode.web.view.search.BeansExplorer;
import net.beanscode.web.view.summary.NewsPanel;
import net.beanscode.web.view.ui.BeansComponent;
import net.hypki.libs5.pjf.components.Component;
import net.hypki.libs5.utils.reflection.SystemUtils;

import org.rendersnake.HtmlCanvas;


public class DashboardPanel extends BeansComponent {

	public DashboardPanel() {
		
	}
	
	public DashboardPanel(Component parent) {
		super(parent);
	}
	
	@Override
	public void renderOn(HtmlCanvas html) throws IOException {
		html
			.div(class_("container-fluid"))
			;
		
		html
			.render(new StatsPanel(this));
		
		html
				.div(class_("row"))
					.section(class_("col-lg-6"))
						.render(new NotebooksTableComponent(this)
								.setCompact(true)
								.setPostponeReload(true))
					._section()
					.section(class_("col-lg-6"))
						.render(new DatasetBetaTableComponent(this)
								.setCompact(true)
								.setPostponeReload(true))
					._section()
				._div()
				
				.div(class_("row"))
					.section(class_("col-lg-6"))
						.render(new NotificationsPanel(this))
					._section()
					
//					.section(class_("col-lg-6"))
						.render(new NewsPanel(this))
//					._section()
				._div()
			._div();
	}
}
