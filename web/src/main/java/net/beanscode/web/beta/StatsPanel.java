package net.beanscode.web.beta;

import static org.rendersnake.HtmlAttributesFactory.class_;

import java.io.IOException;

import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.web.view.dataset.DatasetsBetaContent;
import net.beanscode.web.view.dataset.TablesBetaContent;
import net.beanscode.web.view.notebook.NotebooksBetaContent;
import net.beanscode.web.view.ui.BeansComponent;
import net.beanscode.web.view.users.AllUsersBetaContent;
import net.hypki.libs5.pjf.components.Component;
import net.hypki.libs5.pjf.components.ops.OpComponentClick;
import net.hypki.libs5.pjf.op.OpList;

import org.rendersnake.HtmlCanvas;

public class StatsPanel extends BeansComponent {

	public StatsPanel() {
		
	}
	
	public StatsPanel(Component parent) {
		super(parent);
	}
	
	@Override
	public void renderOn(HtmlCanvas html) throws IOException {
		html
			.div(class_("row"));
		
		long maxhits = new ApiCallCommand(this)
			.setApi("api/dataset/query")
			.run()
			.getResponse()
			.getAsLong("maxHits");
		renderBox(html, maxhits, "Datasets", "fa fa-hdd", "bg-info", 
				new OpList().add(new OpComponentClick(DatasetsBetaContent.class, "onShow")));
		
		maxhits = new ApiCallCommand(this)
			.setApi("api/table/search")
			.run()
			.getResponse()
			.getAsLong("maxHits");
		renderBox(html, maxhits, "Tables", "fa fa-table", "bg-success", 
				new OpList().add(new OpComponentClick(TablesBetaContent.class, "onShow")));
		
		maxhits = new ApiCallCommand(this)
			.setApi("api/notebook/query")
			.run()
			.getResponse()
			.getAsLong("maxHits");
		renderBox(html, maxhits, "Notebooks", "fa fa-book", "bg-danger", 
				new OpList().add(new OpComponentClick(NotebooksBetaContent.class, "onShow")));
		
		maxhits = new ApiCallCommand(this)
			.setApi("api/user/search")
			.run()
			.getResponse()
			.getAsLong("maxHits");
		renderBox(html, maxhits, "Users", "fa fa-users", "bg-warning", 
				new OpList()
					.add(new OpComponentClick(AllUsersBetaContent.class, "onShow")));
		
		html
			._div();
	}

	private void renderBox(HtmlCanvas html, long count, String content, String awesomeIcon, 
			String bgColor, OpList onClick) throws IOException {
		html
			.div(class_("col-lg-3 col-6"))
				.div(class_("small-box " + bgColor))
					.div(class_("inner"))
						.h3(class_(""))
							.content("" + count)
						.p()
							.content(content)
					._div()
					.div(class_("icon"))
						.i(class_(awesomeIcon))
						._i()
					._div()
					.a(class_("small-box-footer")
							.onClick(onClick.toStringOnclick()))
						.write("More info ")
						.i(class_("fas fa-arrow-circle"))
						._i()
					._a()
				._div()
			._div();
	}
	
	
}
