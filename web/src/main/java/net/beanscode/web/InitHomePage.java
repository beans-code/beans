package net.beanscode.web;

import static net.beanscode.cli.BeansCliCommand.API_SETTING_GETSETTING;

import java.io.IOException;

import javax.annotation.security.PermitAll;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.UriInfo;

import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.web.view.notification.NotificationsPanel;
import net.beanscode.web.view.settings.DatabaseContent;
import net.beanscode.web.view.settings.DemoContent;
import net.beanscode.web.view.settings.InternalsContent;
import net.beanscode.web.view.settings.SearchBetaContent;
import net.beanscode.web.view.settings.Setting;
import net.beanscode.web.view.users.LoginPanel;
import net.hypki.libs5.cli.noninteractivecli.CLIException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.pjf.components.buttons.Button;
import net.hypki.libs5.pjf.components.buttons.ButtonGroup;
import net.hypki.libs5.pjf.components.buttons.ButtonSize;
import net.hypki.libs5.pjf.components.ops.OpComponentClick;
import net.hypki.libs5.pjf.op.OpHide;
import net.hypki.libs5.pjf.op.OpInfoCookie;
import net.hypki.libs5.pjf.op.OpJs;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.pjf.op.OpRedirect;
import net.hypki.libs5.pjf.op.OpSet;
import net.hypki.libs5.pjf.op.OpSetAttr;
import net.hypki.libs5.utils.api.APICallType;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.utils.url.Params;

@Path("view")
@PermitAll
public class InitHomePage extends BeansPage {
				
    @POST
    @Path("home")
	@PermitAll
    @Produces(MediaType.APPLICATION_JSON)
    public String init(@Context SecurityContext sc, @Context UriInfo uriInfo) throws IOException {

//		addToResponse(new OpSetAttr("#menu-my-account-btn", 	"onclick", new OpComponentClick(MyAccountContent.class, UUID.random(), "showContent").toStringOnclick()));
//		addToResponse(new OpSetAttr("#menu-account-settings-btn", "onclick", new OpComponentClick(AccountSettings.class, UUID.random(), "showContent").toStringOnclick()));
//		addToResponse(new OpSetAttr("#menu-color-palette-btn", 	"onclick", new OpComponentClick(ColorPaletteContent.class, UUID.random(), "showContent").toStringOnclick()));
//		addToResponse(new OpSetAttr("#menu-app-backup", 		"onclick", new OpComponentClick(BackupPanel.class, UUID.random(), "showContent").toStringOnclick()));
//		if (BeansConst.DEVELOPERS_MODE)
//			addToResponse(new OpSetAttr("#menu-app-demo-btn", 	"onclick", new OpComponentClick(DemoContent.class, UUID.random(), "showContent", null).toStringOnclick()));
//		else
//			addToResponse(new OpHide("#menu-app-demo-btn"));
//		if (getSessionBean(sc).hasRole(Rights.ADMIN)) {
//			addToResponse(new OpSetAttr("#menu-users-btn", 			"onclick", new OpComponentClick(AllUsersContent.class, UUID.random(), "showContent").toStringOnclick()));
//			addToResponse(new OpSetAttr("#menu-vital-checks", 		"onclick", new OpComponentClick(VitalChecksPage.class, UUID.random(), "showContent").toStringOnclick()));
//			addToResponse(new OpSetAttr("#menu-plugins-btn", 		"onclick", new OpComponentClick(PluginsContent.class, UUID.random(), "showContent").toStringOnclick()));
//			addToResponse(new OpSetAttr("#menu-jobs-btn", 			"onclick", new OpComponentClick(JobsPanel.class, UUID.random(), "showContent").toStringOnclick()));
//			addToResponse(new OpSetAttr("#menu-app-options-btn", 	"onclick", new OpComponentClick(AppOptionsContent.class, UUID.random(), "showContent").toStringOnclick()));
//			addToResponse(new OpSetAttr("#menu-app-internals", 		"onclick", new OpComponentClick(InternalsContent.class, UUID.random(), "showContent", null).toStringOnclick()));
//			addToResponse(new OpShow(".administration-menu"));
//		}
		
		// show list of notebooks by default
//		SearchPanel search = new SearchPanel();
//		addToResponse(new OpSet(".search-bar", search.toHtml()));
//		addToResponse(search.getOpList());
    	
    	if (getLoggedUser(sc) == null)
    		throw new SecurityException("User is not logged in");
//    	AssertUtils.assertTrue(getLoggedUser(sc) != null, "User is not logged in!");
		
		ButtonGroup menu = new ButtonGroup()
			.setButtonSize(ButtonSize.NORMAL)
			.addButton(new Button("Data")
//					.addButton(new Button("All notebooks")
//							.add(new OpComponentClick(AllNotebooksContent.class, UUID.random(), "showContent", null)))
//					.addButton(new Button("New notebook")
//							.add(new OpComponentClick(NotebookContent.class, UUID.random(), "create", null)))
					.addButton(new Button("Datasets")
						.setSeparator(true))
//						.addButton(new Button("Datasets")
//					.addButton(new Button("All datasets")
//							.add(new OpComponentClick(AllDatasetsContent.class, UUID.random(), "showContent", null)))
//					.addButton(new Button("New dataset")
//							.add(new OpComponentClick(DatasetContent.class, UUID.random(), "createDataset", null)))
//					.addButton(new Button("Datasets")
//						.setSeparator(true))
//					.addButton(new Button("Bulk changes")
//							.add(new OpComponentClick(BulkChanges.class, UUID.random(), "showContent", null)))
					.addButton(new Button("Help")
						.setSeparator(true))
//					.addButton(new Button("Help")
//							.add(new OpComponentClick(HelpContent.class, UUID.random(), "showContent", null)))
					)
			.addButton(new Button("Account")
//					.addButton(new Button("My account")
//							.add(new OpComponentClick(MyAccountContent.class, UUID.random(), "showContent")))
//					.addButton(new Button("Groups")
//							.add(new OpComponentClick(GroupsContent.class, UUID.random(), "showContent")))
//					.addButton(new Button("Settings")
//							.add(new OpComponentClick(AccountSettings.class, UUID.random(), "showContent")))
//					.addButton(new Button("Color palette")
//							.add(new OpComponentClick(ColorPaletteContent.class, UUID.random(), "showContent")))
					.addButton(new Button("Demo data")
//							.setVisible(BeansConst.DEVELOPERS_MODE)
							.add(new OpComponentClick(DemoContent.class, UUID.random(), "showContent", null)))
					.addButton(new Button("Other")
						.setSeparator(true))
					.addButton(new Button("Log out")
						.add(new OpJs("eraseCookie('userLC'); eraseCookie('userId'); window.location = '/login';")))
					)
			.addButton(new Button()
					.setAwesomeicon("cog")
					.setVisible(isAdmin(sc))
//					.addButton(new Button("Users")
//							.setVisible(isAdmin(sc))
//							.add(new OpComponentClick(AllUsersContent.class, UUID.random(), "showContent")))
//					.addButton(new Button("Diagnostics")
//							.setVisible(isAdmin(sc))
//							.add(new OpComponentClick(VitalChecksPage.class, UUID.random(), "showContent")))
//					.addButton(new Button("Application options")
//							.setVisible(isAdmin(sc))
//							.add(new OpComponentClick(AppOptionsContent.class, UUID.random(), "showContent")))
//					.addButton(new Button("Plugins")
//							.setVisible(isAdmin(sc))
//							.add(new OpComponentClick(PluginsContent.class, UUID.random(), "showContent")))
//					.addButton(new Button("Jobs")
//							.setVisible(isAdmin(sc))
//							.add(new OpComponentClick(JobsPanel.class, UUID.random(), "showContent")))
//					.addButton(new Button("Backup")
//							.add(new OpComponentClick(BackupContent.class, UUID.random(), "showContent")))
					.addButton(new Button("Database")
							.setVisible(isAdmin(sc))
							.add(new OpComponentClick(DatabaseContent.class, UUID.random(), "showContent", null)))
					.addButton(new Button("Search index")
							.setVisible(isAdmin(sc))
							.add(new OpComponentClick(SearchBetaContent.class, UUID.random(), "showContent", null)))
					.addButton(new Button("BEANS internals")
							.setVisible(isAdmin(sc))
							.add(new OpComponentClick(InternalsContent.class, UUID.random(), "showContent", null)))
					)
					;
		addToResponse(new OpSet(".main-menu", menu.toHtml()));
		
		// refresh jobs statuses
		addToResponse(new OpSet("#notifications", new NotificationsPanel(sc, new Params()).toHtml()));
		
		// table lookup
//		addToResponse(new OpSet("#tables-dropdown", new QuickSearchPanel(sc, new Params()).toHtml()));
		
		// BEANS subtitle
		Setting s = new ApiCallCommand(getSessionBean(sc))
					.setApi(API_SETTING_GETSETTING)
					.addParam("id", "BEANS_SUBTITLE")
					.run()
					.getResponse()
					.getAsObject(Setting.class);
		if (s != null)
			addToResponse(new OpSet(".version sub", s.getValues().get(0).getValueAsString() + ", ver. " + BeansWebConst.VERSION));
		
//		addToResponse(new OpAppend("#main", new DevelPanel().toHtml()));
		
//		addToResponse(new OpAppend("#main", new DashboardPanel(sc).toHtml()));
		addToResponse(new OpSet("#page-title", "Dashboard"));
		
//		addToResponse(new OpAppend("#main", new HtmlCanvas().h1().content("Recent notebooks").toHtml()));
//		addToResponse(new OpAppend("#main", new NotebooksTableComponent(sc, new Params())
//			.setPerPageVisible(false)
//			.setPerPage(10)
//			.toHtml()));
    	
    	return toString();
    }
    
    @POST
    @Path("login")
	@PermitAll
    @Produces(MediaType.APPLICATION_JSON)
    public String initLoginPage(@Context SecurityContext sc, @Context UriInfo uriInfo) throws IOException {
//    	String qs = uriInfo.getRequestUri().get getHost() Query();
//    	if (StringUtilities.notEmpty(resetId)) {
//        	OpList ops = new OpList();
//        	ops
//        		.add(new OpInfoCookie("New password was send to your email"))
//        		.add(new OpRedirect("/login"));
//        	return ops.toString();
//    	}
    	
    	boolean registrationOn = false;
    	
    	try {
			registrationOn = new ApiCallCommand(getSessionBean(sc))
										.setApi("api/setting/registration")
										.setApiCallType(APICallType.GET)
										.setLoginRequired(false)
										.run()
										.getResponse()
										.getAsBoolean("registration");
		} catch (CLIException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//    			new RegistrationIsEnabled(getSessionBean(sc)).run();//BeansSettings.isRegistrationEnabled();
    	
    	OpList ops = new OpList();
    	if (!registrationOn)
    		addToResponse(new OpHide("#signup-button"));
    	
    	// pjfOps([{"type" : "ajax", "url" : "http://localhost:25000/api/user/login", "params" : $("#form-signin :input").serialize()}]); return false;
    	UUID id = UUID.random();
    	addToResponse(new OpSetAttr("#form-signin", "id", id.getId()));
    	addToResponse(new OpSetAttr("#btn-login", "onclick", 
    			new OpComponentClick(LoginPanel.class, id.getId(), "login").toStringOnclick() + "; return false;"));
    	
    	return toString();
    }
    
    @POST
    @Path("recover")
	@PermitAll
    @Produces(MediaType.APPLICATION_JSON)
    public String recover(@Context SecurityContext sc, @Context UriInfo uriInfo,
    		@FormParam("email") String user,
    		@FormParam("resetId") String resetId) throws IOException {
    	OpList ops = new OpList();
    	
    	ApiCallCommand api = (ApiCallCommand) ((ApiCallCommand) new ApiCallCommand(getSessionBean(sc))
    		.setApi("api/user/recover")
    		.addParam("email", user)
    		.setLoginRequired(false))
    		.run();
    	
    	ops
    		.add(new OpInfoCookie("New password was send to your email"))
    		.add(new OpRedirect("/login"));
    	
    	return ops.toString();
    }
    
    @GET
    @Path("confirm")
	@PermitAll
    @Produces(MediaType.APPLICATION_JSON)
    public String recoverGet(@Context SecurityContext sc, @Context UriInfo uriInfo,
    		@QueryParam("userId") String user,
    		@QueryParam("resetId") String resetId) throws IOException {
    	OpList ops = new OpList();
    	
    	if (StringUtilities.notEmpty(user)) {
	    	ApiCallCommand api = (ApiCallCommand) ((ApiCallCommand) new ApiCallCommand(getSessionBean(sc))
	    		.setLoginRequired(false))
	    		.setApi("api/user/reset")
	    		.addParam("userId", user)
	    		.run();
    	}
    	
    	ops
    		.add(new OpInfoCookie("New password was send to your email"))
    		.add(new OpRedirect("/login"));
    	
    	return ops.toString();
    }
}
