package net.beanscode.cli.settings;

import java.util.List;

import com.google.gson.annotations.Expose;

public class SettingsSearchResults {

	@Expose
	private List<Property> properties = null;
	
	@Expose
	private int maxHits = 0;
	
	public SettingsSearchResults() {
		
	}

	public List<Property> getProperties() {
		return properties;
	}

	public SettingsSearchResults setProperties(List<Property> datasets) {
		this.properties = datasets;
		return this;
	}

	public int getMaxHits() {
		return maxHits;
	}

	public SettingsSearchResults setMaxHits(int maxHits) {
		this.maxHits = maxHits;
		return this;
	}
	
	public SettingsSearchResults setMaxHits(long maxHits) {
		this.maxHits = (int) maxHits;
		return this;
	}
}
