package net.beanscode.cli.settings;

import java.io.Serializable;
import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;

import net.hypki.libs5.utils.LibsLogger;

import com.google.gson.annotations.Expose;

public class MailCredentials {

	@Expose
	private String host = null;
	
	@Expose
	private int port = 0;
	
	@Expose
	private String from = null;
	
	@Expose
	private String pass = null;
	
	@Expose
	private boolean ssl = false;
	
	@Expose
	private int timeoutMs = 25000;
	
	public MailCredentials() {
		
	}
	
	public MailCredentials(String host, int port, String from, String pass, boolean ssl) {
		setHost(host);
		setPort(port);
		setFrom(from);
		setPass(pass);
		setSsl(ssl);
	}
	
	public MailCredentials(String host, int port, String from, String pass, boolean ssl, int timeoutMs) {
		setHost(host);
		setPort(port);
		setFrom(from);
		setPass(pass);
		setSsl(ssl);
		setTimeoutMs(timeoutMs);
	}
	
	@Override
	public String toString() {
		return String.format("Host=%s Port=%d Username=%s", getHost(), getPort(), getFrom());
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public boolean isSsl() {
		return ssl;
	}

	public void setSsl(boolean ssl) {
		this.ssl = ssl;
	}

	public int getTimeoutMs() {
		return timeoutMs;
	}

	public void setTimeoutMs(int timeoutMs) {
		this.timeoutMs = timeoutMs;
	}
}
