package net.beanscode.cli.settings;

import net.hypki.libs5.db.db.weblibs.utils.UUID;

import com.google.gson.annotations.Expose;

public class Property {
	
	@Expose
	private String key = null;
	
	@Expose
	private String value = null;
	
	@Expose
	private String description = null;
	
	@Expose
	private String lang = null;
	
	@Expose
	private UUID userId = null;
	
	public Property() {
		
	}

	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public UUID getUserId() {
		return userId;
	}

	public void setUserId(UUID userId) {
		this.userId = userId;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getDescription() {
		return description;
	}
	
	public String getDescriptionNotNull() {
		return description != null ? description : "";
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getValueAsString() {
		return getValue() != null ? String.valueOf(getValue()) : "";
	}
	
}
