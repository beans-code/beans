package net.beanscode.cli.jobs;

import net.hypki.libs5.utils.date.SimpleDate;

import com.google.gson.annotations.Expose;

public class JobsWorker {
	
	@Expose
	private long id = 0;

	@Expose
	private String channel = null;
	
	@Expose
	private String currentJobName = null;
	
	@Expose
	private long currentJobStart = 0L;
	
	public JobsWorker() {
		
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getCurrentJobName() {
		return currentJobName;
	}

	public void setCurrentJobName(String currentJobName) {
		this.currentJobName = currentJobName;
	}

	public SimpleDate getCurrentJobStart() {
		return new SimpleDate(currentJobStart);
	}

	public void setCurrentJobStart(long currentJobStart) {
		this.currentJobStart = currentJobStart;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

}
