package net.beanscode.cli.web.utils;

import net.hypki.libs5.utils.LibsLogger;

import org.markdownj.MarkdownProcessor;

public class MarkdownUtils {
	private static MarkdownProcessor markdownProcessor = null;
	
	static {
		LibsLogger.debug(MarkdownUtils.class, "Initializing MarkdownProcessor");
		markdownProcessor = new MarkdownProcessor();
	}
	
	public static String toHtml(final String markdownText) {
		return markdownProcessor.markdown(markdownText);
	}
}
