package net.beanscode.cli.notebooks;

import java.util.List;
import java.util.regex.Pattern;

import net.beanscode.cli.users.User;
import net.beanscode.pojo.NotebookEntry;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.date.SimpleDate;
import net.hypki.libs5.utils.string.Meta;
import net.hypki.libs5.utils.string.RegexUtils;
import net.hypki.libs5.utils.string.StringUtilities;

// TODO name to PigEntry
public class PigScript extends NotebookEntry {
	
	public static final String PIG_MODE_LOCAL 		= "local";
	public static final String PIG_MODE_TEZ_LOCAL 	= "tez_local";
	public static final String PIG_MODE_MAPREDUCE 	= "mapreduce";
	public static final String PIG_MODE_TEZ 		= "tez";
	
	public static final String META_MODE 			= "META_MODE";
	public static final String META_QUERY 			= "META_QUERY";
	public static final String META_DRAFT 			= "META_DRAFT";
	public static final String META_DATAID 		= "META_DATAID";
	public static final String META_READ_TABLES	= "META_READ_TABLES";
		
	public PigScript() {
		setDataId(UUID.random());
	}
	
	public PigScript(UUID userId, UUID notebookId, String name) {
		setId(UUID.random());
		setUserId(userId);
		setNotebookId(notebookId);
		setName(name);
		setCreate(SimpleDate.now());
		setDataId(UUID.random());
	}
	
	public PigScript(User user, UUID notebookId, String name) {
		setId(UUID.random());
		setUserId(user.getId());
		setNotebookId(notebookId);
		setName(name);
		setCreate(SimpleDate.now());
		setDataId(UUID.random());
	}
	
	public PigScript(Notebook notebook, String name) {
		setId(UUID.random());
		setUserId(notebook.getUserId());
		setNotebookId(notebook.getId());
		setName(name);
		setCreate(SimpleDate.now());
		setDataId(UUID.random());
	}
		
	public String getQuery() {
		return getMetaAsString(META_QUERY, null);
	}
	
	@Override
	public String toString() {
		return getName();
	}

	public PigScript setQuery(String query) {
		setMeta(META_QUERY, query);
		return this;
	}
	

	public UUID getDataId() {
		String tmp = getMetaAsString(META_DATAID, null);
		return tmp != null ? new UUID(tmp) : null;
	}

	public void setDataId(UUID dataId) {
		setMeta(META_DATAID, dataId != null ? dataId.getId() : null);
	}
	
	public List<String> getLoadGroups() {
		return RegexUtils.allGroups("(load[\\s]+'.*'[\\s]+using[\\s]+BeansTable)", 
				getQuery(), 
				Pattern.CASE_INSENSITIVE);
	};
	
	public String getPigMode() {
		Meta meta = getMeta().get(META_MODE);
		return meta != null && meta.getValue() != null ? meta.getAsString() : null;
	}
	
	public void clearDraft() {
		setDraft(null);
	}
	
	public boolean isDraftAvailable() {
		return StringUtilities.notEmpty(getDraft());
	}

	public String getDraft() {
		return getMetaAsString(META_DRAFT);
	}

	public void setDraft(String pig) {
		setMeta(META_DRAFT, pig);
	}
}
