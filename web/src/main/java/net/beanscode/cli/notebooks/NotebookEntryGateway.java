package net.beanscode.cli.notebooks;

import static net.beanscode.cli.BeansCliCommand.API_NOTEBOOKENTRY_QUERY;
import static net.beanscode.cli.BeansCliCommand.API_NOTEBOOK_QUERY;

import java.io.IOException;
import java.util.Iterator;

import net.beanscode.cli.BeansCliCommand;
import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.cli.datasets.Table;
import net.beanscode.pojo.NotebookEntry;
import net.hypki.libs5.cli.noninteractivecli.CLIException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.pjf.components.jersey.SessionBean;
import net.hypki.libs5.search.SearchResults;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.json.JsonUtils;

import com.google.gson.JsonObject;

public class NotebookEntryGateway {

	public static NotebookEntry getNotebookEntry(JsonObject je) {
		if (je == null || je.get("type") == null)
			return null;
		
		String type = je.get("type").getAsString();
		if (type.endsWith(PigScript.class.getSimpleName()))
			return JsonUtils.fromJson(je, PigScript.class);
		else if (type.endsWith(ButcheredPigEntry.class.getSimpleName()))
			return JsonUtils.fromJson(je, ButcheredPigEntry.class);
		else if (type.endsWith(Plot.class.getSimpleName()))
			return JsonUtils.fromJson(je, Plot.class);
		else if (type.endsWith(TextEntry.class.getSimpleName()))
			return JsonUtils.fromJson(je, TextEntry.class);
		else if (type.endsWith(AwkEntry.class.getSimpleName()))
			return JsonUtils.fromJson(je, AwkEntry.class);
		
		else if (type.endsWith(PlotParallelCoordinatesEntry.class.getSimpleName()))
			return JsonUtils.fromJson(je, PlotParallelCoordinatesEntry.class);
		
		else if (type.endsWith(PlotHistogramEntry.class.getSimpleName()))
			return JsonUtils.fromJson(je, PlotHistogramEntry.class);
		
		else if (type.endsWith(AttachmentEntry.class.getSimpleName()))
			return JsonUtils.fromJson(je, AttachmentEntry.class);
		
		else if (type.endsWith(GacsEntry.class.getSimpleName()))
			return JsonUtils.fromJson(je, GacsEntry.class);
		
		else if (type.endsWith(GnuplotEntry.class.getSimpleName()))
			return JsonUtils.fromJson(je, GnuplotEntry.class);
		
		else if (type.endsWith(PythonEntry.class.getSimpleName()))
			return JsonUtils.fromJson(je, PythonEntry.class);
		
		else if (type.endsWith(LinkEntry.class.getSimpleName()))
			return JsonUtils.fromJson(je, LinkEntry.class);
		
		else if (type.endsWith(MLEntry.class.getSimpleName()))
			return JsonUtils.fromJson(je, MLEntry.class);
		
		else {
			
			try {
				return (NotebookEntry) JsonUtils.fromJson(je, NotebookEntry.class);
			} catch (Throwable e) {
				LibsLogger.error(NotebookEntryGateway.class, "Cannot find a class " + type, e);
				return null;
			}
			
		}
			
//			throw new NotImplementedException();
	}
	
	public static NotebookEntry getNotebookEntry(SessionBean sb, String entryId) throws CLIException, IOException {
		return getNotebookEntry(sb, new UUID(entryId));
	}
	
	public static NotebookEntry getNotebookEntry(SessionBean sb, UUID entryId) throws CLIException, IOException {
		return NotebookEntryGateway.getNotebookEntry(new ApiCallCommand(sb)
				.setApi("api/notebook/entry/get")
				.addParam("entryId", entryId.getId())
				.run()
				.getResponse()
				.getAsJson()
				.getAsJsonObject());
	}
	
	public static NotebookEntry getNotebook(SessionBean sb, UUID entryId) throws CLIException, IOException {
		return new ApiCallCommand(sb)
			.setApi("/api/notebook/get")
			.addParam("entryId", entryId)
			.run()
			.getResponse()
			.getAsObject(NotebookEntry.class);
	}
	
	public static SearchResults<NotebookEntry> searchEntries(SessionBean sb, String notebookQueryOrId, 
			String filter, int page, int perPage) throws CLIException, IOException {
		ApiCallCommand call = new ApiCallCommand(sb)
				.setApi(API_NOTEBOOKENTRY_QUERY)
				.addParam("notebookId", notebookQueryOrId != null ? notebookQueryOrId.toString() : "")
				.addParam("userId", sb.getUserId().getId())
				.addParam("query", filter == null ? "" : filter)
				.addParam("from", page * perPage)
				.addParam("size", perPage)
				.run()
//				.getResponse()
//				.getAsObject(SearchResults.class)
				;
		
		return new SearchResults<NotebookEntry>(call.getResponse().getAsList("entries", NotebookEntry.class), 
						call.getResponse().getAsLong("maxHits"));
	}

	public Iterable<NotebookEntry> iterateEntries(final SessionBean sessionBean, Notebook notebook) {
		return new Iterable<NotebookEntry>() {
			@Override
			public Iterator<NotebookEntry> iterator() {
				return new Iterator<NotebookEntry>() {
					private Iterator<UUID> entriesIds = notebook.getEntries().iterator();
					
					@Override
					public NotebookEntry next() {
						try {
							return NotebookEntryGateway.getNotebookEntry(new ApiCallCommand(sessionBean)
										.setApi("api/notebook/entry/get")
										.addParam("entryId", entriesIds.next().getId())
										.run()
										.getResponse()
										.getAsJson()
										.getAsJsonObject());
						} catch (CLIException | IOException e) {
							LibsLogger.error(Notebook.class, "Cannot get NotebookEntry in iterator", e);
							return null;
						}
					}
					
					@Override
					public boolean hasNext() {
						return entriesIds.hasNext();
					}
				};
			}
		};
	}
}
