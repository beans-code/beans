package net.beanscode.cli.notebooks;

import static net.hypki.libs5.utils.string.StringUtilities.nullOrEmpty;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.google.gson.annotations.Expose;

import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.pojo.NotebookEntry;
import net.beanscode.pojo.tags.Tags;
import net.hypki.libs5.cli.noninteractivecli.CLIException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.pjf.components.jersey.SessionBean;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.date.SimpleDate;
import net.hypki.libs5.utils.string.MetaList;
import net.hypki.libs5.weblibs.user.User;
import net.hypki.libs5.weblibs.user.UserFactory;

public class Notebook {

	@Expose
	private UUID id = null;
	
	@Expose
	private UUID userId = null;
	
	@Expose
	private String userEmail = null;

	@Expose
	private String name = null;
	
	@Expose
	private String description = null;
	
	@Expose
	private List<UUID> entries = null;
	
	@Expose
	private long creationMs = 0;
	
	@Expose
	private long lastEditMs = 0;
	
	@Expose
	private MetaList meta = null;
	
	@Expose
	private Tags tags = null;
	
	public Notebook() {
		
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public UUID getUserId() {
		return userId;
	}

	public void setUserId(UUID userId) {
		this.userId = userId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}
	
	public String getDescriptionNotNull() {
		return description != null ? description : "";
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<UUID> getEntries() {
		if (entries == null)
			entries = new ArrayList<UUID>();
		return entries;
	}

	public void setEntries(List<UUID> entries) {
		this.entries = entries;
	}

	public long getCreationMs() {
		return creationMs;
	}

	public void setCreationMs(long creationMs) {
		this.creationMs = creationMs;
	}

	public MetaList getMeta() {
		if (meta == null)
			meta = new MetaList();
		return meta;
	}

	public void setMeta(MetaList meta) {
		this.meta = meta;
	}

	public SimpleDate getCreationDate() {
		return new SimpleDate(getCreationMs());
	}

	public String getUserEmail() {
		if (nullOrEmpty(userEmail)) {
			try {
				User user = UserFactory.getUser(getUserId());
				return user != null ? user.getEmail().getEmail() : null;
			} catch (IOException e) {
				LibsLogger.error(Notebook.class, "Cannot get user email from db", e);
			}
		}
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}
	
	public Iterable<NotebookEntry> iterateEntries(final SessionBean sessionBean) {
		return new Iterable<NotebookEntry>() {
			@Override
			public Iterator<NotebookEntry> iterator() {
				return new Iterator<NotebookEntry>() {
					private Iterator<UUID> entriesIds = getEntries().iterator();
					
					@Override
					public NotebookEntry next() {
						try {
							return NotebookEntryGateway.getNotebookEntry(new ApiCallCommand(sessionBean)
									.setApi("api/notebook/entry/get")
									.addParam("entryId", entriesIds.next().getId())
									.run()
									.getResponse()
									.getAsJson()
									.getAsJsonObject());
						} catch (CLIException | IOException e) {
							LibsLogger.error(Notebook.class, "Cannot get NotebookEntry in iterator", e);
							return null;
						}
					}
					
					@Override
					public boolean hasNext() {
						return entriesIds.hasNext();
					}
				};
			}
		};
	}
	
	public SimpleDate getLastEdit() {
		return new SimpleDate(getLastEditMs());
	}

	public long getLastEditMs() {
		return lastEditMs;
	}

	public void setLastEditMs(long lastEditMs) {
		this.lastEditMs = lastEditMs;
	}

	public Tags getTags() {
		if (tags == null)
			tags = new Tags();
		return tags;
	}

	public void setTags(Tags tags) {
		this.tags = tags;
	}
}
