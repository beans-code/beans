package net.beanscode.cli.notebooks;

import net.beanscode.cli.users.User;
import net.beanscode.pojo.NotebookEntry;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.date.SimpleDate;
import net.hypki.libs5.utils.string.Meta;

public class ButcheredPigEntry extends NotebookEntry {
		
	public static final String META_QUERY 			= "META_QUERY";
	public static final String META_DATAID 		= "META_DATAID";
	public static final String META_READ_TABLES	= "META_READ_TABLES";
		
	public ButcheredPigEntry() {
		setDataId(UUID.random());
	}
	
	public ButcheredPigEntry(UUID userId, UUID notebookId, String name) {
		setId(UUID.random());
		setUserId(userId);
		setNotebookId(notebookId);
		setName(name);
		setCreate(SimpleDate.now());
		setDataId(UUID.random());
	}
	
	public ButcheredPigEntry(User user, UUID notebookId, String name) {
		setId(UUID.random());
		setUserId(user.getId());
		setNotebookId(notebookId);
		setName(name);
		setCreate(SimpleDate.now());
		setDataId(UUID.random());
	}
	
	public ButcheredPigEntry(Notebook notebook, String name) {
		setId(UUID.random());
		setUserId(notebook.getUserId());
		setNotebookId(notebook.getId());
		setName(name);
		setCreate(SimpleDate.now());
		setDataId(UUID.random());
	}
		
	public String getQuery() {
		return getMetaAsString(META_QUERY, null);
	}
	
	@Override
	public String toString() {
		return getName();
	}

	public ButcheredPigEntry setQuery(String query) {
		setMeta(META_QUERY, query);
		return this;
	}
	

	public UUID getDataId() {
		String tmp = getMetaAsString(META_DATAID, null);
		return tmp != null ? new UUID(tmp) : null;
	}

	public void setDataId(UUID dataId) {
		setMeta(META_DATAID, dataId != null ? dataId.getId() : null);
	}
	
	public String getPigMode() {
		Meta meta = getMeta().get("PIG_MODE");
		return meta != null && meta.getValue() != null ? meta.getAsString() : null;
	}
}
