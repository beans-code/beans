package net.beanscode.cli.notebooks;

import static net.beanscode.cli.BeansCliCommand.API_NOTEBOOK_QUERY;

import java.io.IOException;

import net.beanscode.cli.api.ApiCallCommand;
import net.hypki.libs5.cli.noninteractivecli.CLIException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.pjf.components.jersey.SessionBean;

public class NotebookGateway {
	
	public static Notebook getNotebook(SessionBean sb, String notebookId) throws CLIException, IOException {
		return getNotebook(sb, new UUID(notebookId));
	}

	public static Notebook getNotebook(SessionBean sb, UUID notebookId) throws CLIException, IOException {
		return new ApiCallCommand(sb)
			.setApi("/api/notebook/get")
			.addParam("notebookId", notebookId)
			.run()
			.getResponse()
			.getAsObject(Notebook.class);
	}
	
	public static NotebookSearchResults searchNotebooks(SessionBean sb, String filter, int page, int perPage) throws CLIException, IOException {
		return new ApiCallCommand(sb)
				.setApi(API_NOTEBOOK_QUERY)
				.addParam("query", filter == null ? "" : filter)
				.addParam("from", page * perPage)
				.addParam("size", perPage)
				.run()
				.getResponse()
				.getAsObject(NotebookSearchResults.class);
	}
}
