package net.beanscode.cli.notebooks;

import net.beanscode.pojo.NotebookEntry;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.string.StringUtilities;

public class MLEntry extends NotebookEntry {
	public static final String META_DS_TRAIN_QUERY 	= "META_DS_TRAIN_QUERY";
	public static final String META_TB_TRAIN_QUERY 	= "META_TB_TRAIN_QUERY";
	public static final String META_TRAIN_COLUMNS	= "META_TRAIN_COLUMNS";
	public static final String META_PREDICT_COLUMN	= "META_PREDICT_COLUMN";
	public static final String META_DS_TEST_QUERY 	= "META_DS_TEST_QUERY";
	public static final String META_TB_TEST_QUERY 	= "META_TB_TEST_QUERY";
	public static final String META_OUTPUT_TABLE	= "META_OUTPUT_TABLE";
	public static final String META_OUTPUT_COLUMN	= "META_OUTPUT_COLUMN";
	
	public MLEntry() {
		super();
	}
	
	public MLEntry(Notebook notebook, String name) {
		super();
		
		setUserId(notebook.getUserId());
		setNotebookId(notebook.getId());
		setName(name);
	}
	
	public MLEntry(UUID userId, UUID notebookId, String name, String script) {
		super();
		
		setUserId(userId);
		setNotebookId(notebookId);
		setName(name);
	}

	public String getTbTestQuery() {
		return getMetaAsString(META_TB_TEST_QUERY);
	}

	public String getDsTestQuery() {
		return getMetaAsString(META_DS_TEST_QUERY);
	}

	public String getDsTrainQuery() {
		return getMetaAsString(META_DS_TRAIN_QUERY);
	}
	
	public String getTbTrainQuery() {
		return getMetaAsString(META_TB_TRAIN_QUERY);
	}

	public String getTrainColumns() {
		return getMetaAsString(META_TRAIN_COLUMNS);
	}

	public String getPredictColumn() {
		return getMetaAsString(META_PREDICT_COLUMN);
	}

	public String getOutputTable() {
		return getMetaAsString(META_OUTPUT_TABLE);
	}
	
	public String getOutputColumn() {
		return getMetaAsString(META_OUTPUT_COLUMN);
	}
}
