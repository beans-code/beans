package net.beanscode.cli.notebooks;

import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import net.hypki.libs5.plot.PlotType;
import net.hypki.libs5.utils.string.RegexUtils;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.utils.utils.NumberUtils;
import net.sf.oval.constraint.NotNull;

import com.google.gson.annotations.Expose;

public class PlotSeries implements Serializable {

	@Expose
	@NotNull
	private List<String> columnsExpr = null;
	
	@Expose
	@NotNull
	private List<String> params = null;
	
	@Expose
	private String legend = null;
	
	@Expose
	private PlotType plotType = PlotType.POINTS;
	
	@Expose
	private double sample = 1.0;
	
	public PlotSeries() {
		
	}
	
	public PlotSeries(String x, String y, String legend) {
		setX(x);
		setY(y);
		setLegend(legend);
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		if (notEmpty(getLegend())) {
			sb.append(getLegend());
			sb.append(" ");
		}
		for (String col : getColumnsExpr()) {
			sb.append(col);
			sb.append(":");
		}
		return sb.toString();
	}

	public String getX() {
		return getColumnsExpr().size() > 0 ? getColumnsExpr().get(0) : null;
	}

	public void setX(String x) {
		if (getColumnsExpr().size() > 0) {
			getColumnsExpr().remove(0);
			getColumnsExpr().add(0, x);
		} else
			getColumnsExpr().add(0, x);
		rebuildParams();
	}

	public String getY() {
		return getColumnsExpr().size() > 1 ? getColumnsExpr().get(1) : null;
	}

	public void setY(String y) {
		if (getColumnsExpr().size() > 1) {
			getColumnsExpr().remove(1);
			getColumnsExpr().add(1, y);
		} else
			getColumnsExpr().add(y);
		rebuildParams();
	}

	public String getLegend() {
		return legend;
	}

	public void setLegend(String legend) {
		this.legend = legend;
	}

	public boolean contains(String colName) {
		return getX().equals(colName) || getY().equals(colName);
	}

	public List<String> getColumnsExpr() {
		if (columnsExpr == null)
			columnsExpr = new ArrayList<>();
		return columnsExpr;
	}

	private void setColumnsExpr(List<String> columnExpr) {
		this.columnsExpr = columnExpr;
		rebuildParams();
	}

	public void addColumnExpr(String columnExpr) {
		getColumnsExpr().add(columnExpr);
		rebuildParams();
	}

	private void rebuildParams() {
		this.params = null;
		for (String columnExpr : getColumnsExpr()) {
			if (!columnExpr.matches("[\\w]+"))
				for (String param : RegexUtils.allGroups("([\\$]*[\\w]+)", columnExpr)) {
					if (!getParams().contains(param))
						if (!param.equalsIgnoreCase("LOG10") 
								&& !param.equalsIgnoreCase("PI")
								&& !param.equalsIgnoreCase("MAX")
								&& !param.equalsIgnoreCase("MIN")
								)
							getParams().add(param);
				}
		}
	}
	
	public boolean isMathExpression() {
		return params != null && params.size() > 0;
	}

	public List<String> getParams() {
		if (params == null)
			params = new ArrayList<>();
		return params;
	}

	private void setParams(List<String> params) {
		this.params = params;
	}
	
	public Iterable<String> iterateColumnExpr() {
		return new Iterable<String>() {
			@Override
			public Iterator<String> iterator() {
				return getColumnsExpr().iterator();
			}
		};
	}
	
	public int getColumnsSize() {
		return getColumnsExpr().size();
	}
	
	public Iterable<String> iterateParams() {
		return new Iterable<String>() {
			@Override
			public Iterator<String> iterator() {
				return getParams().iterator();
			}
		};
	}

	public PlotType getPlotType() {
		return plotType;
	}

	public PlotSeries setPlotType(PlotType plotType) {
		this.plotType = plotType;
		return this;
	}

	public boolean isLegendDefined() {
		return notEmpty(getLegend());
	}

	public double getSample() {
		return sample;
	}

	public void setSample(double sample) {
		this.sample = sample;
	}
	
	public void setSample(String sample) {
		this.sample = NumberUtils.toDouble(sample, 1.0);
	}
}
