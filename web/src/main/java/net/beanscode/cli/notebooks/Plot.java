package net.beanscode.cli.notebooks;

import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;
import static net.hypki.libs5.utils.string.StringUtilities.nullOrEmpty;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.beanscode.cli.datasets.Table;
import net.beanscode.pojo.NotebookEntry;
import net.hypki.libs5.db.db.schema.ColumnType;
import net.hypki.libs5.plot.Color;
import net.hypki.libs5.plot.Figure;
import net.hypki.libs5.plot.PlotType;
import net.hypki.libs5.plot.Range;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.reflection.SystemUtils;
import net.hypki.libs5.utils.string.Base64Utils;
import net.hypki.libs5.utils.string.Meta;
import net.hypki.libs5.utils.string.StringUtilities;

public class Plot extends NotebookEntry {
	
	public static final String PNG = "PNG";
	public static final String PDF = "PDF";
	public static final String PS = "PS";
	public static final String ASCII = "ASCII";

//	private static final String META_DS_QUERY 		= "META_DS_QUERY";
//	private static final String META_NOTEBOOK_QUERY = "META_NOTEBOOK_QUERY";
//	private static final String META_TB_QUERY 		= "META_TB_QUERY";
//	private static final String META_PLOT_STATUS	= "META_PLOT_STATUS";
//	private static final String META_TABLES			= "META_TABLES";
////	private static final String META_TITLE			= "META_TITLE";
//	private static final String META_SERIES			= "META_SERIES";
//	private static final String META_LABELS			= "META_LABELS";
//	private static final String META_SORTBY			= "META_SORTBY";
//	private static final String META_LOGSCALE_ON 	= "META_LOGSCALE_ON";
//	private static final String META_COLORS 		= "META_COLORS";
//	private static final String META_SIZES 			= "META_SIZES";
//	private static final String META_TYPES 			= "META_TYPES";
//	private static final String META_COLORBY		= "META_COLORBY";
//	private static final String META_SPLITBY		= "META_SPLITBY";
//	private static final String META_XRANGE			= "META_XRANGE";
//	private static final String META_YRANGE			= "META_YRANGE";
//	private static final String META_BOXWIDTH		= "META_BOXWIDTH";
//	private static final String META_FILENAME_TEMPLATE		= "META_FILENAME_TEMPLATE";
//	private static final String META_LEGEND			= "META_LEGEND";
//	private static final String META_READ_TABLES	= "META_READ_TABLES";
//	private static final String META_SAMPLE			= "META_SAMPLE";
	
	private static final String META_FIGURE			= "FIGURE";
	
//	private PlotStatus plotStatus = null;
//	private List<PlotSeries> series = null;
//	private List<String> labels = null;
//	private List<String> sortBy = null;
//	private Map<String, Color> colors = null;
//	private Map<String, Float> sizes = null;
//	private Map<String, ColumnType> types = null;
//	private Range xRange = null;
//	private Range yRange = null;
//	private Map<String, String> legend = null;
	
	// cache
	private Figure figure = null;
		
	public Plot() {
		
	}
	
	public Plot(Notebook notebook) {
		setNotebookId(notebook.getId());
		setUserId(notebook.getUserId());
	}
	
	@Override
	public String toString() {
		return getName();
	}
			
	public static String getDefaultText() {
		return SystemUtils.readFileContent(Plot.class, "PlotDefaultText");
	}

//	public String getTitle() {
//		return getMetaAsString(META_TITLE, null);
//	}
//
//	public void setTitle(String title) {
//		setMeta(META_TITLE, title);
//	}
	
//	TODO dealing with such fields like labels is ugly
//	public String getLabel(int i) {
//		return i < getLabels().size() ? getLabels().get(i) : null;
//	}

//	public List<String> getLabels() {
//		if (labels == null) {
//			String labStr = getMetaAsString(META_LABELS, null);
//			if (nullOrEmpty(labStr)) {
//				labels = new ArrayList<>();
//			} else {
//				labStr = Base64Utils.decode(labStr);
//				labels = JsonUtils.readList(labStr, String.class);
//			}
//		}
//		return labels;
//	}
	
//	public void clearLabels() {
//		this.labels = null;
//		setMeta(META_LABELS, null);
//	}
//	
//	public Plot addLabel(String label) {
//		getLabels().add(label);
//		setMeta(META_LABELS, JsonUtils.objectToStringPretty(labels));
//		return this;
//	}
//
//	private void setLabels(List<String> labels) {
//		this.labels = labels;
//		setMeta(META_LABELS, JsonUtils.objectToStringPretty(labels));
//		setMeta(META_LEGEND, null);
//	}
//
//	private Map<String, Color> getColors() {
//		if (colors == null) {
//			String labStr = getMetaAsString(META_COLORS, null);
//			if (nullOrEmpty(labStr)) {
//				colors = new HashMap<>();
//			} else {
//				colors = JsonUtils.fromJson(labStr, Map.class);
//			}
//		}
//		return colors;
//	}
//	
//	public Plot addColor(String key, Color color) {
//		getColors().put(key, color);
//		setColors(getColors());
//		return this;
//	}
//
//	private void setColors(Map<String, Color> colors) {
//		this.colors = colors;
//		setMeta(META_COLORS, JsonUtils.objectToStringPretty(colors));
//	}
//	
//	public Plot putSize(String column, Float size) {
//		getSizes().put(column, size);
//		return this;
//	}
//
//	private Map<String, Float> getSizes() {
//		if (sizes == null) {
//			String labStr = getMetaAsString(META_SIZES, null);
//			if (nullOrEmpty(labStr)) {
//				sizes = new HashMap<>();
//			} else {
//				labStr = Base64Utils.decode(labStr);
//				sizes = JsonUtils.fromJson(labStr, Map.class);
//			}
//		}
//		return sizes;
//	}
//
//	private void setSizes(Map<String, Float> sizes) {
//		this.sizes = sizes;
//		setMeta(META_SIZES, JsonUtils.objectToStringPretty(sizes));
//	}
//
//	public ColumnType getColumnType(String colName) {
//		return getTypes().get(colName);
//	}
//
//	public Map<String, ColumnType> getTypes() {
//		if (types == null) {
//			String labStr = getMetaAsString(META_TYPES, null);
//			if (nullOrEmpty(labStr)) {
//				types = new HashMap<>();
//			} else {
//				labStr = Base64Utils.decode(labStr);
//				types = JsonUtils.fromJson(labStr, Map.class);
//			}
//		}
//		return types;
//	}
//
//	private void setTypes(Map<String, ColumnType> types) {
//		this.types = types;
//		setMeta(META_TYPES, JsonUtils.objectToStringPretty(types));
//	}
//
//	public String getColorBy() {
//		return getMetaAsString(META_COLORBY, null);
//	}
//	
//	public boolean isColorByDefined() {
//		return getColorBy() != null && getColorBy().length() > 0;
//	}
//
//	public void setColorBy(String colorBy) {
//		setMeta(META_COLORBY, notEmpty(colorBy) ? colorBy : null);
//	}
//
//	public boolean areAllColumnTypesKnown() {
//		for (ColumnType type : getTypes().values()) {
//			if (type == ColumnType.UNKNOWN)
//				return false;
//		}
//		return true;
//	}
//
//	public void setType(String colName, ColumnType columnType) {
//		getTypes().put(colName, columnType);
//	}
//
//	public String getSplitBy() {
//		return getMetaAsString(META_SPLITBY, null);
//	}
//
//	public void setSplitBy(String splitBy) {
//		setMeta(META_SPLITBY, notEmpty(splitBy) ? splitBy : null);
//	}
//
//	public Range getXRange() {
//		// TODO in the places like this Range is returnes, if it is changed and setRange is not called
//		// it is not saved. Fix it.
//		if (xRange == null)
//			xRange = getMetaAsObject(META_XRANGE, Range.class);
//		return xRange;
//	}
//	
//	public Plot setXRange(double from, double to) {
//		return setXRange(new Range(from, to));
//	}
//	
//	public Plot setYRange(double from, double to) {
//		return setYRange(new Range(from, to));
//	}
//
//	public Plot setXRange(Range xRange) {
//		this.xRange = xRange;
//		setMeta(META_XRANGE, JsonUtils.objectToStringPretty(xRange));
//		return this;
//	}
//
//	public Range getYRange() {
//		if (yRange == null)
//			yRange = getMetaAsObject(META_YRANGE, Range.class);
//		return yRange;
//	}
//
//	public Plot setYRange(Range yRange) {
//		this.yRange = yRange;
//		setMeta(META_YRANGE, JsonUtils.objectToStringPretty(yRange));
//		return this;
//	}
//
//	public Double getBoxWidth() {
//		return getMetaAsDouble(META_BOXWIDTH, null);
//	}
//
//	public void setBoxWidth(Double boxWidth) {
//		setMeta(META_BOXWIDTH, boxWidth);
//	}
//
//	public boolean containsColumn(String colName) {
//		for (PlotSeries columns : getSeries()) {
//			if (columns.contains(colName))
//				return true;
//		}
//		if (getSplitBy() != null && getSplitBy().equals(colName))
//			return true;
//		if (getColorBy() != null && getColorBy().equals(colName))
//			return true;
//		return false;
//	}
//
//	private List<String> getSortBy() {
//		if (sortBy == null) {
//			String tbStr = getMetaAsString(META_SORTBY, null);
//			if (nullOrEmpty(tbStr)) {
//				sortBy = new ArrayList<>();
//			} else {
//				sortBy = JsonUtils.readList(tbStr, String.class);
//			}
//		}
//		return sortBy;
//	}
//	
//	public boolean isSortByDefined() {
//		return getSortBy().size() > 0;
//	}
//	
//	public String getSortByAsString() {
//		if (getSortBy().size() == 0)
//			return null;
//		return StringUtilities.join(getSortBy(), ", ");
//	}
//	
//	public Plot clearSortBy() {
//		setSortBy(null);
//		return this;
//	}
//	
//	public Plot addSortBy(String sortBy) {
//		getSortBy().add(sortBy);
//		setSortBy(getSortBy());
//		return this;
//	}
//
//	private void setSortBy(List<String> sortBy) {
//		this.sortBy = sortBy;
//		setMeta(META_SORTBY, JsonUtils.objectToStringPretty(sortBy));
//	}
//	
//	public int getSeriesSize() {
//		return getSeries().size();
//	}
//	
//	public void clearSeries() {
//		this.series = null;
//		setMeta(META_SERIES, null);
//	}
//	
//	public PlotSeries getSeries(int i) {
//		return i < getSeries().size() ? getSeries().get(i) : null;
//	}
//
//	public List<PlotSeries> getSeries() {
//		if (series == null) {
//			String tbStr = getMetaAsString(META_SERIES, null);
//			if (nullOrEmpty(tbStr)) {
//				series = new ArrayList<>();
//			} else {
//				tbStr = Base64Utils.decode(tbStr);
//				series = JsonUtils.readList(tbStr, PlotSeries.class);
//			}
//		}
//		return series;
//	}
//
//	private void setSeries(List<PlotSeries> series) {
//		this.series = series;
//		setMeta(META_SERIES, JsonUtils.objectToStringPretty(series));
//	}
//	
//	public Plot addSeries(PlotSeries series) {
//		getSeries().add(series);
//		setSeries(getSeries());
//		return this;
//	}

//	public Set<String> getAllColumns() {
//		Set<String> allColumns = new HashSet<>();
//		for (PlotSeries pointsSet : getSeries()) {
//			allColumns.add(pointsSet.getX());
//			allColumns.add(pointsSet.getY());
//		}
//		if (getSplitBy() != null)
//			allColumns.add(getSplitBy());
//		if (getColorBy() != null)
//			allColumns.add(getColorBy());
//		return allColumns;
//	}

//	private List<Table> getTables() {
//		List<Table> tables = null;
//		String tbStr = getMetaAsString(META_TABLES, null);
//		if (nullOrEmpty(tbStr)) {
//			tables = new ArrayList<>();
//		} else {
//			tables = JsonUtils.readList(tbStr, Table.class);
//		}
//		return tables;
//	}
//
//	private void setTables(List<Table> tables) {
//		setMeta(META_TABLES, JsonUtils.objectToStringPretty(tables));
//	}
	
//	public PlotStatus getPlotStatus() {
//		if (plotStatus == null) {
//			String tmp = getMetaAsString(META_PLOT_STATUS, null);
//			if (nullOrEmpty(tmp)) {
//				plotStatus = new PlotStatus();
//				setPlotStatus(plotStatus);
//			} else { 
//				plotStatus = JsonUtils.fromJson(tmp, PlotStatus.class);
//			}
//		}
//		return plotStatus;
//	}
//
//	private void setPlotStatus(PlotStatus plotStatus) {
//		this.plotStatus = plotStatus;
//		setMeta(META_PLOT_STATUS, JsonUtils.objectToStringPretty(plotStatus));
//	}
	
	public String getSplitBy() {
		return getFigure().getMeta().getAsString("META_SPLITBY");
	}
	
	public String getColorBy() {
		return getFigure().getMeta().getAsString("META_COLORBY");
	}

	public boolean isSplitBySet() {
		return StringUtilities.notEmpty(getSplitBy());
	}

//	public String getDsQuery() {
//		return getMetaAsString(META_DS_QUERY, null);
//	}
//
//	public void setDsQuery(String dsQuery) throws IOException {
//		setMeta(META_DS_QUERY, dsQuery);
//	}
	
//	public boolean isDatasetQuery() {
//		return notEmpty(getDsQuery());
//	}
//	
//	public boolean isNotebookQuery() {
//		return notEmpty(getNotebookQuery());
//	}

//	public String getTbQuery() {
//		return getMetaAsString(META_TB_QUERY, null);
//	}
//
//	public void setTbQuery(String tbQuery) {
//		setMeta(META_TB_QUERY, tbQuery);
//	}
//
//	public String getNotebookQuery() {
//		return getMetaAsString(META_NOTEBOOK_QUERY, null);
//	}
//
//	public void setNotebookQuery(String notebookQuery) {
//		setMeta(META_NOTEBOOK_QUERY, notebookQuery);
//	}

//	public PlotType getPlotType(int seriesIndex) {
//		if (seriesIndex < getSeries().size() && seriesIndex >= 0) {
//			PlotSeries ps = getSeries().get(seriesIndex);
//			return ps.getPlotType();
//		}
//		return null;
//	}
//	
//	public boolean isLabelXDefined() {
//		return getLabels().size() > 0 && notEmpty(getLabels().get(0));
//	}
//	
//	public boolean isLabelYDefined() {
//		return getLabels().size() > 1 && notEmpty(getLabels().get(1));
//	}
//	
//	public String getFilenameTemplate() {
//		return getMetaAsString(META_FILENAME_TEMPLATE, null);
//	}
//
//	public Plot setFilenameTemplate(File filename) {
//		setFilename(filename.getAbsolutePath());
//		return this;
//	}
//	
//	public Plot setFilename(String filename) {
//		setMeta(META_FILENAME_TEMPLATE, filename);
//		return this;
//	}
//	
//	public boolean isLogscaleOn() {
//		return getMetaAsBoolean(META_LOGSCALE_ON, false);
//	}
//	
//	public void setLogscaleOn() {
//		setMeta(META_LOGSCALE_ON, true);
//	}
//	
//	public void setLogscaleOff() {
//		setMeta(META_LOGSCALE_ON, false);
//	}
//	
//	public boolean getMetaAsBoolean(String metaName, boolean defaultValue) {
//		Meta m = getMeta().get(metaName);
//		if (m != null)
//			return m.getAsBoolean();
//		return defaultValue;
//	}
//	
//	public Plot setMeta(String name, boolean value) {
//		getMeta().add(name, value);
//		return this;
//	}
//	
//	private Map<String, String> getLegend() {
//		if (legend == null) {
//			String tbStr = getMetaAsString(META_LEGEND, null);
//			if (nullOrEmpty(tbStr)) {
//				legend = new HashMap<String, String>();
//			} else {
//				legend = JsonUtils.fromJson(tbStr, HashMap.class);
//			}
//		}
//		return legend;
//	}
//
//	private void setLegend(Map<String, String> legend) {
//		this.legend = legend;
//		setMeta(META_LEGEND, JsonUtils.objectToStringPretty(legend));
//	}

//	public double getSample() {
//		return getMetaAsDouble(META_SAMPLE, 1.0);
//	}
	
	public Figure getFigure() {
		if (figure == null) {
			this.figure = getMetaAsObject(META_FIGURE, Figure.class);
		}
		if (figure == null) {
			this.figure = new Figure();
			setMeta(META_FIGURE, JsonUtils.toJson(this.figure));
		}
		return figure;
	}
}
