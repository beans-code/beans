package net.beanscode.cli.notebooks;

import java.io.IOException;

import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.pojo.NotebookEntry;
import net.beanscode.pojo.NotebookEntryHistory;
import net.hypki.libs5.cli.noninteractivecli.CLIException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.pjf.components.jersey.SessionBean;

public class NotebookEntryHistoryGateway {

	
	public static NotebookEntryHistory getNotebookEntryHistory(SessionBean sb, String entryId) throws CLIException, IOException {
		return getNotebookEntryHistory(sb, new UUID(entryId));
	}
	
//	public static NotebookEntryHistory getNotebookEntryHistory(SessionBean sb, UUID entryId) throws CLIException, IOException {
//		return NotebookEntryHistoryGateway.getNotebookEntry(new ApiCallCommand(sb)
//				.setApi("api/notebook/entry/get")
//				.addParam("entryId", entryId.getId())
//				.run()
//				.getResponse()
//				.getAsJson()
//				.getAsJsonObject());
//	}
	
	public static NotebookEntryHistory getNotebookEntryHistory(SessionBean sb, UUID entryId) throws CLIException, IOException {
		return new ApiCallCommand(sb)
			.setApi("/api/notebook/entry/history/get")
			.addParam("entryId", entryId)
			.run()
			.getResponse()
			.getAsObject(NotebookEntryHistory.class);
	}
	
}
