package net.beanscode.cli.notebooks;

import java.util.ArrayList;
import java.util.List;

import net.beanscode.pojo.NotebookEntry;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.file.FileExt;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.string.Meta;

public class AttachmentEntry extends NotebookEntry {

	public final String META_FILES = "META_FILES";
		
	public AttachmentEntry() {
		
	}
	
	private List<String> getRemovedFiles() {
		List<String> removedFiles = new ArrayList<>();
		for (Meta meta : getMeta()) {
			if (meta.getName().startsWith("removed-")) {
				removedFiles.add(meta.getAsString());
			}
		}
		return removedFiles;
	}

	public List<String> getAttachmentFiles() {
		List<String> files = new ArrayList<>();
		Meta filesMeta = getMeta().get(META_FILES);
		if (filesMeta != null)
			files = JsonUtils.fromJson(filesMeta.getAsString(), List.class);
		
		// removing files marked for removal
		for (String f : getRemovedFiles())
			files.remove(f);
		
		return files;
	}

	public boolean isRemoved(String f) {
		for (Meta meta : getMeta()) {
			if (meta.getName().startsWith("removed-")
					&& meta.getValue().equals(f))
				return true;
		}
		return false;
	}
}
