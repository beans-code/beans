package net.beanscode.cli.notebooks;

import net.beanscode.cli.web.utils.MarkdownUtils;
import net.beanscode.pojo.NotebookEntry;
import net.beanscode.web.view.notebook.NotebookEntryEditorFactory;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.string.RegexUtils;
import net.hypki.libs5.utils.string.StringUtilities;

public class TextEntry extends NotebookEntry {
	
	public static final String META_TEXT_MARKDOWN 	= "META_TEXT_MARKDOWN";
	public static final String META_TEXT_HTML 		= "META_TEXT_HTML";
	
	public TextEntry() {
		super();
	}
	
	public TextEntry(Notebook notebook, String text) {
		super();
		
		setUserId(notebook.getUserId());
		setNotebookId(notebook.getId());
//		setName(name);
		setText(text);
	}
	
	public TextEntry(UUID userId, UUID notebookId, String name, String text) {
		super();
		
		setUserId(userId);
		setNotebookId(notebookId);
		setName(name);
		setText(text);
	}
	
	@Override
	public String toString() {
		return StringUtilities.substringMax(getText(), 0, 100);
	}
	
	@Override
	public String getName() {
		return StringUtilities.substringMax(getText(), 0, 100);
	}

	public String getText() {
		return getMetaAsString(META_TEXT_MARKDOWN, null);
	}

	public void setText(String text) {
		setMeta(META_TEXT_MARKDOWN, text);
		setMeta(META_TEXT_HTML, MarkdownUtils.toHtml(getText()));
	}
	
	public String getTextAsHtml() {
		String html = getMetaAsString(META_TEXT_HTML, null);
		if (html == null) {
			html = MarkdownUtils.toHtml(getText());
			setMeta(META_TEXT_HTML, html);
		}
		
		return html;
	}
		
	public boolean isEmpty() {
		return StringUtilities.nullOrEmpty(getText());
	}
}
