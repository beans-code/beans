package net.beanscode.cli.notebooks;

import net.beanscode.pojo.NotebookEntry;

public class GacsEntry extends NotebookEntry {
	
	public static final String SETTING_GACS = "SETTING_GACS";

	public static final String SETTING_GACS_USERNAME = "username";
	
	public static final String SETTING_GACS_PASSWORD = "password";
	
	public static final String META_ADQL = "ADQL";
}
