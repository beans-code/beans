package net.beanscode.cli.notebooks;

import net.beanscode.pojo.NotebookEntry;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.string.Meta;

public class LinkEntry extends NotebookEntry {
	
	private final String META_SOURCE_ENTRY_ID = "sourceEntryId";
	private final String META_SOURCE_NOTEBOOK_ID = "sourceNotebookId";
	
	public LinkEntry() {
		super();
	}
	
	public LinkEntry(NotebookEntry sourceEntry) {
		super();
		
		setSourceEntryId(sourceEntry.getId());
		setNotebookId(sourceEntry.getNotebookId());
	}

	public LinkEntry setSourceEntryId(UUID sourceEntryId) {
		getMeta().add(META_SOURCE_ENTRY_ID, sourceEntryId);
		return this;
	}
	
	public UUID getSourceEntryId() {
		Meta meta = getMeta().get(META_SOURCE_ENTRY_ID);
		return meta != null ? new UUID(meta.getAsString()) : null;
	}
	
	public LinkEntry setSourceNotebookId(UUID sourceNotebookId) {
		getMeta().add(META_SOURCE_NOTEBOOK_ID, sourceNotebookId);
		return this;
	}
	
	public UUID getSourceNotebookId() {
		Meta meta = getMeta().get(META_SOURCE_NOTEBOOK_ID);
		return meta != null ? new UUID(meta.getAsString()) : null;
	}
}
