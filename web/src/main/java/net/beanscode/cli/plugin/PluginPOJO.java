package net.beanscode.cli.plugin;

import com.google.gson.annotations.Expose;

public class PluginPOJO {
	@Expose
	private String name = null;
	
	@Expose
	private String version = null;
	
	@Expose
	private String description = null;
	
	public PluginPOJO() {
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
