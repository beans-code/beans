package net.beanscode.cli.plugin;

import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;
import static net.hypki.libs5.utils.string.StringUtilities.split;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import net.beanscode.cli.BeansCliCommand;
import net.hypki.libs5.cli.noninteractivecli.CLIException;
import net.hypki.libs5.pjf.RestMethod;
import net.hypki.libs5.pjf.components.jersey.SessionBean;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.url.Params;

import com.google.gson.JsonElement;

public class PluginFuncGetList extends BeansCliCommand {
	
//	@CliArg(name = "user",
//			description = "User for which the list of Plugins will be checked",
//			required = true)
//	private String email = null;

	public PluginFuncGetList(SessionBean sb) {
		super(RestMethod.GET, sb);
	}

	@Override
	public String getUrlPath() {
		return "api/plugin/func/list";
	}

	@Override
	public void processResponse() {
		LibsLogger.debug(PluginFuncGetList.class, "List of plugins UDF: ", getResponse().toString());
	}

	@Override
	public Params getRESTParams() {
		return new Params()
//			.add("userId", getUserIdFromEmail().getId())
			;
	}

	@Override
	public String getCLICommandName() {
		return "plugin func list";
	}

	@Override
	public String getCLICommandDescription() {
		return "Gets the list of UDF from plugins";
	}
	
	public List<FuncPOJO> run(String filter) throws CLIException, IOException {
		super.run(null);
		
		String [] filterParts = notEmpty(filter) ? split(filter.toLowerCase()) : null;
		List<FuncPOJO> plugins = new ArrayList<FuncPOJO>();
		for (JsonElement oneFunc : getResponse().getAsJson().getAsJsonArray()) {
			boolean add = true;
			if (filterParts != null) {
				for (String one : filterParts) {
					if (one.startsWith("-")) {
						if (oneFunc.toString().toLowerCase().contains(one.substring(1))) {
							add = false;
							break;
						}
					} else {
						if (!oneFunc.toString().toLowerCase().contains(one)) {
							add = false;
							break;
						}
					}
				}
			}
			
			if (add)
				plugins.add(JsonUtils.fromJson(oneFunc, FuncPOJO.class));
		}
		return plugins;
	}
}
