package net.beanscode.cli.plugin;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import net.beanscode.cli.BeansCliCommand;
import net.beanscode.cli.notebooks.NotebookEntryGateway;
import net.beanscode.pojo.NotebookEntry;
import net.hypki.libs5.cli.noninteractivecli.CLIException;
import net.hypki.libs5.cli.noninteractivecli.CliArg;
import net.hypki.libs5.pjf.RestMethod;
import net.hypki.libs5.pjf.components.jersey.SessionBean;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.url.Params;

import com.google.gson.JsonElement;

public class PluginNotebookEntryGetList extends BeansCliCommand {
	
	@CliArg(name = "user",
			description = "User for which the list of Plugins will be checked",
			required = true)
	private String email = null;

	public PluginNotebookEntryGetList(SessionBean sb) {
		super(RestMethod.GET, sb);
	}

	@Override
	public String getUrlPath() {
		return "api/plugin/entry/list";
	}

	@Override
	public void processResponse() {
		LibsLogger.debug(PluginNotebookEntryGetList.class, "List of notebook entries for user ", getEmail(), ": ", 
				getResponse().toString());
	}

	@Override
	public Params getRESTParams() {
		return new Params()
			.add("userId", getUserId(getEmail()))
			;
	}

	@Override
	public String getCLICommandName() {
		return "plugin entryList";
	}

	@Override
	public String getCLICommandDescription() {
		return "";
	}
	
	public List<NotebookEntry> run(String userEmail) throws CLIException, IOException {
		super.run(new String[] {"--user", userEmail});
		
		List<NotebookEntry> plugins = new ArrayList<NotebookEntry>();
		for (JsonElement onePlugin : getResponse().getAsJson().getAsJsonArray()) {
			plugins.add(NotebookEntryGateway.getNotebookEntry(onePlugin.getAsJsonObject()));
		}
		return plugins;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}
