package net.beanscode.cli.plugin;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import net.beanscode.cli.BeansCliCommand;
import net.hypki.libs5.cli.noninteractivecli.CLIException;
import net.hypki.libs5.pjf.RestMethod;
import net.hypki.libs5.pjf.components.jersey.SessionBean;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.url.Params;

import com.google.gson.JsonElement;

public class PluginDbGetList extends BeansCliCommand {
	
//	@CliArg(name = "user",
//			description = "User for which the list of Plugins will be checked",
//			required = true)
//	private String email = null;

	public PluginDbGetList(SessionBean sb) {
		super(RestMethod.GET, sb);
	}

	@Override
	public String getUrlPath() {
		return "api/plugin/db/list";
	}

	@Override
	public void processResponse() {
		LibsLogger.debug(PluginDbGetList.class, "List of plugins database providers: ", getResponse().toString());
	}

	@Override
	public Params getRESTParams() {
		return new Params()
//			.add("userId", getUserIdFromEmail().getId())
			;
	}

	@Override
	public String getCLICommandName() {
		return "plugin db list";
	}

	@Override
	public String getCLICommandDescription() {
		return "Gets the list of database providers from plugins";
	}
	
	public List<DbProviderPOJO> getDbProviders() throws CLIException, IOException {
		super.run(null);
		
		List<DbProviderPOJO> plugins = new ArrayList<DbProviderPOJO>();
		for (JsonElement oneFunc : getResponse().getAsJson().getAsJsonArray()) {
			plugins.add(JsonUtils.fromJson(oneFunc, DbProviderPOJO.class));
		}
		
		return plugins;
	}
}
