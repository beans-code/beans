package net.beanscode.cli.datasets;

import java.util.List;

import com.google.gson.annotations.Expose;

public class TableSearchResults {

	@Expose
	private List<Table> tables = null;
	
	@Expose
	private int maxHits = 0;
	
	public TableSearchResults() {
		
	}

	public List<Table> getTables() {
		return tables;
	}

	public TableSearchResults setTables(List<Table> tables) {
		this.tables = tables;
		return this;
	}

	public int getMaxHits() {
		return maxHits;
	}

	public TableSearchResults setMaxHits(int maxHits) {
		this.maxHits = maxHits;
		return this;
	}
}
