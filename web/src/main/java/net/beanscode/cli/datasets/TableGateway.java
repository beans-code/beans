package net.beanscode.cli.datasets;

import java.io.IOException;

import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.web.view.dataset.DatasetTablesBetaTableComponent;
import net.hypki.libs5.cli.noninteractivecli.CLIException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.pjf.components.jersey.SessionBean;
import net.hypki.libs5.search.SearchResults;
import net.hypki.libs5.utils.LibsLogger;

public class TableGateway {
	
	public static Table getTable(SessionBean sb, String tableId) throws CLIException, IOException {
		return tableId != null ? getTable(sb, new UUID(tableId)) : null;
	}
	
	public static Table getTable(SessionBean sb, UUID tableId) throws CLIException, IOException {
		return new ApiCallCommand(sb)
			.setApi("/api/table/get")
			.addParam("table-id", tableId)
			.run()
			.getResponse()
			.getAsObject(Table.class);
	}
	
	public static Table createTable(SessionBean sb, Table table) throws CLIException, IOException {
		return new ApiCallCommand(sb)
			.setApi("/api/table/create")
			.addParam("id", table.getId().getId())
			.addParam("name", table.getName())
			.addParam("datasetId", table.getDatasetId())
			.run()
			.getResponse()
			.getAsObject(Table.class);
	}
	
	public static SearchResults<Table> getTables(SessionBean sb, String datasetId, String filter,
			int from, int size) throws CLIException, IOException {
//		try {
			ApiCallCommand call = new ApiCallCommand(sb)
				.setApi("api/table/search")
				.addParam("datasetQuery", datasetId)
				.addParam("tableQuery", filter)
				.addParam("from", from)
				.addParam("size", size)
				.run();
			
			return new SearchResults<Table>(call.getResponse().getAsList("tables", Table.class), 
					call.getResponse().getAsLong("maxHits"));
//		} catch (CLIException | IOException e) {
//			LibsLogger.error(DatasetTablesBetaTableComponent.class, "Cannot search for Tables", e);
//		}
	}
}
