package net.beanscode.cli.datasets;

import java.io.IOException;
import java.util.List;

import net.beanscode.cli.api.ApiCallCommand;
import net.beanscode.pojo.dataset.Dataset;
import net.beanscode.pojo.tables.ColumnDef;
import net.beanscode.pojo.tables.ConnectorList;
import net.hypki.libs5.cli.noninteractivecli.CLIException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.pjf.components.jersey.SessionBean;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.date.SimpleDate;
import net.hypki.libs5.utils.string.MetaList;

import com.google.gson.annotations.Expose;

public class Table {

	@Expose
	private UUID id = null;
	
	@Expose
	private UUID userId = null;
	
	@Expose
	private UUID datasetId = null;
	
	@Expose
	private String importPath = null;
	
	@Expose
	private TableStatus tableStatus = TableStatus.IMPORTING;
	
	@Expose
	private SimpleDate create = null;
	
	@Expose
	private TableSearchStatus searchStatus = TableSearchStatus.NOT_INDEXED;
	
	@Expose
	private String name = null;
	
	@Expose
	private List<ColumnDef> columns = null;
	
	@Expose
	private MetaList meta = null;
	
	@Expose
	private ConnectorList connectorList = null;

//	@Expose
//	private ConnectorList connectorList = null;
	
	public Table() {
		
	}

	@Override
	public String toString() {
		return getName() + " (" + getId() + ")";
	}

	public UUID getId() {
		return id;
	}


	public void setId(UUID id) {
		this.id = id;
	}


	public UUID getUserId() {
		return userId;
	}


	public void setUserId(UUID userId) {
		this.userId = userId;
	}


	public UUID getDatasetId() {
		return datasetId;
	}


	public Table setDatasetId(UUID datasetId) {
		this.datasetId = datasetId;
		return this;
	}


	public String getImportPath() {
		return importPath;
	}


	public void setImportPath(String importPath) {
		this.importPath = importPath;
	}


	public TableStatus getTableStatus() {
		return tableStatus;
	}


	public void setTableStatus(TableStatus tableStatus) {
		this.tableStatus = tableStatus;
	}


	public SimpleDate getCreate() {
		return create;
	}


	public void setCreate(SimpleDate create) {
		this.create = create;
	}


	public TableSearchStatus getSearchStatus() {
		return searchStatus;
	}


	public void setSearchStatus(TableSearchStatus searchStatus) {
		this.searchStatus = searchStatus;
	}


	public String getName() {
		return name;
	}


	public Table setName(String name) {
		this.name = name;
		return this;
	}


	public List<ColumnDef> getColumns() {
		return columns;
	}

	public ColumnDef getColumnDef(String columnName) {
		for (ColumnDef columnDef : getColumns()) {
			if (columnDef.getName().equals(columnName))
				return columnDef;
		}
		return null;
	}

	public void setColumns(List<ColumnDef> columns) {
		this.columns = columns;
	}


	public MetaList getMeta() {
		if (meta == null)
			meta = new MetaList();
		return meta;
	}


	public void setMeta(MetaList meta) {
		this.meta = meta;
	}

	public Dataset getDataset(SessionBean sb) throws CLIException, IOException {
		try {
			return new ApiCallCommand(sb)
						.setApi("api/dataset")
						.addParam("dataset-id", getDatasetId().getId())
						.run()
						.getResponse()
						.getAsObject(Dataset.class);
		} catch (Throwable t) {
			LibsLogger.error(Table.class, "Cannot get Dataset", t);
			return null;
		}
	}
	
	public boolean isInNotebook() {
		return getMeta().get("notebookId") != null;
	}
	
	public boolean isInDataset() {
		return !isInNotebook();
	}

	public ConnectorList getConnectorList() {
		if (connectorList == null)
			connectorList = new ConnectorList();
		return connectorList;
	}

	public void setConnectorList(ConnectorList connectorList) {
		this.connectorList = connectorList;
	}
	
	public Table addMeta(String key, Object value) {
		getMeta().add(key, value);
		return this;
	}
}
