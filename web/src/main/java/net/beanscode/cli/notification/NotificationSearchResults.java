package net.beanscode.cli.notification;

import java.util.List;

import com.google.gson.annotations.Expose;

public class NotificationSearchResults {

	@Expose
	private List<Notification> notification = null;
	
	@Expose
	private int maxHits = 0;
	
	public NotificationSearchResults() {
		
	}

	public List<Notification> getNotification() {
		return notification;
	}

	public NotificationSearchResults setNotification(List<Notification> notification) {
		this.notification = notification;
		return this;
	}

	public int getMaxHits() {
		return maxHits;
	}

	public NotificationSearchResults setMaxHits(int maxHits) {
		this.maxHits = maxHits;
		return this;
	}
	
	public NotificationSearchResults setMaxHits(long maxHits) {
		this.maxHits = (int) maxHits;
		return this;
	}
}
