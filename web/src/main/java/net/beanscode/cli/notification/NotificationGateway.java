package net.beanscode.cli.notification;

import java.io.IOException;

import net.beanscode.cli.api.ApiCallCommand;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.pjf.components.jersey.SessionBean;

public class NotificationGateway {

	public static void save(SessionBean sb, Notification notification) throws IOException {
		new ApiCallCommand(sb)
			.setApi("/api/notification")
			.addParam("notification", notification)
			.run();;
	}
	
	public static Notification getNotification(SessionBean sb, UUID notificationId) throws IOException {
		return new ApiCallCommand(sb)
			.setApi("/api/notification")
			.addParam("id", notificationId)
			.run()
			.getResponse()
			.getAsObject(Notification.class);
	}
}
