package net.beanscode.cli.cassandra;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.hypki.libs5.cli.noninteractivecli.CLICommand;
import net.hypki.libs5.cli.noninteractivecli.CLIException;
import net.hypki.libs5.cli.noninteractivecli.CliArg;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.file.FileExt;
import net.hypki.libs5.utils.file.FileUtils;
import net.hypki.libs5.utils.file.LazyFileIterator;
import net.hypki.libs5.utils.string.RegexUtils;

import com.esotericsoftware.yamlbeans.YamlReader;
import com.esotericsoftware.yamlbeans.YamlWriter;

public class CassandraYaml extends CLICommand {
	
	@CliArg(name = "path", 
			required = false, 
			description = "Path to the folder where Cassandra yaml files will be searched for. If not given the "
					+ "current directory is taken. One may "
					+ "specify any number of --path folders")
	private List<String> paths = null;
	
	@CliArg(name = "recursively", 
			required = false, 
			description = "If true then Cassandra yaml files will be searched for in all subfolders specified with --path")
	private boolean recursively = false;
	
	@CliArg(name = "get", 
			required = false, 
			description = "")
	private List<String> get = null;
	
	@CliArg(name = "set", 
			required = false, 
			description = "")
	private List<String> set = null;
	
	public CassandraYaml() {
		
	}

	@Override
	protected CLICommand doCommand() throws CLIException, IOException {
		Set<String> uniquePaths = new java.util.HashSet<>();
		
		// it no path specified add the current path
		if (getPaths().size() == 0)
			getPaths().add(".");
		
		for (String path : getPaths()) {
			if (isRecursively()) {
				for (File file : new LazyFileIterator(new FileExt(path), true, false, true)) {
					if (file.getAbsolutePath().endsWith("conf") || file.getAbsolutePath().endsWith("conf/"))
						if (FileUtils.exist(file.getAbsolutePath(), "cassandra.yaml"))
							uniquePaths.add(file.getAbsolutePath());
				}
			}
		}
		
		int changedValues = 0;
		
		for (String cassPath : uniquePaths) {
			LibsLogger.debug(CassandraYaml.class, "Working on ", cassPath, "/cassandra.yaml");
			
		    
		    if (getGet() != null) {
		    	for (String get : getGet()) {					
		    		YamlReader reader = new YamlReader(new FileReader(cassPath + "/cassandra.yaml"));
		    		Object object = reader.read();
		    		Map cassYaml = (Map) object;
		    		
		    		if (cassYaml != null)
		    			LibsLogger.debug(CassandraYaml.class, cassPath, ": ", get, ": ", cassYaml.get(get));
				}
		    }
		    
		    if (getSet() != null) {
		    	for (String set : getSet()) {
		    		String[] keyVal = set.split(":");
		    		String key = keyVal[0].trim();
		    		String val = keyVal[1].trim();
		    		
//		    	FileWriter fileWriter = new FileWriter(cassPath + "/cassandra.yaml");
//				YamlWriter writer = new YamlWriter(fileWriter, reader.getConfig());
//				writer.write(cassYaml);
//				writer.close();
		    		
		    		List<String> newLines = new ArrayList<>();
		    		List<String> lines = FileUtils.readLines(new File(cassPath + "/cassandra.yaml"));
		    		for (String line : lines) {
		    			if (line.matches("[\\s]*" + key + "[\\s]*:" + "[\\s]*.*")) {
		    				String newLine = RegexUtils.replaceFirstGroup("[\\s]*" + key + "[\\s]*:" + "[\\s]*(.*)", line, val);
		    				LibsLogger.debug(CassandraYaml.class, line, " => ", newLine);
		    				newLines.add(newLine);
		    			} else
		    				newLines.add(line);
		    		}
		    		
		    		FileUtils.saveToFile(newLines, new File(cassPath + "/cassandra.yaml"));
		    		changedValues++;
				}
		    }
		}
		
		LibsLogger.debug(CassandraYaml.class, "YAML read/write finished, total changes made in files ", changedValues);
		return this;
	}
	
	@Override
	public String getCLICommandName() {
		return "yaml";
	}

	@Override
	public String getCLICommandDescription() {
		return "reads/writes values from cassandra.yaml configuration files";
	}

	public List<String> getPaths() {
		if (paths == null)
			paths = new ArrayList<>();
		return paths;
	}

	public void setPaths(List<String> paths) {
		this.paths = paths;
	}

	public boolean isRecursively() {
		return recursively;
	}

	public void setRecursively(boolean recursively) {
		this.recursively = recursively;
	}

	public List<String> getGet() {
		return get;
	}

	public void setGet(List<String> get) {
		this.get = get;
	}

	public List<String> getSet() {
		return set;
	}

	public void setSet(List<String> set) {
		this.set = set;
	}

	
}
