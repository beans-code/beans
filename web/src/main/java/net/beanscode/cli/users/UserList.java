package net.beanscode.cli.users;

import static java.lang.String.valueOf;

import java.io.IOException;
import java.util.ArrayList;

import net.beanscode.cli.BeansCliCommand;
import net.hypki.libs5.cli.noninteractivecli.CLIException;
import net.hypki.libs5.cli.noninteractivecli.CliArg;
import net.hypki.libs5.pjf.RestMethod;
import net.hypki.libs5.pjf.components.jersey.SessionBean;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.url.Params;

import com.google.gson.JsonElement;

public class UserList extends BeansCliCommand {
	
	@CliArg(name = "search",
			description = "Search term to look for user. Required.",
			required = false)
	private String search = null;
	
	@CliArg(name = "from",
			description = "From index. Be dafault 0.",
			required = false)
	private int from = 0;
	
	@CliArg(name = "size",
			description = "Number of users to search for. By default 20.",
			required = false)
	private int size = 20;
	
	private UsersSearchResult usersSearchResult = null;

	public UserList(SessionBean sb) {
		super(RestMethod.GET, sb);
	}

	@Override
	public String getUrlPath() {
		return "api/user/search";
	}

	@Override
	public void processResponse() {
		setUsersSearchResult(getResponse().getAsObject(UsersSearchResult.class));
		
		LibsLogger.debug(UserList.class, "Found in total ", getUsersSearchResult().getMaxHits(), " users");
		for (User user : getUsersSearchResult().getUsers()) {
			LibsLogger.debug(UserList.class, user.toString());
		}
	}

	@Override
	public Params getRESTParams() {
		return new Params()
			.add("search-term", getSearch())
			.add("from", getFrom())
			.add("size", getSize());
	}

	@Override
	public String getCLICommandName() {
		return "user search";
	}

	@Override
	public String getCLICommandDescription() {
		return "Searches for users.";
	}
	
	public UsersSearchResult run(String keywords, int from, int size) throws CLIException, IOException {
		super.run(new String[] {"--search", keywords, "--from", valueOf(from), "--size", valueOf(size)});
		
		return getUsersSearchResult();
	}

	public String getSearch() {
		return search;
	}

	public void setSearch(String search) {
		this.search = search;
	}

	public int getFrom() {
		return from;
	}

	public void setFrom(int from) {
		this.from = from;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public UsersSearchResult getUsersSearchResult() {
		if (usersSearchResult == null && getResponse() != null) {
			usersSearchResult = new UsersSearchResult();
			
			usersSearchResult.setMaxHits(JsonUtils.readInt(getResponse().getAsJson(), "maxHits", 0));
			
			usersSearchResult.setUsers(new ArrayList<User>());
			for (JsonElement je : getResponse().getAsJson().getAsJsonObject().get("users").getAsJsonArray()) {
				usersSearchResult.getUsers().add(JsonUtils.fromJson(je, User.class));
			}
		}
		return usersSearchResult;
	}

	public void setUsersSearchResult(UsersSearchResult usersSearchResult) {
		this.usersSearchResult = usersSearchResult;
	}
}
