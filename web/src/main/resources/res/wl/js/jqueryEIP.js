$(document).ready(function() {
	var ii = 0;
	$("label").each(function(i) {
		var clsName = this.className;
		if (clsName.toLowerCase() == "editable") {
//			alert(this + ii)
			ii = ii + 1;
			setClickable(this, ii);
		}
	});
});

$(function() {
	$('#datepicker').datepicker({
		numberOfMonths: 3,
		showButtonPanel: true
	});
});

function setClickable(obj, i) {
	var clsName = obj.className;
	if (clsName.toLowerCase() == "editable") {
		$(obj).dblclick(
					function() {
//						alert('setClickable, i=' + i + ', ' + $(this).html());
						var textarea = '<div style="display: inline;"><input class=\"medium\" value=\"' + $(this).html() + '\" onkeypress=\"return disableEnterKey(event)\" />';
						//var button = '<div style="display: inline;"> <input type="button" value="SAVE" class="saveButton" style="width: 60px;"/> <input type="button" value="CANCEL" class="cancelButton"  style="width: 60px;"/></div></div>';
						var button = '<div style="display: inline;"> <a class="saveButton">' + msg["Save"] + '<a/> <a class="cancelButton">' + msg["Cancel"] + '<a/></div></div>';
						var revert = $(obj).html();
						$(obj).after(textarea + button).remove();
						$('.saveButton').click(function() {
							saveChanges(this, false, i);
						});
						$('.cancelButton').click(function() {
							saveChanges(this, revert, i);
						});
					}).mouseover(function() {
				$(obj).addClass("editable");
			}).mouseout(function() {
//				$(obj).removeClass("editable");
			});
	
	}
}

function saveChanges(obj, cancel, n) {
	if (!cancel) {
//		alert('saveChanges ' + n);
		var t = $(obj).parent().siblings(0).val();
		var numi = document.getElementById("label" + n);
		numi.value = t;
	} else {
		var t = cancel;
	}
//	if (t == '')
//		t = '(click to add text)';
	var dblClickTooltip = document.getElementById("fieldCounter").title;
	$(obj).parent().parent().after('<label id="label' + n + '" for="checkbox' + n + '" class="editable" title="' + dblClickTooltip + '">' + t + '</label>').remove();
//	setClickable($("label").get(n), n); // oryginalne
	setClickable(document.getElementById("label" + n), n);
}