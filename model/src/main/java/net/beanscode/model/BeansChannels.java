package net.beanscode.model;

public class BeansChannels {

	public static final String CHANNEL_LONG_HIDDEN = "LONG_HIDDEN";
	
	public static final String CHANNEL_LONG_NORMAL = "LONG_NORMAL";
	
	public static final String CHANNEL_INDEX = "INDEX";
}
