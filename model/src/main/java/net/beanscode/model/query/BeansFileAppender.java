package net.beanscode.model.query;

import org.apache.log4j.FileAppender;
import org.apache.log4j.Logger;
import org.apache.log4j.spi.LoggingEvent;

public class BeansFileAppender extends org.apache.log4j.RollingFileAppender {

	public BeansFileAppender() {
		super();
	}
	
	@Override
	public void append(LoggingEvent event) {
		int idx = event.getThreadName().indexOf("LogSeparately:");
		if (idx >= 0) {
				Logger
					.getLogger("00")
//						.getRootLogger()
					.getAppender(event.getThreadName().substring(idx + "LogSeparately:".length()))
					.doAppend(event);
		} else
			super.append(event);
	}
}
