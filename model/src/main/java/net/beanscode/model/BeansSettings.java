package net.beanscode.model;

import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;
import static net.hypki.libs5.utils.utils.AssertUtils.assertTrue;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashSet;
import java.util.Set;

import net.beanscode.model.connectors.H5Connector;
import net.beanscode.model.notebook.autoupdate.AutoUpdateOption;
import net.beanscode.model.notebook.autoupdate.CreateAutoUpdatesJobsForUsersJob;
import net.beanscode.model.plugins.Connector;
import net.beanscode.model.plugins.ConnectorInitParams;
import net.beanscode.model.plugins.Plugin;
import net.beanscode.model.plugins.PluginManager;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.collections.ArrayUtils;
import net.hypki.libs5.utils.date.SimpleDate;
import net.hypki.libs5.utils.file.FileExt;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.reflection.ReflectionUtility;
import net.hypki.libs5.utils.reflection.SystemUtils;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.utils.utils.NumberUtils;
import net.hypki.libs5.weblibs.WeblibsConst;
import net.hypki.libs5.weblibs.mail.MailCredentials;
import net.hypki.libs5.weblibs.property.Property;
import net.hypki.libs5.weblibs.settings.Setting;
import net.hypki.libs5.weblibs.settings.SettingFactory;
import net.hypki.libs5.weblibs.settings.SettingValue;
import net.hypki.libs5.weblibs.settings.SettingsManager;
import net.hypki.libs5.weblibs.user.Email;
import net.hypki.libs5.weblibs.user.User;
import net.hypki.libs5.weblibs.user.UserFactory;

import org.python.modules.errno;
import org.quartz.SchedulerException;

import com.google.gson.JsonElement;

public class BeansSettings {

	private static final String PRIV = "8210b759b6e688a9485e9b92bf77315e";
	
	public static final String SETTING_LAST_RELOAD = "SETTING_LAST_RELOAD";
	public static final String SETTING_BACKUP_PATH = "BACKUP_PATH";
	public static final String SETTING_NEW_USER_REGISTRATION = "NEW_USER_REGISTRATION";
	public static final String SETTING_NOTIFICATION_MAIL = "NOTIFICATION_MAIL";
	public static final String SETTING_PIG_MODE = "PIG_MODE";
	public static final String SETTING_PIG_FOLDER = "PIG_FOLDER";
	public static final String SETTING_UPLOAD_DEFAULT_FOLDER = "UPLOAD_DEFAULT_FOLDER";
	public static final String SETTING_CONNECTOR_DEFAULT_FOLDER = "CONNECTOR_DEFAULT_FOLDER";
	public static final String SETTING_CONNECTOR_DEFAULT_CLASS = "CONNECTOR_DEFAULT_CLASS";
	
	public static final String SETTING_BEANS_HOST = "BEANS_HOST";
		
	public static final String SETTING_DATABASE_VERSION = "BEANS_DB_VERSION";
	
	public static final String BEANS_HOME_ENV_NAME = "BEANS_HOME";
	
	public static final String DEFAULT_DB_DRIVER = "DEFAULT_DB_DRIVER";
//	public static final String DEFAULT_PLAIN_CONNECTOR_PATH = "DEFAULT_PLAIN_CONNECTOR_PATH";
	public static final String SETTING_AUTO_UPDATE_SCHEDULER_MINUTES = "SETTING_AUTO_UPDATE_SCHEDULER_MINUTES";
	public static final String SETTING_AUTO_UPDATE_CURRENT_ID = "SETTING_AUTO_UPDATE_CURRENT_ID";
	public static final String SETTING_AUTO_UPDATE_ENTRY_DEFAULT_OPTION = "SETTING_AUTO_UPDATE_ENTRY_DEFAULT_OPTION";
	public static final String SETTING_PLOT_FONT_SIZE = "SETTING_PLOT_FONT_SIZE";
	
	public static final String SETTING_NR_FIELDS_INCREASED = "SETTING_NR_FIELDS_INCREASED";
	
	public static final String SETTING_PLUGIN_LIST = "PLUGIN_LIST";
	
	public static final String SETTING_PLUGIN_FOLDER = "PLUGIN_FOLDER";

	public static final boolean VALIDATE_PIG = false; // TODO CONF
	public static final boolean TEST_RECORDWRITER_FAILED_ATTEMPTS = false;
	
	public static final long SORT_MAX_MEMORY_BYTES = 50_000_000; // 50 MB
	
	private static FileExt uploadDefaultFolderCache = null;
	private static FileExt connectorDefaultFolderCache = null;
	
	private static boolean tmpCreated = false;
	
	static {
//		LibsLogger.debug(BeansSettings.class, "BEANS setting dir " + SETTINGS_DIR);
//		LibsLogger.debug(BeansSettings.class, "BEANS setting filename " + SETTINGS_FILENAME);
//		LibsLogger.debug(BeansSettings.class, "BEANS setting filename path " + SETTINGS_FILENAME_PATH);
//		
//		// creating folder it does not exist
//		if (new File(SETTINGS_DIR).exists() == false) {
//			new File(SETTINGS_DIR).mkdirs();
//			LibsLogger.debug(BeansSettings.class, "Folder " + SETTINGS_DIR + " created");
//		}
		
//		new FileExt(UPLOAD_DEFAULT_FOLDER).mkdirs();
		
//		new FileExt(CONNECTOR_DEFAULT_FOLDER).mkdirs();
		
//		new FileExt(getExecutionPath() + "/tables/").mkdirs();
	}
	
	public static FileExt getBeansHomeFolder() {
		return new FileExt(SystemUtils.getHomePath() + "/.beans-software/");
	}
	
	public static String getPluginDefaultFolder() {
		return PluginManager.getPluginDefaultFolder();
	}
	
//	public static FileExt getPluginFolder(Plugin plugin) {
//		return new FileExt(getPluginDefaultFolder(), plugin.getName())
//	}
	
	public static FileExt getUploadDefaultFolder() throws IOException {
		if (uploadDefaultFolderCache != null)
			return uploadDefaultFolderCache;
		
		Setting s = SettingFactory.getSetting(SETTING_UPLOAD_DEFAULT_FOLDER);
		
		String path = null;
		if (s != null)
			path = s.getValue("path").getValueAsString();
		
		path = path.replaceAll("\\$HOME", System.getProperty("user.home"));
		path = path.replaceAll("\\$" + BEANS_HOME_ENV_NAME, getBeansHomeFolder().getAbsolutePath());
		
		uploadDefaultFolderCache = new FileExt(path);
		
		uploadDefaultFolderCache.mkdirs();
		
		return uploadDefaultFolderCache;
	}
	
	public static FileExt getConnectorDefaultFolder() throws IOException {
		if (connectorDefaultFolderCache != null)
			return connectorDefaultFolderCache;
		
		Setting s = SettingFactory.getSetting(SETTING_CONNECTOR_DEFAULT_FOLDER);
		
		String path = null;
		if (s != null)
			path = s.getValue("path").getValueAsString();
		else
			path = new FileExt("$HOME/.beans-software/connectors/").getAbsolutePath();
		
		path = path.replaceAll("\\$HOME", System.getProperty("user.home"));
		path = path.replaceAll("\\$" + BEANS_HOME_ENV_NAME, getBeansHomeFolder().getAbsolutePath());
		
		connectorDefaultFolderCache = new FileExt(path);
		
		connectorDefaultFolderCache.mkdirs();
		
		return connectorDefaultFolderCache;
	}
	
	public static Connector getDefaultConnector(UUID tableId, String partId) throws IOException {
		Connector conn;
		try {
			String clazz = SettingFactory
					.getSetting(SETTING_CONNECTOR_DEFAULT_CLASS)
					.getValue("class")
					.getValueAsString();
			
			conn = (Connector) Class.forName(clazz).newInstance();
			
			ConnectorInitParams init = new ConnectorInitParams();
			init.setTableId(tableId);
			init.getMeta().add("tableId", tableId.getId());
			init.getMeta().add("uniTableId", partId);
			
			conn.setInitParams(init);
			
			return conn;
		} catch (Throwable e) {
			LibsLogger.error(BeansSettings.class, "Cannot create Connector based on BEANS setting, "
					+ "creating default H5Connector instead", e);
			
			return new H5Connector(tableId, 
					new File(getConnectorDefaultFolder().getAbsolutePath() 
							+ "/" + tableId + "-" + partId + ".h5"))
				.setUseCache(true);
		}
		
//		return new PlainConnector(new File(tableId + "-" + uniTableId + ".dat"));
//		return new LuceneConnector().setInitParams(init);
//		return new MapDbConnector().setInitParams(init);
	}
	
	public static FileExt getExecutionPath() {
		try {
			if (SystemUtils.isRunningFromJar())
				return new FileExt(SystemUtils.getJarExecutionPath().getAbsolutePath());
			else
				return new FileExt(".");
		} catch (UnsupportedEncodingException e) {
			LibsLogger.error(BeansSettings.class, "Cannot get execution path", e);
			return null;
		}
	}
	
//	public static String getProperty(String key) {
//		try {
//			Property prop = PropertyFactory.getProperty(key);
//			return prop != null ? prop.getMsg() : null;
//		} catch (IOException e) {
//			LibsLogger.error(BeansSettings.class, "Cannot read setting from DB", e);
//			return null;
//		}
//	}
	
//	public static String getProperty(String key, String defaultValue) {
//		String prop = getProperty(key);
//		return prop != null ? prop : defaultValue;
//	}
	
	public static FileExt getTmpFolder() {
//		return new FileExt(System.getProperty("java.io.tmpdir"));
		FileExt tmp = new FileExt(getBeansHomeFolder(), "tmp");
		if (tmpCreated == false) {
			tmp.mkdirs();
			tmpCreated = true;
		}
		return tmp;
	}
	
	public static MailCredentials getMailCredentials() throws IOException {
		Setting mailSett = SettingFactory.getSetting("MAIL_CONFIGURATION");
		
		MailCredentials mc = new MailCredentials();
		mc.setHost(mailSett.getValue("host").getValueAsString());
		mc.setPort(mailSett.getValue("port").getValueAsInteger());
		mc.setFrom(mailSett.getValue("username").getValueAsString());
		String pass = mailSett.getValue("password").getValueAsString();
//		mc.setPass(notEmpty(pass) ? EncryptionUtils.decrypt(PRIV, pass) : null);
		mc.setPass(pass);
		mc.setTimeoutMs(5000);
		mc.setSsl(true);
		
		return mc;
	}
	
	public static boolean isRegistrationEnabled() throws IOException {
		Setting s = SettingFactory.getSetting(BeansSettings.SETTING_NEW_USER_REGISTRATION);
		return s != null ? s.getValue("registration").getValueAsBoolean() : false; 
	}

	public static void setRegistrationEnabled(boolean registrationEnabled) throws IOException {
		try {
			Setting s = SettingFactory.getSetting(BeansSettings.SETTING_NEW_USER_REGISTRATION);
			s.getValue("registration").setValue(registrationEnabled);
			s.save();
			WeblibsConst.REGISTRATION_ENABLED = registrationEnabled;
		} catch (ValidationException e) {
			throw new IOException("Cannot save setting " + BeansSettings.SETTING_NEW_USER_REGISTRATION, e);
		}
	}
	
	public static void setSetting(final UUID userId, final String settingName, final String name, 
			final Object value) {
		try {
			Setting s = SettingFactory.getSetting(userId, settingName);
			
			if (s == null) {
				s = new Setting();
				if (userId != null)
					s.setUserId(userId);
				s.setId(settingName);
				s.setName(settingName);
			}
			
			s.setValue(name, value);
			s.save();
		} catch (Throwable e) {
			LibsLogger.error(BeansSettings.class, "Cannot save setting", e);
		}
	}
	
	public static void setSetting(final String settingName, final String name, final Object value) {
		try {
			Setting s = SettingFactory.getSetting(settingName);
			
			if (s == null) {
				s = new Setting();
				s.setName(settingName);
			}
			
			s.setValue(name, value);
			s.save();
		} catch (Throwable e) {
			LibsLogger.error(BeansSettings.class, "Cannot save setting", e);
		}
	}
	
	public static Object getSetting(final String settingName, final String name, final Object defaultValue) {
		try {
			Setting s = SettingFactory.getSetting(settingName);
			
			if (s == null)
				return defaultValue;
			
			SettingValue sv = s.getValue(name);
			return sv != null ? sv.getValue() : defaultValue;
		} catch (Throwable e) {
			LibsLogger.error(BeansSettings.class, "Cannot get setting " + BeansSettings.SETTING_NEW_USER_REGISTRATION + ", "
					+ "returning default setting", e);
			return defaultValue;
		}
	}
	
	public static String getSettingAsString(final String settingName, final String name, final String defaultValue) {
		Object value = getSetting(settingName, name, defaultValue);
		
		if (value instanceof String)
			return (String) value;
		
		return value != null ? String.valueOf(value) : null;
	}
	
	public static int getSettingAsInt(final String settingName, final String name, final int defaultValue) {
		Object value = getSetting(settingName, name, defaultValue);
		
		if (value instanceof Integer)
			return (Integer) value;
		
		return NumberUtils.toInt(value);
	}
	
	public static boolean getSettingAsBoolean(final String settingName, final String name, final boolean defaultValue) {
		Object value = getSetting(settingName, name, defaultValue);
		
		if (value instanceof Boolean)
			return (boolean) value;
		
		return NumberUtils.toInt(value) == 1;
	}
	
	public static Object getSetting(final UUID userId, final String settingName, final String name, 
			final Object defaultValue) {
		try {
			Setting s = SettingFactory.getSetting(userId, settingName);
			
			if (s == null)
				return defaultValue;
			
			SettingValue sv = s.getValue(name);
			return sv != null ? sv.getValue() : defaultValue;
		} catch (Throwable e) {
			LibsLogger.error(BeansSettings.class, "Cannot save setting " + BeansSettings.SETTING_NEW_USER_REGISTRATION, e);
			return defaultValue;
		}
	}
	
	public static String getSettingAsString(final UUID userId, final String settingName, final String name, 
			final String defaultValue) {
		Object value = getSetting(userId, settingName, name, defaultValue);
		
		if (value instanceof String)
			return (String) value;
		
		return String.valueOf(value);
	}
	
	public static int getSettingAsInt(final UUID userId, final String settingName, final String name, 
			final int defaultValue) {
		Object value = getSetting(userId, settingName, name, defaultValue);
		
		if (value instanceof Integer)
			return (Integer) value;
		
		return NumberUtils.toInt(value);
	}
	
	public static boolean getSettingAsBoolean(final UUID userId, final String settingName, final String name, 
			final boolean defaultValue) {
		Object value = getSetting(userId, settingName, name, defaultValue);
		
		if (value instanceof Boolean)
			return (boolean) value;
		
		return NumberUtils.toInt(value) == 1;
	}

//	public static String getProperty(Email user, String key) throws IOException {
//		return BeansSettings.getProperty(User.getUser(user), key);
//	}

//	public static String getProperty(User user, String key) {
//		return BeansSettings.getSe Property(user.getUserId(), key);
//	}
//
//	public static long getPropertyAsLong(UUID userId, String key, long defaultValue) {
//		String tmp = BeansSettings.getProperty(userId, key);
//		return tmp != null ? NumberUtils.toLong(tmp, defaultValue) : defaultValue;
//	}
//	
//	public static boolean getSettingAsBoolean(UUID userId, String key, boolean defaultValue) {
//		String tmp = BeansSettings.getProperty(userId, key);
//		return tmp != null ? tmp.equals("true") : defaultValue;
//	}
//
//	public static int getSettingAsInt(UUID userId, String key, int defaultValue) {
//		String tmp = BeansSettings.getProperty(userId, key);
//		return tmp != null ? NumberUtils.toInt(tmp, defaultValue) : defaultValue;
//	}
//
//	public static String getSetting(UUID userId, String key, String defaultValue) {
//		String tmp = BeansSettings.getProperty(userId, key);
//		return tmp != null ? tmp : defaultValue;
//	}

//	public static String getProperty(UUID userId, String key) {
//		try {
//			Property prop = PropertyFactory.getProperty(userId, key);
//			return prop != null ? prop.getMsg() : null;
//		} catch (IOException e) {
//			LibsLogger.error(BeansSettings.class, "Cannot read user's setting from DB", e);
//			return null;
//		}
//	}

//	public static void putSetting(String userEmail, String key, String value) throws IOException {
//		User user = User.getUser(new Email(userEmail));
//		BeansSettings.putProperty(user.getUserId(), key, value);
//	}

//	public static void putProperty(UUID userId, String key, String value) {
//		try {
//			PropertyFactory.saveProperty(userId, key, value);
//		} catch (IOException | ValidationException e) {
//			LibsLogger.error(BeansSettings.class, "Cannot read user's setting from DB", e);
//		}
//	}

//	public static void removeProperty(UUID userId, String key) {
//		try {
//			Property prop = PropertyFactory.getProperty(userId, key);
//			if (prop != null)
//				prop.remove();
//		} catch (IOException | ValidationException e) {
//			LibsLogger.error(BeansSettings.class, "Cannot read user's setting from DB", e);
//		}
//	}

	public static void saveNewDefaultConnector(User user, String driverName) {
		BeansSettings.saveNewDefaultConnector(user.getUserId(), driverName);
	}

	public static void saveNewDefaultConnector(UUID userId, String driverName) {
		assertTrue(ReflectionUtility.getClass(driverName) != null, "Cannot find Connector class ", driverName);
		
		BeansSettings.setSetting(BeansSettings.DEFAULT_DB_DRIVER, "class", driverName);
	}

//	public static String getDefaultConnector(UUID userId) {
//		String tmp = getProperty(userId, BeansSettings.DEFAULT_DB_DRIVER);
//		return tmp != null ? tmp : LuceneConnector.class.getName();
//	}

	public static void setAutoUpdateIntervalMinutes(UUID userId, int every, String type) throws IOException, SchedulerException {
		assertTrue(ArrayUtils.contains(new String[]{"Minutes", "Hours", "Days"}, type), "Auto-updates interval type ", type, " is unknown");
		assertTrue(every > 0, "Auto-updates interval value ", every, " is wrong");
		
		BeansSettings.setSetting(userId, 
				BeansSettings.SETTING_AUTO_UPDATE_SCHEDULER_MINUTES, 
				"interval", 
				every + ":" + type);
		
		CreateAutoUpdatesJobsForUsersJob.reloadScheduler(userId);
	}

	/**
	 * Returns settings in the form of e.g. 1:Days, 10:Hours, 5:Minutes
	 * @param userId
	 * @return
	 */
	public static String getAutoUpdateIntervalMinutes(UUID userId) {
		return getSettingAsString(userId, BeansSettings.SETTING_AUTO_UPDATE_SCHEDULER_MINUTES, "interval", "1:Days");
	}

	public static AutoUpdateOption getAutoUpdateEntryDefaultOption(UUID userId) {
		String tmp = getSettingAsString(userId, BeansSettings.SETTING_AUTO_UPDATE_ENTRY_DEFAULT_OPTION, 
				"autoupdate", null);
		return notEmpty(tmp) && tmp.equals("null") == false
				? AutoUpdateOption.valueOf(tmp) : AutoUpdateOption.CONFIRMATION_NEEDED;
	}

	public static void setAutoUpdateEntryDefaultOption(UUID userId, AutoUpdateOption option) {
		BeansSettings.setSetting(userId, 
				BeansSettings.SETTING_AUTO_UPDATE_ENTRY_DEFAULT_OPTION, 
				"autoupdate", 
				option.name());
	}

	public static void reloadSettings(boolean force) {
		try {
			boolean reloaded = false;
			final Setting lastReload = SettingFactory.getSetting(BeansSettings.SETTING_LAST_RELOAD);
			SimpleDate lastReloadDate = lastReload != null ? 
					(SimpleDate) lastReload.getValue("lastEdit").getValueAsDate() : new SimpleDate(2000, 1, 1);
			if (lastReloadDate == null)
				lastReloadDate = new SimpleDate(2000, 1, 1);
			
			String settingsContent = SystemUtils.readFileContent(BeansSettings.class, "beans-settings.json");
			JsonElement jsonSettings = JsonUtils.parseJson(settingsContent);
			
			reloaded = SettingsManager.reloadSettings(jsonSettings, force, lastReloadDate);
			
			if (reloaded)
				new Setting()
					.setId(BeansSettings.SETTING_LAST_RELOAD)
					.setName("Last full reload of settings")
					.addValue(new SettingValue("lastEdit", SimpleDate.now().toStringISO()))
					.save();
		} catch (Exception e) {
			LibsLogger.error(BeansSettings.class, "Cannot reload settings", e);
		}
	}

	public static String getDomain() {
		return getSettingAsString(null, BeansSettings.SETTING_BEANS_HOST, "host", WeblibsConst.DOMAIN);
	}

	public static FileExt getTmpFolder(UUID entryId) {
		FileExt f = getTmpFolder();
		return new FileExt(f.getAbsolutePath(), entryId.getId());
	}

	public static FileExt getBackupFolder() {
		try {
			Setting backupPathSetting = SettingFactory.getSetting(BeansSettings.SETTING_BACKUP_PATH);
			return backupPathSetting != null ? new FileExt(backupPathSetting.getValue("path").getValueAsString()) : null;
		} catch (IOException e) {
			LibsLogger.error(BeansSettings.class, "Cannot get backup folder for BEANS");
			return null;
		}
	}

}
