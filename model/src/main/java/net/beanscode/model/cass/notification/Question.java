package net.beanscode.model.cass.notification;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;

import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.reflection.ReflectionException;
import net.hypki.libs5.utils.reflection.ReflectionUtility;
import net.hypki.libs5.utils.string.Meta;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

public class Question {
	
	@Expose
	@NotNull
	@NotEmpty
	private String text = null;
	
	@Expose
	@NotNull
	@NotEmpty
	private String questionId = null;
	
	@Expose
	@NotNull
	@NotEmpty
	private List<String> answers = null;
	
	@Expose
	@NotNull
	@NotEmpty
	private Meta meta = null;
	
	@Expose
	@NotNull
	@NotEmpty
	private String clazzToCall = null;
	
	@Expose
	@NotNull
	@NotEmpty
	private String clazzMethodToCall = null;
	
	public Question() {
		
	}
	
	public Question(String questionText, String questionId, Class clazzToCall, String clazzMethodToCall, String[] answers) {
		setText(questionText);
		setQuestionId(questionId);
		for (String answer : answers) {
			getAnswers().add(answer);
		}
		setClazzToCall(clazzToCall.getName());
		setClazzMethodToCall(clazzMethodToCall);
	}

	public String getText() {
		return text;
	}

	public Question setText(String text) {
		this.text = text;
		return this;
	}

	public String getQuestionId() {
		return questionId;
	}

	public Question setQuestionId(String questionId) {
		this.questionId = questionId;
		return this;
	}

	public List<String> getAnswers() {
		if (answers == null)
			answers = new ArrayList<>();
		return answers;
	}
	
	public Question setAnswers(String ... answers) {
		for (String s : answers) {
			getAnswers().add(s);
		}
		return this;
	}

	public Question setAnswers(List<String> answers) {
		this.answers = answers;
		return this;
	}
	
	public Question addAnswer(String answer) {
		getAnswers().add(answer);
		return this;
	}

	public Meta getMeta() {
		if (meta == null)
			meta = new Meta();
		return meta;
	}

	public void setMeta(Meta meta) {
		this.meta = meta;
	}

	public String getClazzToCall() {
		return clazzToCall;
	}
	
	public Question setClazzToCall(Class clazzToCall) {
		this.clazzToCall = clazzToCall.getName();
		return this;
	}

	public Question setClazzToCall(String clazzToCall) {
		this.clazzToCall = clazzToCall;
		return this;
	}

	public String getClazzMethodToCall() {
		return clazzMethodToCall;
	}

	public Question setClazzMethodToCall(String clazzMethodToCall) {
		this.clazzMethodToCall = clazzMethodToCall;
		return this;
	}
}
