package net.beanscode.model.cass;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import net.hypki.libs5.utils.LibsLogger;

import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapreduce.InputSplit;

public class UniTableInputSplit extends InputSplit implements Writable {
	
	private String[] locations = new String[]{};
	
	public UniTableInputSplit() {
		LibsLogger.debug(UniTableInputSplit.class, "Constr");
	}
	
	public UniTableInputSplit(String[] locations) {
		this.locations = locations;
	}
	
	public void setLocations(String[] locations) {
		this.locations = locations;
	}
	
	@Override
	public String[] getLocations() throws IOException, InterruptedException {
		return locations;
	}
	
	@Override
	public long getLength() throws IOException, InterruptedException {
//		return Long.MAX_VALUE;
		return 10000 * 1000000; // 10 GB
	}

	@Override
	public void readFields(DataInput arg0) throws IOException {
		LibsLogger.debug(UniTableInputSplit.class, "read");
		this.locations = new String[2];
		this.locations[0] = arg0.readUTF();
		this.locations[1] = arg0.readUTF();
	}

	@Override
	public void write(DataOutput arg0) throws IOException {
		LibsLogger.debug(UniTableInputSplit.class, "write", arg0.toString());
		arg0.writeUTF(locations[0]);
		arg0.writeUTF(locations[1]);
	}
}
