package net.beanscode.model.cass.notification;

import java.io.IOException;

import net.beanscode.model.notebook.Notebook;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.weblibs.jobs.Job;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

import com.google.gson.annotations.Expose;

public class NotificationIndexJob extends Job {
	
	@Expose
	@NotNull
	@NotEmpty
	private UUID userId = null;
	
	@Expose
	@NotNull
	@NotEmpty
	private UUID notificationId = null;
	
	public NotificationIndexJob() {
		
	}
	
	public NotificationIndexJob(UUID userId, UUID notificationId) {
		setUserId(userId);
		setNotificationId(notificationId);
	}

	@Override
	public void run() throws IOException, ValidationException {
		Notification n = NotificationFactory.getNotification(getNotificationId());
		
		if (n == null) {
			LibsLogger.error(NotificationIndexJob.class, String.format("Cannot find Notification %s for user %s. Consuming job...", getNotificationId().getId(), getUserId().getId()));
			return;
		}
		
		n.index();
		LibsLogger.debug(NotificationIndexJob.class, "Notebook " + getNotificationId() + " indexed");
	}

	public UUID getNotificationId() {
		return notificationId;
	}

	public void setNotificationId(UUID notebookId) {
		this.notificationId = notebookId;
	}

	public UUID getUserId() {
		return userId;
	}

	public void setUserId(UUID userId) {
		this.userId = userId;
	}
}
