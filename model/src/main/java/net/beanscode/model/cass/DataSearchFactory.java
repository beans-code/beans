package net.beanscode.model.cass;

import java.io.IOException;

import net.beanscode.model.dataset.Table;
import net.beanscode.model.plugins.Connector;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.search.SearchResults;
import net.hypki.libs5.search.query.Query;
import net.hypki.libs5.search.query.StringTerm;
import net.hypki.libs5.weblibs.SearchManager;
import net.hypki.libs5.weblibs.WeblibsConst;

public class DataSearchFactory {

	public static SearchResults<Row> getRows(UUID tableId, String query, int from, int size) throws IOException {
		Query q = Query.parseQuery(query);
		
		if (tableId != null)
			q.addTerm(new StringTerm("tbid", tableId.getId()));
		
		q.addPrefixNames("columns.");
				
		SearchResults<Row> rows = SearchManager
			.searchInstance()
			.search(Row.class, WeblibsConst.KEYSPACE_LOWERCASE, Data.COLUMN_FAMILY.toLowerCase(), q, from, size);
		
		return rows;
	}
	
	public static SearchResults<Row> getRows(String query, int from, int size) throws IOException {
		return getRows(null, query, from, size);
	}
}
