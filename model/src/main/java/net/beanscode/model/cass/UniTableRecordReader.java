package net.beanscode.model.cass;

import java.io.IOException;
import java.nio.file.FileSystem;
import java.nio.file.Path;

import net.hypki.libs5.utils.utils.NumberUtils;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;

public class UniTableRecordReader extends RecordReader {

    private long start;
    private long end;
    
    public UniTableRecordReader() {
    }
    
    @Override
    public void initialize(InputSplit genericSplit, TaskAttemptContext context)
            throws IOException, InterruptedException {
    	UniTableInputSplit split = (UniTableInputSplit) genericSplit;
        Configuration job = context.getConfiguration();
        
        this.start = NumberUtils.toInt(split.getLocations()[0]) + NumberUtils.toInt(split.getLocations()[1]);

//        start = split.getStart();
//        end = start + split.getLength();
//        final Path file = split.getPath();

//        // open the file and seek to the start of the split
//        FileSystem fs = file.getFileSystem(job);
//        FSDataInputStream fileIn = fs.open(split.getPath());
//    
//        this.xmlLoaderBPIS = new XMLLoaderBufferedPositionedInputStream(fileIn);
    }

    
    @Override
    public void close() throws IOException {
//        xmlLoaderBPIS.close();
    }

    @Override
    public Object getCurrentKey() throws IOException, InterruptedException {
        return null;
    }

    @Override
    public Object getCurrentValue() throws IOException,
            InterruptedException {
    	return null;
//        return xmlLoaderBPIS.collectTag(recordIdentifier, end);
    }

    @Override
    public float getProgress() throws IOException, InterruptedException {

        return 0;
    }


    @Override
    public boolean nextKeyValue() throws IOException, InterruptedException {
//        return xmlLoaderBPIS.isReadable();
    	return true;
    }

}
