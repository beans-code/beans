package net.beanscode.model.cass;

import java.util.HashMap;

import com.google.gson.annotations.Expose;

import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.utils.string.StringUtilities;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotNull;

public class Cumulative extends Function {
	
	@Expose
	@NotNull
	@AssertValid
	private String sourceColumn = null;
	
	@Expose
	@NotNull
	@AssertValid
	private String destinationColumn = null;
	
	@Expose
	@NotNull
	@AssertValid
	private String[] splitByColumns = null;
	
	private HashMap<String, Double> cumValues = new HashMap<String, Double>();

	public Cumulative() {
		
	}
	
	public Cumulative(String sourceColumn, String destinationColumn, String[] splitByColumns) {
		setSourceColumn(sourceColumn);
		setDestinationColumn(destinationColumn);
		setSplitByColumns(splitByColumns);
	}
	
	@Override
	public String getName() {
		return "Cumulative";
	}
	
	@Override
	public void run(Row row) {
		double d = row.getAsDouble(getSourceColumn());
		
		String key = null;
		if (splitByColumns != null && splitByColumns.length > 0) {
			for (int i = 0; i < splitByColumns.length; i++) {
				if (i == 0)
					key = String.valueOf(row.get(splitByColumns[i]));
				else
					key += ";" + String.valueOf(row.get(splitByColumns[i]));
			}
		} else
			key = "_";
		
		Double cum = cumValues.get(key);
		
		if (cum == null)
			cum = 0.0;
		
		cum += d;
		
		row.set(getDestinationColumn(), cum);
		cumValues.put(key, cum);
	}

	public String getSourceColumn() {
		return sourceColumn;
	}

	public void setSourceColumn(String sourceColumn) {
		this.sourceColumn = sourceColumn;
	}

	public String getDestinationColumn() {
		return destinationColumn;
	}

	public void setDestinationColumn(String destinationColumn) {
		this.destinationColumn = destinationColumn;
	}

	public String[] getSplitByColumns() {
		return splitByColumns;
	}

	public void setSplitByColumns(String[] splitByColumns) {
		this.splitByColumns = splitByColumns;
	}

//	public double getValue() {
//		return value;
//	}
//
//	public void setValue(double value) {
//		this.value = value;
//	}
}
