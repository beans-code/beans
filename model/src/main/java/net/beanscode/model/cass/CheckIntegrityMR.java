package net.beanscode.model.cass;

import static java.lang.String.format;
import static net.hypki.libs5.utils.utils.AssertUtils.assertTrue;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.SortedMap;

import net.beanscode.model.dataset.Table;
import net.beanscode.model.dataset.TableFactory;
import net.hypki.libs5.db.cassandra.MapReduce2;
import net.hypki.libs5.db.cassandra.MapReduceUtils;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.sha.MD5Checksum;
import net.hypki.libs5.utils.string.ByteUtils;
import net.hypki.libs5.utils.utils.NumberUtils;
import net.hypki.libs5.weblibs.CacheManager;
import net.hypki.libs5.weblibs.WeblibsConst;

import org.apache.hadoop.io.Text;
//import org.apache.hadoop.hbase.util.ByteBufferUtils;

public class CheckIntegrityMR extends MapReduce2 {
	
	private static final String TABLE_CACHE_NAME = "Table-MP-Cache";
	
	public CheckIntegrityMR() {
		super(WeblibsConst.KEYSPACE, "Data.COLUMN_FAMILY", "Data.COLUMN_FAMILY");
	}
	
	@Override
	protected void afterJob() {
		super.afterJob();
		
		CacheManager.cacheInstance().clearCache(TABLE_CACHE_NAME);
	}

	@Override
	public void mapMap(ByteBuffer key, SortedMap<ByteBuffer, ByteBuffer> columns, org.apache.hadoop.mapreduce.Mapper.Context context) throws IOException, InterruptedException {
		final String rowKey = toString(key);
		final String tableId = rowKey.substring(0, UUID.LENGTH);
		final long row = NumberUtils.toLong(rowKey.substring(UUID.LENGTH + 1));
		
		for (ByteBuffer colKey : columns.keySet()) {			
			String x = tableId + "_" + toString(colKey);
			ByteBuffer y = columns.get(colKey);
			context.write(MapReduceUtils.toText(x), new Text(ByteUtils.append(ByteUtils.toBytes(row), MapReduceUtils.getBytes(y))));
		}
		
	}
		
	@Override
	public void reduceReduce(Text keyIn, Iterable<Text> values, org.apache.hadoop.mapreduce.Reducer.Context context) throws IOException, InterruptedException {
		String tableId = keyIn.toString().substring(0, UUID.LENGTH);
		String colName = keyIn.toString().substring(UUID.LENGTH + 1);
		
		LibsLogger.debug(CheckIntegrityMR.class, "Checking MD5 for table id: " + tableId + ", col: " + colName);
		
		
		try {
			MD5Checksum crc = new MD5Checksum();
			HashMap<Long, byte[]> valuesSorted = new HashMap<>();
			
			for (Text v : values) {
				valuesSorted.put(ByteUtils.toLong(v.getBytes()), Arrays.copyOfRange(v.getBytes(), 8, v.getBytes().length));
			}
			
			List<Long> keys = new ArrayList<>();
			for (Long key : valuesSorted.keySet()) {
				keys.add(key);
			}
			
			Collections.sort(keys);
			
			byte[] tmp = null;
			for (Long key : keys) {
				tmp = valuesSorted.get(key);
				crc.update(tmp);
			}
			
			Table table = getTable(tableId);
			assertTrue(table != null, "Cannot find table for tableId " + tableId);
			
			final String md5Expected = "not implemented"; //table.getColumnsMD5().get(colName);
			final String md5Computed = crc.getChecksum();
			assertTrue(md5Expected.equalsIgnoreCase(md5Computed), format("MD5 check failed for table=%s column=%s, expected=%s, computed=%s", tableId, colName, md5Expected, md5Computed));
			
			LibsLogger.debug(CheckIntegrityMR.class, format("MD5 valid for table %s for column %s", tableId, colName));
		} catch (Exception e) {
			LibsLogger.error(CheckIntegrityMR.class, format("Cannot compute MD5 for table %s for column %s", tableId, colName), e);
		}
	}
	
	private Table getTable(String tableId) {
		Table tmp = CacheManager.cacheInstance().get(Table.class, TABLE_CACHE_NAME, tableId);
		if (tmp != null)
			return tmp;
		
		try {
			Table tableDB = TableFactory.getTable(new UUID(tableId));
			assertTrue(tableDB != null, "Expected to find one table for query: " + tableId);
			
			CacheManager.cacheInstance().put(TABLE_CACHE_NAME, tableId, tableDB);
			return tableDB;
		} catch (IOException | RuntimeException e) {
			LibsLogger.error(CheckIntegrityMR.class, "Cannot find table with id " + tableId, e);
			return null;
		}
	}
	
}
