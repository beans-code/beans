package net.beanscode.model.cass;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.apache.pig.parser.AliasMasker.foreach_clause_return;

import com.google.gson.annotations.Expose;

import net.beanscode.model.BeansChannels;
import net.beanscode.model.BeansConst;
import net.beanscode.model.cass.notification.Notification;
import net.beanscode.model.cass.notification.NotificationFactory;
import net.beanscode.model.cass.notification.NotificationType;
import net.beanscode.model.notebook.Notebook;
import net.beanscode.model.notebook.NotebookEntryFactory;
import net.beanscode.model.notebook.NotebookFactory;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.date.SimpleDate;
import net.hypki.libs5.weblibs.jobs.Job;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

public class ValidateBeansJob extends Job {
	
	@Expose
	@NotNull
	@NotEmpty
	private UUID userId = null;

	public ValidateBeansJob() {
		super(BeansChannels.CHANNEL_LONG_HIDDEN);
	}
	
	@Override
	public void run() throws IOException, ValidationException {
		final UUID notId = UUID.random();
		
		new Notification()
			.setDescription("Validating data in BEANS, looking for errors...")
			.setUserId(getUserId())
			.setId(notId)
			.setNotificationType(NotificationType.INFO)
			.setTitle("Validating BEANS")
			.save();
		
		try {
			removeMissingNotebookEntries();
						
			unlockDeadNotifications();
			
			new Notification()
				.setDescription("Validating data in BEANS, looking for errors...")
				.setUserId(getUserId())
				.setId(notId)
				.setNotificationType(NotificationType.OK)
				.setTitle("Validating BEANS")
				.save();
		} catch (Exception e) {
			LibsLogger.error(ValidateBeansJob.class, "Validating BEANS failed", e);
			
			new Notification()
				.setDescription("Validating data in BEANS, looking for errors...")
				.setUserId(getUserId())
				.setId(notId)
				.setNotificationType(NotificationType.ERROR)
				.setTitle("Validating BEANS")
				.save();
		}
	}

	private void unlockDeadNotifications() throws ValidationException, IOException {
		for (Notification notif : NotificationFactory.iterateNotifications(null, false, null)) {
			if (notif.getCreationDate().isBefore(SimpleDate.now().subtractDays(2))) {
				LibsLogger.debug(ValidateBeansJob.class, "Notification ", notif.getId(), " is locked for > 2 days, unlocking it");
				notif.setLocked(false);
				notif.save();
			}
		}
	}

	private void removeMissingNotebookEntries() throws IOException, ValidationException {
		for (Notebook notebook : NotebookFactory.iterateNotebooks()) {
			Set<UUID> toRemove = new HashSet<UUID>();
			for (UUID entryId : notebook.getEntries()) {
				if (NotebookEntryFactory.getNotebookEntry(entryId) == null) {
					LibsLogger.error(ValidateBeansJob.class, "Notebook " + notebook.getId() + " contains "
							+ "entry " + entryId + " but the NotebookEntry is missing. Removing entry from Notebook.");
					toRemove.add(entryId);
				}
			}
			if (toRemove.size() > 0) {
				for (UUID toRemoveEntryId : toRemove)
					notebook.removeEntry(toRemoveEntryId);
				notebook.save();
			}
		}
	}

	public UUID getUserId() {
		return userId;
	}

	public ValidateBeansJob setUserId(UUID userId) {
		this.userId = userId;
		return this;
	}
}
