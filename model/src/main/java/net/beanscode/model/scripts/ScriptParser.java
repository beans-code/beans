package net.beanscode.model.scripts;

import net.hypki.libs5.utils.string.RegexUtils;

public class ScriptParser {

	public static Script parse(String script) {
		Script scr = new Script();
		
		Part part = null;
		for (String line : script.split("\n")) {
			
			if (line.startsWith("#!")) {
				
				// new script part with new language
				Language lng = Language.valueOf(RegexUtils.firstGroup("\\#\\!([\\w]+)", line));
				part = new Part(lng);
				scr.getParts().add(part);
				
			} else if (line.toLowerCase().matches("[\\s]*plot[\\s]+.*")) {
				
				// plot script
				part = new Part(Language.PLOT);
				scr.getParts().add(part);
				
			}
			
			// by default it is PIG script
			if (part == null) {
				part = new Part();
				scr.getParts().add(part);
			}
			
			part.append(line);
		}
		
		return scr;
	}
	
}
