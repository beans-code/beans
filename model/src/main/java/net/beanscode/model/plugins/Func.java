package net.beanscode.model.plugins;

import org.apache.pig.EvalFunc;

public abstract class Func<T> extends EvalFunc<T> {
	
	public abstract String getName();

	public abstract String getHelp();
}
