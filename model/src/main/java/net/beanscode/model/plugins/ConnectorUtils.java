package net.beanscode.model.plugins;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.NotImplementedException;

import net.beanscode.model.BeansSettings;
import net.beanscode.model.connectors.ConnectorLink;
import net.beanscode.model.connectors.ConnectorList;
import net.beanscode.model.connectors.H5Connector;
import net.beanscode.model.connectors.PlainConnector;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.date.Watch;
import net.hypki.libs5.utils.file.FileExt;

public class ConnectorUtils {
	
	static {
		new FileExt("bpTemp/").mkdirs();
	}
	
	public static void sort(Iterator<Row> iterRows,
			ColumnDefList columnDefList,
			Connector dest, 
			final String column) throws IOException {
		ConnectorList connList = new ConnectorList();
		long rowRAM = -1;
		List<Row> toSort = new ArrayList<Row>();
		
		long currentRAM = 0;
		long maxRAM = BeansSettings.SORT_MAX_MEMORY_BYTES; // TODO it was here ButcheredPigSettings.SORT_MAX_MEMORY_BYTES
				
		while (iterRows.hasNext()) {
			Row row = iterRows.next();
			
			if (row == null)
				break;
			
			if (rowRAM == -1)
				rowRAM = row.getMemorySize();
			
			if (currentRAM < maxRAM) {
				
				toSort.add(row);
				currentRAM += rowRAM;
				
			} else {
				toSort.add(row);
				
				// sorting in memory one part
				sortAndAdd(toSort, columnDefList, connList, column);
				
				currentRAM = 0;
				toSort.clear();
			}
		}
		
		if (toSort.size() > 0)
			sortAndAdd(toSort, columnDefList, connList, column);
		
		// reading values from parts and sorting them in memory to output connector
		int connSize = connList.size();
		if (connSize == 1) {
			for (Row row : connList)
				dest.write(row);
		} else if (connSize > 1) {
//			LibsLogger.info(ConnectorUtils.class, "Combining ", source.toString(), " part ", part, 
//					" from all partsCount ", partsCount);
			
			combineSortedConnectors(connList, dest, column);
			
//			LibsLogger.info(ConnectorUtils.class, "Done combining ", source.toString(), " part ", part, 
//					" from all partsCount ", partsCount);
		}
		
		dest.close();
//		LibsLogger.info(ConnectorUtils.class, "Done sorting ", source.toString(), " part ", part, 
//				" from all partsCount ", partsCount);
	}
	
	public static void sort(ConnectorList source,
			Connector dest, 
			final String column) throws IOException {
		combineSortedConnectors(source, dest, column);
	}
	
	public static void sort(List<Iterable<Row>> source,
			Connector dest, 
			final String column) throws IOException {
		combineSortedConnectors(source, dest, column);
	}

	public static void sort(Connector source, int part, int partsCount,
			Connector dest, 
			final String column) throws IOException {
		LibsLogger.info(ConnectorUtils.class, "Sorting ", source.toString(), " part ", part, 
				" from all partsCount ", partsCount);
		
		ConnectorList connList = new ConnectorList();
		long rowRAM = -1;
		List<Row> toSort = new ArrayList<Row>();
		
		long currentRAM = 0;
		long maxRAM = BeansSettings.SORT_MAX_MEMORY_BYTES; // TODO it was here ButcheredPigSettings.SORT_MAX_MEMORY_BYTES
				
		for (Row row : source.iterateRows(null, part, partsCount)) {
			if (rowRAM == -1)
				rowRAM = row.getMemorySize();
			
			if (currentRAM < maxRAM) {
				
				toSort.add(row);
				currentRAM += rowRAM;
				
			} else {
				toSort.add(row);
				
				// sorting in memory one part
				sortAndAdd(toSort, source.getColumnDefs(), connList, column);
				
				currentRAM = 0;
				toSort.clear();
			}
		}
		
		if (toSort.size() > 0)
			sortAndAdd(toSort, source.getColumnDefs(), connList, column);
		
		// reading values from parts and sorting them in memory to output connector
		int connSize = connList.size();
		if (connSize == 1) {
			for (Row row : connList)
				dest.write(row);
		} else if (connSize > 1) {
			LibsLogger.info(ConnectorUtils.class, "Combining ", source.toString(), " part ", part, 
					" from all partsCount ", partsCount);
			
			combineSortedConnectors(connList, dest, column);
			
			LibsLogger.info(ConnectorUtils.class, "Done combining ", source.toString(), " part ", part, 
					" from all partsCount ", partsCount);
		}
		
		dest.close();
		LibsLogger.info(ConnectorUtils.class, "Done sorting ", source.toString(), " part ", part, 
				" from all partsCount ", partsCount);
	}
	
	public static void combineSortedConnectors(final ConnectorList connList, 
			Connector dest, 
			final List<String> columns) throws IOException {
		final int connSize = connList.size();
		final Row[] peeks = new Row[connSize];
		final Iterator<Row>[] iters = new Iterator[connSize];
		
		// opening parts iterators
		int i = 0;
		for (ConnectorLink connLink : connList.getConnectorsLinks()) {
			iters[i] = connLink.getConnectorInstance().iterateRows(null).iterator();
			if (iters[i].hasNext())
				peeks[i] = iters[i].next();
			else
				peeks[i] = null;
			i++;
		}
		
		// sorting and writing to the destination
		while (true) {
			// looking for minimum value from all connectors
			Row min = null;
			int minIter = -1;
			for (int j = 0; j < connSize; j++) {
				if (peeks[j] != null) {
					if (min == null) {
						min = peeks[j];
						minIter = j;
					} else if (min.compare(columns, peeks[j]) > 0) {
						min = peeks[j];
						minIter = j;
					}
				}
			}
			
			if (min == null) {
				// no more rows
				dest.close();
				break;
			}
			
			dest.write(min);
			
			if (iters[minIter].hasNext())
				peeks[minIter] = iters[minIter].next();
			else
				peeks[minIter] = null;
		}
	}
	
	private static void combineSortedConnectors(final List<Iterable<Row>> connList, 
			Connector dest, 
			final String column) throws IOException {
		final Watch combiningWatch = new Watch();
		final int connSize = connList.size();
		final Row[] peeks = new Row[connSize];
		final Iterator<Row>[] iters = new Iterator[connSize];
		
		// opening parts iterators
		int i = 0;
		for (Iterable<Row> connLink : connList) {
			iters[i] = connLink.iterator();
			if (iters[i].hasNext())
				peeks[i] = iters[i].next();
			else
				peeks[i] = null;
			i++;
		}
		
		// sorting and writing to the destination
		while (true) {
			// looking for minimum value from all connectors
			Row min = null;
			int minIter = -1;
			for (int j = 0; j < connSize; j++) {
				if (peeks[j] != null) {
					if (min == null) {
						min = peeks[j];
						minIter = j;
					} else if (min.compare(column, (Comparable) peeks[j].get(column)) > 0) {
						min = peeks[j];
						minIter = j;
					}
				}
			}
			
			if (min == null) {
				// no more rows
				dest.close();
				break;
			}
			
			dest.write(min);
			
			if (iters[minIter].hasNext())
				peeks[minIter] = iters[minIter].next();
			else
				peeks[minIter] = null;
		}
		LibsLogger.info(ConnectorUtils.class, "Sort parts combined in " + combiningWatch);
	}
	
	private static void combineSortedConnectors(final ConnectorList connList, 
			Connector dest, 
			final String column) throws IOException {
		final int connSize = connList.size();
		final Row[] peeks = new Row[connSize];
		final Iterator<Row>[] iters = new Iterator[connSize];
		
		// opening parts iterators
		int i = 0;
		for (ConnectorLink connLink : connList.getConnectorsLinks()) {
			iters[i] = connLink.getConnectorInstance().iterateRows(null).iterator();
			if (iters[i].hasNext())
				peeks[i] = iters[i].next();
			else
				peeks[i] = null;
			i++;
		}
		
		// sorting and writing to the destination
		while (true) {
			// looking for minimum value from all connectors
			Row min = null;
			int minIter = -1;
			for (int j = 0; j < connSize; j++) {
				if (peeks[j] != null) {
					if (min == null) {
						min = peeks[j];
						minIter = j;
					} else if (min.compare(column, (Comparable) peeks[j].get(column)) > 0) {
						min = peeks[j];
						minIter = j;
					}
				}
			}
			
			if (min == null) {
				// no more rows
				dest.close();
				break;
			}
			
			dest.write(min);
			
			if (iters[minIter].hasNext())
				peeks[minIter] = iters[minIter].next();
			else
				peeks[minIter] = null;
		}
	}
	
	private static void sortAndAdd(List<Row> toSort, ColumnDefList columnsDefs,
			ConnectorList connList, final String column) throws IOException {
		LibsLogger.info(ConnectorUtils.class, "Sorting in memory ", toSort.size(), " rows");
		
		Collections.sort(toSort, new Comparator<Row>() {
			@Override
			public int compare(Row o1, Row o2) {
				Object v1 = o1.get(column);
				Object v2 = o2.get(column);
				if (v1 instanceof Double)
					return ((Double) v1).compareTo((Double) v2);
				else if (v1 instanceof Integer)
					return ((Integer) v1).compareTo((Integer) v2);
				else if (v1 instanceof Long)
					return ((Long) v1).compareTo((Long) v2);
				else if (v1 instanceof String)
					return ((String) v1).compareTo((String) v2);
				else if (v1 == null || v2 == null)
					return 0;
				else
					// TODO 
					throw new NotImplementedException();
			}
		});
		
		H5Connector part = new H5Connector(UUID.random(), new FileExt("bpTemp/" + UUID.random() + ".h5"));
		
		part.setColumnDefs(columnsDefs);
		
		for (Row r : toSort) {
			part.write(r);
		}
		
		part.close();
		
		connList.addConnector(part);
		
		LibsLogger.info(ConnectorUtils.class, "Done sorting in memory ", toSort.size(), " rows");
	}
	
	public static void pipe(Connector source, Connector dest) throws IOException {
		dest.setColumnDefs(source.getColumnDefs());
		for (Row s : source.iterateRows(null)) {
			dest.write(s);
		}
	}

	public static boolean theSame(Connector connector1, Connector connector2) {
		List<String> sha = new ArrayList<>();
		for (Row row : connector1.iterateRows(null)) {
			sha.add(row.getSha512());
		}
		
		for (Row row : connector2.iterateRows(null)) {
			if (!sha.remove(row.getSha512()))
				return false;
		}
		
		return sha.isEmpty();
	}
}
