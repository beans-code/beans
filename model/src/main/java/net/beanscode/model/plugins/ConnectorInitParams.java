package net.beanscode.model.plugins;

import java.io.Serializable;

import com.google.gson.annotations.Expose;

import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.string.MetaList;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotNull;

public class ConnectorInitParams implements Serializable {

	@Expose
	@NotNull
	@AssertValid
	private UUID tbid = null;
	
	@Expose
	@NotNull
	@AssertValid
	private MetaList meta = null;
	
	public ConnectorInitParams() {
		
	}

	public UUID getTableId() {
		return tbid;
	}
	
	@Override
	public String toString() {
		return JsonUtils.objectToStringPretty(this);
//		return String.format("tableId=%s,meta=%s",
//				getTableId() != null ? getTableId().toString() : "",
//				meta != null ? meta.toString() : "");
	}
	
	@Override
	public boolean equals(Object obj) {
		return JsonUtils.objectToString(this).equals(JsonUtils.objectToString(obj));
	}

	public ConnectorInitParams setTableId(UUID tableId) {
		this.tbid = tableId;
		return this;
	}

	public MetaList getMeta() {
		if (meta == null)
			this.meta = new MetaList();
		return meta;
	}

	public ConnectorInitParams setMeta(MetaList meta) {
		this.meta = meta;
		return this;
	}
}
