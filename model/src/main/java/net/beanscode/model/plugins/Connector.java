package net.beanscode.model.plugins;

import java.io.IOException;
import java.util.List;

import net.hypki.libs5.db.db.Results;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.weblibs.C3Where;
import net.hypki.libs5.search.query.Query;

public interface Connector {
	public String getName();
	
	public ConnectorInitParams getInitParams();
	public Connector setInitParams(ConnectorInitParams initParams);
	
	public Iterable<Row> iterateRows(Query filter);
	public Iterable<Row> iterateRows(Query filter, int splitNr, int splitCount);
	public Results 		 iterateRows(Query filter, String state, int size);
	
	public void write(Row row) throws IOException;
	public void close() throws IOException;
	
	public long getAproxBytesSize() throws IOException;
	
	public void clear() throws IOException;
	
	public void setColumnDefs(ColumnDefList defs);
	public ColumnDefList getColumnDefs();
	
	public void validate() throws IOException;
	
	public String getVersion();
}
