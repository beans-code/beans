package net.beanscode.model.plugins;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.List;

import net.beanscode.model.notebook.NotebookEntry;
import net.hypki.libs5.db.db.DatabaseProvider;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.search.SearchEngineProvider;

public interface Plugin {
	
	public String getName();
	
	public String getVersion();
	
	public String getDescription();
	
	public String getPanel(UUID userId) throws IOException;
	
	public List<Func> getFuncList();
	
	public List<Class<? extends Connector>> getConnectorClasses();
	
	public List<Class<? extends NotebookEntry>> getNotebookEntries();
	
	public List<Class<? extends DatabaseProvider>> getDatabaseProviderList();
	
	public List<Class<? extends SearchEngineProvider>> getSearchEngineProviderList();
	
	public boolean selfTest(UUID userId, OutputStreamWriter output) throws ValidationException, IOException;
}
