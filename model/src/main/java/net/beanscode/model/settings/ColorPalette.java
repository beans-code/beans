package net.beanscode.model.settings;

import java.awt.Color;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import net.beanscode.model.BeansSettings;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.utils.ColorUtils;

import com.google.gson.annotations.Expose;

public class ColorPalette {
	
	private static final String USER_COLOR_PALETTE_PROPERTY_KEY = "USER_COLOR_PALETTE";
	
	public static final int DEFINED_COLORS_COUNT = 20;
	
	private static ColorPalette defaultPalette = null;
	
	@Expose
	private List<Integer> colors = null;

	public ColorPalette() {
		
	}
	
	public static ColorPalette getColorPalette(UUID userId, ColorPalette defaultPalette) throws IOException {
		ColorPalette palette = getColorPalette(userId);
		return palette != null && palette.getColors().size() > 0 ? palette : defaultPalette;
	}
	
	public static ColorPalette getColorPalette(UUID userId) throws IOException {
		String palette = BeansSettings.getSettingAsString(userId, USER_COLOR_PALETTE_PROPERTY_KEY, "palette", null);
		return palette != null ? JsonUtils.fromJson(palette, ColorPalette.class) : null;
	}
	
	public static void saveColorPalette(UUID userId, ColorPalette palette) throws ValidationException, IOException {
		BeansSettings.setSetting(userId, USER_COLOR_PALETTE_PROPERTY_KEY, "palette", JsonUtils.objectToString(palette));
		LibsLogger.debug(ColorPalette.class, "Color palette saved for user ", userId);
	}
	
	public static ColorPalette defaultPalette() {
		if (defaultPalette == null) {
			try {
				defaultPalette = JsonUtils.fromJson("{\"colors\":[-4601945,-12913586,-13446451,-13162330,-2818278,-719306,-1843244,-5887853,-58740,"
					+ "-14503366,-7363422,-6341080,-3301831,-15011841,-40422,-12969217,-1384505,-6720913,-475828,-9885781]}", ColorPalette.class);
			} catch (Exception e) {
				LibsLogger.error(ColorPalette.class, "Cannot deserialize default application palette. Using random palette instead.", e);
				defaultPalette = randomPalette();
			}
		}
		return defaultPalette;
	}

	public static ColorPalette randomPalette() {
		ColorPalette palette = new ColorPalette();
		
		for (int i = 0; i < DEFINED_COLORS_COUNT; i++) {
//			palette.getColors().add(ColorUtils.randomPastelColor().getRGB());
//			palette.getColors().add(ColorUtils.randomPastelColor2().getRGB());
			palette.getColors().add(ColorUtils.randomPastelColor3().getRGB());
//			palette.getColors().add(ColorUtils.randomColor().getRGB());
		}
		
		return palette;
	}
	
	
	public List<Integer> getColors() {
		if (colors == null)
			colors = new ArrayList<>();
		return colors;
	}

	public void setColors(List<Integer> colors) {
		this.colors = colors;
	}
	
	public String toCssColor(int colorIndex) {
		colorIndex = colorIndex % DEFINED_COLORS_COUNT;
		return ColorUtils.colorToHex(new Color(getColors().get(colorIndex)));
	}
}
