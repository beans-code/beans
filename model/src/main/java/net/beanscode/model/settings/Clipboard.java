package net.beanscode.model.settings;

import java.io.IOException;
import java.io.Serializable;

import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.string.RegexUtils;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.weblibs.property.Property;
import net.hypki.libs5.weblibs.property.PropertyFactory;

public class Clipboard {
	
	private static void putProperty(String key, String value) {
		try {
			PropertyFactory.saveProperty(key, value);
		} catch (IOException | ValidationException e) {
			LibsLogger.error(Clipboard.class, "Cannot read user's setting from DB", e);
		}
	}
	
	private static String getPropertyNoCache(String key) {
		try {
			Property prop = PropertyFactory.getPropertyNoCache(key);
			return prop != null ? prop.getMsg() : null;
		} catch (IOException e) {
			LibsLogger.error(Clipboard.class, "Cannot read setting from DB", e);
			return null;
		}
	}

	public static void set(final String key, final Object value) {
		try {
			String ser = StringUtilities.serialize((Serializable) value);
			putProperty("CLIPBOARD_" + key, ser);
		} catch (Throwable e) {
			LibsLogger.error(Clipboard.class, "Cannot save " + key + ":" + value + " to Clipboard", e);
		}
	}
	
	public static Object get(final String key) {
		try {
			String prop = getPropertyNoCache("CLIPBOARD_" + key);
			return prop != null ? StringUtilities.deserialize(prop) : null;
		} catch (Throwable e) {
			LibsLogger.error(Clipboard.class, "Cannot get " + key + " from Clipboard", e);
			return null;
		}
	}
	
	public static void remove(final String wildcardKey) {
		// TODO OPTY
		for (Property prop : PropertyFactory.iterateProperty()) {
			try {
				if (StringUtilities.wildcardMatches("*CLIPBOARD_" + wildcardKey, prop.getKey()))
					prop.remove();
			} catch (ValidationException | IOException e) {
				LibsLogger.error(Clipboard.class, "Cannot remove property " + prop.getKey(), e);
			}
		}		
	}
}
