package net.beanscode.model.settings;

import java.io.IOException;

import net.beanscode.model.BeansSearchManager;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.weblibs.jobs.Job;

public class RebuildSearchIndexForUsersJob extends Job {

	public RebuildSearchIndexForUsersJob() {
		
	}
	
	@Override
	public void run() throws IOException, ValidationException {
		try {
			BeansSearchManager.reloadIndexForUsers();
		} catch (Throwable t) {
			LibsLogger.error(RebuildSearchIndexForUsersJob.class, "Cannot reload index, fix the code and try again later", t);
		}
	}
}
