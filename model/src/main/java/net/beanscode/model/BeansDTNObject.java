package net.beanscode.model;

import java.io.IOException;

import net.beanscode.model.dataset.Dataset;
import net.beanscode.model.dataset.Table;
import net.beanscode.model.notebook.MagicKey;
import net.beanscode.model.notebook.MagicKeyFactory;
import net.beanscode.model.notebook.Notebook;
import net.beanscode.model.rights.Group;
import net.beanscode.model.rights.GroupFactory;
import net.beanscode.model.rights.Permission;
import net.beanscode.model.rights.PermissionFactory;
import net.beanscode.model.rights.PermissionRecursiveJob;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.search.query.Query;
import net.hypki.libs5.search.query.StringTerm;
import net.hypki.libs5.search.query.TermRequirement;
import net.hypki.libs5.search.query.WildcardTerm;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.weblibs.WeblibsConst;
import net.hypki.libs5.weblibs.user.User;
import net.hypki.libs5.weblibs.user.UserFactory;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotNull;

import com.google.gson.JsonElement;
import com.google.gson.annotations.Expose;

public abstract class BeansDTNObject<T> extends BeansObject<T> {
	
	@Expose
	@NotNull
	@AssertValid
	private UUID id = null;
	
	@Expose
	@NotNull
	@AssertValid
	private UUID userId = null;
	
	private Permission permissionCache = null;
	
	public UUID getId() {
		return id;
	}

	public T setId(UUID id) {
		this.id = id;
		return (T) this;
	}
	
	public UUID getUserId() {
		return userId;
	}
	
	public User getUser() throws IOException {
		return UserFactory.getUser(getUserId());
	}

	public T setUserId(UUID userId) {
		this.userId = userId;
		return (T) this;
	}

	public boolean isReadAllowed(UUID id) throws IOException {
		if (getUserId().equals(id))
			return true;
			
		Permission perm = getPermission();
		
		if (perm == null) {
			return false;
		}
		
		return perm.isReadAllowedForUser(id);
		
		// check magic key
		
//		for (UUID groupId : perm.getReadAllowedGroups()) {
//			Group group = GroupFactory.getGroup(groupId);
//			if (group != null && group.containsUser(userId))
//				return true;
//		}
//		
//		return false;
	}
	
	public boolean isReadAllowedByShareLink(UUID shareId) throws IOException {
		MagicKey mk = MagicKeyFactory.getMagicKey(shareId);
			
		if (mk != null) {
			return mk.getBeansObjectId().equals(getId());
		}
			
		return false;
	}
		
	public Permission getPermission() throws IOException {
		if (permissionCache == null)
			permissionCache = PermissionFactory.getPermission(getClass().getName(), getKey());
		if (permissionCache == null) {
			permissionCache = new Permission(this);
//			permissionCache.save();
		}
		return permissionCache;
	}
	
	public boolean isWriteAllowed(UUID userId) throws IOException {
		if (getUserId().equals(userId))
			return true;
			
		Permission perm = getPermission();
		
		if (perm == null)
			return false;
		
		for (UUID groupId : perm.getWriteAllowedGroups()) {
			Group group = GroupFactory.getGroup(groupId);
			if (group != null && group.containsUser(userId))
				return true;
		}
		
		return false;
	}
	
	public void setReadPermission(Group group, boolean readAllowed) throws IOException, ValidationException {
		Permission perm = getPermission();
		if (perm == null)
			perm = new Permission(this);
		perm.setReadAllowed(group.getId(), readAllowed);
		perm.save();
	}
	
	public void setWritePermission(Group group, boolean writeAllowed) throws IOException, ValidationException {
		Permission perm = getPermission();
		if (perm == null)
			perm = new Permission(this);
		perm.setWriteAllowed(group.getId(), writeAllowed);
		perm.save();
	}
	
	protected void indexPermissions() throws IOException {
		final Permission perm = getPermission();
		
		if (perm == null
				|| (perm.getGroupReadSize() == 0 && perm.getGroupWriteSize() == 0))
			return; // no nothing
		
//		final Map<String, String> toSave = new HashMap<String, String>();
		final String index = WeblibsConst.KEYSPACE_LOWERCASE;
		final JsonElement objectJson = JsonUtils.toJson(this);
		
		String type = null;
		if (perm.isDataset())
			type = Dataset.COLUMN_FAMILY.toLowerCase();
		else if (perm.isTable())
			type = Table.COLUMN_FAMILY.toLowerCase();
		else if (perm.isNotebook())
			type = Notebook.COLUMN_FAMILY.toLowerCase();		
		
		// for the given DTN removing all sharing information and saving them from scratch
		BeansSearchManager.getSearchManager().remove(index, type, new Query()
			.addTerm(new StringTerm("id.id", perm.getId().getId(), TermRequirement.MUST))
			.addTerm(new WildcardTerm("sharedGroup", "*", TermRequirement.MUST)));
		
		try {
			// TODO it has to be here.... fix all such places
			Thread.sleep(1500);
		} catch (Throwable t) {
			LibsLogger.error(BeansDTNObject.class, "Cannot remove index for Notebook", t);
		}
		
		for (UUID groupId : perm.getReadAllowedGroups()) {	
			objectJson.getAsJsonObject().addProperty("sharedGroup", groupId.getId());
			
			Dataset ds = null;
			
			if (this instanceof Table) {
				ds = ((Table) this).getDataset();
				objectJson.getAsJsonObject().add("dataset", JsonUtils.toJson(ds));
			}
			
			// indexing sharing for every group
			BeansSearchManager.getSearchManager().indexDocument(index, 
					type, 
					groupId.getId() + ":" + perm.getKey(), 
					objectJson.toString());
			
			if (this instanceof Table) {
				// doing the same for the parent Dataset
				JsonElement dsJson = JsonUtils.toJson(ds);
				dsJson.getAsJsonObject().addProperty("sharedGroup", groupId.getId());
				
				// indexing sharing for every group
				BeansSearchManager.getSearchManager().indexDocument(index, 
						Dataset.COLUMN_FAMILY.toLowerCase(), 
						groupId.getId() + ":" + ds.getId().getId(), 
						dsJson.toString());
			}
		}
	}

	//
	public void reloadPermissions(boolean inBackground) throws ValidationException, IOException {
		Permission permission = getPermission();
		
		if (permission == null) {
			permission = new Permission(this);
			permission.save();
		}
		
		if (inBackground)
			new PermissionRecursiveJob(getPermission()).save();
		else
			getPermission().setRightsRecursively();
	}
}
