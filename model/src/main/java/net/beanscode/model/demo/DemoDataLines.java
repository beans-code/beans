package net.beanscode.model.demo;

import static net.hypki.libs5.utils.utils.AssertUtils.assertTrue;

import java.io.File;
import java.io.IOException;

import net.beanscode.model.BeansConst;
import net.beanscode.model.dataset.Dataset;
import net.beanscode.model.dataset.Table;
import net.beanscode.model.notebook.Notebook;
import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.args.ArgsUtils;
import net.hypki.libs5.utils.reflection.SystemUtils;
import net.hypki.libs5.weblibs.SearchManager;
import net.hypki.libs5.weblibs.WeblibsConst;
import net.hypki.libs5.weblibs.jobs.JobsManager;
import net.hypki.libs5.weblibs.user.Email;
import net.hypki.libs5.weblibs.user.User;

public class DemoDataLines {

	public static void main(String[] args) throws ValidationException, IOException {
		System.setProperty("weblibsSettings", "settings-beans-dev.json");
		System.setProperty("javax.xml.parsers.DocumentBuilderFactory", "com.sun.org.apache.xerces.internal.jaxp.DocumentBuilderFactoryImpl");
		
		assertTrue(ArgsUtils.getString(args, "user") != null, "Arg --user is not specified");
		
		final User user = User.getUser(new Email(ArgsUtils.getString(args, "user")));
		final UUID userId = user.getUserId();
		final UUID dsLine1 = UUID.random();
		final UUID dsLine5 = UUID.random();
		final UUID dsLine20 = UUID.random();
		final UUID notebookId = UUID.random();
		final String tableName = "points";
		final String notebookName = "Test lines queries";
		final String plotName = "plot1";
		
		DbObject.getDatabaseProvider().clearKeyspace(WeblibsConst.KEYSPACE);
		SearchManager.searchInstance().clearIndex(WeblibsConst.KEYSPACE_LOWERCASE);
		
		JobsManager.runAllJobs(BeansConst.getAllChannelsNames());

		// create objects
		Dataset line1 = new Dataset().setId(dsLine1).setName("line1").setUserId(userId).save();
		Dataset line5 = new Dataset().setId(dsLine5).setName("line5").setUserId(userId).save();
		Dataset line20 = new Dataset().setId(dsLine20).setName("line20").setUserId(userId).save();
		
		Table tabLine1 = new Table().setDatasetId(dsLine1).setUserId(userId).setName(tableName).save();
		Table tabLine5 = new Table().setDatasetId(dsLine5).setUserId(userId).setName(tableName).save();
		Table tabLine20 = new Table().setDatasetId(dsLine20).setUserId(userId).setName(tableName).save();
		
		Notebook notebook = new Notebook().setId(notebookId).setUserId(userId).setName(notebookName).save();
				
		// importing data to the tables
		tabLine1.importFile(new File(SystemUtils.getResourceURL("testdata/line-1-10").getPath()));
		tabLine5.importFile(new File(SystemUtils.getResourceURL("testdata/line-5-25").getPath()));
		tabLine20.importFile(new File(SystemUtils.getResourceURL("testdata/line-20-30").getPath()));
		
		JobsManager.runAllJobs(BeansConst.getAllChannelsNames());
		
		LibsLogger.debug(DemoDataLines.class, "Finished");
	}
}
