package net.beanscode.model.demo;

import java.io.IOException;

import com.google.gson.annotations.Expose;

import net.beanscode.model.BeansSettings;
import net.beanscode.model.cass.notification.Notification;
import net.beanscode.model.cass.notification.NotificationType;
import net.beanscode.model.connectors.H5Connector;
import net.beanscode.model.dataset.Dataset;
import net.beanscode.model.dataset.Table;
import net.beanscode.model.plugins.ColumnDef;
import net.beanscode.model.plugins.ColumnDefList;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.schema.ColumnType;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.file.FileExt;
import net.hypki.libs5.utils.string.RandomUtils;
import net.hypki.libs5.weblibs.jobs.Job;

public class StressTestingJob extends Job {
	
	@Expose
	private UUID userId = null;
	
	@Expose
	private int datasetCount = 0;
	
	@Expose
	private int tableCount = 0;
	
	@Expose
	private int rowsPerTable = 0;
	
	@Expose
	private int columnsPerTable = 0;

	public StressTestingJob() {
		
	}
	
	@Override
	public void run() throws IOException, ValidationException {
		new FileExt(BeansSettings.getBeansHomeFolder().getAbsolutePath(), "stresstests")
			.mkdirs();
		
		for (int d = 0; d < getDatasetCount(); d++) {
			Dataset ds = new Dataset(getUserId(), "[Stress testing] New dataset " + d);
			ds.save();
			
			for (int t = 0; t < getTableCount(); t++) {
				Table tb = new Table(ds, "Stress testing Table " + t);
				
				FileExt h5File = new FileExt(BeansSettings.getBeansHomeFolder().getAbsolutePath(), "stresstests", "tb-" + tb.getId() + ".h5");
				H5Connector conn = new H5Connector(tb.getId(), h5File);
				
				ColumnDefList columnsDefs = new ColumnDefList();
				for (int c = 0; c < getColumnsPerTable(); c++) {
					columnsDefs.add(new ColumnDef("col" + c, null, ColumnType.DOUBLE));
				}
				conn.setColumnDefs(columnsDefs);
				
				for (int r = 0; r < getRowsPerTable(); r++) {
					Row row = new Row(UUID.random().getId());
					for (int c = 0; c < getColumnsPerTable(); c++) {
						row.addColumn("col" + c, RandomUtils.nextDouble());
					}
					conn.write(row);
				}
				conn.close();
				
				tb.addConnector(conn);
				
				tb.save();
			}
		}
		
		LibsLogger.debug(StressTestingJob.class, "Self test objects creation finished");
		
		new Notification()
			.setDescription("Self test objects creation finished")
			.setNotificationType(NotificationType.OK)
			.setTitle("Self tests objects")
			.setUserId(getUserId())
			.save();
	}

	public int getDatasetCount() {
		return datasetCount;
	}

	public StressTestingJob setDatasetCount(int datasetCount) {
		this.datasetCount = datasetCount;
		return this;
	}

	public int getTableCount() {
		return tableCount;
	}

	public StressTestingJob setTableCount(int tableCount) {
		this.tableCount = tableCount;
		return this;
	}

	public UUID getUserId() {
		return userId;
	}

	public StressTestingJob setUserId(UUID userId) {
		this.userId = userId;
		return this;
	}

	public int getRowsPerTable() {
		return rowsPerTable;
	}

	public StressTestingJob setRowsPerTable(int rowsPerTable) {
		this.rowsPerTable = rowsPerTable;
		return this;
	}

	public int getColumnsPerTable() {
		return columnsPerTable;
	}

	public StressTestingJob setColumnsPerTable(int columnsPerTable) {
		this.columnsPerTable = columnsPerTable;
		return this;
	}
}
