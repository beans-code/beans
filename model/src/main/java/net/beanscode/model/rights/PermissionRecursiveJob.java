package net.beanscode.model.rights;

import java.io.IOException;

import net.beanscode.model.dataset.Table;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.weblibs.jobs.Job;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

import com.google.gson.annotations.Expose;

public class PermissionRecursiveJob extends Job {
	
	@Expose
	@NotNull
	@AssertValid
	private UUID permissionId = null;
	
	@Expose
	@NotNull
	@NotEmpty
	private String permissionClass = null;

	public PermissionRecursiveJob() {
		
	}
	
	public PermissionRecursiveJob(Permission permission) {
		setPermissionId(permission.getId());
		setPermissionClass(permission.getClazz());
	}

	@Override
	public void run() throws IOException, ValidationException {
		final Permission parentPerm = PermissionFactory.getPermission(getPermissionClass(), getPermissionId());
		
		if (parentPerm == null) {
			LibsLogger.error(PermissionRecursiveJob.class, "Permission " + getPermissionId() + " does not exist, consuming job...");
			return;
		}
		
		parentPerm.setRightsRecursively();
	}

	public UUID getPermissionId() {
		return permissionId;
	}

	public void setPermissionId(UUID permissionId) {
		this.permissionId = permissionId;
	}

	public String getPermissionClass() {
		return permissionClass;
	}

	public void setPermissionClass(String permissionClass) {
		this.permissionClass = permissionClass;
	}
}
