package net.beanscode.model.rights;

import java.io.IOException;

import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.db.db.TableIterator;
import net.hypki.libs5.db.db.weblibs.C3Clause;
import net.hypki.libs5.db.db.weblibs.C3Where;
import net.hypki.libs5.db.db.weblibs.ClauseType;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.weblibs.WeblibsConst;
import net.hypki.libs5.weblibs.settings.Setting;

public class GroupFactory {
	public static Group getGroup(String groupId) throws IOException {
		return GroupFactory.getGroup(new UUID(groupId));
	}

	public static Group getGroup(UUID groupId) throws IOException {
		return DbObject.getDatabaseProvider().get(WeblibsConst.KEYSPACE, 
				Group.COLUMN_FAMILY, 
				new C3Where()
						.addClause(new C3Clause(DbObject.COLUMN_PK, ClauseType.EQ, groupId.getId())), 
				DbObject.COLUMN_DATA, 
				Group.class);
	}
	
	public static Iterable<Group> iterateGroups() {
		return new TableIterator<Group>(WeblibsConst.KEYSPACE, 
				Group.COLUMN_FAMILY, 
				DbObject.COLUMN_DATA, 
				null,
				Group.class);
	}
	
//	public NoteSeaRes
}
