package net.beanscode.model.dataset;

import java.io.IOException;

import javax.mail.MessagingException;

import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.weblibs.jobs.Job;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotNull;

import com.google.gson.annotations.Expose;

public class TableValidationJob extends Job {
	
	@Expose
	@NotNull
	@AssertValid
	private UUID tableId = null;
	
	private Table tableCache = null;

	public TableValidationJob() {
		
	}
	
	public TableValidationJob(Table table) {
		setTableId(table != null ? table.getId() : null);
		setTableCache(table);
	}
	
	@Override
	public void run() throws IOException, ValidationException {
		if (getTableId() == null)
			return;
		
		if (getTableCache() == null)
			return;
		
		getTableCache().validate(false, true);
	}

	public UUID getTableId() {
		return tableId;
	}

	public void setTableId(UUID tableId) {
		this.tableId = tableId;
	}

	private Table getTableCache() {
		if (tableCache == null) {
			try {
				tableCache = TableFactory.getTable(getTableId());
			} catch (IOException e) {
				LibsLogger.error(TableValidationJob.class, "Cannot get Table from DB", e);
			}
		}
		return tableCache;
	}

	private void setTableCache(Table tableCache) {
		this.tableCache = tableCache;
	}
}
