package net.beanscode.model.dataset;

public enum TableStatus {
	IMPORTING,
	CLEARING,
	HEALTHY,
	EMPTY
}
