package net.beanscode.model.dataset;

import com.google.gson.annotations.Expose;

import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotNull;

public class DatasetId extends UUID {
	
	public DatasetId() {
		
	}

	public DatasetId(UUID id) {
		super(id != null ? id.getId() : null);
	}
}
