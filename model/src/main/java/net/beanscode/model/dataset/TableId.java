package net.beanscode.model.dataset;

import com.google.gson.annotations.Expose;

import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotNull;

public class TableId extends UUID {
	
	public TableId() {
		
	}

	public TableId(UUID id) {
		super(id != null ? id.getId() : null);
	}
}
