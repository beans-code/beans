package net.beanscode.model.dataset;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import net.beanscode.model.BeansChannels;
import net.beanscode.model.BeansConst;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.weblibs.jobs.Job;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

import com.google.gson.annotations.Expose;

public class DatasetClearJob extends Job {
	
	@Expose
	@NotNull
	@NotEmpty
	@AssertValid
	private List<UUID> tableToRemove = null;

	public DatasetClearJob() {
		super(BeansChannels.CHANNEL_LONG_NORMAL);
	}
	
	@Override
	public void run() throws IOException, ValidationException {
		for (UUID tableId : getTableToRemove()) {
			Table table = TableFactory.getTable(tableId);
			
			if (table != null) {
				table.remove();
				LibsLogger.debug(DatasetClearJob.class, "Table ", table.getId(), " removed");
			}
		}
	}

	public List<UUID> getTableToRemove() {
		if (tableToRemove == null)
			tableToRemove = new ArrayList<UUID>();
		return tableToRemove;
	}

	public void setTableToRemove(List<UUID> tableToRemove) {
		this.tableToRemove = tableToRemove;
	}
	
	public void addToRemove(UUID tableId) {
		getTableToRemove().add(tableId);
	}
}
