package net.beanscode.model.dataset;

import java.io.IOException;

import net.beanscode.model.BeansChannels;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.weblibs.jobs.Job;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

import com.google.gson.annotations.Expose;

public class ReindexTable extends Job {
	
	@Expose
	@NotNull
	@NotEmpty
	private UUID datasetId = null;
	
	@Expose
	@NotNull
	@NotEmpty
	private UUID tableId = null;
	
	public ReindexTable() {
		super(BeansChannels.CHANNEL_INDEX);
	}
	
	public ReindexTable(Table tab) {
		super(BeansChannels.CHANNEL_INDEX);
		setDatasetId(tab.getDatasetId());
		setTableId(tab.getId());
	}
	
	@Override
	public void run() throws IOException, ValidationException {
		Table table = TableFactory.getTable(getTableId());
		
		if (table == null) {
			LibsLogger.debug(ReindexTable.class, "Cannot find table with datasetId " + getDatasetId() + ", tableId " + getTableId(), ". Indexing skipped.");
			return;
		}
		
		table.index();
	}

	public UUID getTableId() {
		return tableId;
	}

	public void setTableId(UUID tableId) {
		this.tableId = tableId;
	}

	public UUID getDatasetId() {
		return datasetId;
	}

	public void setDatasetId(UUID datasetId) {
		this.datasetId = datasetId;
	}
}
