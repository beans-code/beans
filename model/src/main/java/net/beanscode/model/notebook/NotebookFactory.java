package net.beanscode.model.notebook;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.NotImplementedException;

import net.beanscode.model.BeansSearchManager;
import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.db.db.ResultsIter;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.search.SearchResults;
import net.hypki.libs5.search.query.Bracket;
import net.hypki.libs5.search.query.Query;
import net.hypki.libs5.search.query.SortOrder;
import net.hypki.libs5.search.query.Term;
import net.hypki.libs5.search.query.TermRequirement;
import net.hypki.libs5.search.query.WildcardTerm;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.string.RegexUtils;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.weblibs.SearchManager;
import net.hypki.libs5.weblibs.WeblibsConst;

public class NotebookFactory {
	
	public static Iterable<Notebook> iterateNotebooks() {
		return iterateNotebooks(null);
	}
	
	public static Iterable<Notebook> iterateNotebooks(final UUID userId) {
		return new Iterable<Notebook>() {
			@Override
			public Iterator<Notebook> iterator() {
				return new Iterator<Notebook>() {
					private ResultsIter rowsIter = null;
					
					private boolean initiated = false;
					
					private Notebook nextNotebook = null;
					
					@Override
					public Notebook next() {
						if (!initiated)
							hasNext();
						return nextNotebook;
					}
					
					@Override
					public boolean hasNext() {
						if (!initiated) {
							rowsIter = new ResultsIter(WeblibsConst.KEYSPACE, Notebook.COLUMN_FAMILY, 
									new String[]{DbObject.COLUMN_DATA}, null);
							initiated = true;
						}
						
						while (rowsIter.hasNext()) {
							Row row = rowsIter.next();
							
							if (row != null && row.contains(DbObject.COLUMN_DATA)) {
								nextNotebook = JsonUtils.fromJson((String) row.getColumns().get(DbObject.COLUMN_DATA), Notebook.class);
								
								if (userId == null)
									return true;
								else if (nextNotebook.getUserId().equals(userId))
									return true;
							}
						}
						
						return false;
					}
				};
			}
		};
	}

	public static SearchResults<Notebook> searchNotebooks(UUID userId, String keywords, int from, int size) throws IOException {
		// dealing with tags
		ArrayList<String> plusTags = new ArrayList<>();
		ArrayList<String> minusTags = new ArrayList<>();
		if (StringUtilities.notEmpty(keywords)) {
			for (String plusTag : RegexUtils.allGroups("(\\+[\\w]+)", keywords)) {
				plusTags.add(plusTag.substring(1));
				keywords = keywords.replace(plusTag, "");
			}
			for (String minusTag : RegexUtils.allGroups("(\\![\\w]+)", keywords)) {
				minusTags.add(minusTag.substring(1));
				keywords = keywords.replace(minusTag, "");
			}
		}
	
		Query q = Query.parseQuery(keywords);
		Query esQuery = new Query();
		
		for (Term term : q.getTerms()) {
			if (term.getField() == null) {
				if (term instanceof WildcardTerm) {
					WildcardTerm wild = (WildcardTerm) term;
					esQuery.addTerm(new Bracket(term.getRequirement())
										.addTerm(new WildcardTerm("name", wild.getTerm(), TermRequirement.SHOULD))
										.addTerm(new WildcardTerm("id.id", wild.getTerm(), TermRequirement.SHOULD))
//										.addTerm(new WildcardTerm("description", wild.getTerm(), TermRequirement.SHOULD))
							);
				}
			} else {
				term.setField("meta.meta." + term.getField() + ".value");
				esQuery.addTerm(term);
			}
		}
		
		for (String plus : plusTags) {
			// TODO _ character is not good
			esQuery.addTerm(new WildcardTerm("tags.tagsjoined", "_" + plus.toLowerCase() + "_", TermRequirement.MUST));
//			esQuery.addTerm(new WildcardTerm("tagsjoined", plus.toLowerCase(), TermRequirement.MUST));
		}
		for (String minus : minusTags) {
			esQuery.addTerm(new WildcardTerm("tags.tagsjoined", "_" + minus.toLowerCase() + "_", TermRequirement.MUST_NOT));
		}
		
		if (esQuery.getTerms().size() == 0)
			esQuery.addTerm(new WildcardTerm("name", "*", TermRequirement.SHOULD));
		
		if (userId != null)
			BeansSearchManager.addPermissionTerms(esQuery, userId);
		
//			esQuery.setSortBy("creationMs", SortOrder.DESCENDING);
		esQuery.setSortBy("lastEditMs", SortOrder.DESCENDING);
	
		return SearchManager.searchInstance().search(Notebook.class, 
				WeblibsConst.KEYSPACE_LOWERCASE, 
				Notebook.COLUMN_FAMILY.toLowerCase(), 
				esQuery, from, size);
	}

	public static Iterable<Notebook> iterateNotebooks(final UUID userId, final String notebookQuery) {
		return new Iterable<Notebook>() {
			@Override
			public Iterator<Notebook> iterator() {
				return new Iterator<Notebook>() {
					private int from = 0;
					
					private int notebooksIdx = 0;
					
					private List<Notebook> notebooks = null;
					
					@Override
					public void remove() {
						throw new NotImplementedException();
					}
					
					@Override
					public Notebook next() {
						return notebooks.get(notebooksIdx++);
					}
					
					@Override
					public boolean hasNext() {
						try {
							if (notebooks == null || notebooks.size() == notebooksIdx) {
								notebooks = searchNotebooks(userId, notebookQuery, from, BeansSearchManager.PER_PAGE).getObjects();
								notebooksIdx = 0;
								from += notebooks.size();
							}
							return notebooksIdx < notebooks.size();
						} catch (IOException e) {
							LibsLogger.error(BeansSearchManager.class, "Cannot get the next page with results", e);
							return false;
						}
					}
				};
			}
		};
	}

}
