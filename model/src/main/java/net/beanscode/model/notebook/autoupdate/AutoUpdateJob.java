package net.beanscode.model.notebook.autoupdate;

import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang.NotImplementedException;

import com.google.gson.annotations.Expose;

import net.beanscode.model.BeansChannels;
import net.beanscode.model.BeansSettings;
import net.beanscode.model.cass.notification.Notification;
import net.beanscode.model.cass.notification.NotificationType;
import net.beanscode.model.cass.notification.Question;
import net.beanscode.model.notebook.Notebook;
import net.beanscode.model.notebook.NotebookEntry;
import net.beanscode.model.notebook.NotebookEntryFactory;
import net.beanscode.model.notebook.NotebookFactory;
import net.beanscode.model.settings.Clipboard;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.weblibs.WeblibsConst;
import net.hypki.libs5.weblibs.jobs.Job;
import net.hypki.libs5.weblibs.jobs.JobFactory;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

public class AutoUpdateJob extends Job {
	
	public static final String CHANNEL = WeblibsConst.CHANNEL_NORMAL;
	
	private static final long SEC_15 = 15000;
	
	@NotNull
	@NotEmpty
	@AssertValid
	@Expose
	private UUID userId = null;
	
	@Expose
	private Set<UUID> entriesDone = null;
	
	@Expose
	private Set<UUID> entriesConfirmationOK = null;
	
	@Expose
	private Set<UUID> entriesToIgnore = null;
	
	@Expose
	private boolean allConfirmationsOK = false;
	
	@Expose
	private UUID currentlyRunningEntry = null;
	
	
	
	public AutoUpdateJob() {
		super(CHANNEL);
	}
	
	public AutoUpdateJob(UUID userId) {
		super(CHANNEL);
		setUserId(userId);
	}
	
	@Override
	public void run() throws IOException, ValidationException {
		// TODO DEBUG 
		if (true) return;
		
		// check if another auto-update job is running for the user - if true then skip this one
		String currentAutoUpdateId = (String) Clipboard.get(getUserId() + "_" + BeansSettings.SETTING_AUTO_UPDATE_CURRENT_ID);

		if (notEmpty(currentAutoUpdateId) && currentAutoUpdateId.contains("_")) {
			if (!currentAutoUpdateId.equals(getKey())) {
				// make sure that the AutoJob does not exist 
//				if (JobFactory.getJob(currentAutoUpdateId) != null) {
//					LibsLogger.debug(AutoUpdateJob.class, "Auto-update job is already running for the User ", userId, ", consuming AutoUpdateJob with ID ", getId());
					
					return;
//				}
			}
		}
		
		// saving setting what is the current running auto-update job
		Clipboard.set(getUserId() + "_" + BeansSettings.SETTING_AUTO_UPDATE_CURRENT_ID, getKey());
		
		new Notification()
			.setId(getUserId())
			.setDescription("Auto-update job is running...")
			.setNotificationType(NotificationType.INFO)
			.setTitle("Auto-update")
			.setUserId(getUserId())
			.setLocked(true)
			.save();
		
		// check if an entry is still running
		if (getCurrentlyRunningEntry() != null) {
			NotebookEntry ne = NotebookEntryFactory.getNotebookEntry(getCurrentlyRunningEntry());
			if (ne != null && ne.isRunning()) {
				setPostponeMs(SEC_15);
				return;
			}
			setCurrentlyRunningEntry(null);
		}
		
		// for every Notebook
		for (Notebook notebook : NotebookFactory.iterateNotebooks(getUserId())) {
			
			// for every entry in the notebook
			for (NotebookEntry entry : notebook.iterateEntries()) {
				
				// if entry needs to be reload, reload it and wait with this job
				if (entry != null && entry.isReloadNeeded()) {
					
					// detect cycles -- if the cycle is detected skip that entry
					if (getEntriesDone().contains(entry.getId())) {
						LibsLogger.warn(AutoUpdateJob.class, "Cannot auto-update entry " + entry.getId() + " because a cycle was detected");
						continue;
					}
					
					// check what are the auto-update options for the NotebookEntry
					if (entry.getAutoUpdateOptions().getOption() == AutoUpdateOption.IGNORE) {
						continue;
					} if (entry.getAutoUpdateOptions().getOption() == AutoUpdateOption.IGNORE_NOTEBOOK) {
						continue;
					} else if (entry.getAutoUpdateOptions().getOption() == AutoUpdateOption.NO_CONFIRMATION_NEEDED) {
						// do nothing
					} else if (entry.getAutoUpdateOptions().getOption() == AutoUpdateOption.CONFIRMATION_NEEDED) {
						// check if confirmation was already granted
						if (!getEntriesConfirmationOK().contains(entry.getId())) {
							// check if one has to ignore this entry
							if (getEntriesToIgnore().contains(entry.getId())) {
								continue;
							}
							
							new Notification()
//								.setId(getUserId())
								.setDescription("")
								.setNotificationType(NotificationType.QUESTION)
								.setTitle("Auto-update paused")
								.setUserId(getUserId())
								.addMeta("jobData", getData())
								.addMeta("entryId", entry.getId().getId())
								.setLocked(true)
								.addQuestion(new Question()
										.setText("Entry '" + (entry.getName() == null ? entry.getId() : entry.getName()) 
										+ "' from Notebook '" + notebook.getName() + "' asks for a confirmation to reload")
										.setQuestionId("reload")
										.setClazzToCall(AutoUpdateJob.class)
										.setClazzMethodToCall("questionAnswered")
										.setAnswers((new String[] {"Update", "Ignore this entry", "Ignore notebook", "Stop Auto-update"}))
										)
								.save();
							
							return; // this will remove this job. The job will be restarted by the user
						}
					} else {
						LibsLogger.error(AutoUpdateJob.class, "Unimplemented case for auto-update: " + entry.getAutoUpdateOptions().getOption() + ". "
								+ "Assuming " + AutoUpdateOption.NO_CONFIRMATION_NEEDED.name() + "...");
					}
						
					// all clear, one can reload that entry
					getEntriesDone().add(entry.getId());
					// TODO to wcale nie jest zapisywane do nastepnych AutoUpdateJob
					
					entry.reload();
					
					if (entry.isRunning()) {
						setCurrentlyRunningEntry(entry.getId());
						setPostponeMs(SEC_15);
						return;
					}
				}
			}
		}
		
		// clearing to be ready for next auto-update call
		Clipboard.remove(getUserId() + "_" + BeansSettings.SETTING_AUTO_UPDATE_CURRENT_ID);
		
		new Notification()
			.setId(getUserId())
			.setDescription("Auto-update job finished successfully")
			.setNotificationType(NotificationType.OK)
			.setTitle("Auto-update")
			.setUserId(getUserId())
			.save();
		
		LibsLogger.debug(AutoUpdateJob.class, "Auto-updates for user ", getUserId(), " is completed");
	}
	
	@Override
	public void remove() throws ValidationException, IOException {
		super.remove();
		
		// clearing to be ready for next auto-update call
		Clipboard.remove(getUserId() + "_" + BeansSettings.SETTING_AUTO_UPDATE_CURRENT_ID);
	}
	
	private void questionAnswered(Notification notification, Question question, String answer) throws ValidationException ,IOException {
		final String jobMeta = notification.getMeta().get("jobData").getAsString();
		final AutoUpdateJob autoUpdateJob = JsonUtils.fromJson(jobMeta, AutoUpdateJob.class);
		
		autoUpdateJob.setId(UUID.random());
		if (answer.equals("Update")) {
			BeansSettings.setSetting(autoUpdateJob.getUserId(), BeansSettings.SETTING_AUTO_UPDATE_CURRENT_ID, 
					"autoupdate", autoUpdateJob.getKey());
			
			autoUpdateJob.getEntriesConfirmationOK().add(new UUID(notification.getMeta().get("entryId").getAsString()));
			autoUpdateJob.save();
		} else if (answer.equals("Ignore this entry")) {
			BeansSettings.setSetting(autoUpdateJob.getUserId(), BeansSettings.SETTING_AUTO_UPDATE_CURRENT_ID, 
					"autoupdate", autoUpdateJob.getKey());
			
			autoUpdateJob.getEntriesToIgnore().add(new UUID(notification.getMeta().get("entryId").getAsString()));
			autoUpdateJob.save();
		} else if (answer.equals("Ignore notebook")) {
			BeansSettings.setSetting(autoUpdateJob.getUserId(), BeansSettings.SETTING_AUTO_UPDATE_CURRENT_ID, 
					"autoupdate", autoUpdateJob.getKey());
			
			NotebookEntry ne = NotebookEntryFactory.getNotebookEntry(notification.getMeta().getAsString("entryId"));
			if (ne != null) {
				for (NotebookEntry neToIgnore : ne.getNotebook().iterateEntries()) {
					autoUpdateJob.getEntriesToIgnore().add(neToIgnore.getId());
				}
				autoUpdateJob.save();
			}
		} else if (answer.equals("Stop Auto-update")) {
			notification.setLocked(false);
		} else
			throw new NotImplementedException("Unknown answer " + answer);
		
		notification.setArchived(true);
		notification.save();
	};
	
	public UUID getUserId() {
		return userId;
	}

	public AutoUpdateJob setUserId(UUID userId) {
		this.userId = userId;
//		setId(getUserId());
		return this;
	}

	private Set<UUID> getEntriesDone() {
		if (entriesDone == null)
			entriesDone = new HashSet<UUID>();
		return entriesDone;
	}

	private void setEntriesDone(Set<UUID> entriesDone) {
		this.entriesDone = entriesDone;
	}

	public UUID getCurrentlyRunningEntry() {
		return currentlyRunningEntry;
	}

	private AutoUpdateJob setCurrentlyRunningEntry(UUID currentlyRunningEntry) {
		this.currentlyRunningEntry = currentlyRunningEntry;
		return this;
	}

	public Set<UUID> getEntriesConfirmationOK() {
		if (entriesConfirmationOK == null)
			entriesConfirmationOK = new HashSet<>();
		return entriesConfirmationOK;
	}

	private AutoUpdateJob setEntriesConfirmationOK(Set<UUID> entriesConfirmationOK) {
		this.entriesConfirmationOK = entriesConfirmationOK;
		return this;
	}

	public Set<UUID> getEntriesToIgnore() {
		if (entriesToIgnore == null)
			entriesToIgnore = new HashSet<>();
		return entriesToIgnore;
	}

	private AutoUpdateJob setEntriesToIgnore(Set<UUID> entriesToIgnore) {
		this.entriesToIgnore = entriesToIgnore;
		return this;
	}

	public boolean isAllConfirmationsOK() {
		return allConfirmationsOK;
	}

	public AutoUpdateJob setAllConfirmationsOK(boolean allConfirmationsOK) {
		this.allConfirmationsOK = allConfirmationsOK;
		return this;
	}

}
