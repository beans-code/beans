package net.beanscode.model.notebook.autoupdate;

public class AutoUpdateUtils {

	public static AutoUpdateSettings getDefaultAutoUpdateOptions() {
		return new AutoUpdateSettings()
				.setOption(AutoUpdateOption.NO_CONFIRMATION_NEEDED);
	}
}
