package net.beanscode.model.notebook;

import java.io.IOException;

import com.google.gson.annotations.Expose;

import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.weblibs.jobs.Job;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotNull;

public class IndexNotebookEntryJob extends Job {
	
	@Expose
	@NotNull
	@AssertValid
	private UUID notebookEntryId = null;

	public IndexNotebookEntryJob() {
		
	}
	
	public IndexNotebookEntryJob(UUID notebookEntryId) {
		setNotebookEntryId(notebookEntryId);
	}
	
	@Override
	public void run() throws IOException, ValidationException {
		NotebookEntry ne = NotebookEntryFactory.getNotebookEntry(getNotebookEntryId());
		
		if (ne != null)
			ne.index();
	}

	public UUID getNotebookEntryId() {
		return notebookEntryId;
	}

	public void setNotebookEntryId(UUID notebookEntryId) {
		this.notebookEntryId = notebookEntryId;
	}
}
