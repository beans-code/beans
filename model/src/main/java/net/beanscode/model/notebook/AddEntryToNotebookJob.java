package net.beanscode.model.notebook;

import java.io.IOException;

import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.weblibs.jobs.Job;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

import com.google.gson.annotations.Expose;

public class AddEntryToNotebookJob extends Job {
	
	@Expose
	@NotNull
	@NotEmpty
	@AssertValid
	private UUID notebookId = null;
	
	@Expose
	@NotNull
	@NotEmpty
	@AssertValid
	private UUID userId = null;

	@Expose
	@NotNull
	@NotEmpty
	@AssertValid
	private UUID entryId = null;

	public AddEntryToNotebookJob() {
		
	}
	
	public AddEntryToNotebookJob(NotebookEntry ne) {
		setNotebookId(ne.getNotebookId());
		setUserId(ne.getUserId());
		setEntryId(ne.getId());
	}
	
	@Override
	public void run() throws IOException, ValidationException {
		Notebook notebook = Notebook.getNotebook(getNotebookId());
		
		if (notebook == null) {
			LibsLogger.error(AddEntryToNotebookJob.class, "Cannot find notebook ", getNotebookId(), " for user ", getUserId(), ", consuming job anyway");
			return;
		}
		
		if (!notebook.getEntries().contains(getEntryId())) {
			notebook.addEntryId(getEntryId());
			notebook.save();
			LibsLogger.debug(AddEntryToNotebookJob.class, "Entry ", getEntryId(), " added to notebook ", getNotebookId());
		}
	}

	public UUID getNotebookId() {
		return notebookId;
	}

	public void setNotebookId(UUID notebookId) {
		this.notebookId = notebookId;
	}

	public UUID getUserId() {
		return userId;
	}

	public void setUserId(UUID userId) {
		this.userId = userId;
	}

	public UUID getEntryId() {
		return entryId;
	}

	public void setEntryId(UUID entryId) {
		this.entryId = entryId;
	}
}
