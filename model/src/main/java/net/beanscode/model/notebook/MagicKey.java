package net.beanscode.model.notebook;

import java.io.IOException;

import com.google.gson.annotations.Expose;

import net.beanscode.model.BeansObject;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

public class MagicKey extends BeansObject<MagicKey> {
	
	public final static String COLUMN_FAMILY = "magickey";

	@Expose
	@NotNull
	@NotEmpty
	private UUID id = null;
	
	@Expose
	@NotNull
	@NotEmpty
	private UUID beansObjectId = null;
	
	public MagicKey() {
		setId(UUID.random());
	}
	
	public MagicKey(UUID beansObjectId) {
		setId(UUID.random());
		setBeansObjectId(beansObjectId);
	}
	
	@Override
	public String getColumnFamily() {
		return COLUMN_FAMILY;
	}
	
	@Override
	public String getKey() {
		return getBeansObjectId().getId();
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public UUID getBeansObjectId() {
		return beansObjectId;
	}

	public void setBeansObjectId(UUID beansObjectId) {
		this.beansObjectId = beansObjectId;
	}

	@Override
	public void index() throws IOException {
		// do nothing
	}

	@Override
	public void deindex() throws IOException {
		// do nothing
	}
}
