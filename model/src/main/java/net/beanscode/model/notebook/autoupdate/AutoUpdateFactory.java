package net.beanscode.model.notebook.autoupdate;

import java.io.IOException;

import net.beanscode.model.BeansChannels;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.date.DateUtils;
import net.hypki.libs5.utils.date.SimpleDate;
import net.hypki.libs5.weblibs.ScheduleManager;
import net.hypki.libs5.weblibs.jobs.JobFactory;

import org.quartz.JobDetail;
import org.quartz.SchedulerException;

public class AutoUpdateFactory {

	public AutoUpdateJob getAutoUpdateJob(final UUID userId) throws IOException {
		return (AutoUpdateJob) JobFactory.getJobData(AutoUpdateJob.CHANNEL, DateUtils.YEAR_2000, userId);
	}

	public static SimpleDate getNextFireAlarmForAutoUpdates(UUID userId) throws IOException {
		try {
			JobDetail jd = CreateAutoUpdatesJobsForUsersJob.getReloadScheduler(userId);
			return jd != null ? ScheduleManager.getNextFireAlarm(jd) : null;
		} catch (SchedulerException e) {
			LibsLogger.error(AutoUpdateFactory.class, "Cannot find out what is the next fire alarm for auto-updayes for user " + userId, e);
			return null;
		}
	}
}
