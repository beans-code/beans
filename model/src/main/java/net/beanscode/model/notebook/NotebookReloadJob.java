package net.beanscode.model.notebook;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.google.gson.annotations.Expose;

import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.date.DateUtils;
import net.hypki.libs5.weblibs.jobs.Job;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotNull;

public class NotebookReloadJob extends Job {

	@Expose
	@NotNull
	@AssertValid
	private UUID notebookId = null;
	
	private Notebook notebookCache = null;
	
	public NotebookReloadJob() {
		
	}
	
	public NotebookReloadJob(Notebook notebook) {
		setNotebookId(notebook.getId());
		this.notebookCache = notebook;
	}
	
	@Override
	public void run() throws IOException, ValidationException {
		if (getNotebook() == null) {
			LibsLogger.warn(NotebookReloadJob.class, "There is no notebook " + getNotebookId() 
				+ " anymore, consuming job...");
			return;
		}
		
		// going over all entries and reloading the next entry
		if (getNotebook() != null && getNotebook().reloadStep() == ReloadPropagator.BLOCK) {
			LibsLogger.debug(NotebookReloadJob.class, "Notebook ", getNotebookId(), " not yet reloaded, "
					+ "postponing...");
			setPostponeMs(DateUtils.FIFTHTEEN_SEC_MS);
			return;
		}
	}
	
	public Notebook getNotebook() throws IOException {
		if (notebookCache == null)
			notebookCache = Notebook.getNotebook(getNotebookId());
		return notebookCache;
	}

	public UUID getNotebookId() {
		return notebookId;
	}

	public void setNotebookId(UUID notebookId) {
		this.notebookId = notebookId;
	}
}
