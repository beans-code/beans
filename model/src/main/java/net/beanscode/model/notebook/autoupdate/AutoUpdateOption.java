package net.beanscode.model.notebook.autoupdate;

public enum AutoUpdateOption {
	IGNORE,
	IGNORE_NOTEBOOK,
	NO_CONFIRMATION_NEEDED,
	CONFIRMATION_NEEDED;
	
	public static AutoUpdateOption parse(String s) {
		if (s == null)
			return null;
		
		s = s.toLowerCase();
		if (s.equals("ignore"))
			return IGNORE;
		else if (s.contains("notebook"))
			return IGNORE_NOTEBOOK;
		else if (s.contains("without"))
			return NO_CONFIRMATION_NEEDED;
		else if (s.contains("ask"))
			return CONFIRMATION_NEEDED;
		return null;
	}
}
