package net.beanscode.model.notebook;

import static net.hypki.libs5.utils.string.StringUtilities.split;

import java.lang.reflect.Type;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSyntaxException;

import net.beanscode.model.plugins.Plugin;
import net.beanscode.model.plugins.PluginManager;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.string.StringUtilities;

public class NotebookEntrySerializer implements JsonDeserializer<NotebookEntry> {
	
	private static NotebookEntrySerializer notebookEntrySerializer = null;
	
	public static NotebookEntrySerializer getInstance() {
		if (notebookEntrySerializer == null)
			notebookEntrySerializer = new NotebookEntrySerializer();
		return notebookEntrySerializer;
	}

	@Override
	public NotebookEntry deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
			throws JsonParseException {
		try {
			String type = json.getAsJsonObject().get("type").getAsString();
			String typeNameonly = type.substring(type.lastIndexOf('.') + 1);
			
			// changing classes packages to the new ones
			// TODO remove later on
			if (typeNameonly.equalsIgnoreCase("TextEntry"))
				type = "net.beanscode.plugin.text.TextEntry";
			
			else if (typeNameonly.equalsIgnoreCase("GnuplotEntry"))
				type = "net.beanscode.plugin.gnuplot.GnuplotEntry";
			
			else if (typeNameonly.equalsIgnoreCase("PythonEntry"))
				type = "net.beanscode.plugin.python.PythonEntry";
			
			else if (typeNameonly.equalsIgnoreCase("TextEntry"))
				type = "net.beanscode.plugin.text.TextEntry";
			
			else if (typeNameonly.equalsIgnoreCase("AttachmentEntry"))
				type = "net.beanscode.plugin.attachment.AttachmentEntry";
		
			else if (typeNameonly.equalsIgnoreCase("ButcheredPigEntry"))
				type = "net.beanscode.plugin.butcheredpig.ButcheredPigEntry";
			
			else if (typeNameonly.equalsIgnoreCase("PigScript"))
				type = "net.beanscode.plugin.pig.PigScript";
			
			else if (typeNameonly.equalsIgnoreCase("PlotParallelCoordinatesEntry"))
				type = "net.beanscode.plugin.plot.PlotParallelCoordinatesEntry";
			
			else if (typeNameonly.equalsIgnoreCase("PlotHistogramEntry"))
				type = "net.beanscode.plugin.plot.PlotHistogramEntry";
			
			else if (typeNameonly.equalsIgnoreCase("Plot"))
				type = "net.beanscode.plugin.plot.Plot";
			
			try {
				// trying to find the class with full path
				Class<? extends NotebookEntry> clazz = (Class<? extends NotebookEntry>) Class.forName(type);
				return JsonUtils.fromJson(json, clazz);
			} catch (Throwable e) {
				LibsLogger.warn(NotebookEntrySerializer.class, "Cannot find type: " + e.getMessage());
				
				// trying to find the class among plugins
				for (Plugin plugin : PluginManager.iterateAllPlugins()) {
					if (plugin.getNotebookEntries() != null)
						for (Class neClass : plugin.getNotebookEntries()) {
							if (neClass.getSimpleName().equals(typeNameonly)) {
								// now, trying to find with just the class simple name
								return (NotebookEntry) JsonUtils.fromJson(json, neClass);
							}
						}
				}
				
				return null;
			}
		} catch (JsonSyntaxException e) {
			LibsLogger.error(NotebookEntrySerializer.class, "Cannot deserialize " + json, e);
			return null;
		} catch (IllegalStateException e) {
			LibsLogger.error(NotebookEntrySerializer.class, "Cannot deserialize " + json, e);
			return null;
		} catch (Throwable e) {
			LibsLogger.error(NotebookEntrySerializer.class, "Cannot deserialize " + json, e);
			return null;
		}
	}
}
