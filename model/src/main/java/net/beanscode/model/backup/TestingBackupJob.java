package net.beanscode.model.backup;

import java.io.FileReader;
import java.io.IOException;
import java.util.List;

import net.beanscode.model.BeansConst;
import net.beanscode.model.UserUtils;
import net.beanscode.model.notebook.Notebook;
import net.beanscode.model.notebook.NotebookEntry;
import net.beanscode.model.notebook.NotebookEntryProgress;
import net.beanscode.model.utils.JvmUtils;
import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.plot.Figure;
import net.hypki.libs5.plot.Plot;
import net.hypki.libs5.plot.Range;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.date.DateUtils;
import net.hypki.libs5.utils.date.SimpleDate;
import net.hypki.libs5.utils.file.FileExt;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.string.Base64Utils;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.weblibs.jobs.Job;
import net.hypki.libs5.weblibs.user.User;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotNull;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import com.google.gson.annotations.Expose;
import com.google.gson.stream.JsonReader;

import kotlin.NotImplementedError;

public class TestingBackupJob extends Job {
	
	public TestingBackupJob() {
		
	}
	
	@Override
	public void run() throws IOException, ValidationException {
		String inputFile = "/home/ahypki/.beans-software/backups/mocca2_2024-09-09T21:09:34.025+02:00_beansdev3.bac";
		
		if (!new FileExt(inputFile).exists())
			return;
		
		try (JsonReader reader = new JsonReader(new FileReader(inputFile))) {

			// beginning of backup (JsonObject)
            reader.beginObject();

            while (reader.hasNext()) {

            	// table name (key)
            	final String currentTableName = reader.nextName();
            	            	
            	// table rows (JsonArray)
            	reader.beginArray();
            	
            	while (reader.hasNext()) {
            		try {
						// reading rows (JsonObject) from JsonArray
						Row r = JsonUtils.getGson().fromJson(reader, Row.class);
						
						if (!currentTableName.equalsIgnoreCase(NotebookEntry.COLUMN_FAMILY))
							continue;
						
						NotebookEntry ne = JsonUtils.fromJson((String) r.get("data"), NotebookEntry.class);
						
						if (!ne.getClass().getSimpleName().equals("Plot")) {
							if (!ne.getClass().getSimpleName().equals("TextEntry")
									&& !ne.getClass().getSimpleName().equals("PigScript")
									&& !ne.getClass().getSimpleName().equals("GnuplotEntry")
									&& !ne.getClass().getSimpleName().equals("ButcheredPigEntry")
									&& !ne.getClass().getSimpleName().equals("PlotHistogramEntry")
									&& !ne.getClass().getSimpleName().equals("PlotParallelCoordinatesEntry")
									&& !ne.getClass().getSimpleName().equals("AwkEntry"))
								LibsLogger.info(TestingBackupJob.class, ne.getClass().getSimpleName() + " not a plot, skipping");
							continue;
						}
						
						// converting Plot entry to use the class Figure
						Figure fig = new Figure();
						
						BeansConst.toNewPlotFormat(ne, fig);
						
//						LibsLogger.info(TestingBackupJob.class, "NotebookEntry after conversion " + ne.getData());
					} catch (Exception e) {
						LibsLogger.info(TestingBackupJob.class, "Conv failed", e);
					}
            	}
            	
            	// end of table rows
            	reader.endArray();
            }

            reader.endObject();
		}
		
		LibsLogger.info(TestingBackupJob.class, "Finished testing");
	}

}
