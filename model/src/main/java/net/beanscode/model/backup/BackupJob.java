package net.beanscode.model.backup;

import static net.hypki.libs5.utils.utils.AssertUtils.assertTrue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;

import net.beanscode.model.BeansSettings;
import net.beanscode.model.cass.Data;
import net.beanscode.model.cass.notification.Notification;
import net.beanscode.model.cass.notification.NotificationType;
import net.beanscode.model.users.BeansMailJob;
import net.hypki.libs5.db.db.BackupUtils;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.date.SimpleDate;
import net.hypki.libs5.utils.file.FileExt;
import net.hypki.libs5.weblibs.WeblibsConst;
import net.hypki.libs5.weblibs.jobs.Job;
import net.hypki.libs5.weblibs.mail.MailJob;
import net.hypki.libs5.weblibs.settings.Setting;
import net.hypki.libs5.weblibs.settings.SettingFactory;
import net.hypki.libs5.weblibs.user.UserFactory;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotNull;

public class BackupJob extends Job {
	
	@Expose
	@NotNull
	@AssertValid
	private UUID userId = null;

	public BackupJob() {
		
	}
	
	@Override
	public void run() throws IOException, ValidationException {
		final Notification notification = new Notification()
			.setUserId(getUserId())
			.setTitle("Backup")
			;
		
		try {
			notification
				.setDescription("Preparing backup...")
				.setNotificationType(NotificationType.INFO)
				.save();
			
			// backup folder
			FileExt backupFolder = BeansSettings.getBackupFolder();
			assertTrue(backupFolder != null, " Cannot determine backup folder");
			
			// tables to skip
			List<String> skipTables = new ArrayList<>();
			skipTables.add(Data.COLUMN_FAMILY);
			
			// backup file name
			String backupFile = SimpleDate.now().toStringISO() + "_" + WeblibsConst.KEYSPACE + ".bac";
			
			// backup keyspace
			BackupUtils.backupKeyspace(WeblibsConst.KEYSPACE,
					skipTables,
					backupFolder.getAbsolutePath() + "/" + backupFile);
			
			notification
				.setDescription("Backup done successfully")
				.setNotificationType(NotificationType.OK)
				.save();
			
			try {
				new BeansMailJob()
					.setTo(UserFactory.getUser(getUserId()).getEmail())
					.setTitle("[BEANS] Backup successfull")
					.setBody("Backup done for " + WeblibsConst.KEYSPACE + " keyspace")
					.save();
			} catch (Exception e) {
				LibsLogger.error(BackupJob.class, "Cannot send email, but backup is done successfully", e);
			}
		} catch (Exception e) {
			notification
				.setDescription("Error while doing backup")
				.setNotificationType(NotificationType.ERROR)
				.save();
			
			throw e;
		}
	}

	public UUID getUserId() {
		return userId;
	}

	public BackupJob setUserId(UUID userId) {
		this.userId = userId;
		return this;
	}
}
