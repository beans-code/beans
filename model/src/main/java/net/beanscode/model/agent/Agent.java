package net.beanscode.model.agent;

import java.io.File;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import net.hypki.libs5.db.cassandra.CassandraUtils;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.distributed.Lock;
import net.hypki.libs5.distributed.Member;
import net.hypki.libs5.distributed.hazelcast.DistributedHazelcast;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.date.SimpleDate;
import net.hypki.libs5.utils.file.FileExt;
import net.hypki.libs5.utils.file.FileUtils;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.utils.system.Basher;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.PumpStreamHandler;
import org.apache.commons.io.output.ByteArrayOutputStream;

import com.google.gson.annotations.Expose;

public class Agent {

	private static final String SLAVE_MAP_AGENT_STATUS_NAME = "SLAVE_MAP_AGENT_STATUS_NAME";

	private static final int SLEEP_SLAVE_THREAD_MS = 1000;

	private static final int CASSANDRA_CMD_SLEEP_MS = 2000;

	@Expose
	private CassandraStatus cassandraStatus = null;

	@Expose
	private SimpleDate lastHeartbeat = null;

	@Expose
	private String agentId = null;

	@Expose
	private String host = null;

	@Expose
	private AgentStatus status = null;

	private boolean changed = false;

	@Expose
	private String cassandraHome = null;

	// last member cache
	private static String lastMembers = "";
	private static int lastMembersPrint = 0;

	// cassandra cache
	private static SimpleDate cassSleep = SimpleDate.now();
	
	@Expose
	private boolean allowToStartCassandra = true;

	public Agent() {
		setAgentId(UUID.random().getId());
		try {
			setHost(InetAddress.getLocalHost().getHostName());
		} catch (UnknownHostException e) {
			LibsLogger.error(Agent.class, "Cannot get host name", e);
		}
//		setCassandraStatus(CassandraStatus.DOWN);
	}

	// public Agent(String agentId, String host) {
	// setAgentId(agentId);
	// setHost(host);
	// }

	public CassandraStatus getCassandraStatus() {
		return cassandraStatus;
	}

	public String getData() {
		return JsonUtils.objectToString(this);
	}

	public Agent setCassandraStatus(CassandraStatus cassandraStatus) {
		if (getCassandraStatus() != cassandraStatus)
			setChanged(true);
		this.cassandraStatus = cassandraStatus;
		return this;
	}

	public SimpleDate getLastHeartbeat() {
		return lastHeartbeat;
	}

	public Agent setLastHeartbeat(SimpleDate lastHeartbeat) {
		if (this.lastHeartbeat != null && this.lastHeartbeat.getTimeInMillis() != lastHeartbeat.getTimeInMillis())
			setChanged(true);
		this.lastHeartbeat = lastHeartbeat;
		return this;
	}

	public Agent updateHeartbeat() {
		setLastHeartbeat(SimpleDate.now());
		return this;
	}

	public static Agent getAgent(String agentId) {
		Object stCached = DistributedHazelcast.getInstance().getMap(SLAVE_MAP_AGENT_STATUS_NAME).get(agentId);

		return stCached != null ? Agent.parse(stCached) : null;
	}

	private static Agent parse(Object data) {
		return JsonUtils.fromJson((String) data, Agent.class);
	}

	private Agent setChanged(boolean changed) {
		this.changed = changed;
		return this;
	}

	public boolean isChanged() {
		return changed;
	}

	public void save() {
		if (!isDown())
			setStatus(AgentStatus.UP);
		else
			setStatus(AgentStatus.DOWN);

//		if (isChanged()) {
			DistributedHazelcast.getInstance().getMap(SLAVE_MAP_AGENT_STATUS_NAME).put(getAgentId(), getData());
//		}
	}

	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public static Iterable<Agent> iterateAgents() {
		return new Iterable<Agent>() {
			@Override
			public Iterator<Agent> iterator() {
				return new Iterator<Agent>() {
					private Iterator keys = null;
					
					private Iterator getKeys() {
						if (this.keys == null) {
							Set<String> keysSet = new HashSet<>();
							keysSet.addAll(DistributedHazelcast.getInstance().getMap(SLAVE_MAP_AGENT_STATUS_NAME).keySet());
							this.keys = keysSet.iterator();
						}
						return this.keys;
					}

					@Override
					public boolean hasNext() {
						return getKeys().hasNext();
					}

					@Override
					public Agent next() {
						return Agent.getAgent((String) getKeys().next());
					}

				};
			}
		};
	}

	public boolean isDown() {
		SimpleDate now = SimpleDate.now();
		return now.getTimeInMillis() - getLastHeartbeat().getTimeInMillis() > BeansAgent.AGENT_DOWN_MS;
	}

	public boolean isDead() {
		SimpleDate now = SimpleDate.now();
		return now.getTimeInMillis() - getLastHeartbeat().getTimeInMillis() > BeansAgent.AGENT_DEAD_MS;
	}

	public void remove() {
		DistributedHazelcast.getInstance().getMap(SLAVE_MAP_AGENT_STATUS_NAME).remove(getAgentId());
	}

	public AgentStatus getStatus() {
		return status;
	}

	public Agent setStatus(AgentStatus status) {
		if (getStatus() != null && getStatus() != status)
			setChanged(true);
		this.status = status;
		return this;
	}

	public String getHost() {
		return host;
	}

	public String getIP() {
		if (getHost().indexOf(':') >= 0)
			return getHost().substring(0, getHost().indexOf(':'));
		else
			return getHost();
	}

	public Agent setHost(String host) {
		this.host = host;
		return this;
	}

	public String getCassandraHome() {
		return cassandraHome;
	}

	public Agent setCassandraHome(String cassandraHome) {
		if (getCassandraHome() == null || !getCassandraHome().equals(cassandraHome))
			setChanged(true);
		this.cassandraHome = cassandraHome;
		return this;
	}

	public boolean isCassandraHomeDefined() {
		return StringUtilities.notEmpty(getCassandraHome());
	}

	private String getCurrentCommand() {
		final Object cmd = DistributedHazelcast.getInstance().getMap(BeansAgent.CURRENT_COMMAND_MAP_NAME).get(getAgentId());
		return cmd != null ? (String) cmd : "";
	}

	private void setCurrentCommand(String cmd) {
		if (cmd == null) {
			DistributedHazelcast.getInstance().getMap(BeansAgent.CURRENT_COMMAND_MAP_NAME).remove(getAgentId());
		} else {
			DistributedHazelcast.getInstance().getMap(BeansAgent.CURRENT_COMMAND_MAP_NAME).put(getAgentId(), cmd);
		}
		LibsLogger.debug(BeansAgent.class, "Agent ", getAgentId(), " new command is: ", cmd);
	}

	public void run(String[] args) {
		try {
			while (true) {
				String cmd = getCurrentCommand();

				if (StringUtilities.nullOrEmpty(cmd)) {

					// do nothing

				} else if (cmd.equals(BeansAgent.CMD_START_CASSANDRA)) {

					cassandraStart();

				} else if (cmd.equals(BeansAgent.CMD_STOP_CASSANDRA)) {

					cassandraStop();

				} else if (cmd.equals(BeansAgent.CMD_CHECK_CASSANDRA)) {

					cassandraCheck();
					
				} else if (cmd.startsWith(BeansAgent.CMD_SET_CASSANDRA)) {

					String keyValue = cmd.substring(BeansAgent.CMD_SET_CASSANDRA.length());
					cassandraSet(keyValue.split("=")[0], keyValue.split("=")[1]);

				} else if (cmd.equals(BeansAgent.CMD_STOP_AGENT)) {

					// stopping this agent
					LibsLogger.debug(Agent.class, "Agent ", getAgentId(), " stopping...");
					break;

				} else

					LibsLogger.error(BeansAgent.class, "Unrecognized command " + cmd);

				printMembers();

				updateAgentStatus();

				Thread.sleep(SLEEP_SLAVE_THREAD_MS);
			}
		} catch (Exception e) {
			LibsLogger.error(BeansAgent.class, "Beans Agent thread interrupted", e);
		}
	}

	private void cassandraSet(String key, String value) {
		if (!isCassandraHomeDefined()) {
			LibsLogger.debug(Agent.class, "For agent ", getAgentId(), " there is no Cassandra Hom set, skipping setting value");
			return;
		}
		
		CassandraUtils.setYaml(new FileExt(getCassandraHome()).getAbsolutePath() + "/conf/cassandra.yaml", key, value);
		LibsLogger.debug(Agent.class, "Yaml for cassandra changed to ", key, "=", value);
		
		setCurrentCommand(null);
		save();
	}

	private void printMembers() {
		StringBuilder members = new StringBuilder();

		for (Member m : DistributedHazelcast.getInstance().getMembers()) {
			members.append(m.toString());
			members.append("; ");
		}

		if (!lastMembers.equals(members.toString()) || (++lastMembersPrint % 10) == 0) {
			lastMembers = members.toString();
			LibsLogger.debug(BeansAgent.class, "Members: ", lastMembers);
		}
	}

	private void updateAgentStatus() {
		updateHeartbeat();
		
		updateCassandraStatus();

		save();

		printAgentStatus();
	}

	private Agent updateCassandraStatus() {
		// operations on Cass cannot be executed too often
		if (!isCassandraOperationAllowed())
			return this;
		cassSleep.addMillisecond(CASSANDRA_CMD_SLEEP_MS);
		
		if (CassandraUtils.isCassandraRunning())
			setCassandraStatus(CassandraStatus.UP);
		else
			setCassandraStatus(CassandraStatus.DOWN);
		
		return this;
	}

	private void cassandraStart() {
		// operations on Cass cannot be executed too often
		if (!isCassandraOperationAllowed())
			return;
		if (!isAllowToStartCassandra()) {
			setCurrentCommand(null);
			save();
			return;
		}
		cassSleep.addMillisecond(CASSANDRA_CMD_SLEEP_MS);
		
		boolean lockedHere = false;
		Lock lockCassStart = null;
		try {
			lockCassStart = DistributedHazelcast.getInstance().getLock("ONE_CASS_START_PER_HOST");
		
			if (lockCassStart.tryLock(1000)) {
				lockedHere = true;
				
				// if cassandra is running do nothing
				if (CassandraUtils.isCassandraRunning()) {
					// do nothing
					LibsLogger.debug(BeansAgent.class, "Cassandra is already running, do nothing");
					if (getCurrentCommand() != null && getCurrentCommand().equals(BeansAgent.CMD_START_CASSANDRA)) {
						setCurrentCommand(null);
						save();
					}
					return;
				}
		
				LibsLogger.debug(BeansAgent.class, "Trying to start Cassandra");
				
				// trying to start cassandra
				for (String cassandraHome : BeansAgent.getCassandraHomes()) {
					
					// check if the cassandra home is already occupied by other agent
					boolean cassandraHomeOccupied = false;
					for (Agent agent : Agent.iterateAgents()) {
						if (agent.getCassandraHome() != null 
								&& agent.getCassandraHome().equals(cassandraHome)
								&& !agent.getAgentId().equals(getAgentId())) {
							cassandraHomeOccupied = true;
							break;
						}
					}
					if (cassandraHomeOccupied)
						continue;
					
					// get lock on cassandra home and start cassandra
					Lock lockCassHome = null;
					try {
						lockCassHome = DistributedHazelcast.getInstance().getLock(cassandraHome);
						if (lockCassHome.tryLock(500)) {
							LibsLogger.debug(BeansAgent.class, "Agent ", agentId, " got lock on CassandraHome ", cassandraHome);
							
							setCassandraHome(cassandraHome);
							save();
							
							String allHosts = "";
							for (Agent agent : iterateAgents()) {
								if (allHosts.length() > 0)
									allHosts += ",";
								
								if (agent.getCurrentCommand() != null
										&& (agent.getCurrentCommand().equals(BeansAgent.CMD_CHECK_CASSANDRA) || agent.getCurrentCommand().equals(BeansAgent.CMD_START_CASSANDRA)))
										allHosts += agent.getHost();
							}
							
							// setting rpc_address, seeds and listen_address
							File yamlFile = new FileExt(cassandraHome + "/conf/cassandra.yaml");
							String yaml = FileUtils.readString(yamlFile);
							yaml = yaml.replaceFirst("-[\\s]*seeds:[\\s]*\"[\\w\\,\\.\\-]*\"", "- seeds: \"" + allHosts + "\"");
							yaml = yaml.replaceFirst("rpc_address[\\s]*:[\\s]*[\\w\\,\\.\\-]+", "rpc_address: " + getHost());
							yaml = yaml.replaceFirst("listen_address[\\s]*:[\\s]*[\\w\\,\\.\\-]+", "listen_address: " + getHost());
							
							FileUtils.saveToFile(yaml, yamlFile, true);
							
//							CassandraUtils.startCassandra(cassandraHome);
							
							String cmd = "cd " + cassandraHome + " && env _JAVA_OPTIONS=-Xmx2999m ./bin/cassandra > cassandra.log 2>&1";
							LibsLogger.debug(CassandraUtils.class, "Running command: ", cmd);
							LibsLogger.debug(CassandraUtils.class, Basher.executeCommand(cmd, false));
							
							
//							String line = "./bin/cassandra";
//							CommandLine commandLine = CommandLine.parse(line);
//							
//						    ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
//							PumpStreamHandler streamHandler = new PumpStreamHandler(outputStream);
//							
//							DefaultExecutor executor = new DefaultExecutor();
//							executor.setWorkingDirectory(new FileExt(cassandraHome));
//						    executor.setStreamHandler(streamHandler);
//							executor.setExitValue(1);
//							int exitValue = executor.execute(commandLine);
//							LibsLogger.debug(Agent.class, "Cass start command ext value ", exitValue);
//							LibsLogger.debug(Agent.class, "Output ", outputStream.toString());
							
							Thread.sleep(CASSANDRA_CMD_SLEEP_MS);
							
							break;
						}
					} catch (Exception e) {
						LibsLogger.error(BeansAgent.class, "Cannot start cassandra ", e);
					} finally {
						if (lockCassHome != null)
							lockCassHome.unlock();
					}
				}
			}
		} finally {
			if (lockCassStart != null && lockedHere)
				lockCassStart.unlock();
		}
	}

	private void cassandraStop() {
		// operations on Cass cannot be executed too often
		if (!isCassandraOperationAllowed())
			return;
		cassSleep.addMillisecond(CASSANDRA_CMD_SLEEP_MS);

		if (!CassandraUtils.isCassandraRunning()) {
			// do nothing
			LibsLogger.debug(BeansAgent.class, "Cassandra is not running, do nothing");
			if (getCurrentCommand() != null && getCurrentCommand().equals(BeansAgent.CMD_STOP_CASSANDRA)) {
				setCassandraStatus(CassandraStatus.DOWN);
				setCurrentCommand(null);
				save();
			}
			return;
		}

		LibsLogger.debug(BeansAgent.class, "Trying to stop Cassandra");
		CassandraUtils.stopCassandra();

		setCassandraStatus(CassandraStatus.DOWN);
		setCassandraHome(null);
		save();
	}
	
	public boolean isCassandraOperationAllowed() {
		if ((SimpleDate.now().getTimeInMillis() - cassSleep.getTimeInMillis()) < CASSANDRA_CMD_SLEEP_MS) {
			return false;
		}
		return true;
	}

	private void cassandraCheck() {
		// operations on Cass cannot be executed too often
		if (!isCassandraOperationAllowed())
			return;
		cassSleep.addMillisecond(CASSANDRA_CMD_SLEEP_MS);

		// if cassandra is running do nothing
		if (CassandraUtils.isCassandraRunning()) {
			// do nothing
			LibsLogger.debug(BeansAgent.class, "Cassandra is already running, do nothing");
			if (getCassandraStatus() == null || getCassandraStatus() != CassandraStatus.UP) {
				setCassandraStatus(CassandraStatus.UP);
				save();
			}
			return;
		}

		LibsLogger.debug(BeansAgent.class, "Trying to check if Cassandra is running");
		
		setCassandraHome(null);
		save();

		cassandraStart();
	}

	private void printAgentStatus() {
		if ((lastMembersPrint % 5) != 0)
			return;

		LibsLogger.debug(BeansAgent.class, String.format("-------------------------"));
		LibsLogger.debug(BeansAgent.class, String.format("%10s %10s %20s  %s", "CASSANDRA", "STATUS", "CURRENT CMD", "ID"));
		for (Agent agent : Agent.iterateAgents()) {
			if (agent == null) {
				LibsLogger.error(Agent.class, "Agent is null, something is wrong");
				continue;
			}
			
			if (agent.isDead()) {
				agent.remove();
				continue;
			}

			if (agent.isDown()) {
				agent
					.setStatus(AgentStatus.DOWN)
					.save();
			}
			
			if (agent.getCassandraStatus() == null) {
				agent
					.updateCassandraStatus()
					.save();
			}

			LibsLogger.debug(BeansAgent.class, String.format("%10s %10s %20s  %s(%s%s%s)", 
					agent.getCassandraStatus() != null ? agent.getCassandraStatus().toString() : "", 
					agent.getStatus().toString(), 
					BeansAgent.getCurrentCommand(agent.getAgentId()),
					agent.getAgentId(), agent.getAgentId().equals(agentId) ? "this " : "", agent.getHost(),
					(agent.getCassandraHome() != null ? " " + agent.getCassandraHome() : "")));
		}
	}

	public boolean isAllowToStartCassandra() {
		return allowToStartCassandra;
	}

	public Agent setAllowToStartCassandra(boolean allowToStartCassandra) {
		this.allowToStartCassandra = allowToStartCassandra;
		return this;
	}
}
