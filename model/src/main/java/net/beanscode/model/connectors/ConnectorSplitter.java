package net.beanscode.model.connectors;

import static java.lang.String.valueOf;
import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;
import static net.hypki.libs5.utils.utils.NumberUtils.toInt;

import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.NotImplementedException;

import net.beanscode.model.BeansSettings;
import net.beanscode.model.dataset.Dataset;
import net.beanscode.model.dataset.Table;
import net.beanscode.model.plugins.ColumnDef;
import net.beanscode.model.plugins.ColumnDefList;
import net.beanscode.model.plugins.Connector;
import net.beanscode.model.plugins.ConnectorInitParams;
import net.hypki.libs5.db.db.Results;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.weblibs.C3Where;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.search.query.Query;
import net.hypki.libs5.utils.string.Meta;
import net.hypki.libs5.utils.string.RegexUtils;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

import com.google.gson.annotations.Expose;

public class ConnectorSplitter implements Connector, Serializable, Iterable<Row> {

	@Expose
	@NotNull
	@NotEmpty
	private ConnectorInitParams connectorInitParams = null;
	
	private ColumnDefList defsCache = null;
	
	private Map<String, Connector> nameToConnector = new HashMap<>();
	
	private Dataset datasetCache = null;
	
	public ConnectorSplitter() {
		
	}
	
	public ConnectorSplitter(String pattern, Dataset ds) {
		setPattern(pattern);
		this.datasetCache = ds;
	}

	public String getPattern() {
		return getInitParams().getMeta().get("PATTERN").getAsString();
	}

	public void setPattern(String pattern) {
		getInitParams().getMeta().add(new Meta("PATTERN", pattern));
	}
	
	@Override
	public void validate() throws IOException {
		// do nothing
	}

	@Override
	public Iterator<Row> iterator() {
		throw new NotImplementedException("Reading is not implemented for " + getClass().getSimpleName());
	}

	@Override
	public String getName() {
		return getClass().getSimpleName();
	}

	@Override
	public ConnectorInitParams getInitParams() {
		if (connectorInitParams == null)
			connectorInitParams = new ConnectorInitParams();
		return connectorInitParams;
	}

	@Override
	public Connector setInitParams(ConnectorInitParams initParams) {
		this.connectorInitParams = initParams;
		return this;
	}

//	@Override
//	public List<Row> readRows(String... pk) throws IOException {
//		throw new NotImplementedException("Reading is not implemented for " + getClass().getSimpleName());
//	}

	@Override
	public void write(Row row) throws IOException {
		String name = buildName(row);
		
		Connector conn = nameToConnector.get(name);
		
		if (conn == null) {
			try {
				Table table = new Table(datasetCache, name);
				
				for (Meta meta : getInitParams().getMeta()) {
					table.getMeta().add(meta);
				}
								
				conn = BeansSettings.getDefaultConnector(table.getId(), UUID.random().getId());
				conn.setColumnDefs(defsCache);
				
				table.getConnectorList().addConnector(conn);
				
				table.save();
			} catch (Exception e) {
				throw new net.hypki.libs5.utils.utils.ValidationException("Cannot create Table for Connector", e);
			}
			
			nameToConnector.put(name, conn);
		}
		
		conn.write(row);
	}
	
	@Override
	public String getVersion() {
		return getClass().getSimpleName() + ", ver. 1.0";
	}

	private String buildName(Row row) {
		String gr = null;
		String name = getPattern();
		while ((gr = RegexUtils.firstGroup("(\\@\\w[\\w\\d]*)", name)) != null) {
			String val = row.getAsString(gr.substring(1));
			if (val != null)
				name = name.replace(gr, val);
			else
				name = name.replace(gr, "");
		}
		return name;
	}

	@Override
	public void close() throws IOException {
		for (Connector conn : nameToConnector.values()) {
			// check if all connectors have set column defs
//			if (conn.getColumnDefs() == null)
//				conn.setColumnDefs(defsCache);
			
			// close connector
			conn.close();
		}
	}

	@Override
	public void clear() throws IOException {
		// do nothing
	}

	@Override
	public void setColumnDefs(ColumnDefList defs) {
		this.defsCache = defs;
	}

	@Override
	public ColumnDefList getColumnDefs() {
		return this.defsCache;
	}
	
	@Override
	public Iterable<Row> iterateRows(Query filter, int splitNr, int splitCount) {
		// TODO implement it
		if (splitNr == 0)
			return iterateRows(filter);
		else
			return new Iterable<Row>() {
				
				@Override
				public Iterator<Row> iterator() {
					return new Iterator<Row>() {
						
						@Override
						public Row next() {
							return null;
						}
						
						@Override
						public boolean hasNext() {
							return false;
						}
					};
				}
			};
	}
	
	@Override
	public Iterable<Row> iterateRows(Query filter) {
		return new Iterable<Row>() {
			
			@Override
			public Iterator<Row> iterator() {
				return new Iterator<Row>() {
					
					private Iterator<Row> allRowsIter = iterateRows(filter).iterator();
					private Row row = null;
					
					@Override
					public Row next() {
						return row;
					}
					
					@Override
					public boolean hasNext() {
						while (allRowsIter.hasNext()) {
							row = allRowsIter.next();
							
							if (row == null)
								return false;
							else if (filter.isFulfilled(row))
								return true;
						}
						
						return row != null;
					}
				};
			}
		};
	}
	
	@Override
	public Results iterateRows(Query filter, String state, int size) {
		int from = 0;
		if (notEmpty(state))
			from = toInt(state);
		
		int i = 0;
		Results res = new Results();
		for (Row row : this.iterateRows(filter)) {
			if (i >= from)
				res.addRow(row);
			
			i++;
			
			if (res.size() == size)
				break;
		}
		res.setNextPage(valueOf(i));
		return res;
	}
	
	@Override
	public long getAproxBytesSize() throws IOException {
		return 0; // this is splitter
	}
}
