package net.beanscode.model.connectors;

import static net.hypki.libs5.utils.utils.AssertUtils.assertTrue;

import java.io.File;
import java.io.Flushable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import ncsa.hdf.hdf5lib.exceptions.HDF5FileNotFoundException;
import net.beanscode.model.BeansSettings;
import net.beanscode.model.dataset.Table;
import net.beanscode.model.plugins.ColumnDef;
import net.beanscode.model.plugins.ColumnDefList;
import net.beanscode.model.plugins.Connector;
import net.beanscode.model.plugins.ConnectorInitParams;
import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.db.db.Results;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.schema.ColumnType;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.search.query.Query;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.file.FileExt;
import net.hypki.libs5.utils.file.FileUtils;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.system.Basher2;
import net.hypki.libs5.utils.utils.ValidationException;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotNull;

import org.apache.commons.lang.NotImplementedException;

import ch.systemsx.cisd.hdf5.HDF5Factory;
import ch.systemsx.cisd.hdf5.HDF5FloatStorageFeatures;
import ch.systemsx.cisd.hdf5.HDF5FloatStorageFeatures.HDF5FloatStorageFeatureBuilder;
import ch.systemsx.cisd.hdf5.HDF5IntStorageFeatures;
import ch.systemsx.cisd.hdf5.IHDF5Reader;
import ch.systemsx.cisd.hdf5.IHDF5Writer;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.annotations.Expose;

public class H5Connector implements Connector, AutoCloseable, Flushable {
	
	public 			final String 	META_PATH = "path";
	private static 	final int 		H5_STRING_MAX_LENGTH = 100;
	
	@Expose
	private Integer cacheSize = null;
	
	@Expose
	@NotNull
	@AssertValid
	private ConnectorInitParams connectorInitParams = null;
	
	@Expose
	@NotNull
	@AssertValid
	private ColumnDefList columnDefs = null;
	
	private IHDF5Writer rowsWriter = null;
	private HashMap<Integer, Cache> cache = null;
	private int rowCount = 0;
	private long rowCountAll = 0;
	private long blockId = 0;
	
	private HashMap<String, Integer> nameToIndex = new HashMap<String, Integer>();
	
	private UUID thisInstance = UUID.random();
	
	private Boolean isH5FileExist = null;
	
//	static {
//		Signal.handle(new Signal("SEGV"), signal -> {
//            System.out.println(signal.getName() + " (" + signal.getNumber() + ")");
//        });
//	}
	
	private Boolean containsTbid = null;
	
	public H5Connector() {
		
	}
	
	public H5Connector(UUID tableId, File h5Path) {
		ConnectorInitParams init = new ConnectorInitParams();
		
		init.setTableId(tableId);
		init.getMeta().add(META_PATH, h5Path.getAbsolutePath());
		
		setInitParams(init);
	}
	
	@Override
	public String toString() {
		return String.format("%s %s %s", H5Connector.class.getSimpleName(), 
				getInitParams() != null ? getInitParams().getTableId() : "", 
				getH5Path());
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof H5Connector) {
			H5Connector h5Conn = (H5Connector) obj;
			if (getH5Path() != null && h5Conn.getH5Path() != null)
				return getH5Path().getAbsolutePath().equals(h5Conn.getH5Path().getAbsolutePath());
		}
		return super.equals(obj);
	}

	@Override
	public String getName() {
		return "HDF5";
	}
	
	@Override
	public void flush() throws IOException {
		saveCache();
	}

	@Override
	public ConnectorInitParams getInitParams() {
		return connectorInitParams;
	}

	@Override
	public Connector setInitParams(ConnectorInitParams initParams) {
		this.connectorInitParams = initParams;
		
		try {
			assertTrue(connectorInitParams != null, "Init params cannot be null or empty");
			assertTrue(connectorInitParams.getTableId() != null, "Init params TableId cannot be null or empty");
			
			if (connectorInitParams.getMeta().get(META_PATH) == null) {
				assertTrue(connectorInitParams.getMeta().get("uniTableId") != null, 
						"Init params uniTableId cannot be null or empty");
				
				String tableId = connectorInitParams.getTableId().getId();
				String uniTableId = connectorInitParams.getMeta().get("uniTableId").getAsString();
				
				FileExt h5Folder = new FileExt(BeansSettings.getConnectorDefaultFolder().getAbsolutePath()
						+ "/" + tableId.substring(0, 2)
						+ "/" + tableId.substring(0, 4));
				h5Folder.mkdirs();
				
				FileExt h5Path = new FileExt(h5Folder.getAbsolutePath()
						+ "/" + tableId + "-" + uniTableId + ".h5");
				
				this.connectorInitParams.getMeta().add(META_PATH, h5Path.getAbsolutePath());
				
				assertTrue(connectorInitParams.getMeta().get(META_PATH) != null, "Init params " + META_PATH + 
						" cannot be null or empty");
			}
		} catch (ValidationException | IOException e) {
			LibsLogger.error(H5Connector.class, "Cannot initialize", e);
		}
		
		return this;
	}
	
	@Override
	public Iterable<Row> iterateRows(Query filter, int splitNr, int splitCount) {
		// TODO implement it
		if (splitNr == 0)
			return iterateRows(filter);
		else
			return new Iterable<Row>() {
				
				@Override
				public Iterator<Row> iterator() {
					return new Iterator<Row>() {
						
						@Override
						public Row next() {
							return null;
						}
						
						@Override
						public boolean hasNext() {
							return false;
						}
					};
				}
			};
	}

	@Override
	public Iterable<Row> iterateRows(Query filter) {
		return new Iterable<Row>() {
			@Override
			public Iterator<Row> iterator() {
				return new Iterator<Row>() {
					private IHDF5Reader reader = null;
					private long maxRows = 0;
					private long rowCounter = 0;
					private int blockCurrentIdx = 0;
					private int blockLength = 0;
					private int []blockYesNo = null;
					private int blockNumber = 0;
					
					@Override
					public Row next() {
						if (reader == null) {
							if (!hasNext()) {
								if (reader != null) {
									logStatus("Closing reader in iter");
									reader.close();
								}
								
								return null;
							}
						}
						
						if (blockCurrentIdx >= blockLength)
							if (!hasNext()) {
								if (reader != null) {
									logStatus("Closing reader in iter");
									reader.close();
								}
								
								return null;
							}
												
						rowCounter++;
						Row row = new Row(rowCounter);
						
						for (int i = 0; i < getColumnDefs().size(); i++) {
							ColumnDef columnDef = getColumnDefs().get(i);
							
//							try {
								if (columnDef.getType() == ColumnType.DOUBLE) {
									row.addColumn(columnDef.getName(), cache.get(i).doubles[blockCurrentIdx]);
								} else if (columnDef.getType() == ColumnType.INTEGER) {
									row.addColumn(columnDef.getName(), cache.get(i).integers[blockCurrentIdx]);
								} else if (columnDef.getType() == ColumnType.LONG) {
									row.addColumn(columnDef.getName(), cache.get(i).longes[blockCurrentIdx]);
								} else if (columnDef.getType() == ColumnType.STRING) {
									row.addColumn(columnDef.getName(), cache.get(i).strings[blockCurrentIdx]);
								} else
									throw new NotImplementedException();
//							} catch (Exception e) {
//								// TODO Auto-generated catch block
//								e.printStackTrace();
//							}
						}
						
//						if (!row.contains(Table.COLUMN_TBID))
							row.addColumn(Table.COLUMN_TBID, getInitParams().getTableId().getId());
						
						blockCurrentIdx++;
						
						return row;
					}
					
					@Override
					public boolean hasNext() {
						if (reader == null) {
							try {
								if (filter != null)
									blockYesNo = new int[getCacheSize()];
								
								if (getH5Path().exists()) {
									reader = HDF5Factory.openForReading(getH5Path());
									
									maxRows = getNumberOfRows();
									
									readNextBlock();
								} else {
									// there is no file
									LibsLogger.info(H5Connector.class, "There is no file " + getH5Path() + ", skipping H5Connector..");
									maxRows = 0;
									return false;
								}
							} catch (Throwable e) {
								LibsLogger.error(H5Connector.class, "Cannot open " + getH5Path() + " for reading", e);
								
								if (reader != null) {
									logStatus("Closing reader in iter");
									reader.close();
								}
								
								return false;
							}
						}
						
						if (rowCounter >= maxRows) {
							if (reader != null) {
								logStatus("Closing reader in iter");
								reader.close();
							}
							
							return false;
						}

						if (blockCurrentIdx < blockLength) {
							if (filter != null) {
								for (; blockCurrentIdx < blockLength; blockCurrentIdx++) {
									if (blockYesNo[blockCurrentIdx] == 1)
										return true;
								}
							} else 
								return true;
						}
						
						readNextBlock();
						
						if (blockCurrentIdx < blockLength) {
							if (filter != null) {
								for (; blockCurrentIdx < blockLength; blockCurrentIdx++) {
									if (blockYesNo[blockCurrentIdx] == 1)
										return true;
								}
							} else 
								return true;
						}
						
						if (reader != null) {
							logStatus("Closing reader in iter");
							reader.close();
						}
						
						return false;
					}
					
					private void readNextBlock() {
						try {
							LibsLogger.debug(H5Connector.class, "readNextBlock ", getH5Path());
							
							if (blockNumber * getCacheSize() >= maxRows) {
								return;
							}
							
							while (true) {
								cache = new HashMap<Integer, Cache>();
								
								blockCurrentIdx = 0;
								
								if (filter != null) {
									
									blockCurrentIdx = -1;
									
									// first getting only relevant columns
									for (int i = 0; i < getColumnDefs().size(); i++) {
										ColumnDef columnDef = getColumnDefs().get(i);
										if (filter != null && filter.containsField(columnDef.getName()) == false)
											continue;
										
										String colPath = "/" + columnDef.getName();
						
										Cache c = new Cache(getCacheSize());
										if (columnDef.getType() == ColumnType.DOUBLE) {
											c.type = ColumnType.DOUBLE;
											c.doubles = reader.float64().readArrayBlock(colPath, getCacheSize(), blockNumber);
											blockLength = c.doubles.length;							
										} else if (columnDef.getType() == ColumnType.INTEGER) {
											c.type = ColumnType.INTEGER;
											c.integers = reader.int32().readArrayBlock(colPath, getCacheSize(), blockNumber);
											blockLength = c.integers.length;
										} else if (columnDef.getType() == ColumnType.LONG) {
											c.type = ColumnType.LONG;
											c.longes = reader.int64().readArrayBlock(colPath, getCacheSize(), blockNumber);
											blockLength = c.longes.length;
										} else if (columnDef.getType() == ColumnType.STRING) {
											if (columnDef.getName().equals(Table.COLUMN_TBID) && containsTbid == false) {
												c.type = ColumnType.STRING;
												c.strings = new String[getCacheSize()];
												for (int j = 0; j < getCacheSize(); j++) {
													c.strings[j] = getInitParams().getTableId().getId();
												}
											} else {
												c.type = ColumnType.STRING;
												c.strings = reader.string().readArrayBlock(colPath, getCacheSize(), blockNumber);
												blockLength = c.strings.length;
											}
										} else
											throw new NotImplementedException();
											
										cache.put(i, c);
									}
									
									if ((blockNumber + 1) * getCacheSize() > maxRows)
										blockLength = (int) (maxRows % getCacheSize());
									
									// searching for rows which fulfill the filter
									Row row = new Row(0);
									List<String> fieldNames = filter.getFieldNames();
									for (int i = 0; i < blockLength; i++) {
										
										boolean include = true;
	//									row.clear();
										
										for (String fieldName : fieldNames) {
											int columnIdx = getColumnIndex(fieldName);
											Cache c = cache.get(columnIdx);
											ColumnDef moccaColumn = getColumnDefs().get(columnIdx);// getColumnByName(fieldName);
											
											Object v = null;
											if (moccaColumn.getType() == ColumnType.DOUBLE)
												v = c.doubles[i];
											else if (moccaColumn.getType() == ColumnType.INTEGER)
												v = c.integers[i];
											else if (moccaColumn.getType() == ColumnType.LONG)
												v = c.longes[i];
											else if (moccaColumn.getType() == ColumnType.STRING)
												v = c.strings[i];
											else
												throw new NotImplementedException();
											
											if (v == null) {
												include = false;
												break;
											}
											
											row.addColumn(fieldName, v);
										}
										
										if (include)
											include = filter.isFulfilled(row);
										
										blockYesNo[i] = include ? 1 : 0;
										
										if (include && blockCurrentIdx == -1)
											blockCurrentIdx = i;
									}
									
									// if there is at least one row in the current block which fullfill the filter then one 
									// has to read all other columns
									if (blockCurrentIdx >= 0) {
										for (int i = 0; i < getColumnDefs().size(); i++) {
											ColumnDef columnDef = getColumnDefs().get(i);
											if (filter != null && filter.containsField(columnDef.getName()) == false) {							
												String colPath = "/" + columnDef.getName();
								
												Cache c = new Cache(getCacheSize());
												if (columnDef.getType() == ColumnType.DOUBLE) {
													c.type = ColumnType.DOUBLE;
													c.doubles = reader.float64().readArrayBlock(colPath, getCacheSize(), blockNumber);
													blockLength = c.doubles.length;							
												} else if (columnDef.getType() == ColumnType.INTEGER) {
													c.type = ColumnType.INTEGER;
													c.integers = reader.int32().readArrayBlock(colPath, getCacheSize(), blockNumber);
													blockLength = c.integers.length;
												} else if (columnDef.getType() == ColumnType.LONG) {
													c.type = ColumnType.LONG;
													c.longes = reader.int64().readArrayBlock(colPath, getCacheSize(), blockNumber);
													blockLength = c.longes.length;
												} else if (columnDef.getType() == ColumnType.STRING) {
													if (columnDef.getName().equals(Table.COLUMN_TBID) && containsTbid == false) {
														c.type = ColumnType.STRING;
														c.strings = new String[getCacheSize()];
														for (int j = 0; j < getCacheSize(); j++) {
															c.strings[j] = getInitParams().getTableId().getId();
														}
													} else {
														c.type = ColumnType.STRING;
														c.strings = reader.string().readArrayBlock(colPath, getCacheSize(), blockNumber);
														blockLength = c.strings.length;
													}
												} else
													throw new NotImplementedException();
													
												cache.put(i, c);
											}
										}
									} else {
										// there is actually no rows fullfilling the filter, moving with blockCurrentIdx to the end 
										blockCurrentIdx = blockLength;
									}
									
								} else {
									for (int i = 0; i < getColumnDefs().size(); i++) {
										ColumnDef columnDef = getColumnDefs().get(i);
										String colPath = "/" + columnDef.getName();
						//				HDF5DataSetInformation info = reader.object().getDataSetInformation(colPath);
						
										Cache c = new Cache(getCacheSize());
										if (columnDef.getType() == ColumnType.DOUBLE) {
											c.type = ColumnType.DOUBLE;
											c.doubles = reader.float64().readArrayBlock(colPath, getCacheSize(), blockNumber);
											blockLength = c.doubles.length;
										} else if (columnDef.getType() == ColumnType.INTEGER) {
											c.type = ColumnType.INTEGER;
											c.integers = reader.int32().readArrayBlock(colPath, getCacheSize(), blockNumber);
											blockLength = c.integers.length;
										} else if (columnDef.getType() == ColumnType.LONG) {
											c.type = ColumnType.LONG;
											c.longes = reader.int64().readArrayBlock(colPath, getCacheSize(), blockNumber);
											blockLength = c.longes.length;
										} else if (columnDef.getType() == ColumnType.STRING) {
//											boolean isDataset = reader.object().isDataSet(colPath);
//											boolean exists = reader.object().exists(colPath);
//											long numElements = reader.object().getNumberOfElements(colPath);
//											long dims = reader.object().getSize(colPath);
//											long [] dim2 = reader.object().getDimensions(colPath);
//											long [] spaceDim = reader.object().getSpaceDimensions(colPath);
//											int [] arrDims = reader.object().getArrayDimensions(colPath);
//											HDF5ObjectInformation objInfo = reader.object().getObjectInformation(colPath);
//											HDF5DataSetInformation dsInfo = reader.object().getDataSetInformation(colPath);
//											String blas = reader.string().readRaw(colPath);
											c.type = ColumnType.STRING;
//											if (dims <= 0) {
//												c.strings = new String[getCacheSize()];
//												for (int j = 0; j < getCacheSize(); j++) {
//													c.strings[j] = "";
//												}
//											} else {
											if (columnDef.getName().equals(Table.COLUMN_TBID) && containsTbid == false) {
												c.type = ColumnType.STRING;
												c.strings = new String[getCacheSize()];
												for (int j = 0; j < getCacheSize(); j++) {
													c.strings[j] = getInitParams().getTableId().getId();
												}
											} else {
												c.strings = reader.string().readArrayBlock(colPath, getCacheSize(), blockNumber);
											}
//											}
//											String[] bb = reader.string().readArrayRaw(colPath);
											blockLength = c.strings.length;
										} else
											throw new NotImplementedException();
											
										cache.put(i, c);
									}
									
									if ((blockNumber + 1) * getCacheSize() > maxRows)
										blockLength = (int) (maxRows % getCacheSize());
								}
								
								blockNumber++;
								
								if (blockCurrentIdx < blockLength)
									return;
								
								if (blockNumber * getCacheSize() >= maxRows)
									return;
							}
						} catch (Throwable t) {
							LibsLogger.error(H5Connector.class, "Problem while reading next block from H5", t);
							return;
						}
					}
				};
			}
		};
	}
	
	public int getColumnIndex(String columnName) {
		Integer idx = nameToIndex.get(columnName);
		
		if (idx != null)
			return idx;
				
		int i = 0;
		for (ColumnDef columnDef : getColumnDefs()) {
			if (columnDef.getName().equals(columnName)) {
				nameToIndex.put(columnName, i);
				return i;
			}
			i++;
		}
		return -1;
	}
	
	public ColumnDef getColumnByName(String columnName) {
		int idx = getColumnIndex(columnName);
		if (idx >= 0)
			return getColumnDefs().get(idx);
		return null;
	}
	
	private int compare(@SuppressWarnings("rawtypes") Comparable c1, @SuppressWarnings("rawtypes") Comparable c2) {
		if (c1.getClass().equals(c2.getClass()))
			return c1.compareTo(c2);
		else if (c1 instanceof Double && c2 instanceof Integer)
			return ((Double)c1).compareTo(Double.valueOf((Integer) c2));
		else if (c1 instanceof Double && c2 instanceof Long)
			return ((Double)c1).compareTo(Double.valueOf((Long) c2));
		else if (c2 instanceof Double && c1 instanceof Integer)
			return ((Double)c2).compareTo(Double.valueOf((Integer) c1));
		else if (c2 instanceof Double && c1 instanceof Long)
			return ((Double)c2).compareTo(Double.valueOf((Long) c1));
		else
			return 0;
	}

	@Override
	public Results iterateRows(Query filter, String state, int size) {
		throw new NotImplementedException();
	}

	@Override
	public void write(Row row) throws IOException {
		if (row == null)
			return;
		
		// open writer if needed
		if (rowsWriter == null) {
			openRowsWriter();
		}
		
		// columns have to be already defined
		assertTrue(getColumnDefs() != null && getColumnDefs().size() > 0, 
				"Columns are not defined, they are needed to write a Row");
		
		// TODO DEBUG
		if (rowCount == getCacheSize()) {
			LibsLogger.error(H5Connector.class, "There is already ", getCacheSize(),
					"rows in the cache, they should be flushed already");
			saveCache();
		}
			
		// saving to cache
		for (ColumnDef columnDef : getColumnDefs()) {
			int k = getColumnIndex(columnDef.getName());
//			for (int k = 0; k < getColumnDefs().size(); k++) {
//				ColumnDef columnDef = getColumnDefs().get(k);
			
			if (columnDef.getType() == ColumnType.DOUBLE)
				cache.get(k).doubles[rowCount] = row.getAsDouble(columnDef.getName());
			else if (columnDef.getType() == ColumnType.INTEGER)
				cache.get(k).integers[rowCount] = row.getAsInt(columnDef.getName());
			else if (columnDef.getType() == ColumnType.LONG)
				cache.get(k).longes[rowCount] = row.getAsLong(columnDef.getName());
			else if (columnDef.getType() == ColumnType.STRING) {
				String tmp = row.getAsString(columnDef.getName());
				if (tmp == null)
					tmp = "";
				cache.get(k).strings[rowCount] = tmp;
			} else
				throw new NotImplementedException();
		}
		
		rowCount++;
		rowCountAll++;
		
		if (rowCount == getCacheSize()) {
			saveCache();
		}
	}
	
	private void logStatus(String context) {
		LibsLogger.debug(H5Connector.class, "TICK ", context, 
				", ", getH5Path().getAbsolutePath(), 
				", instance ", thisInstance.getId());
	}
	
	private void openRowsWriter() {
		if (rowsWriter == null) {
			
			final long nrRows = getNumberOfRows();
			final String path = getH5Path().getAbsolutePath();
			
			logStatus("Opening H5 writer nrRows= " + nrRows);
			
			rowsWriter = HDF5Factory.open(path);
			rowsWriter.file().addFlushable(this);
			
			if (nrRows > 0) {
				// opening already existing H5 file
				
				// read the last block to cache to append new rows into this file
				rowCountAll = nrRows;
				rowCount = (int) (nrRows % getCacheSize());
				
				// determine which blockId is currently in use (based on the cache size)
				blockId = nrRows / getCacheSize();
				
				// reading already opened blockId (only if the cacheSize > 1)
				if (getCacheSize() >= 1) {
					cache = new HashMap<Integer, H5Connector.Cache>();
					if (rowCount == 0) {
						// the current block is actually full, moving to the next one
						for (int i = 0; i < getColumnDefs().size(); i++)
							cache.put(i, new Cache(getCacheSize()));
					} else {
						for (int i = 0; i < getColumnDefs().size(); i++) {
							ColumnDef columnDef = getColumnDefs().get(i);
							
							String colPath = "/" + columnDef.getName();
			
							Cache c = new Cache(getCacheSize());
							if (columnDef.getType() == ColumnType.DOUBLE) {
								c.type = ColumnType.DOUBLE;
								c.doubles = rowsWriter.float64().readArrayBlock(colPath, getCacheSize(), blockId);
							} else if (columnDef.getType() == ColumnType.INTEGER) {
								c.type = ColumnType.INTEGER;
								c.integers = rowsWriter.int32().readArrayBlock(colPath, getCacheSize(), blockId);
							} else if (columnDef.getType() == ColumnType.LONG) {
								c.type = ColumnType.LONG;
								c.longes = rowsWriter.int64().readArrayBlock(colPath, getCacheSize(), blockId);
							} else if (columnDef.getType() == ColumnType.STRING) {
								c.type = ColumnType.STRING;
								c.strings = rowsWriter.string().readArrayBlock(colPath, getCacheSize(), blockId);
							} else
								throw new NotImplementedException();
								
							cache.put(i, c);
						}
					}
				}
			} else {
				// opening new H5 file
				
				// create columns
				assertTrue(getColumnDefs() != null && getColumnDefs().size() > 0, 
						"Columns are not defined");
				
				for (ColumnDef columnDef : getColumnDefs()) {
					if (columnDef.getType() == ColumnType.DOUBLE)
						rowsWriter.createDoubleArray(columnDef.getName(), 0, getCacheSize(), HDF5FloatStorageFeatures.FLOAT_CONTIGUOUS);
					else if (columnDef.getType() == ColumnType.INTEGER)
						rowsWriter.createIntArray(columnDef.getName(), 0, getCacheSize(), HDF5IntStorageFeatures.INT_CONTIGUOUS);
					else if (columnDef.getType() == ColumnType.LONG)
						rowsWriter.createLongArray(columnDef.getName(), 0, getCacheSize(), HDF5IntStorageFeatures.INT_CONTIGUOUS);
					else if (columnDef.getType() == ColumnType.STRING)
						rowsWriter.string().createArrayVL(columnDef.getName(), 
								H5_STRING_MAX_LENGTH
								);
					else
						throw new NotImplementedException("Unimplemented writing type: " + columnDef.getType());
				}
				
				rowCount = 0;
				rowCountAll = 0;
				cache = new HashMap<Integer, H5Connector.Cache>();
				for (int i = 0; i < getColumnDefs().size(); i++)
					cache.put(i, new Cache(getCacheSize()));
				
			}
		}
	}
	
//	@Override
	// TODO czemu to jest zakomentowane??
//	protected void finalize() throws Throwable {
//		LibsLogger.debug(H5Connector.class, "TICK finalizing");
//		
//		close();
//		
//		super.finalize();
//	}
	
	private synchronized void saveCache() {
		try {
			if (rowCount == 0) {
				// do nothing
				logStatus("rowCount == 0, no cache to save");
				return;
			}
			
			// if H5 file is already closed, reopen it
			if (rowsWriter != null
					&& rowsWriter.file().isClosed() == true) {
				rowsWriter = null;
				openRowsWriter();
			}
			
//			// H5 file cannot be closed already
//			assertTrue(rowsWriter != null, "H5 writer cannot be null in saveCache");
//			assertTrue(rowsWriter != null && rowsWriter.file().isClosed() == false, 
//					"H5 file " + getH5Path() + " is already closed");
						
			// saving to H5
			int i = 0;
			for (ColumnDef columnDef : getColumnDefs()) {
				if (columnDef.getType() == ColumnType.DOUBLE)
					rowsWriter.float64().writeArrayBlock(columnDef.getName(), cache.get(i).doubles, blockId);
				else if (columnDef.getType() == ColumnType.INTEGER)
					rowsWriter.int32().writeArrayBlock(columnDef.getName(), cache.get(i).integers, blockId);
				else if (columnDef.getType() == ColumnType.LONG)
					rowsWriter.int64().writeArrayBlock(columnDef.getName(), cache.get(i).longes, blockId);
				else if (columnDef.getType() == ColumnType.STRING) {
					Cache c = cache.get(i);
//					for (int j = 0; j < c.strings.length; j++) {
//						if (c.strings[j] == null)
//							c.strings[j] = "";
//					}
					rowsWriter.string().writeArrayBlock(columnDef.getName(), c.strings, blockId);
				} else
					throw new NotImplementedException();
				
				i++;
			}
			
			if (rowCount == getCacheSize()) {
				// move to the next block in H5 if this block is full
				blockId++;
				
				// and clear cache
				rowCount = 0;
				cache = new HashMap<Integer, H5Connector.Cache>();
				for (i = 0; i <= getColumnDefs().size(); i++)
					cache.put(i, new Cache(getCacheSize()));
			}
			
			// save total number of rows of this file
			rowsWriter.int64().write("meta/rows", rowCountAll);
			
		} catch (Exception e) {
			LibsLogger.error(H5Connector.class, "Cannot save cached rows in H5Connector", e);
		}
	}

	@Override
	public synchronized void close() throws IOException {
		if (rowsWriter != null) {
			logStatus("close()");
			
			saveCache();
			
			if (rowsWriter != null && rowsWriter.file().isClosed() == false) {
				logStatus("actually closing writer");
				rowsWriter.close();
			}
			rowsWriter = null;
			
			logStatus("Closed");
			
			LibsLogger.debug(H5Connector.class, getH5Path().getFilenameOnly(), " H5 closed");
			
//			LocalLogger.debug("close()", 
//					", tableId= ", getInitParams().getTableId(),
//					", connector= ", this.toString());
		}
		
		if (cache != null) {
			cache.clear();
			cache = null;
		}
	}
	
	public long getNumberOfRows() {
		try {
			return getMetaLong("meta/rows");
		} catch (HDF5FileNotFoundException e) {
			LibsLogger.error(H5Connector.class, "File " + getH5Path() + " does not exist", e);
			return 0;
		}
	}

	@Override
	public void clear() throws IOException {
		FileExt h5 = getH5Path();
		
		if (h5 != null) {
			h5.deleteIfExists();
			LibsLogger.debug(H5Connector.class, "File ", h5.getAbsolutePath(), " deleted");
		}
	}
	
	private FileExt getH5Path() {
		if (connectorInitParams != null)
			return new FileExt(connectorInitParams.getMeta().get(META_PATH).getAsString());
		return null;
	}
	
	private void saveMeta(String key, long value) {
		if (rowsWriter != null)
			rowsWriter.int64().write(key, value);
		else {
			IHDF5Writer writer = HDF5Factory.open(getH5Path().getAbsolutePath());
			writer.int64().write(key, value);
			writer.close();
		}
	}
	
	private void saveMeta(String key, String value) {
		if (rowsWriter != null)
			rowsWriter.string().write(key, value);
		else {
			IHDF5Writer writer = HDF5Factory.open(getH5Path().getAbsolutePath());
			writer.string().write(key, value);
			writer.close();
		}
	}
	
	private String getMeta(String key) {
		if (rowsWriter != null)
			try {
				return rowsWriter.string().read(key);
			} catch (Exception e) {
				if (!new FileExt(getH5Path().getAbsolutePath()).exists())
					LibsLogger.error(H5Connector.class, "Cannot read meta " + key + " from " + 
							getH5Path().getAbsolutePath(), e);
				return null;
			}
		else {
			if (!isH5FileExist())
				return null;
			
			IHDF5Reader reader = null;
			try {
				reader = HDF5Factory.openForReading(getH5Path().getAbsolutePath());
				String value = reader.string().read(key);
				logStatus("Closing reader getMeta");
				reader.close();
				return value;
			} catch (Exception e) {
				if (!new FileExt(getH5Path().getAbsolutePath()).exists())
					LibsLogger.error(H5Connector.class, "Cannot read meta " + key + " from " + 
							getH5Path().getAbsolutePath(), e);
				return null;
			} finally {
				if (reader != null) {
					logStatus("Closing reader getMeta");
					reader.close();
				}
			}
		}
	}
	
	public boolean isH5FileExist() {
		if (isH5FileExist != null)
			return isH5FileExist;
		isH5FileExist = new FileExt(getH5Path().getAbsolutePath()).exists();
		return isH5FileExist;
	}
	
	private long getMetaLong(String key) {
		if (rowsWriter != null)
			return rowsWriter.int64().read(key);
		else {
			if (!isH5FileExist())
				return 0;
			
			IHDF5Reader reader = HDF5Factory.openForReading(getH5Path().getAbsolutePath());
			try {
				long value = reader.int64().read(key);
	//			reader.close();
				return value;
			} catch (Exception e) {
				if (!new FileExt(getH5Path().getAbsolutePath()).exists())
					LibsLogger.error(H5Connector.class, "Cannot read meta " + key + " from " + 
							getH5Path().getAbsolutePath(), e);
				return 0;
			} finally {
				if (reader != null) {
					logStatus("Closing reader getMetaLong");
					reader.close();
				}
			}
		}
	}

	@Override
	public void setColumnDefs(ColumnDefList defs) {
		this.columnDefs = defs;
		this.nameToIndex = new HashMap<String, Integer>();
		
		saveMeta("/meta/columns", JsonUtils.objectToString(defs));
	}

	@Override
	public ColumnDefList getColumnDefs() {
		if (columnDefs == null) {
			String columns = getMeta("/meta/columns");
			
			if (columns != null) {
				this.columnDefs = new ColumnDefList();
				this.containsTbid = false;
				
				for (JsonElement je : ((JsonArray) JsonUtils.parseJson(columns))) {
					ColumnDef colDef = JsonUtils.fromJson(je, ColumnDef.class);
					
					this.columnDefs.add(colDef);
					
					if (colDef.getName().equals(Table.COLUMN_TBID))
						containsTbid = true;
				}
				
				if (containsTbid == false)
					this.columnDefs.add(0, new ColumnDef(Table.COLUMN_TBID, "TableID", ColumnType.STRING));
			}
		} else {
			if (containsTbid == null) {
				this.containsTbid = false;
				for (ColumnDef columnDef : columnDefs) {
					if (columnDef.getName().equals(Table.COLUMN_TBID))
						containsTbid = true;
				}
			}
		}
		return columnDefs;
	}

	public boolean isUseCache() {
		return getCacheSize() > 1;
	}

	public H5Connector setUseCache(boolean useCache) {
		if (!useCache)
			setCacheSize(1);
		return this;
	}

	public int getCacheSize() {
		if (cacheSize == null)
			cacheSize = 1000;
		return cacheSize;
	}

	public H5Connector setCacheSize(int cacheSize) {
		assertTrue(this.cacheSize == null, "Cache size is already set for H5Connector, cannot change it anymore");
		
		this.cacheSize = cacheSize;
		return this;
	}

	private class Cache {
		int cacheSize = 0;
		ColumnType type = null;
		int 	[] integers 	= null;
		double 	[] doubles 		= null;
		long 	[] longes 		= null;
		String 	[] strings 		= null;
		
		public Cache(int cacheSize) {
			integers 	= new int[cacheSize];
			doubles 	= new double[cacheSize];
			longes 		= new long[cacheSize];
			strings 	= new String[cacheSize];
		}
	}
	
	@Override
	public void validate() throws IOException {
		try {
			if (getColumnDefs() != null)
				for (ColumnDef columnDef : getColumnDefs()) {
					if (validateH5(getH5Path().getAbsolutePath(), columnDef.getName()) == false)
						if (columnDef.getName().equals(Table.COLUMN_TBID) == false)
							throw new IOException("File " + getH5Path() + ", column " + columnDef.getName() + 
								" is corrupted, validation fails");
				}
		} catch (IOException e) {
			throw e;
		} catch (Throwable e) {
			throw new IOException("Cannot validate " + getH5Path() + ", or file is corrupted", e);
		}
	}
	
	@Override
	public String getVersion() {
		try {
			StringBuilder sb = new StringBuilder();
			
			sb.append("H5 file; ");
			
			FileExt realPath = getH5Path();
			
			sb.append(realPath);
			
			sb.append("; " + realPath.getLastEditDate());
			
			return sb.toString();
		} catch (Throwable t) {
			return "H5 unknown version";
		}
	}
	
	public static boolean validateH5(String h5File, String column) throws IOException {
		UUID bashId = UUID.random();
		FileExt bashFile = new FileExt("/tmp/h5check-" + bashId + ".bash");
		
		FileUtils.saveToFile("h5dump -d /" + column + " " 
				+ h5File
				+ " | head -n 20", 
				bashFile,
				true);
		
		boolean dataDatasetRead = false;
		boolean isValid = true;
		for (String line : new Basher2()
			.add("bash")
			.add(bashFile.getAbsolutePath())
//			.add("h5dump")
//			.add("-d")
//			.add("/tbidSnap")
//			.add("./d5f9ce12e0b333b44b8db081ddb196c0-ca1f9b92872f8ab7e1ff7047909c0214.h5")
//			.add("|")
//			.add("head")
//			.add("-n")
//			.add("20")
			.runIter()) {
			if (line.contains("DATA {"))
				dataDatasetRead = true;
				
			if ((line.contains("error") && !line.contains("/error"))
					|| line.contains("Segmentation fault"))
				isValid = false;
		}
		
		if (!dataDatasetRead)
			isValid = false;
		
		if (!isValid)
			LibsLogger.error(H5Connector.class, "H5 file ", h5File, " column= ", column, " valid= ", isValid);
		
		bashFile.deleteIfExists();
		
		return isValid;
	}
	
	@Override
	public long getAproxBytesSize() throws IOException {
		FileExt h5 = getH5Path();
		
		if (h5 != null) {
			return h5.getSizeBytes();
		}

		return -1;
	}
}
