package net.beanscode.model.connectors;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.NotImplementedException;

import com.google.gson.annotations.Expose;

import net.beanscode.model.plugins.ColumnDefList;
import net.beanscode.model.plugins.Connector;
import net.beanscode.model.plugins.ConnectorInitParams;
import net.hypki.libs5.db.db.Results;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.search.query.Query;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

public class CachedConnector implements Connector, Iterable<Row> {
	
	@Expose
	@NotNull
	@NotEmpty
	private UUID id = null;

	@Expose
	@NotNull
	@NotEmpty
	private H5Connector connector = null;
	
	@Expose
	@NotNull
	@NotEmpty
	private List<Row> inMemoryRows = null;
	
	@Expose
	@NotNull
	@NotEmpty
	private boolean savingInMemory = true;
	
	@Expose
	@NotNull
	@NotEmpty
	private ColumnDefList columnDefs = null;
	
	public CachedConnector() {
		
	}

	private H5Connector getConnector() {
		if (connector == null)
			connector = new H5Connector(getId(), new File(getId().getId() + ".h5cache"));// TODO conf
		return connector;
	}
	
	@Override
	public Iterator<Row> iterator() {
		return iterateRows(null).iterator();
	}

	private void setConnector(H5Connector connector) {
		this.connector = connector;
	}

	@Override
	public String getName() {
		return "Cached connector - stores as much as possible in the memory, but if needed saves the rows to the file";
	}

	@Override
	public ConnectorInitParams getInitParams() {
		return null;
	}

	@Override
	public Connector setInitParams(ConnectorInitParams initParams) {
		return null;
	}

	@Override
	public Iterable<Row> iterateRows(Query filter) {
		if (isSavingInMemory())
			return getInMemoryRows();
		else
			return getConnector().iterateRows(null);
	}

	@Override
	public Iterable<Row> iterateRows(Query filter, int splitNr, int splitCount) {
		// TODO Auto-generated method stub
		throw new NotImplementedException();
	}

	@Override
	public Results iterateRows(Query filter, String state, int size) {
		// TODO Auto-generated method stub
		throw new NotImplementedException();
	}

	@Override
	public void write(Row row) throws IOException {
		if (isSavingInMemory())
			getInMemoryRows().add(row);
		else
			getConnector().write(row);
	}

	@Override
	public void close() throws IOException {
		if (!isSavingInMemory())
			getConnector().close();
	}

	@Override
	public long getAproxBytesSize() throws IOException {
		if (isSavingInMemory())
			return getInMemoryRows().size() * getColumnDefs().size() * 8 /*bytes*/;
		else
			return getConnector().getAproxBytesSize();
	}

	@Override
	public void clear() throws IOException {
		if (isSavingInMemory())
			getInMemoryRows().clear();
		else
			getConnector().clear();
	}

	@Override
	public void setColumnDefs(ColumnDefList defs) {
		this.columnDefs = defs;
	}

	@Override
	public ColumnDefList getColumnDefs() {
		return columnDefs;
	}

	@Override
	public void validate() throws IOException {
		
	}

	@Override
	public String getVersion() {
		return "0.1.0";
	}

	private List<Row> getInMemoryRows() {
		if (inMemoryRows == null)
			inMemoryRows = new ArrayList<>();
		return inMemoryRows;
	}

	private void setInMemoryRows(List<Row> inMemoryRows) {
		this.inMemoryRows = inMemoryRows;
	}

	private boolean isSavingInMemory() {
		return savingInMemory;
	}

	private void setSavingInMemory(boolean savingInMemory) {
		this.savingInMemory = savingInMemory;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}
}
