package net.beanscode.model.unittests.connectors;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import net.beanscode.model.connectors.H5Connector;
import net.beanscode.model.plugins.ColumnDef;
import net.beanscode.model.plugins.ColumnDefList;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.schema.ColumnType;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.date.Watch;
import net.hypki.libs5.utils.file.FileExt;
import net.hypki.libs5.utils.string.RandomUtils;

import org.junit.Test;

public class H5ConnectorNotClosingTestCase extends ConnectorTestCase {

	public H5ConnectorNotClosingTestCase() {
		
	}
	
	@Test
	public void testConnector() throws IOException {		
		testConnector(1200, 50, 1, true);
		testConnector(1200, 50, 50, true);
		testConnector(1200, 50, 100, true);
		testConnector(1200, 50, 123, true);
		
		testConnector(1200, 1500, 123, false);
		
		testConnector(1200, 200, 5000, true);
	}
	
	private void testConnector(int rowsCount, int closeForFunEveryXLines, int cacheSize, 
			boolean closeAtAll) throws IOException {
		FileExt h5File = new FileExt("test.h5");
		h5File.deleteIfExists();
		
		H5Connector conn = new H5Connector(UUID.random(), h5File)
			.setCacheSize(cacheSize);
		
		ColumnDefList defs = new ColumnDefList();
		defs.add(new ColumnDef("c1", "c1 desc", ColumnType.INTEGER));
		defs.add(new ColumnDef("c2", "c2 desc", ColumnType.STRING));
		defs.add(new ColumnDef("c3", "c3 desc", ColumnType.STRING));
		conn.setColumnDefs(defs);
//		testSimple(conn, rowCount);
		int c1Sum = 0;
		boolean checkStrings = false;
		List<String> c2List = new ArrayList<String>();
		
		
		int percent = 0;
		Watch w = new Watch();
		for (int i = 0; i < rowsCount; i++) {
			Row r = new Row(UUID.random());
			
			int randomInt = RandomUtils.nextInt(100);
			c1Sum += randomInt;
			
			r.addColumn("c1", randomInt);
			
			String c2 = UUID.random().getId() + UUID.random().getId();
			r.addColumn("c2", c2);
			
			r.addColumn("c3", RandomUtils.nextPronounceableWord(5 + RandomUtils.nextInt(90)));
			
			if (checkStrings)
				c2List.add(c2);
			
			conn.write(r);
			
			int newPercent = (int)(i / (rowsCount / 100)) % 10;
			if (newPercent == 0 && newPercent > percent) {
				percent = newPercent;
				LibsLogger.info(H5ConnectorNotClosingTestCase.class, "Written ", i, " rows, ", newPercent, "%");
			}
			
			if (i % closeForFunEveryXLines == 0) {
				LibsLogger.info(H5ConnectorNotClosingTestCase.class, "Closing for fun");
				conn.close();
			}
		}
		if (closeAtAll)
			conn.close();
		else
			conn.flush();
		LibsLogger.info(H5ConnectorNotClosingTestCase.class, "All ", rowsCount, " written in ", w);
		
		// checking sum
		int sumCheck = (int) sumColumn(conn, "c1");
		assertTrue(sumCheck == c1Sum, "Expected " + c1Sum + " but it " + sumCheck);
		
		sumCheck = (int) sumColumn(conn, 512, "c1");
		assertTrue(sumCheck == c1Sum, "Expected " + c1Sum + " but it " + sumCheck);
		
		if (checkStrings)
			compareColumn(conn, "c2", c2List);
		
//		conn.clear();
		conn.close();
		
		LibsLogger.info(H5ConnectorNotClosingTestCase.class, "Sum is ", c1Sum, " == ", sumCheck);
	}
}
