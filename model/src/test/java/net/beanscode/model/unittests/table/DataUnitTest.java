package net.beanscode.model.unittests.table;

import java.io.IOException;

import org.junit.Test;

import net.beanscode.model.BeansConst;
import net.beanscode.model.cass.Data;
import net.beanscode.model.cass.DataFactory;
import net.beanscode.model.cass.DataUtils;
import net.beanscode.model.connectors.CassandraConnector;
import net.beanscode.model.dataset.Dataset;
import net.beanscode.model.dataset.Table;
import net.beanscode.model.tests.BeansTestCase;
import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.weblibs.WeblibsConst;
import net.hypki.libs5.weblibs.user.User;

public class DataUnitTest extends BeansTestCase {

	@Test
	public void testDataSave() throws IOException, ValidationException {
		DbObject.getDatabaseProvider().clearTable(WeblibsConst.KEYSPACE, Data.COLUMN_FAMILY);
		
		User user = createTestUser("test");
		Dataset ds = createDataset(user, "datasetName");
		Table table = createTable(ds, "tableName");
		
		CassandraConnector conn = new CassandraConnector(table);
		
		for (int i = 0; i < 10; i++) {
			conn.write(new Row(UUID.random().getId())
							.addColumn(BeansConst.COLUMN_TBID, table.getId().getId())
//							.addColumn(Data.COLUMN_ROW, (long) i)
							.addColumn("c1", i));
		}
		
		conn.close();
		
		for (Row row : DataFactory.getData(table.getId().getId())) {
			LibsLogger.debug(DataUnitTest.class, "Row BEFORE removal: " + row);
		}
		
		DataUtils.removeData(WeblibsConst.KEYSPACE, table.getId().getId());
		
		runAllJobs();
		
		int left = 0;
		for (Row row : DataFactory.getData(table.getId().getId())) {
			LibsLogger.debug(DataUnitTest.class, "Row AFTER removal: " + row);
		}
		assertTrue(left == 0, "Not all rows from a given Data table were removed");
	}
}
