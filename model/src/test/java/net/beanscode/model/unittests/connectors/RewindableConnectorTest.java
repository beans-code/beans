package net.beanscode.model.unittests.connectors;

import java.io.IOException;

import org.junit.Test;

import net.beanscode.model.connectors.PlainConnector;
import net.beanscode.model.plugins.RewindableConnector;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.utils.file.FileExt;

public class RewindableConnectorTest {

	@Test
	public void testSimple() throws IOException {
		RewindableConnector rew = new RewindableConnector(1000);
		
		FileExt source = new FileExt("src/main/resources/testdata/line-1-10.json");
		PlainConnector sourceConnector = new PlainConnector(source);
		
		rew.setColumnDefs(sourceConnector.getColumnDefs());
		
		for (int i = 0; i < 1000; i++) {
			for (Row row : sourceConnector)
				rew.write(row);
		}
			
//		rew.close();
		
		// tests
		for (Row row : rew.iterateRows(null))
			System.out.println(row);
		
		rew.clear();
	}
}
