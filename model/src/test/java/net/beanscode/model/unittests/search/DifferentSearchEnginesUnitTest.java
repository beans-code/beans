package net.beanscode.model.unittests.search;

import java.io.IOException;

import net.beanscode.model.tests.BeansTestCase;
import net.hypki.libs5.search.SearchResults;
import net.hypki.libs5.search.elastic.ElasticSearchProvider;
import net.hypki.libs5.search.lucene.LuceneSearchEngineProvider;
import net.hypki.libs5.search.lucene.TestObject;
import net.hypki.libs5.search.query.DoubleTerm;
import net.hypki.libs5.search.query.IntTerm;
import net.hypki.libs5.search.query.Query;
import net.hypki.libs5.utils.LibsLogger;

import org.junit.Test;

public class DifferentSearchEnginesUnitTest extends BeansTestCase {

	@Test
	public void testLucene() throws Exception {
		try {
			final String index = "idx";
			final String type = TestObject.class.getSimpleName().toLowerCase();
			
			LuceneSearchEngineProvider lucene = new LuceneSearchEngineProvider();
			ElasticSearchProvider elastic = new ElasticSearchProvider();
			
			TestObject to = new TestObject("title1")
				.setDoublevalue(3.0);
			
			lucene.indexDocument(index, type, to.getId().getId(), to.getData());
			elastic.indexDocument(index, type, to.getId().getId(), to.getData());
			
			SearchResults<TestObject> foundInLucene = lucene.search(TestObject.class,
					index, 
					type, 
					new Query()
						.addTerm(new DoubleTerm("doublevalue", 3.0)), 
					0, 
					10);
			SearchResults<TestObject> foundInElastic = elastic.search(TestObject.class,
					index, 
					type, 
					new Query()
						.addTerm(new DoubleTerm("doublevalue", 3.0)), 
					0, 
					10);
			
			takeANap(1000);
			
			LibsLogger.debug(DifferentSearchEnginesUnitTest.class, "Found ", foundInLucene.size(), " objects in Lucene");
			for (TestObject t : foundInLucene) {
				LibsLogger.debug(DifferentSearchEnginesUnitTest.class, "Found in Lucene object: ", t.getData());
			}
			
			LibsLogger.debug(DifferentSearchEnginesUnitTest.class, "Found ", foundInElastic.size(), " objects in Elastic");
			for (TestObject t : foundInElastic) {
				LibsLogger.debug(DifferentSearchEnginesUnitTest.class, "Found in Elastic object: ", t.getData());
			}
		} catch (Throwable e) {
			e.printStackTrace();
			
			throw e;
		}
	}
}
