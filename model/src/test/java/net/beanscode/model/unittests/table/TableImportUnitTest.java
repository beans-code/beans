package net.beanscode.model.unittests.table;

import static net.hypki.libs5.utils.string.StringUtilities.nullOrEmpty;
import net.beanscode.model.cass.CheckIntegrityMR;
import net.beanscode.model.dataset.Dataset;
import net.beanscode.model.dataset.Table;
import net.beanscode.model.tests.BeansTestCase;
import net.hypki.libs5.db.db.schema.ColumnType;
import net.hypki.libs5.utils.reflection.SystemUtils;

import org.junit.Test;

public class TableImportUnitTest extends BeansTestCase {
	
	@Test
	public void importDifferentHeaders() throws Exception {
		final Dataset dataset = createDataset(testUser(), "bulk-snapshot");
		
		final Table tableNoHeader = createTable(dataset, "line-no-header");
		
		tableNoHeader.importFile(SystemUtils.getResourceURL("testdata/test-line-no-header").getFile());
		
		runAllJobs(1000);
		
		Table tabDb = Table.getTable(dataset.getId(), "line-no-header");
		assertTrue(tabDb != null);
		assertTrue(tabDb.getColumnDefs().size() == 3);
		assertTrue(tabDb.getColumnType("c1") == ColumnType.LONG);
		assertTrue(tabDb.getColumnType("c2") == ColumnType.DOUBLE);
		assertTrue(nullOrEmpty(tabDb.getColumnDef("c1").getDescription()));
		
		final Table tableSimpleHeader = createTable(dataset, "line-simple-header");
		
		tableSimpleHeader.importFile(SystemUtils.getResourceURL("testdata/test-line-simple-header").getFile());
		
		runAllJobs(1000);
		
		tabDb = Table.getTable(dataset.getId(), "line-simple-header");
		assertTrue(tabDb != null);
		assertTrue(tabDb.getColumnDefs().size() == 3);
		assertTrue(tabDb.getColumnType("x") == ColumnType.LONG);
		assertTrue(tabDb.getColumnType("y") == ColumnType.DOUBLE);
		assertTrue(nullOrEmpty(tabDb.getColumnDef("x").getDescription()));
	}
	
	@Test
	public void testIntegrity() throws Exception {
		final Dataset dataset = createDataset(testUser(), "bulk-snapshot");
		final Table tableSnapshot = createTable(dataset, "snapshot");
		final Table tableSystem = createTable(dataset, "system");
		
		tableSnapshot.importFile(SystemUtils.getResourceURL("testdata/mocca/n=3000/fracb=0.1/snapshot.dat.beans").getFile());
		tableSystem.importFile(SystemUtils.getResourceURL("testdata/mocca/n=3000/fracb=0.1/system.dat.beans").getFile());
		
		runAllJobs();
		takeANap(1000);
		
		new CheckIntegrityMR().runMapReduce();
	}
}
