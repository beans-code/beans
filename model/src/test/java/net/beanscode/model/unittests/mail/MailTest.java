package net.beanscode.model.unittests.mail;

import java.security.GeneralSecurityException;
import java.util.Date;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import net.beanscode.model.tests.BeansTestCase;
import net.hypki.libs5.utils.file.FileExt;
import net.hypki.libs5.utils.file.FileUtils;
import net.hypki.libs5.weblibs.mail.MailCredentials;
import net.hypki.libs5.weblibs.mail.Mailer;

import org.junit.Test;

import com.sun.mail.util.MailSSLSocketFactory;

public class MailTest extends BeansTestCase {

	@Test
	public void testSend() throws MessagingException, GeneralSecurityException {
		MailCredentials credentials = new MailCredentials(FileUtils.readString(new FileExt("$HOME/.beans-software/test-hostname")), 
				587, 
				FileUtils.readString(new FileExt("$HOME/.beans-software/test-username")), 
				FileUtils.readString(new FileExt("$HOME/.beans-software/test-password")), 
				true, 
				20000);
		
		Mailer.sendMail(credentials, 
				FileUtils.readString(new FileExt("$HOME/.beans-software/test-to")), 
				null, 
				null,
				"Test 3", 
				"Body 3", 
				false, 
				null);
		
		final String fromEmail = FileUtils.readString(new FileExt("$HOME/.beans-software/test-username")); //requires valid gmail id
		final String password = FileUtils.readString(new FileExt("$HOME/.beans-software/test-password")); // correct password for gmail id
		final String toEmail = FileUtils.readString(new FileExt("$HOME/.beans-software/test-to")); // can be any email id 
		
		System.out.println("TLSEmail Start");
		Properties props = new Properties();

		MailSSLSocketFactory sf = new MailSSLSocketFactory();
		sf.setTrustAllHosts(true); 
		
		props.put("mail.imap.ssl.trust", "*");
		props.put("mail.imap.ssl.socketFactory", sf);
		props.put("mail.smtp.ssl.trust", "*");
		props.put("mail.smtp.ssl.socketFactory", sf);
		props.put("mail.smtp.host", FileUtils.readString(new FileExt("$HOME/.beans-software/test-hostname"))); //SMTP Host
		props.put("mail.smtp.port", "587"); //TLS Port
		props.put("mail.smtp.auth", "true"); //enable authentication
		props.put("mail.smtp.starttls.enable", "true"); //enable STARTTLS
		
                //create Authenticator object to pass in Session.getInstance argument
		Authenticator auth = new Authenticator() {
			//override the getPasswordAuthentication method
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(fromEmail, password);
			}
		};
		Session session = Session.getInstance(props, auth);
		
		sendEmail(session, toEmail,"Test 2", "TLSEmail Testing Body");
		
	}
	
	public static void sendEmail(Session session, String toEmail, String subject, String body){
		try
	    {
	      MimeMessage msg = new MimeMessage(session);
	      //set message headers
	      msg.addHeader("Content-type", "text/HTML; charset=UTF-8");
	      msg.addHeader("format", "flowed");
	      msg.addHeader("Content-Transfer-Encoding", "8bit");

	      msg.setFrom(new InternetAddress("no_reply@example.com", "NoReply-JD"));

	      msg.setReplyTo(InternetAddress.parse("no_reply@example.com", false));

	      msg.setSubject(subject, "UTF-8");

	      msg.setText(body, "UTF-8");

	      msg.setSentDate(new Date());

	      msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail, false));
	      System.out.println("Message is ready");
    	  Transport.send(msg);  

	      System.out.println("EMail Sent Successfully!!");
	    }
	    catch (Exception e) {
	      e.printStackTrace();
	    }
	}
}
