package net.beanscode.model.unittests.setting;

import java.io.IOException;

import org.junit.Test;

import net.beanscode.model.tests.BeansTestCase;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.search.SearchResults;
import net.hypki.libs5.weblibs.settings.Setting;
import net.hypki.libs5.weblibs.settings.SettingFactory;

public class SettingsUnitTest extends BeansTestCase {

	@Test
	public void testSetting() throws ValidationException, IOException {
		final String key = "TEST_SETT";
		
		Setting s = new Setting()
			.setId(key)
			.setDescription("Some desc")
			.setName("Testing setting");
		s.save();
		
		takeANap(1000);
		
		Setting fromDb = SettingFactory.getSetting(key);
		
		assertTrue(fromDb != null, "Setting not found in DB");
		assertTrue(fromDb.getName().equals(s.getName()), "Setting has wrong name");
		
		SearchResults<Setting> found = SettingFactory.searchSettings(null, "test", false, 0, 10);
		
		assertTrue(found.size() == 1, "Should find 1 object");
		assertTrue(found.getObjects().get(0).getKey().equals(s.getKey()), "Should find Setting " + key);
	}
}
