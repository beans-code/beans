package net.beanscode.model.unittests.notebook;

import java.io.IOException;

import org.junit.Test;

import net.beanscode.model.notebook.Notebook;
import net.beanscode.model.notebook.NotebookEntry;
import net.beanscode.model.notebook.NotebookEntryFactory;
import net.beanscode.model.notebook.NotebookEntryHistory;
import net.beanscode.model.notebook.NotebookEntryHistoryFactory;
import net.beanscode.model.tests.BeansTestCase;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.weblibs.user.User;

public class NotebookEntryUnitTest extends BeansTestCase {

	@Test
	public void testHistory() throws ValidationException, IOException {
		User user = createTestUser("user1");
		
		Notebook n = new Notebook(user.getUserId(), "notebook1");
		n.save();
		
		NotebookEntry ne = new NotebookEntry();
		ne.setUserId(user.getUserId());
		ne.setNotebookId(n.getId());
		ne.setMeta("text", "one");
		ne.save();
		
		addToRemove(ne);
		
		NotebookEntry neDb = NotebookEntryFactory.getNotebookEntry(ne.getId());
		assertTrue(neDb.getMetaAsString("text").equals("one"));
		
		ne.setMeta("text", "two");
		ne.save();
		
		neDb = NotebookEntryFactory.getNotebookEntry(ne.getId());
		assertTrue(neDb.getMetaAsString("text").equals("two"));
		
		runAllJobs(1500);
		
		for (NotebookEntryHistory neh : NotebookEntryHistoryFactory.iterateNotebookEntryHistory(ne.getId())) {
			LibsLogger.info(NotebookEntryUnitTest.class, "HISTORY: " + neh.getMetaAsString("text"));
		}
		
		for (NotebookEntryHistory neh : NotebookEntryHistoryFactory.iterateNotebookEntryHistorySearch(ne.getId())) {
			LibsLogger.info(NotebookEntryUnitTest.class, "HISTORY FROM SEARCH: " + neh.getMetaAsString("text"));
		}
	}
}
