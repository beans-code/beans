package net.beanscode.model.unittests.connectors;

import java.io.File;
import java.io.IOException;

import net.beanscode.model.connectors.H5Connector;
import net.beanscode.model.connectors.PlainConnector;
import net.beanscode.model.plugins.ColumnDef;
import net.beanscode.model.plugins.ColumnDefList;
import net.beanscode.model.plugins.Connector;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.schema.ColumnType;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.search.query.DoubleRangeTerm;
import net.hypki.libs5.search.query.DoubleTerm;
import net.hypki.libs5.search.query.IntTerm;
import net.hypki.libs5.search.query.Query;
import net.hypki.libs5.search.query.TermRequirement;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.date.Watch;
import net.hypki.libs5.utils.file.FileExt;
import net.hypki.libs5.utils.string.RandomUtils;

import org.junit.Test;

public class H5ConnectorLargeFileTestCase extends ConnectorTestCase {

	public H5ConnectorLargeFileTestCase() {
		
	}
	
	@Test
	public void testConnector() throws IOException {
//		testConnector(100000, 10000000); // 100 columns, SUPER SLOW!
//		testConnector(1000, 10000000); // 100 columns, SUPER SLOW!

		testConnector(100, 1000,   1_000_000); // 100 columns, SUPER SLOW! 32 s, with fixed nameToIndex it goes to 12 s
//		testConnector(120, 1000, 596_609_499); // WD histories Survey1, 10% in 14 minutes with 55 GBs
		
		//		testConnector(100000, 1000000000); // slow
//		testConnector(1000, 1000000000); // slow
//		testConnector(1000, 10000000);
		
//		testConnector(1000, 10000000); // 30 s
		
		
//		testConnector(1, 	1000000);
	}
	
	public void testConnector(final int nrColumns, int cachesize, int nrRows) throws IOException {
		Watch write = new Watch();
		
		FileExt h5File = new FileExt("large-test-" + cachesize + ".h5");
		h5File.deleteIfExists();
		
		H5Connector conn = new H5Connector(UUID.random(), h5File)
			.setCacheSize(cachesize);
//		Connector conn = new PlainConnector(new File(UUID.random().getId() + ".dat"));
		
		ColumnDefList defs = new ColumnDefList();
		for (int i = 1; i <= nrColumns; i++) {			
			defs.add(new ColumnDef("c" + i, "c" + i + " desc", ColumnType.DOUBLE));
		}
//		defs.add(new ColumnDef("c2", "c2 desc", ColumnType.DOUBLE));
//		defs.add(new ColumnDef("c3", "c3 desc", ColumnType.DOUBLE));
		conn.setColumnDefs(defs);

		double c1Sum = 0;
				
		Watch total = new Watch();
		int percent = 0;
		write.reset();
		write.pause();
		for (int i = 0; i < nrRows; i++) {
			Row r = new Row(UUID.random());
			
//			int randomInt = RandomUtils.nextInt(100);
//			c1Sum += randomInt;
			
//			r.addColumn("c1", randomInt);
//			r.addColumn("c2", UUID.random().getId() + UUID.random().getId());
//			r.addColumn("c3", RandomUtils.nextPronounceableWord(5 + RandomUtils.nextInt(90)));
			
			double randomDouble = RandomUtils.nextDouble();
			c1Sum += randomDouble;
			
			for (int j = 1; j <= nrColumns; j++) {
				r.addColumn("c" + j, randomDouble);
			}
//			r.addColumn("c2", randomInt);
//			r.addColumn("c3", randomInt);
			
			write.unpause();
			conn.write(r);
			write.pause();
			
			int newPercent = (int)(i / (nrRows / 100));
			if (newPercent % 10 == 0 && newPercent > percent) {
				percent = newPercent;
				LibsLogger.info(H5ConnectorLargeFileTestCase.class, "Written ", i, " rows, ", newPercent, "%");
			}
		}
		conn.close();
		LibsLogger.info(H5ConnectorLargeFileTestCase.class, "All ", nrRows, " written in ", total, " cachesize ", cachesize);
		LibsLogger.info(H5ConnectorLargeFileTestCase.class, "H5 writes in ", write);
		
		// checking sum
		double sumCheck = (double) sumColumn(conn, "c1");
		assertTrue(sumCheck == c1Sum, "Expected " + c1Sum + " but it " + sumCheck);
		
		sumCheck = (double) sumColumn(conn, 512, "c1");
		assertTrue(sumCheck == c1Sum, "Expected " + c1Sum + " but it " + sumCheck);
		
//		conn.clear();
		conn.close();
		
		LibsLogger.info(H5ConnectorLargeFileTestCase.class, "Sum is ", c1Sum, " == ", sumCheck, " [", total.toString(), "]");
		
		// filtering
		Watch filterWatch = new Watch();
		int rows = 0;
		for (Row row : conn.iterateRows(new Query().addTerm(new DoubleTerm("c1", 0.1)))) {
			rows++;
		}
		LibsLogger.info(H5ConnectorLargeFileTestCase.class, "Filtered ", rows, " in ", filterWatch);
		
		filterWatch = new Watch();
		rows = 0;
		for (Row row : conn.iterateRows(new Query().addTerm(new DoubleRangeTerm("c1", 0.0, 0.01, TermRequirement.MUST)))) {
			rows++;
		}
		LibsLogger.info(H5ConnectorLargeFileTestCase.class, "Filtered ", rows, " in ", filterWatch);
		
	}
}
