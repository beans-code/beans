package net.beanscode.model.unittests.connectors;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import net.beanscode.model.plugins.ColumnDef;
import net.beanscode.model.plugins.ColumnDefList;
import net.beanscode.model.plugins.Connector;
import net.beanscode.model.tests.BeansTestCase;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.schema.ColumnType;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.date.Watch;
import net.hypki.libs5.utils.string.RandomUtils;

public class ConnectorTestCase extends BeansTestCase {
	
	public ConnectorTestCase() {
		
	}
	
	protected void writeRows(Connector conn, int nrOfRows) throws IOException {
		ColumnDefList defs = new ColumnDefList();
		defs.add(new ColumnDef("c1", "c1 desc", ColumnType.INTEGER));
		defs.add(new ColumnDef("c2", "c2 desc", ColumnType.STRING));
		conn.setColumnDefs(defs);
		
//		testSimple(conn, rowCount);
		int c1Sum = 0;
		boolean checkStrings = false;
		List<String> c2List = new ArrayList<String>();
		
		int percent = 0;
		Watch w = new Watch();
		for (int i = 0; i < nrOfRows; i++) {
			Row r = new Row(UUID.random());
			
			int randomInt = RandomUtils.nextInt(100);
			c1Sum += randomInt;
			
			r.addColumn("c1", randomInt);
			
			String c2 = UUID.random().getId() + UUID.random().getId();
			r.addColumn("c2", c2);
			
			if (checkStrings)
				c2List.add(c2);
			
			conn.write(r);
			
			if (nrOfRows > 100
					&& (int)(i / (nrOfRows / 100)) > percent)
				LibsLogger.info(H5ConnectorParallelReadTestCase.class, "Written ", i, " rows, ", ++percent, "%");
			
//			if (i % 1343 == 0) {
//				LibsLogger.info(H5ConnectorParallelReadTestCase.class, "Closing for fun");
//				conn.close();
//			}
		}
		conn.close();
		LibsLogger.info(H5ConnectorParallelReadTestCase.class, "All ", nrOfRows, " written in ", w);
	}
	
	protected double sumColumn(Connector conn, int splitCount, String colName) {
		double sum = 0.0;
		for (int i = 0; i < splitCount; i++) {			
			for (Row row : conn.iterateRows(null, i, splitCount)) {
				sum += row.getAsDouble(colName);
			}
		}
		return sum;
	}

	protected double sumColumn(Connector conn, String colName) {
		double sum = 0.0;
		for (Row row : conn.iterateRows(null)) {
			sum += row.getAsDouble(colName);
		}
		return sum;
	}
	
	protected double sumColumn(Connector conn, String colName, int splitsCount) {
		double sum = 0.0;
		for (int splitNr = 0; splitNr < splitsCount; splitNr++) {			
			for (Row row : conn.iterateRows(null, splitNr, splitsCount)) {
				sum += row.getAsDouble(colName);
			}
		}
		return sum;
	}
	
	protected void compareColumn(Connector conn, String colName, List expectedValues) {
		int i = 0;
		for (Row row : conn.iterateRows(null)) {
			assertTrue(row.get(colName).equals(expectedValues.get(i)), 
					"Expected " + expectedValues.get(i) + " but it is " + row.get(colName));
			i++;
		}
	}
	
	protected void testSimple(Connector conn) throws IOException {
		testSimple(conn, 10);
	}
	
	protected void testSimple(Connector conn, final int rowCount) throws IOException {
		int c1Sum = 0;
		
		for (int i = 0; i < rowCount; i++) {
			Row r = new Row(UUID.random());
			
			int randomInt = RandomUtils.nextInt(100);
			c1Sum += randomInt;
			
			r.addColumn("c1", randomInt);
			
			conn.write(r);
		}
		conn.close();
		
		int sumCheck = (int) sumColumn(conn, "c1");
		assertTrue(sumCheck == c1Sum, "Expected " + c1Sum + " but it " + sumCheck);
	}
}
