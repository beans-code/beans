package net.beanscode.model.unittests.connectors;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import net.beanscode.model.connectors.H5Connector;
import net.beanscode.model.plugins.ColumnDef;
import net.beanscode.model.plugins.ColumnDefList;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.schema.ColumnType;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.date.Watch;
import net.hypki.libs5.utils.file.FileExt;
import net.hypki.libs5.utils.string.RandomUtils;

import org.junit.Test;

public class H5ConnectorTestCase extends ConnectorTestCase {

	public H5ConnectorTestCase() {
		
	}

	@Test
	public void testConnectorWithSplits() throws IOException {
		int nrRows = 12000;
		int nrSplits = 100;
		
		runOneTestConnector(nrRows, 50, 1, nrSplits);
		runOneTestConnector(nrRows, 50, 50, nrSplits);
		runOneTestConnector(nrRows, 50, 100, nrSplits);
		runOneTestConnector(nrRows, 50, 123, nrSplits);
		
		runOneTestConnector(nrRows, 1500, 123, nrSplits);
		
		runOneTestConnector(nrRows, 200, 5000, nrSplits);
	}
	
	@Test
	public void testConnectorNoSplits() throws IOException {
		int nrRows = 12000;
		
		runOneTestConnector(nrRows, 50, 1);
		runOneTestConnector(nrRows, 50, 50);
		runOneTestConnector(nrRows, 50, 100);
		runOneTestConnector(nrRows, 50, 123);
		
		runOneTestConnector(nrRows, 1500, 123);
		
		runOneTestConnector(nrRows, 200, 5000);
		
//		testConnector(120000, 200, 500);
	}
	
	private void runOneTestConnector(int rowsCount, int closeForFunEveryXLines, int cacheSize) throws IOException {
		runOneTestConnector(rowsCount, closeForFunEveryXLines, cacheSize, 0);
	}
	
	private void runOneTestConnector(int rowsCount, int closeForFunEveryXLines, int cacheSize, int nrSplits) throws IOException {
		LibsLogger.info(H5ConnectorTestCase.class, "Testing rowsCount ", rowsCount, 
				" closeForFunEveryXLines ", closeForFunEveryXLines, " cacheSize ", cacheSize);
		
		FileExt h5File = new FileExt("test.h5");
		h5File.deleteIfExists();
		
		H5Connector conn = new H5Connector(UUID.random(), h5File)
			.setCacheSize(cacheSize);
		
		ColumnDefList defs = new ColumnDefList();
		defs.add(new ColumnDef("c1", "c1 desc", ColumnType.INTEGER));
		defs.add(new ColumnDef("c2", "c2 desc", ColumnType.STRING));
		defs.add(new ColumnDef("c3", "c3 desc", ColumnType.STRING));
		conn.setColumnDefs(defs);
//		testSimple(conn, rowCount);
		int c1Sum = 0;
		boolean checkStrings = true;
		List<String> c2List = new ArrayList<String>();
				
		Watch w = new Watch();
		int percent = 0;
		for (int i = 0; i < rowsCount; i++) {
			Row r = new Row(UUID.random());
			
			int randomInt = RandomUtils.nextInt(100);
			c1Sum += randomInt;
			
			r.addColumn("c1", randomInt);
			
			String c2 = UUID.random().getId() + UUID.random().getId();
			r.addColumn("c2", c2);
			
			r.addColumn("c3", RandomUtils.nextPronounceableWord(5 + RandomUtils.nextInt(90)));
			
			if (checkStrings)
				c2List.add(c2);
			
			conn.write(r);
			
			int newPercent = (int)(i / (rowsCount / 100));
			if (newPercent % 10 == 0 && newPercent > percent) {
				percent = newPercent;
				LibsLogger.info(H5ConnectorTestCase.class, "Written ", i, " rows, ", newPercent, "%");
			}
			
			if (i > 0 && i % closeForFunEveryXLines == 0) {
				LibsLogger.debug(H5ConnectorTestCase.class, "Closing for fun");
				conn.close();
			}
		}
		conn.close();
		LibsLogger.info(H5ConnectorTestCase.class, "All ", rowsCount, " written in ", w);
		
		// checking sum
		int sumCheck = (int) sumColumn(conn, "c1");
		assertTrue(sumCheck == c1Sum, "Expected " + c1Sum + " but it " + sumCheck);
		
		sumCheck = (int) sumColumn(conn, 512, "c1");
		assertTrue(sumCheck == c1Sum, "Expected " + c1Sum + " but it " + sumCheck);
		
		if (checkStrings)
			compareColumn(conn, "c2", c2List);
		
//		conn.clear();
		conn.close();
		
		LibsLogger.info(H5ConnectorTestCase.class, "Sum is ", c1Sum, " == ", sumCheck, " [", w.toString(), "]");
	}
}
