package net.beanscode.model.unittests.table;

import java.io.IOException;

import net.beanscode.model.dataset.Dataset;
import net.beanscode.model.dataset.Table;
import net.beanscode.model.dataset.TableFactory;
import net.beanscode.model.tests.BeansTestCase;
import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.weblibs.WeblibsConst;
import net.hypki.libs5.weblibs.user.User;

import org.junit.Test;

public class TableUnitTests extends BeansTestCase {
	
	@Test
	public void testTableList() throws ValidationException, IOException {
		final User user = testUser();
		final Dataset ds1 = createDataset(user, "ds1");
		final Dataset ds2 = createDataset(user, "ds2");
		
		createTable(ds1, "ds1-tb1");
		createTable(ds1, "ds1-tb2");
		createTable(ds1, "ds1-tb3");
		
		createTable(ds2, "ds2-tb1");
		createTable(ds2, "ds2-tb2");
		
		runAllJobs(1000);
		
		int counter = 0;
		for (Table table : Table.iterateTables(ds1.getId())) {
			assertTrue(table.getName().startsWith("ds1-"), "Table.getTables(..) does not work");
			counter++;
		}
		assertTrue(counter == 3, "Expected 3 tables");
		
		counter = 0;
		for (Table table : Table.iterateTables(ds2.getId())) {
			assertTrue(table.getName().startsWith("ds2-"), "Table.getTables(..) does not work");
			counter++;
		}
		assertTrue(counter == 2, "Expected  tables");
	}
	
	@Test
	public void testMove() throws ValidationException, IOException {
		final User user = testUser();
		final Dataset ds1 = createDataset(user, "ds1-move");
		final Table t1 = createTable(ds1, "tb1-move");
		final Dataset ds2 = createDataset(user, "ds2-move");
		
		t1.move(ds2);
		
		runAllJobs(1000);
		
		Table fromDb = TableFactory.getTable(t1.getId());
		assertTrue(fromDb != null, "Table should be found");
		assertTrue(fromDb.getDatasetId().equals(ds2.getId()), "Dataset id is wrong");
		assertTrue(fromDb.getName().equals("tb1-move"), "Table name is wrong, expected: tb1-move");
	}

	@Test
	public void testTableNames() {
		assertTrue(Table.isTableNameValid("tab1"));
		assertTrue(Table.isTableNameValid("TAB1"));
		assertTrue(Table.isTableNameValid("tab-1"));
		assertTrue(Table.isTableNameValid("tab_1"));
		assertTrue(Table.isTableNameValid("1"));
		
		assertTrue(Table.isTableNameValid("tab 1"));
		assertTrue(Table.isTableNameValid("tab1 "));
		assertTrue(Table.isTableNameValid(" tab1"));
		assertTrue(Table.isTableNameValid("tab$1"));
		
		assertTrue(Table.isTableNameValid("") == false);
		assertTrue(Table.isTableNameValid("ab\rcd") == false);
		assertTrue(Table.isTableNameValid("ab\ncd") == false);
	}
	
	@Test
	public void testTableDelete() throws ValidationException, IOException {		
		final UUID userId = UUID.random();
		final UUID datasetId = UUID.random();
		
		Table tb = new Table(userId, datasetId, "test2");
		tb.save();
		addToRemove(tb);
		LibsLogger.debug(TableUnitTests.class, "Table created");
		
		runAllJobs();
		
		final UUID tableId = tb.getId();
		
		Table fromDb = TableFactory.getTable(tableId);
		assertTrue(tb.getName().equals(fromDb.getName()));
		assertTrue(DbObject.getDatabaseProvider().isTableEmpty(WeblibsConst.KEYSPACE, Table.COLUMN_FAMILY) == false);
		
		fromDb.remove();
		runAllJobs(1000);
		fromDb = TableFactory.getTable(tableId);
		assertTrue(fromDb == null, "Table stil exists, but it should be removed");
	}
	
	@Test
	public void testTableCreate() throws ValidationException, IOException {		
		final UUID userId = UUID.random();
		final UUID datasetId = UUID.random();
		
		Table tb = new Table(userId, datasetId, "test1");
		tb.save();
		addToRemove(tb);
		LibsLogger.debug(TableUnitTests.class, "Table created");
		
		runAllJobs();
		
		final UUID tableId = tb.getId();
		
		Table fromDb = TableFactory.getTable(tableId);
		assertTrue(tb.getName().equals(fromDb.getName()));
		assertTrue(DbObject.getDatabaseProvider().isTableEmpty(WeblibsConst.KEYSPACE, Table.COLUMN_FAMILY) == false);
		
	}
}
