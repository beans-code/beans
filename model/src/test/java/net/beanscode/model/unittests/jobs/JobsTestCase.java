package net.beanscode.model.unittests.jobs;

import java.io.IOException;

import org.junit.Test;

import net.beanscode.model.tests.BeansTestCase;
import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.date.DateUtils;
import net.hypki.libs5.utils.date.SimpleDate;
import net.hypki.libs5.utils.string.RandomUtils;
import net.hypki.libs5.weblibs.WeblibsConst;
import net.hypki.libs5.weblibs.jobs.Job;

public class JobsTestCase extends BeansTestCase {

	
	
	@Test
	public void testJobsOrder() throws ValidationException, IOException {
		DbObject.getDatabaseProvider().clearKeyspace(WeblibsConst.KEYSPACE);
//		assertTrue(DbObject.getDatabaseProvider().isisKeyspaceEmpty(db), "Keyspace should be empty");
		
		final int all = 400;
		JobTest oneJob = null;
		
		for (int i = 0; i < all; i++) {
			JobTest j = new JobTest().setTest(i + 1);
			
			if (oneJob == null)
				oneJob = j;
			
			j.setCreationMs(SimpleDate.now().getTimeInMillis() + RandomUtils.nextInt(1, 10000));
			j.save();
			LibsLogger.debug(JobsTestCase.class, "Created ", i + 1, " job");
		}
		LibsLogger.debug(JobsTestCase.class, "Created ", all, " jobs");
		
		JobTest fromDb = (JobTest) net.hypki.libs5.weblibs.jobs.JobFactory.getJobData(oneJob.getChannel(), oneJob.getCreationMs(), oneJob.getId());
		assertTrue(fromDb != null);
		assertTrue(fromDb.getId().equals(oneJob.getId()));
		
		int allCheck = 0;
		SimpleDate last = null;
		for (Job job : net.hypki.libs5.weblibs.jobs.JobFactory.getOldestJobs(WeblibsConst.CHANNEL_NORMAL, all, DateUtils.YEAR_2008)) {
			LibsLogger.debug(JobsTestCase.class, "Job ", job.getCreationMs(), ((JobTest)job).getTest());
			if (last != null)
				assertTrue(job.getCreationDate().isGE(last));
			
			if (last != null && job.getCreationMs() < last.getTimeInMillis()) {
				assertTrue(false);
			}
			
			allCheck++;
			last = job.getCreationDate();
		}
		
		assertTrue(all == allCheck, "Expected " + all + " but there is " + allCheck);
		
		// remove test
		fromDb.remove();
		fromDb = (JobTest) net.hypki.libs5.weblibs.jobs.JobFactory.getJobData(oneJob.getChannel(), oneJob.getCreationMs(), oneJob.getId());
		assertTrue(fromDb == null);
	}
}
