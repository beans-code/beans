package net.beanscode.model.unittests.connectors;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import net.beanscode.model.connectors.H5Connector;
import net.beanscode.model.plugins.ColumnDef;
import net.beanscode.model.plugins.ColumnDefList;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.schema.ColumnType;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.date.Watch;
import net.hypki.libs5.utils.file.FileExt;
import net.hypki.libs5.utils.string.RandomUtils;

import org.junit.Test;

public class H5ConnectorSmallFileTestCase extends ConnectorTestCase {

	public H5ConnectorSmallFileTestCase() {
		
	}
	
	@Test
	public void testConnector() throws IOException {
		FileExt h5File = new FileExt("test.h5");
		h5File.deleteIfExists();
		
		H5Connector conn = new H5Connector(UUID.random(), h5File)
			.setUseCache(true);
//		conn
//			.getInitParams()
//			.setTableId(UUID.random());
		
		ColumnDefList defs = new ColumnDefList();
		defs.add(new ColumnDef("c1", "c1 desc", ColumnType.INTEGER));
		defs.add(new ColumnDef("c2", "c2 desc", ColumnType.STRING));
		defs.add(new ColumnDef("c3", "c3 desc", ColumnType.STRING));
		conn.setColumnDefs(defs);
		
		int rowCount = 123;
//		testSimple(conn, rowCount);
		int c1Sum = 0;
		boolean checkStrings = false;
		List<String> c2List = new ArrayList<String>();
		
		int percent = 0;
		Watch w = new Watch();
		for (int i = 0; i < rowCount; i++) {
			Row r = new Row(UUID.random());
			
			int randomInt = RandomUtils.nextInt(100);
			c1Sum += randomInt;
			
			r.addColumn("c1", randomInt);
			
			String c2 = UUID.random().getId() + UUID.random().getId();
			r.addColumn("c2", c2);
			
			r.addColumn("c3", RandomUtils.nextPronounceableWord(5 + RandomUtils.nextInt(90)));
			
			if (checkStrings)
				c2List.add(c2);
			
			conn.write(r);
			
			if ((int) ((i / (float) rowCount) * 100) > percent)
				LibsLogger.info(H5ConnectorSmallFileTestCase.class, "Written ", i, " rows, ", ++percent, "%");
			
			if (i % 22343 == 0) {
				LibsLogger.info(H5ConnectorSmallFileTestCase.class, "Closing for fun");
				conn.close();
			}
		}
		conn.close();
		LibsLogger.info(H5ConnectorSmallFileTestCase.class, "All ", rowCount, " written in ", w);
		
		// checking sum
		int sumCheck = (int) sumColumn(conn, "c1");
		assertTrue(sumCheck == c1Sum, "Expected " + c1Sum + " but it " + sumCheck);
		
		sumCheck = (int) sumColumn(conn, 512, "c1");
		assertTrue(sumCheck == c1Sum, "Expected " + c1Sum + " but it " + sumCheck);
		
		if (checkStrings)
			compareColumn(conn, "c2", c2List);
		
//		conn.clear();
		conn.close();
		
		for (Row row : conn.iterateRows(null, 0, 10)) {
			LibsLogger.info(H5ConnectorSmallFileTestCase.class, "row ", row);
		}
		
		LibsLogger.info(H5ConnectorSmallFileTestCase.class, "Sum is ", c1Sum, " == ", sumCheck);
	}
}
